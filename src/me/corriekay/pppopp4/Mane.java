package me.corriekay.pppopp4;

import java.util.HashSet;

import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.command.PonyCommandMap;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.SpamHandler;
import me.corriekay.pppopp4.modules.administrativetools.AdminTools;
import me.corriekay.pppopp4.modules.administrativetools.DoctorWhooves;
import me.corriekay.pppopp4.modules.afk.AfkHandler;
import me.corriekay.pppopp4.modules.banhammer.BanHammer;
import me.corriekay.pppopp4.modules.celestialmanagement.PrincessCelestia;
import me.corriekay.pppopp4.modules.chaos.ElementsOfHarmony;
import me.corriekay.pppopp4.modules.chat.ChannelHandler;
import me.corriekay.pppopp4.modules.chat.PonySpy;
import me.corriekay.pppopp4.modules.classroom.MissCheerilee;
import me.corriekay.pppopp4.modules.entitylogging.DeathLogger;
import me.corriekay.pppopp4.modules.entitylogging.EntityLogger;
import me.corriekay.pppopp4.modules.equestria.Equestria;
import me.corriekay.pppopp4.modules.filly.FillyHandler;
import me.corriekay.pppopp4.modules.herd.HerdManager;
import me.corriekay.pppopp4.modules.horses.MyLittleHorse;
import me.corriekay.pppopp4.modules.invisibilityhandler.InvisibilityHandler;
import me.corriekay.pppopp4.modules.invsee.InvSee;
import me.corriekay.pppopp4.modules.lockdown.Lockdown;
import me.corriekay.pppopp4.modules.minelittleemote.EmoteManager;
import me.corriekay.pppopp4.modules.miscellaneous.MiscCommands;
import me.corriekay.pppopp4.modules.ponymanager.PonyManager;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.modules.postoffice.PostOffice;
import me.corriekay.pppopp4.modules.pvp.PvpBitchPrevention;
import me.corriekay.pppopp4.modules.pvp.PvpRules;
import me.corriekay.pppopp4.modules.rainbowdash.RainbowDash;
import me.corriekay.pppopp4.modules.remotechest.RemoteChestManager;
import me.corriekay.pppopp4.modules.remoteponyadmin.RemotePonyAdmin;
import me.corriekay.pppopp4.modules.unicornhorn.UnicornHorn;
import me.corriekay.pppopp4.modules.warp.WarpHandler;
import me.corriekay.pppopp4.modules.worldedit.WorldEditHookModule;
import me.corriekay.pppopp4.modules.xraylogging.XrayLogger;
import me.corriekay.pppopp4.utilities.PonyLogger;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class Mane extends JavaPlugin {

    private static Mane instance;
    private static PonyCommandMap cmdMap;
    private final HashSet<PSCmdExe> modules = new HashSet<PSCmdExe>();

    public Mane() {
	instance = this;
    }

    public void onEnable() {
	try {
	    if (PonyLogger.setPathToAdmin()) {
		getLogger().severe("Admin command logger not set to custom location!");
	    }
	    try {
		cmdMap = new PonyCommandMap();
	    } catch (Exception e) {
		Bukkit.getLogger().severe("Error: Unable to create PCommandMap!");
		Bukkit.getPluginManager().disablePlugin(this);
		return;
	    }
	    CommandMetadata.setupCommandValidationAndTabbing();
	    modules.add(new Equestria());
	    modules.add(new Ponyville());
	    modules.add(new InvisibilityHandler());
	    modules.add(new InvSee());
	    modules.add(new PonyManager());
	    modules.add(new ChannelHandler());
	    modules.add(new PonySpy());
	    modules.add(new HerdManager());
	    modules.add(new PostOffice());
	    modules.add(new EmoteManager());
	    modules.add(new AfkHandler());
	    modules.add(new WarpHandler());
	    modules.add(new RemoteChestManager());
	    modules.add(new RemotePonyAdmin());
	    modules.add(new XrayLogger());
	    modules.add(new UnicornHorn());
	    modules.add(new SpamHandler());
	    modules.add(new Lockdown());
	    modules.add(new MiscCommands());
	    modules.add(new RainbowDash());
	    modules.add(new ElementsOfHarmony());
	    modules.add(new PrincessCelestia());
	    modules.add(new AdminTools());
	    modules.add(new DoctorWhooves());
	    modules.add(new BanHammer());
	    modules.add(new DeathLogger());
	    modules.add(new EntityLogger());
	    modules.add(new PvpBitchPrevention());
	    modules.add(new PvpRules());
	    modules.add(new FillyHandler());
	    modules.add(new WorldEditHookModule());
	    modules.add(new MissCheerilee());
	    modules.add(new MyLittleHorse());
	} catch (Exception e) {
	    getLogger().severe("UNHANDLED EXCEPTION THROWN ON STARTUP:\n");
	    e.printStackTrace();
	    Bukkit.getPluginManager().disablePlugin(this);
	    return;
	}
    }

    public void onDisable() {
	for (PSCmdExe exe : modules) {
	    exe.deactivate();
	}
    }

    public static Mane getInstance() {
	return instance;
    }

    public static PonyCommandMap getCmdMap() {
	return cmdMap;
    }
}
