package me.corriekay.pppopp4;

public enum Ponies {

    PinkiePie("�dPinkie Pie�f:�d ", "�d"),
    TwilightSparkle("�1Twilight �5Spa�drk�1le�f:�5 ", "�5"),
    Applejack("�6Applejack�f:�6 ", "�6"),
    Fluttershy("�eFluttershy�f:�e ", "�e"),
    RainbowDash("�cR�6a�ei�an�9b�5o�cw �6D�ea�as�9h�f:�b ", "�b"),
    Rarity("�fRarit�5y�f: ", "�f"),
    DerpyHooves("�7Derpy Hooves�8:�e ", "�e"),
    DoctorWhooves("�9Doctor �8Whooves�f:�9 ", "�9"),
    Cheerilee("�5C�dh�5e�de�5r�di�5l�de�5e�f:�e ", "�e"),
    Scootaloo("�6Scootaloo�f:�5 ", "�5"),
    Applebloom("�eApplebloom�f:�c ", "�c"),
    SweetieBelle("�fSweeti�5e �fBell�5e�f:�f ", "�f");

    final String name;
    final String stringColor;

    private Ponies(String name, String color) {
	this.name = name;
	stringColor = color;
    }

    public String says() {
	return name;
    }

    public String c() {
	return stringColor;
    }
}
