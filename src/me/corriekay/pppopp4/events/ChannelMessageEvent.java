package me.corriekay.pppopp4.events;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ChannelMessageEvent extends Event implements Cancellable {
    private String message, channame;
    private final Player player;
    private boolean cancelled = false;
    private static final HandlerList handlers = new HandlerList();

    public ChannelMessageEvent(String message, String chanName, Player player) {
	this.message = message;
	setChanname(chanName);
	this.player = player;
    }

    public String getMessage() {
	return message;
    }

    public void setMessage(String msg) {
	message = msg;
    }

    public String getChanname() {
	return channame;
    }

    private void setChanname(String channame) {
	this.channame = channame;
    }

    public Player getPlayer() {
	return player;
    }

    public boolean isCancelled() {
	return cancelled;
    }

    public void setCancelled(boolean arg0) {
	cancelled = arg0;

    }

    public HandlerList getHandlers() {
	return handlers;
    }

    public static HandlerList getHandlerList() {
	return handlers;
    }
}
