package me.corriekay.pppopp4.events;

import me.corriekay.pppopp4.modules.chat.Channel;

import org.bukkit.ChatColor;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class ChannelBroadcastEvent extends Event {

    private final Channel c;
    private String message;
    public static final HandlerList handlers = new HandlerList();

    public ChannelBroadcastEvent(Channel c, String msg) {
	this.c = c;
	message = msg;
    }

    public Channel getChannel() {
	return c;
    }

    public void setMessage(String msg) {
	message = msg;
    }

    public String getMessage() {
	return message;
    }

    public String getColorlessMessage() {
	return ChatColor.stripColor(message);
    }

    public HandlerList getHandlers() {
	return handlers;
    }

    public static HandlerList getHandlerList() {
	return handlers;
    }

}
