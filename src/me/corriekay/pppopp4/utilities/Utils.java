package me.corriekay.pppopp4.utilities;

/**
 * @Class: Utils
 * @Author: CorrieKay
 * @Purpose: Utils class. Non-instantiated, abstract. Contains utility methods, (functions hurr hurr)
 */
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;

import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldedit.regions.Region;

public abstract class Utils {

    public static String getDate(long time) {
	Date date = new Date(time);
	return new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(date);
    }

    public static String getDatedTimestamp() {
	return getDate(System.currentTimeMillis());
    }

    public static String getSystemTime() {
	return getSystemTime(System.currentTimeMillis());
    }

    public static String getSystemTime(long time) {
	DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	Date cal = Calendar.getInstance().getTime();
	cal.setTime(time);
	return dateFormat.format(cal.getTime());
    }

    public static String getFileDate() {
	return getFileDate(System.currentTimeMillis());
    }

    public static String getFileDate(long time) {
	Date date = new Date(time);
	return new SimpleDateFormat("yyyy-MM-dd").format(date);
    }

    public static String getTimeStamp() {
	return getTimeStamp(System.currentTimeMillis());
    }

    public static String getTimeStamp(long time) {
	DateFormat dateFormat = new SimpleDateFormat("HH:mm:ss");
	Date cal = Calendar.getInstance().getTime();
	cal.setTime(time);
	return dateFormat.format(cal.getTime());
    }

    public static HashSet<Block> getBlocks(int length, int height, int width, Location loc) {
	HashSet<Block> blocks = new HashSet<Block>();
	double startX = loc.getX() - length / 2;
	double startY = loc.getY() - height / 2;
	double startZ = loc.getZ() - width / 2;
	double y = startY;
	double z = startZ;
	double x = startX;
	double maxX = loc.getX() + length / 2;
	double maxY = loc.getY() + height / 2;
	double maxZ = loc.getZ() + width / 2;
	boolean Continue = true;
	while (Continue) {
	    if (x < maxX) {
		x++;
	    } else {
		x = startX;
		if (y < maxY) {
		    y++;
		} else {
		    y = startY;
		    if (z < maxZ) {
			z++;
		    } else {
			y = startY;
			Continue = false;
		    }
		}
	    }
	    Location loc2 = new Location(loc.getWorld(), x, y, z);
	    blocks.add(loc2.getBlock());
	}
	return blocks;
    }

    public static ArrayList<String> getArrayLoc(Location l) {
	ArrayList<String> array = new ArrayList<String>();
	if (l != null) {
	    array.add(l.getWorld().getName());
	    array.add(l.getX() + "");
	    array.add(l.getY() + "");
	    array.add(l.getZ() + "");
	    array.add(l.getPitch() + "");
	    array.add(l.getYaw() + "");
	}
	return array;
    }

    public static Location getLoc(ArrayList<String> array) {
	if (array == null) {
	    return null;
	}
	try {
	    World w;
	    double x, y, z;
	    float pitch, yaw;
	    w = Bukkit.getWorld(array.get(0));
	    x = Double.parseDouble(array.get(1));
	    y = Double.parseDouble(array.get(2));
	    z = Double.parseDouble(array.get(3));
	    pitch = Float.parseFloat(array.get(4));
	    yaw = Float.parseFloat(array.get(5));
	    Location l = new Location(w, x, y, z);
	    l.setPitch(pitch);
	    l.setYaw(yaw);
	    return l;
	} catch (Exception e) {
	    return null;
	}
    }

    private static final HashSet<Character> acceptablechars = new HashSet<Character>(Arrays.asList(new Character[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' }));

    public static String alphanumbericalize(String oldString) {
	return alphanumericalize(oldString, new Character[] {});
    }

    public static String alphanumericalize(String oldString, Character[] otherChars) {
	StringBuilder sb = new StringBuilder();
	char[] charArray = oldString.toCharArray();
	ArrayList<Character> acceptablechars = new ArrayList<Character>(Utils.acceptablechars);
	for (Character character : otherChars) {
	    acceptablechars.add(character);
	}
	for (char c : charArray) {
	    if (acceptablechars.contains(c)) {
		sb.append(c);
	    }
	}
	return sb.toString();
    }

    public static boolean intersects(Selection s1, Selection s2) {
	Location s1Min, s1Max, s2Min, s2Max;
	s1Min = s1.getMinimumPoint();
	s1Max = s1.getMaximumPoint();
	s2Min = s2.getMinimumPoint();
	s2Max = s2.getMaximumPoint();

	if ((s1Min.getBlockX() <= s2Max.getBlockX()) && (s2Min.getBlockX() <= s1Max.getBlockX())) {
	    if ((s1Min.getBlockZ() <= s2Max.getBlockZ()) && (s2Min.getBlockZ() <= s1Max.getBlockZ())) {
		return true;
	    }
	}
	return false;
    }

    public static boolean intersects(Region s1, Region s2) {
	com.sk89q.worldedit.Vector s1Min, s1Max, s2Min, s2Max;
	s1Min = s1.getMinimumPoint();
	s1Max = s1.getMaximumPoint();
	s2Min = s2.getMinimumPoint();
	s2Max = s2.getMaximumPoint();

	if ((s1Min.getBlockX() <= s2Max.getBlockX()) && (s2Min.getBlockX() <= s1Max.getBlockX())) {
	    if ((s1Min.getBlockZ() <= s2Max.getBlockZ()) && (s2Min.getBlockZ() <= s1Max.getBlockZ())) {
		return true;
	    }
	}
	return false;
    }

    public static String joinStringArray(String[] args) {
	return joinStringArray(args, "");
    }

    public static String joinStringArray(String[] args, String delimiter) {
	return joinStringArray(args, delimiter, 0);
    }

    public static String joinStringArray(String[] args, String delimiter, int startingIndex) {
	String s = "";
	for (int i = startingIndex; i < args.length; i++) {
	    s += args[i];
	    if (!(i + 1 >= args.length)) {
		s += delimiter;
	    }
	}
	return s;
    }

    public static String getStringFromIndex(int index, String[] args) {
	String s = "";
	for (int i = index; i < args.length; i++) {
	    s += args[i] + " ";
	}
	return s.trim();
    }
}
