package me.corriekay.pppopp4.utilities;

public class PonyString {
    private final String string;

    public PonyString(String string) {
	this.string = string;
    }

    @Override
    public String toString() {
	return string;
    }
}
