package me.corriekay.pppopp4.utilities;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import me.corriekay.pppopp4.Mane;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

public abstract class PonyLogger {

    private static String pathToAdminLogger = Mane.getInstance().getDataFolder().getAbsolutePath();

    public static boolean setPathToAdmin() throws Exception {
	File file = new File(Mane.getInstance().getDataFolder() + File.separator + "AdminLogger.yml");
	if (!file.exists()) {
	    file.createNewFile();
	}
	FileConfiguration config = YamlConfiguration.loadConfiguration(file);
	String logger = config.getString("adminLogger");
	if (logger != null) {
	    pathToAdminLogger = logger;
	    return true;
	}
	return false;
    }

    public static void logCmdException(Exception e, String errorMessage, String handlerName) {
	Mane plugin = Mane.getInstance();
	File directory = new File(plugin.getDataFolder() + File.separator + "Error Logging" + File.separator + handlerName);
	if (!directory.exists()) {
	    directory.mkdirs();
	}
	File file = new File(plugin.getDataFolder() + File.separator + "Error Logging" + File.separator + handlerName + File.separator + e.getClass().getName().substring(e.getClass().getName().lastIndexOf(".") + 1) + ".txt");
	if (!file.exists()) {
	    try {
		file.createNewFile();
	    } catch (IOException e1) {
		e1.printStackTrace();
	    }
	}
	try {
	    BufferedWriter out = new BufferedWriter(new FileWriter(file, true));
	    out.write(errorMessage);
	    out.newLine();
	    out.write("StackTrace:");
	    out.newLine();
	    for (StackTraceElement ste : e.getStackTrace()) {
		out.write(ste.toString());
		out.newLine();
	    }
	    out.newLine();
	    out.close();
	} catch (IOException e1) {
	    e1.printStackTrace();
	}
    }

    public static void logListenerException(Exception e, String errorMessage, String handlerName, String eventName) {
	Mane plugin = Mane.getInstance();
	File directory = new File(plugin.getDataFolder() + File.separator + "Error Logging" + File.separator + handlerName + File.separator + eventName);
	if (!directory.isDirectory()) {
	    directory.mkdirs();
	}
	File file = new File(directory, e.getClass().getName().substring(e.getClass().getName().lastIndexOf(".") + 1) + ".txt");
	if (!file.exists()) {
	    try {
		file.createNewFile();
	    } catch (IOException e1) {
		e1.printStackTrace();
		return;
	    }
	}
	try {
	    BufferedWriter out = new BufferedWriter(new FileWriter(file, true));
	    out.write("Exception thrown on " + errorMessage + "\r\n");
	    out.write("StackTrace: \r\n");
	    for (StackTraceElement ste : e.getStackTrace()) {
		out.write(ste.toString() + "\r");
	    }
	    out.newLine();
	    out.close();
	} catch (IOException e1) {
	    e1.printStackTrace();
	    return;
	}
    }

    public static void logMessage(String path, String fileName, String messageToLog) {
	Mane plugin = Mane.getInstance();
	File directory = new File(plugin.getDataFolder() + File.separator + path);
	if (!directory.exists()) {
	    directory.mkdirs();
	}
	File file = new File(plugin.getDataFolder() + File.separator + path + File.separator + fileName + ".txt");
	if (!file.exists()) {
	    try {
		file.createNewFile();
	    } catch (IOException e) {
		e.printStackTrace();
		return;
	    }
	}
	try {
	    BufferedWriter out = new BufferedWriter(new FileWriter(file, true));
	    out.write(messageToLog + "\n");
	    out.close();
	    return;
	} catch (IOException e) {
	    e.printStackTrace();
	    return;
	}
    }

    public static void logCommand(String path, String fileName, String cmdToLog) {
	File directory = new File(path);
	if (!directory.exists()) {
	    directory.mkdirs();
	}
	File file = new File(directory + File.separator + fileName + ".txt");
	if (!file.exists()) {
	    try {
		file.createNewFile();
	    } catch (IOException e) {
		e.printStackTrace();
		return;
	    }
	}
	try {
	    BufferedWriter out = new BufferedWriter(new FileWriter(file, true));
	    out.write(cmdToLog);
	    out.newLine();
	    out.close();
	    return;
	} catch (IOException e) {
	    e.printStackTrace();
	    return;
	}
    }

    public static void logAdmin(String msg2Log) {
	try {
	    File file = new File(pathToAdminLogger + File.separator + "AdminLogs.txt");
	    if (!file.exists()) {
		file.createNewFile();
	    }
	    BufferedWriter out = new BufferedWriter(new FileWriter(file, true));
	    out.write(msg2Log);
	    out.close();
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }
}
