package me.corriekay.pppopp4.modules.warp;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.command.validation.command.BasicArgumentRule;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

public class PwplayerArgValidator extends BasicArgumentRule {

    public PwplayerArgValidator(String failure, boolean showFailure,
	    boolean returnOnFail) {
	super(failure, showFailure, returnOnFail);
    }

    @Override
    public boolean checkRule(String[] args) {
	if (args.length < 1) {
	    return true;
	}
	if (args.length == 1) {
	    return false;
	}
	String find = PSCmdExe.getPlayerSilent(args[0]);
	if (find == null || find.equals("TOO_MANY_MATCHES")) {
	    return false;
	}
	Pony pony = Ponyville.getPony(find);
	List<String> validator = new ArrayList<String>();
	validator.add("list");
	validator.add("offline");
	validator.add("home");
	validator.add("back");
	for (String s : pony.getAllNamedWarps().keySet()) {
	    validator.add(s);
	}
	return validator.contains(args[1]);
    }

}
