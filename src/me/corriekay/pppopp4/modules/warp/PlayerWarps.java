package me.corriekay.pppopp4.modules.warp;

import me.corriekay.pppopp4.modules.ponyville.Pony;

import org.bukkit.ChatColor;
import org.bukkit.Location;

public class PlayerWarps extends WarpList {

    private Warp home, back, offline;
    private final Pony pony;

    public PlayerWarps(Pony pony) {
	super(pony.getAllNamedWarpsAsWarps());
	this.home = new Warp("Home", pony.getHomeWarp());
	this.back = new Warp("Back", pony.getBackWarp());
	this.offline = new Warp("Offline", pony.getOfflineWarp());
	this.pony = pony;
	super.name = ChatColor.RED + pony.getName() + ChatColor.RED + "'s Private Warps";
	renderList();
    }

    public boolean hasWarp(String name) {
	return !(getWarp(name) == null);
    }

    public Warp getHome() {
	return home.loc() == null ? null : home;
    }

    public Warp getBack() {
	return back.loc() == null ? null : back;
    }

    public Warp getOffline() {
	return offline.loc() == null ? null : offline;
    }

    public void setHome(Location home) {
	this.home = new Warp("Home", home);
    }

    public void setBack(Location home) {
	this.back = new Warp("Back", home);
    }

    public void setOffline(Location home) {
	this.offline = new Warp("Offline", home);
    }

    public Pony getOwner() {
	return pony;
    }

    public void save() {
	for (String w : pony.getAllNamedWarps().keySet()) {
	    pony.removeNamedWarp(w);
	}
	for (String s : warps()) {
	    Location w = getWarp(s);
	    pony.setNamedWarp(s, w);
	}
	pony.setBackWarp(back.loc());
	pony.setHomeWarp(home.loc());
	pony.setOfflineWarp(offline.loc());
	pony.save();
    }

    protected void renderList() {
	super.renderList();
	if (size() < 1) {
	    renderedList = ChatColor.RED + "No Private Warps!";
	}
    }
}
