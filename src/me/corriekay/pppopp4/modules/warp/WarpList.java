package me.corriekay.pppopp4.modules.warp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

import me.corriekay.pppopp4.modules.equestria.PonyWorld;

import org.bukkit.ChatColor;
import org.bukkit.Location;

public class WarpList {

    private HashMap<String, Warp> warps = new HashMap<String, Warp>();
    protected String name = ChatColor.RED + "Global Warps";
    protected String renderedList;

    public WarpList(HashMap<String, Warp> warps) {
	this.warps = warps;
	renderList();
    }

    protected ArrayList<String> warps() {
	ArrayList<String> warpNames = new ArrayList<String>();
	warpNames.addAll(warps.keySet());
	Collections.sort(warpNames);
	return warpNames;
    }

    protected int size() {
	return warps.size();
    }

    protected void addWarp(String name, Location loc) {
	warps.put(name, new Warp(name, loc));
	renderList();
    }

    protected boolean removeWarp(String name) {
	boolean removed = warps.containsKey(name);
	if (removed) {
	    warps.remove(name);
	    renderList();
	}
	return removed;
    }

    protected Location getWarp(String warpName) {
	Location loc;
	try {
	    loc = warps.get(warpName).loc();
	} catch (NullPointerException e) {
	    return null;
	}
	return loc;
    }

    protected void renderList() {
	StringBuilder builder = new StringBuilder();
	builder.append(name + "\n---------------\n");
	HashMap<PonyWorld, HashSet<Warp>> warpMap = new HashMap<PonyWorld, HashSet<Warp>>();
	for (Warp w : warps.values()) {
	    PonyWorld world = PonyWorld.getPonyWorld(w.loc());
	    HashSet<Warp> warpList = warpMap.get(world);
	    if (warpList == null) {
		warpList = new HashSet<Warp>();
	    }
	    warpList.add(w);
	    warpMap.put(world, warpList);
	}
	if (size() < 1) {
	    renderedList = ChatColor.RED + "No Global Warps!";
	    return;
	}
	for (PonyWorld world : warpMap.keySet()) {
	    HashSet<Warp> warpz = warpMap.get(world);
	    String s = ChatColor.RED + world.getName() + ChatColor.GRAY + ": ";
	    for (Warp w : warpz) {
		s += ChatColor.RED + w.name() + ChatColor.GRAY + ", ";
	    }
	    builder.append(s.substring(0, s.length() - 4) + "\n");
	}
	renderedList = builder.toString();
    }

    public String getRenderedList() {
	return renderedList;
    }
}
