package me.corriekay.pppopp4.modules.warp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.JoinEvent;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.equestria.Equestria;
import me.corriekay.pppopp4.modules.equestria.GameType;
import me.corriekay.pppopp4.modules.equestria.PonyUniverse;
import me.corriekay.pppopp4.modules.equestria.PonyWorld;
import me.corriekay.pppopp4.modules.ponymanager.PonyPermission;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.utilities.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;

public class WarpHandler extends PSCmdExe {

    public static WarpHandler handler;

    private HashMap<String, WarpList> lists = new HashMap<String, WarpList>();

    private HashMap<String, QueuedWarp> queuedWarps = new HashMap<String, QueuedWarp>();

    private HashMap<String, String> pendingTps = new HashMap<String, String>();

    private final int worldWarps, totalWarps;

    public Location spawn;

    public WarpHandler() throws Exception {
	super("WarpHandler", Ponies.TwilightSparkle, "gw", "pw", "pwlist", "pwdel", "pwset", "gwset", "gwdel", "pwplayer", "tp", "tpa", "tpd", "tphere", "home", "sethome", "back", "spawn", "setspawn", "top", "j", "tploc");
	handler = this;
	if (loadConfig("warps.yml")) {
	    getConfig().set("worldWarps", 8);
	    getConfig().set("totalWarps", 15);
	    getConfig().createSection("warps");
	    saveConfig();
	}
	spawn = Utils.getLoc((ArrayList<String>) getConfig().getStringList("serverSpawn"));
	worldWarps = getConfig().getInt("warps.yml", 8);
	totalWarps = getConfig().getInt("totalWarps", 15);
	Bukkit.getScheduler().runTaskTimer(Mane.getInstance(), new Runnable() {
	    public void run() {
		HashSet<String> removeMe = new HashSet<String>();
		for (String name : queuedWarps.keySet()) {
		    QueuedWarp qw = queuedWarps.get(name);
		    if (qw.countdown()) {
			removeMe.add(name);
		    }
		}
		for (String name : removeMe) {
		    queuedWarps.remove(name);
		}
	    }
	}, 0, 20);
	reloadWarps();
    }

    private void reloadWarps() {
	HashMap<String, Warp> gws = new HashMap<String, Warp>();
	for (String s : getConfig().getConfigurationSection("warps").getKeys(false)) {
	    gws.put(s, new Warp(s, Utils.getLoc((ArrayList<String>) getConfig().getStringList("warps." + s))));
	}
	WarpList gwl = new WarpList(gws);
	lists.put("global", gwl);
	for (Player player : Bukkit.getOnlinePlayers()) {
	    loadPlayersWarpList(player);
	}
    }

    private void loadPlayersWarpList(Player player) {
	PlayerWarps pws = new PlayerWarps(Ponyville.getPony(player));
	lists.put(player.getName(), pws);
    }

    private void unloadPlayersWarpList(String playername) {
	lists.remove(playername);
    }

    /*
     * Bookmark Command
     */
    @SuppressWarnings("deprecation")
    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case gw: {
	    Location l = getGlobalWarps().getWarp(args[0]);
	    if (l == null) {
		sendMessage(player, "I couldn't find that warp!");
		return true;
	    }
	    if (!queueWarp(l, player)) {
		sendMessage(player, "...Uh oh, something went wrong!");
	    }
	    return true;
	}
	case pw: {
	    Location l = getPlayerWarpList(player.getName()).getWarp(args[0]);
	    if (l == null) {
		sendMessage(player, "I couldn't find that warp!");
		return true;
	    }
	    if (!queueWarp(l, player)) {
		sendMessage(player, "...Uh oh, something went wrong!");
	    }
	    return true;
	}
	case pwlist: {
	    displayWarps(player);
	    return true;
	}
	case pwdel: {
	    PlayerWarps warps = getPlayerWarpList(player.getName());
	    if (warps.removeWarp(args[0])) {
		sendMessage(player, "Warp deleted!");
		warps.save();
	    } else {
		sendMessage(player, "Uh oh, I couldnt find that warp!");
	    }
	    return true;
	}
	case pwset: {
	    Location l = player.getLocation();
	    PlayerWarps warps = getPlayerWarpList(player.getName());
	    String warpname = args[0];
	    if (warpname.equals("back") || warpname.equals("home") || warpname.equals("offline") || warpname.equals("list")) {
		sendMessage(player, "Sorry! That warp name is reserved! Choose another?");
		return true;
	    }
	    if (canCreateNewPrivateWarp(warps, l, warpname)) {
		warps.addWarp(warpname, l);
		warps.save();
		sendMessage(player, "Wormhole opened! Warp " + args[0] + " set!");
	    } else {
		sendMessage(player, "You have too many warps! You need to delete one, or overwrite an existing one to set another warp!");
	    }
	    return true;
	}
	case gwset: {
	    Location l = player.getLocation();
	    String n = args[0].toLowerCase();
	    setGlobalWarp(l, n);
	    return true;
	}
	case gwdel: {
	    String n = args[0].toLowerCase();
	    if (!removeGlobalWarp(n)) {
		sendMessage(player, "Hey, that global warp doesnt exist!");
	    }
	    return true;
	}
	case pwplayer: {
	    if (args.length < 2) {
		sendMessage(player, "Heres a list of pwplayer commands!\n/pwplayer <player> list - This lists their warps!\n/pwplayer <player> <warpname> -  This teleports to one of their warps!\n/pwplayer <player> offline - This teleports to the location they last logged off at!\n/pwplayer <player> home -  This teleports to their home location!\n/pwplayer <player> back -  This teleports to their back location!");
		return true;
	    }
	    OfflinePlayer target = getOnlineOfflinePlayer(args[0], player);
	    if (target == null) {
		return true;
	    }
	    PlayerWarps warps;
	    if (target.isOnline()) {
		warps = this.getOnlinePlayerWarpList(target.getPlayer());
	    } else {
		warps = getPlayerWarpList(target.getName());
	    }
	    if (args[1].equals("list")) {
		displayTargetWarps(player, warps);
	    } else if (args[1].equals("offline")) {
		Warp w = warps.getOffline();
		if (w == null) {
		    sendMessage(player, "Huh, cant find an offline warp!");
		} else {
		    sendMessage(player, "Warping to " + warps.getOwner().getNickname() + ChatColor.LIGHT_PURPLE + "'s offline location!");
		    player.teleport(w.loc());
		}
	    } else if (args[1].equals("back")) {
		Warp w = warps.getBack();
		if (w == null) {
		    sendMessage(player, "Huh, cant find a back warp!");
		} else {
		    sendMessage(player, "Warping to " + warps.getOwner().getNickname() + ChatColor.LIGHT_PURPLE + "'s back location!");
		    player.teleport(w.loc());
		}
	    } else if (args[1].equals("home")) {
		Warp w = warps.getHome();
		if (w == null) {
		    sendMessage(player, "Huh, cant find a home warp!");
		} else {
		    sendMessage(player, "Warping to " + warps.getOwner().getNickname() + ChatColor.LIGHT_PURPLE + "'s home location!");
		    player.teleport(w.loc());
		}
	    } else {
		Location w = warps.getWarp(args[1]);
		if (w == null) {
		    sendMessage(player, "Huh, cant find that warp!");
		} else {
		    sendMessage(player, "Warping to " + warps.getOwner().getNickname() + ChatColor.LIGHT_PURPLE + "'s " + args[1] + " warp!");
		    player.teleport(w);
		}
	    }
	    return true;
	}
	case tp: {
	    Player target = getOnlinePlayer(args[0], player);
	    if (target != null) {
		if (player.hasPermission(PonyPermission.TP_BYPASS.getPerm())) {
		    player.teleport(target);
		} else {
		    requestTeleport(player, target);
		}
	    }
	    return true;
	}
	case tpa: {
	    String targetPlayer = pendingTps.get(player.getName());
	    if (targetPlayer != null) {
		Player target = Bukkit.getPlayerExact(targetPlayer);
		if (target != null) {
		    sendMessage(player, "Teleport request accepted!");
		    sendMessage(target, "Teleport request accepted!");
		    queueWarp(player.getLocation(), target);
		    pendingTps.remove(player.getName());
		    return true;
		}
	    }
	    sendMessage(player, "You dont have any pending teleport requests, silly!");
	    return true;
	}
	case tpd: {
	    String targetPlayer = pendingTps.get(player.getName());
	    if (targetPlayer != null) {
		Player target = Bukkit.getPlayerExact(targetPlayer);
		if (target != null) {
		    sendMessage(player, "Teleport request denied!");
		    sendMessage(target, "Teleport request denied!");
		    pendingTps.remove(player.getName());
		    return true;
		}
	    }
	    sendMessage(player, "You dont have any pending teleport requests, silly!");
	    return true;
	}
	case tphere: {
	    Player target = getOnlinePlayer(args[0], player);
	    if (target != null) {
		target.teleport(player);
	    }
	    return true;
	}
	case home: {
	    Warp w = getPlayerWarpList(player.getName()).getHome();
	    if (w == null) {
		sendMessage(player, "Uh oh, you have no home set! Try setting a home with /sethome!");
		return true;
	    }
	    queueWarp(w.loc(), player);
	    return true;
	}
	case sethome: {
	    getPlayerWarpList(player.getName()).setHome(player.getLocation());
	    sendMessage(player, "Home set!");
	    return true;
	}
	case back: {
	    this.queueWarp(getPlayerWarpList(player.getName()).getBack().loc(), player);
	    return true;
	}
	case spawn: {
	    Location l;
	    if (args.length > 0) {
		PonyWorld w = PonyWorld.getPonyWorld(args[0]);
		if (w == null) {
		    sendMessage(player, "Spawn not found!");
		    return true;
		}
		l = w.getUniverse().getSpawn();
	    } else {
		l = (spawn == null ? PonyWorld.getPonyWorld(player.getLocation()).getUniverse().getSpawn() : spawn);
	    }
	    queueWarp(l, player);
	    return true;
	}
	case setspawn: {
	    if (args.length > 0) {
		PonyUniverse universe = PonyWorld.getPonyWorld(player.getLocation()).getUniverse();
		Equestria.getHandler().setUniverseSpawnLocation(player.getLocation(), universe);
		sendMessage(player, "Set the spawn for universe " + universe.getName() + "!");
	    } else {
		spawn = player.getLocation();
		setServerSpawn(spawn);
		sendMessage(player, "Spawn set!");
		player.getWorld().setSpawnLocation(spawn.getBlockX(), spawn.getBlockY(), spawn.getBlockZ());
	    }
	    return true;
	}
	case top: {
	    Location l = player.getWorld().getHighestBlockAt(player.getLocation()).getLocation();
	    l.setY(l.getY() + 1);
	    player.teleport(l);
	    return true;
	}
	case j: {
	    Location loc = player.getLocation();
	    List<Block> los = player.getLineOfSight(null, 100);
	    Location nextLoc = los.get(los.size() - 1).getLocation();
	    nextLoc.setPitch(loc.getPitch());
	    nextLoc.setYaw(loc.getYaw());
	    nextLoc.setY(nextLoc.getY() + 1);
	    player.teleport(nextLoc);
	    return true;
	}
	case tploc: {
	    try {
		PonyWorld world = PonyWorld.getPonyWorld(player.getLocation());
		double x, y, z;
		Location l;
		x = Double.parseDouble(args[0]);
		if (args.length < 3) {
		    z = Double.parseDouble(args[1]);
		    l = new Location(world.getWorld(), x, 1, z);
		    l.setY(world.getWorld().getHighestBlockYAt(l));
		} else {
		    y = Double.parseDouble(args[1]);
		    z = Double.parseDouble(args[2]);
		    l = new Location(world.getWorld(), x, y, z);
		}
		player.teleport(l);
		return true;
	    } catch (Exception e) {
		sendMessage(player, "Silly, those aren't coordinates!");
		return false;
	    }
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    /*
     * Bookmark Commands
     */
    public void displayWarps(CommandSender sender) {
	sendMessage(sender, "Here is a list of warps you can use!");
	String gwarps = getGlobalWarps().getRenderedList();
	sender.sendMessage(gwarps);
	if (sender instanceof Player) {
	    Player player = (Player) sender;
	    PlayerWarps pwarps = getOnlinePlayerWarpList(player);
	    String render = pwarps.getRenderedList();
	    if (player.hasPermission(CommandMetadata.pwset.getPermission())) {
		player.sendMessage("\n" + render);
	    }
	}
    }

    public void displayTargetWarps(CommandSender sender, PlayerWarps list) {
	sendMessage(sender, "Here is a list of " + list.getOwner().getNickname() + ChatColor.LIGHT_PURPLE + "'s warps: ");
	String render = list.getRenderedList();
	sender.sendMessage(render);
    }

    public boolean setGlobalWarp(Location l, String name) {
	name = name.toLowerCase();
	WarpList gwarps = getGlobalWarps();
	gwarps.addWarp(name, l);
	saveGlobalWarpList();
	broadcastMessage(pony.says() + "Wormhole opened! New global warp set: " + name + "!");
	return true;
    }

    public boolean removeGlobalWarp(String name) {
	name = name.toLowerCase();
	WarpList gwarps = getGlobalWarps();
	if (gwarps.getWarp(name) == null) {
	    return false;
	}
	gwarps.removeWarp(name);
	saveGlobalWarpList();
	broadcastMessage(pony.says() + "Wormhole closed! Global warp " + name + " deleted!");
	return true;
    }

    public boolean setPrivateWarp(Pony pony, Location warp, String name) {
	PlayerWarps warpList;
	if (pony.getPlayer().isOnline()) {
	    Player player = pony.getPlayer().getPlayer();
	    warpList = getOnlinePlayerWarpList(player);
	} else {
	    warpList = getPlayerWarpList(pony.getName());
	}
	if (!canCreateNewPrivateWarp(warpList, warp, name)) {
	    return false;
	}
	warpList.addWarp(name, warp);
	if (pony.getPlayer().isOnline()) {
	    Player player = pony.getPlayer().getPlayer();
	    sendMessage(player, "");
	}
	warpList.save();
	return true;
    }

    public void setServerSpawn(Location l) {
	spawn = l;
	getConfig().set("serverSpawn", Utils.getArrayLoc(l));
	saveConfig();
    }

    public void requestTeleport(final Player player, final Player target) {
	String currentTarget = pendingTps.get(target.getName());
	if (currentTarget != null && currentTarget.equals(player.getName())) {
	    sendMessage(player, "You've already got a pending teleport request to that person!");
	    return;
	}
	final PonyUniverse currentUniverse = PonyWorld.getPonyWorld(player.getLocation()).getUniverse();
	final PonyUniverse targetUniverse = PonyWorld.getPonyWorld(target.getLocation()).getUniverse();
	if (!currentUniverse.equals(targetUniverse)) {
	    Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), new Runnable() {
		public void run() {
		    String answer = questioner.ask(player, pony.says() + "This player is in a different map: " + targetUniverse.getOverworld().getName() + "! Are you sure you want to teleport to them?" + (targetUniverse.getOverworld().getGameType() == GameType.PVP ? ChatColor.DARK_RED + " THIS WORLD IS PVP." : ""), "yes", "no");
		    if (answer.equals("yes")) {
			Bukkit.getScheduler().runTask(Mane.getInstance(), new Runnable() {
			    public void run() {
				sendMessage(target, player.getDisplayName() + ChatColor.LIGHT_PURPLE + " has sent you a teleport request! type /tpa to accept, or type /tpd to deny it!");
				sendMessage(player, "Yay! you've sent a teleport request to " + target.getDisplayName());
				pendingTps.put(target.getName(), player.getName());
			    }
			});
		    } else {
			sendSyncMessage(player, "Cancelling warp!");
		    }
		}
	    });
	    return;
	}
	sendMessage(target, player.getDisplayName() + pony.c() + " has sent you a teleport request! type /tpa to accept, or type /tpd to deny it!");
	sendMessage(player, "Yay! you've sent a teleport request to " + target.getDisplayName());
	pendingTps.put(target.getName(), player.getName());
    }

    /*
     * Bookmark Listners
     */

    @PonyEvent
    public void onQuit(QuitEvent event) {
	String name = event.getPlayer().getName();
	if (event.isQuitting()) {
	    PlayerWarps warps = getPlayerWarpList(name);
	    warps.setOffline(event.getPlayer().getLocation());
	    warps.save();
	    unloadPlayersWarpList(name);
	}
	removePendingWarps(name);
    }

    @PonyEvent
    public void onJoin(JoinEvent event) {
	if (event.isJoining()) {
	    loadPlayersWarpList(event.getPlayer());
	}
    }

    @PonyEvent(priority = EventPriority.MONITOR, eventTypes = { EntityDamageByEntityEvent.class })
    public void onDamage(EntityDamageEvent event) {
	if (event.getEntity() instanceof Player) {
	    if (event.getDamage() <= 0 || event.isCancelled()) {
		return;
	    }
	    Player player = (Player) event.getEntity();
	    if (pendingTps.containsValue(player.getName())) {
		removePendingWarps(player.getName());
		sendMessage(player, "Oh no! Catastrophic failure! Player teleport aborted to avoid black holes!");
		return;
	    }
	    if (queuedWarps.containsKey(player.getName())) {
		removePendingWarps(player.getName());
		sendMessage(player, "Oh no! Catastrophic failure! Warp aborted to avoid black holes!");
		return;
	    }
	}
    }

    @PonyEvent
    public void teleport(PlayerTeleportEvent event) {
	int count = 0;
	while (!event.getTo().getChunk().load()) {
	    count++;
	    if (count > 5) {
		break;
	    }
	}
	if (event.getCause() == TeleportCause.PLUGIN) {
	    Player player = event.getPlayer();
	    PlayerWarps warps = getPlayerWarpList(player.getName());
	    warps.setBack(event.getFrom());
	}
    }

    @PonyEvent
    public void respawn(PlayerRespawnEvent event) {
	Player player = event.getPlayer();
	PlayerWarps wl = getPlayerWarpList(player.getName());
	Location respawnLoc;
	try {
	    respawnLoc = wl.getHome().loc();
	} catch (NullPointerException e) {
	    respawnLoc = null;
	}
	PonyWorld world = PonyWorld.getPonyWorld(player.getLocation()).getOverworld();
	if (respawnLoc == null || respawnLoc.getWorld() != player.getWorld()) {
	    respawnLoc = world.getSpawn();
	}
	event.setRespawnLocation(respawnLoc);
    }

    /*
     * Bookmark Utilities
     */

    public void removePendingWarps(String name) {
	pendingTps.remove(name);
	HashSet<String> removeMe = new HashSet<String>();
	for (String other : pendingTps.keySet()) {
	    if (pendingTps.get(other).equals(name)) {
		removeMe.add(other);
	    }
	}
	for (String s : removeMe) {
	    pendingTps.remove(s);
	}
	queuedWarps.remove(name);
    }

    public boolean queueWarp(Location l, Player p) {
	try {
	    if (p.hasPermission(PonyPermission.TP_BYPASS.getPerm())) {
		sendMessage(p, "Using the express admin wormhole!");
		p.teleport(l);
		return true;
	    }
	    PonyWorld world = PonyWorld.getPonyWorld(p.getLocation());
	    if (world.getGameType() == GameType.CREATIVE) {
		sendMessage(p, "The peaceful nature of this world allows me to send you instantaneously!");
		p.teleport(l);
		return true;
	    }
	    sendMessage(p, "Okay! setting up the wormhole... This process is delicate...");
	    QueuedWarp w = new QueuedWarp(p.getName(), l, 7);
	    queuedWarps.put(p.getName(), w);
	    return true;
	} catch (Exception e) {
	    return false;
	}
    }

    public void saveGlobalWarpList() {
	WarpList gwl = getGlobalWarps();
	FileConfiguration config = getConfig();
	config.createSection("warps");
	System.out.println("Saving global warp list");
	for (String s : gwl.warps()) {
	    System.out.println(s);
	    Location l = gwl.getWarp(s);
	    config.set("warps." + s, Utils.getArrayLoc(l));
	}
	saveConfig();
    }

    public WarpList getGlobalWarps() {
	return lists.get("global");
    }

    public PlayerWarps getPlayerWarpList(String playername) {
	PlayerWarps warps = (PlayerWarps) lists.get(playername);
	if (warps == null) {
	    warps = new PlayerWarps(Ponyville.getPony(playername));
	}
	return warps;
    }

    public PlayerWarps getOnlinePlayerWarpList(Player player) {
	return (PlayerWarps) lists.get(player.getName());
    }

    public boolean canCreateNewPrivateWarp(PlayerWarps warpList, Location l, String name) {
	if (warpList.size() >= totalWarps) {
	    if (warpList.getWarp(name) != null && !(warpList.size() > totalWarps)) {
		return true;
	    }
	    return false;
	}
	int worldCount = 0;
	String warpWorldName = PonyWorld.getPonyWorld(l).getUniverse().getName();
	for (String s : warpList.warps()) {
	    Location loc = warpList.getWarp(s);
	    if (loc == null) {
		System.out.println("warpList.getWarp(s) returning null warp. Attempting to grab warpname == " + s); // DEBUG
														    // REMOVEME
	    }
	    PonyWorld world = PonyWorld.getPonyWorld(warpList.getWarp(s));
	    if (world == null) {
		System.out.println("canCreateNewPrivateWarp returning null world! warp name == " + s + ". World name == " + warpList.getWarp(s).getWorld().getName() + "."); // DEBUG
																					     // REMOVEME
	    }
	    String secondWarpWorldName = world.getUniverse().getName();
	    if (secondWarpWorldName.equals(warpWorldName)) {
		worldCount++;
	    }
	}
	if (worldCount >= worldWarps) {
	    if (warpList.getWarp(name) != null && !(worldCount > worldWarps)) {
		return true;
	    }
	    return false;
	}
	return true;
    }
}
