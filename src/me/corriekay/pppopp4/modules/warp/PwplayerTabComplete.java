package me.corriekay.pppopp4.modules.warp;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.command.validation.tabbing.BasicTabRule;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.command.CommandSender;

public class PwplayerTabComplete implements BasicTabRule {

    @Override
    public List<String> tab(CommandSender sender, CommandMetadata cmd, String argBit, String[] args) {
	String find = PSCmdExe.getPlayerSilent(args[0]);
	if (find == null) {
	    find = "PLAYER_NOT_FOUND";
	}
	List<String> validator = new ArrayList<String>();
	if (find.equals("PLAYER_NOT_FOUND") || find.equals("TOO_MANY_MATCHES")) {
	    validator.add(find);
	} else {
	    Pony pony = Ponyville.getPony(find);
	    validator.add("list");
	    validator.add("offline");
	    validator.add("home");
	    validator.add("back");
	    for (String s : pony.getAllNamedWarps().keySet()) {
		validator.add(s);
	    }
	}
	return validator;
    }
}
