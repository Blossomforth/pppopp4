package me.corriekay.pppopp4.modules.warp;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.command.validation.tabbing.BasicTabRule;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PWTab implements BasicTabRule {

    @SuppressWarnings("unchecked")
    @Override
    public List<String> tab(CommandSender sender, CommandMetadata cmd, String argBit, String[] args) {
	List<String> returnMe = new ArrayList<String>();
	if (!(sender instanceof Player)) {
	    returnMe.add(argBit);
	} else {
	    PlayerWarps warps = WarpHandler.handler.getOnlinePlayerWarpList((Player) sender);
	    returnMe = (List<String>) warps.warps().clone(); // TODO
	}
	return returnMe;
    }
}
