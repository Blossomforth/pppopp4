package me.corriekay.pppopp4.modules.herd;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.modules.postoffice.PostOffice;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.regions.CuboidRegion;

public class Herd {

    protected Map<String, Rank> members = new HashMap<String, Rank>();
    protected String herdLeader;
    protected CuboidRegion protection;
    protected String worldname;
    protected String name, displayName, motd;
    protected String enterMessage, leaveMessage;
    protected boolean protectionEnabled = false;

    protected Herd(String name, String leader) {
	this.name = name.toLowerCase();
	enterMessage = "Now entering " + name;
	leaveMessage = "Now leaving " + name;
	herdLeader = leader;
	protection = null;
	worldname = null;
	displayName = name;
	motd = "Welcome to " + name + "!";

    }

    protected Herd(ConfigurationSection section) {
	name = section.getName();
	displayName = section.getString("displayName");
	motd = section.getString("motd");
	enterMessage = section.getString("enter");
	leaveMessage = section.getString("leave");
	if (section.contains("protection")) {
	    Vector min, max;
	    double minx, minz, maxx, maxz;
	    World world;
	    world = Bukkit.getWorld(section.getString("protection.world"));
	    minx = section.getDouble("protection.min.x");
	    minz = section.getDouble("protection.min.z");
	    maxx = section.getDouble("protection.max.x");
	    maxz = section.getDouble("protection.max.z");
	    min = new Vector(minx, 0, minz);
	    max = new Vector(maxx, world.getMaxHeight(), maxz);
	    worldname = world.getName();
	    protection = new CuboidRegion(min, max);
	    protectionEnabled = section.getBoolean("enabled");
	}
	herdLeader = section.getString("herdLeader");
	members.put(herdLeader, Rank.LEADER);
	for (String mod : section.getStringList("moderators")) {
	    members.put(mod, Rank.MODERATOR);
	}
	for (String member : section.getStringList("members")) {
	    members.put(member, Rank.MEMBER);
	}
	for (String guest : section.getStringList("guests")) {
	    members.put(guest, Rank.GUEST);
	}
	for (String recruit : section.getStringList("recruits")) {
	    members.put(recruit, Rank.RECRUIT);
	}
    }

    protected void save(FileConfiguration config) {
	config.set(name + ".displayName", displayName);
	config.set(name + ".herdLeader", herdLeader);
	List<String> mods, members, guests, recruits;
	mods = new ArrayList<String>();
	members = new ArrayList<String>();
	guests = new ArrayList<String>();
	recruits = new ArrayList<String>();
	for (String member : this.members.keySet()) {
	    Rank r = this.members.get(member);
	    if (r == null || r == Rank.LEADER) {
		continue;
	    }
	    switch (r) {
	    case MODERATOR: {
		mods.add(member);
		continue;
	    }
	    case MEMBER: {
		members.add(member);
		continue;
	    }
	    case GUEST: {
		guests.add(member);
		continue;
	    }
	    case RECRUIT: {
		recruits.add(member);
		continue;
	    }
	    default: {}
	    }
	}
	config.set(name + ".moderators", mods);
	config.set(name + ".members", members);
	config.set(name + ".guests", guests);
	config.set(name + ".recruits", recruits);
	config.set(name + ".motd", motd);
	config.set(name + ".enter", enterMessage);
	config.set(name + ".leave", leaveMessage);
	if (protection != null) {
	    config.set(name + ".protection.world", worldname);
	    config.set(name + ".protection.min.x", protection.getMinimumPoint().getX());
	    config.set(name + ".protection.min.z", protection.getMinimumPoint().getZ());
	    config.set(name + ".protection.max.x", protection.getMaximumPoint().getX());
	    config.set(name + ".protection.max.z", protection.getMaximumPoint().getZ());
	    config.set(name + ".protection.enabled", protectionEnabled);
	}
    }

    /*
     * Bookmark Checkers
     */

    public boolean isRecruit(String name) {
	return members.get(name) == Rank.RECRUIT;
    }

    public boolean isGuest(String name) {
	return members.get(name) == Rank.GUEST;
    }

    public boolean isMember(String name) {
	return members.get(name) == Rank.MEMBER;
    }

    public boolean isMod(String name) {
	return members.get(name) == Rank.MODERATOR;
    }

    public boolean isLeader(String name) {
	return herdLeader.equals(name);
    }

    public boolean hasModPrivs(String name) {
	return isLeader(name) || isMod(name);
    }

    public boolean hasModPrivs(Player player, boolean notify) {
	boolean is = hasModPrivs(player.getName());
	if (!is && notify) {
	    player.sendMessage(ChatColor.LIGHT_PURPLE + "Pinkie Pie: Oh no! You can't do this.. :c");
	}
	return is;
    }

    public boolean isGeneralMember(String name) {
	return members.containsKey(name);
    }

    public boolean canPromote(String promoter, String promoted, boolean raw) {
	Rank mod, member;
	mod = getRank(promoter);
	member = getRank(promoted).getPromotion();
	if (!hasModPrivs(promoter)) {
	    return false;
	}
	if (raw) {
	    return !mod.isEqualOrGreater(member);
	} else {
	    if (member == Rank.CANT_PROMOTE || member == Rank.NOT_IN_HERD) {
		return false;
	    } else {
		return !mod.isEqualOrGreater(member);
	    }
	}
    }

    public boolean canPromote(Player promoter, String promoted, boolean raw, boolean notify) {
	boolean can = canPromote(promoter.getName(), promoted, raw);
	if (!can && notify) {
	    promoter.sendMessage(ChatColor.LIGHT_PURPLE + "Pinkie Pie: Oh no! You can't do this.. :c");
	}
	return can;
    }

    public boolean canDemote(String demoter, String demoted, boolean raw) {
	Rank mod, member;
	mod = getRank(demoter);
	member = getRank(demoted);
	if (member == Rank.LEADER || member == Rank.GUEST) {
	    return false;
	}
	if (!hasModPrivs(demoter)) {
	    return false;
	}
	if (raw) {
	    return !mod.isLessThan(member);
	} else {
	    if (member == Rank.CANT_DEMOTE || member == Rank.NOT_IN_HERD) {
		return false;
	    } else {
		return !mod.isLessThan(member);
	    }
	}
    }

    public boolean canDemote(Player demoter, String demoted, boolean raw, boolean notify) {
	boolean can = canDemote(demoter.getName(), demoted, raw);
	if (!can && notify) {
	    demoter.sendMessage(ChatColor.LIGHT_PURPLE + "Pinkie Pie: Oh no! You can't do this.. :c");
	}
	return can;
    }

    public boolean isInProtection(Location l) {
	if (!l.getWorld().getName().equals(worldname)) {
	    return false;
	}
	Vector v = new Vector(l.getBlockX(), l.getBlockY(), l.getBlockZ());
	return protection.contains(v);
    }

    public boolean protectionEnabled() {
	return protectionEnabled;
    }

    /*
     * Bookmark Getters
     */
    public Rank getRank(String player) {
	Rank r = members.get(player);
	return r != null ? r : Rank.NOT_IN_HERD;
    }

    /*
     * Bookmark Setters
     */
    public boolean promote(String player, Player promoter) {
	Rank r = getRank(player);
	if (r == null || r == Rank.NOT_IN_HERD) {
	    return false;
	}
	r = r.getPromotion();
	if (r == Rank.CANT_PROMOTE) {
	    return false;
	}
	if (r == Rank.LEADER) {
	    swapLeader(player);
	    return true;
	}
	setRank(player, r);
	return true;
    }

    public boolean demote(String player) {
	Rank r = getRank(player);
	if (r == null || r == Rank.NOT_IN_HERD || r == Rank.LEADER) {
	    return false;
	}
	r = r.getDemotion();
	if (r == Rank.CANT_DEMOTE) {
	    return false;
	}
	setRank(player, r);
	return true;
    }

    private void swapLeader(String newleader) {
	broadcast(ChatColor.LIGHT_PURPLE + "All hail " + displayName + ChatColor.LIGHT_PURPLE + "'s glorious new leader: " + newleader + "!", true, false, true);
	members.put(herdLeader, Rank.MODERATOR);
	members.put(newleader, Rank.LEADER);
	herdLeader = newleader;
    }

    public boolean quit(String name) {
	if (isLeader(name)) {
	    return false;
	} else {
	    members.remove(name);
	    return true;
	}
    }

    public boolean addRecruit(String name) {
	if (isGeneralMember(name) || isRecruit(name)) {
	    return false;
	}
	members.remove(name);
	members.put(name, Rank.RECRUIT);
	return true;
    }

    public boolean promoteRecruit(String name) {
	if (!isRecruit(name)) {
	    return false;
	}
	members.put(name, Rank.GUEST);
	return true;
    }

    public boolean denyRecruit(String name) {
	if (isRecruit(name)) {
	    members.remove(name);
	    return true;
	}
	return false;
    }

    private void setRank(String player, Rank rank) {
	if (rank == null || rank == Rank.LEADER) {
	    return;
	}
	members.put(player, rank);
    }

    public void setProtection(boolean flag) {
	protectionEnabled = flag;
	broadcast("Herd town protection has been " + (protectionEnabled ? "enabled!" : "disabled!"), true, false, false);
    }

    public void setProtection(CuboidRegion region) {
	Vector derp, herp;
	derp = region.getMaximumPoint();
	herp = region.getMinimumPoint();
	worldname = region.getWorld().getName();
	protection = new CuboidRegion(herp, derp);
    }

    /*
     * Bookmark Utility methods
     */
    public void broadcast(String message, boolean pinkie, boolean moderators, boolean notifyOffline) {
	for (String s : members.keySet()) {
	    Player p = Bukkit.getPlayerExact(s);
	    if (moderators) {
		if (!hasModPrivs(s)) {
		    continue;
		}
	    }
	    if (p != null) {
		p.sendMessage((pinkie ? ChatColor.LIGHT_PURPLE + "Pinkie Pie: " : "") + message);
	    } else if (notifyOffline) {
		Pony pony = Ponyville.getPony(s);
		if (pony != null) {
		    PostOffice.sendSystemMessage(pony, (pinkie ? ChatColor.LIGHT_PURPLE + "Pinkie Pie: " : "") + message, true);
		}
	    }
	}
    }

    /*
     * Bookmark Rank Class
     */
    public enum Rank {
	CANT_PROMOTE(7),
	LEADER(6),
	MODERATOR(5),
	MEMBER(4),
	GUEST(3),
	RECRUIT(2),
	CANT_DEMOTE(1),
	NOT_IN_HERD(0);
	private Rank promotion, demotion;

	private final int rank;

	Rank(Integer i) {
	    rank = i;
	}

	static {
	    LEADER.setRanks(MODERATOR, CANT_PROMOTE);
	    MODERATOR.setRanks(MEMBER, LEADER);
	    MEMBER.setRanks(GUEST, MODERATOR);
	    GUEST.setRanks(CANT_DEMOTE, MEMBER);
	    RECRUIT.setRanks(CANT_DEMOTE, CANT_PROMOTE);
	}

	private void setRanks(Rank demotion, Rank promotion) {
	    this.promotion = promotion;
	    this.demotion = demotion;
	}

	public Rank getPromotion() {
	    return promotion;
	}

	public Rank getDemotion() {
	    return demotion;
	}

	public int getRankNumber() {
	    return rank;
	}

	public boolean isEqualOrGreater(Rank r) {
	    int otherNumber = r.getRankNumber();
	    return otherNumber >= getRankNumber();
	}

	public boolean isLessThan(Rank r) {
	    int otherNumber = r.getRankNumber();
	    return getRankNumber() < otherNumber;
	}
    }
}
