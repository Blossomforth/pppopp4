package me.corriekay.pppopp4.modules.herd;

import java.util.ArrayList;
import java.util.HashMap;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.JoinEvent;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.herd.Herd.Rank;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.modules.postoffice.PostOffice;
import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitScheduler;

import com.sk89q.worldedit.IncompleteRegionException;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.Selection;
import com.sk89q.worldedit.regions.CuboidRegion;
import com.sk89q.worldedit.regions.Region;

public class HerdManager extends PSCmdExe {

    private HashMap<String, Herd> herds = new HashMap<String, Herd>();
    private HashMap<String, Herd> playerLocation = new HashMap<String, Herd>();
    private final WorldEditPlugin wep = ((WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit"));

    public HerdManager() {
	super("HerdManager", Ponies.Applejack, "setherdprotection", "createherd", "deleteherd", "herd");
	init();
    }

    /*
     * Bookmark Initialization methods
     */
    private void init() {
	loadConfig("Herd.yml");
	reloadHerds();
	initLocations();
	Bukkit.getScheduler().runTaskTimer(Mane.getInstance(), new Runnable() {
	    public void run() {
		for (Player player : Bukkit.getOnlinePlayers()) {
		    Herd previous = playerLocation.get(player.getName());
		    Herd current = getLocationHerd(player.getLocation());
		    playerLocation.put(player.getName(), current);
		    if (previous == null) {
			if (current == null) {
			    continue;
			}
			player.sendMessage(current.enterMessage);
			continue;
		    } else {
			if (previous.equals(current)) {
			    continue;
			}
			player.sendMessage(previous.leaveMessage);
			if (current != null) {
			    player.sendMessage(current.enterMessage);
			    continue;
			}
		    }
		}
	    }
	}, 20 * 5, 20 * 5);
    }

    private void reloadHerds() {
	for (String name : getConfig().getKeys(false)) {
	    Herd herd = new Herd(getConfig().getConfigurationSection(name));
	    herds.put(name, herd);
	}
    }

    private void initLocations() {
	for (Player player : Bukkit.getOnlinePlayers()) {
	    initPlayerLocation(player);
	}
    }

    private void initPlayerLocation(Player player) {
	playerLocation.put(player.getName(), null);
	for (Herd herd : herds.values()) {
	    if (herd.isInProtection(player.getLocation())) {
		playerLocation.put(player.getName(), herd);
		return;
	    }
	}
    }

    private Herd getLocationHerd(Location l) {
	for (Herd h : herds.values()) {
	    if (h.isInProtection(l)) {
		return h;
	    }
	}
	return null;
    }

    /*
     * Bookmark Command Method
     */

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case herd: {
	    String arg = args[0];
	    if (arg.equals("join")) {
		if (!checkArgs(player, args, 2)) {
		    return true;
		}
		Herd herd = getHerd(args[1]);
		if (herd != null) {
		    joincmd(herd, player);
		} else {
		    sendMessage(player, "I couldnt find that herd! D:!");
		}
		return true;
	    }
	    if (arg.equals("debug")) {
		Herd herd = getPlayerHerd(player.getName());
		if (herd == null) {
		    sendMessage(player, "No herd to debug");
		    return true;
		}
		if (args.length > 1) {
		    herd = this.getHerd(args[1]);
		    if (herd == null) {
			sendMessage(player, "No herd by that name");
			return true;
		    }
		}
		herdDebugCmd(player, herd);
		return true;
	    }
	    Herd h = getCmdHerd(player);
	    if (h == null) {
		return true;
	    }
	    if (arg.equals("leave")) {
		leavecmd(player);
		return true;
	    }
	    if (arg.equals("accept")) {
		if (!checkArgs(player, args, 2)) {
		    return true;
		}
		OfflinePlayer op = getOnlineOfflinePlayer(args[1], player);
		if (op == null) {
		    return true;
		}
		acceptcmd(op.getName(), player, h);
		return true;
	    }
	    if (arg.equals("deny")) {
		OfflinePlayer op = getOnlineOfflinePlayer(args[1], player);
		if (op != null) {
		    denycmd(op.getName(), player, h);
		}
		return true;
	    }
	    if (arg.equals("setleader")) {
		OfflinePlayer op = getOnlineOfflinePlayer(args[1], player);
		if (op != null) {
		    setleadercmd(op.getName(), player, h);
		}
		return true;
	    }
	    if (arg.equals("promote")) {
		OfflinePlayer op = getOnlineOfflinePlayer(args[1], player);
		if (op == null) {
		    return true;
		}
		promotecmd(op.getName(), player, h);
		System.out.println(player.getName());
		return true;
	    }
	    if (arg.equals("demote")) {
		OfflinePlayer op = getOnlineOfflinePlayer(args[1], player);
		if (op == null) {
		    return false;
		}
		demotecmd(op.getName(), player, h);
		return true;
	    }
	    if (arg.equals("kick")) {
		OfflinePlayer op = getOnlineOfflinePlayer(args[1], player);
		if (op != null) {
		    kickcmd(op.getName(), player, h);
		}
		return true;
	    }
	    if (arg.equals("setdisplayname")) {
		setdisplaynamecmd(Utils.joinStringArray(args, " ", 1), player, h);
		return true;
	    }
	    if (arg.equals("list")) {
		listcmd(player, h);
		return true;
	    }
	    if (arg.equals("setmotd")) {
		setmotdcmd(player, Utils.joinStringArray(args, " ", 1), h);
		return true;
	    }
	    if (arg.equals("setenter")) {
		setentercmd(player, Utils.joinStringArray(args, " ", 1), h);
		return true;
	    }
	    if (arg.equals("setleave")) {
		setleavecmd(player, Utils.joinStringArray(args, " ", 1), h);
		return true;
	    }
	    if (arg.equals("toggleprotection")) {
		toggleprotectioncmd(player, h);
		return true;
	    }
	}
	case setherdprotection: {
	    Herd h = this.getHerd(args[0]);
	    if (h != null) {
		setherdprotectioncmd(player, h);
		return true;
	    }
	    sendMessage(player, "Uh oh, I couldn't find that herd!");
	    return false;
	}
	case createherd: {
	    createherdcmd(player, args[0]);
	    return true;
	}
	case deleteherd: {
	    Herd herd = getCmdHerd(player);
	    if (herd != null) {
		deleteherdcmd(player, herd);
	    }
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    /*
     * Bookmark Command Methods
     */

    public void herdDebugCmd(Player player, Herd herd) {
	sendMessage(player, "DEBUGGING HERD " + herd.displayName);
	sendMessage(player, "Players and Ranks:");
	String msg = "";
	for (String s : herd.members.keySet()) {
	    msg += s + " " + herd.members.get(s).name() + "\n";
	}
	player.sendMessage(msg);
	sendMessage(player, "Players and ranks by \"getRank\" method");
	msg = "";
	for (String s : herd.members.keySet()) {
	    msg += s + " " + herd.getRank(s).name() + "\n";
	}
	player.sendMessage(msg);
    }

    public boolean setherdprotectioncmd(Player player, Herd herd) {
	Selection sel = wep.getSelection(player);
	if (sel == null) {
	    sendMessage(player, "You must have a world edit selection to perform this command!");
	    return false;
	}
	try {
	    if (!(sel.getRegionSelector().getRegion() instanceof CuboidRegion)) {
		sendMessage(player, "You must have a cuboid selection to perform this command!");
		return false;
	    }
	    CuboidRegion region = (CuboidRegion) sel.getRegionSelector().getRegion();
	    if (region != null && herd != null) {
		if (!setherdprotection(region, herd)) {
		    sendMessage(player, "Looks like that region is intersecting another region! Try resizing the area and submitting it again!");
		    return false;
		}
		return true;
	    }
	    sendMessage(player, "Uh oh, looks like your selection had an issue! reselect the area and try again!");
	    return false;
	} catch (IncompleteRegionException e) {
	    return false;
	}
    }

    public boolean deleteherdcmd(final Player player, final Herd herd) {
	if (!herd.hasModPrivs(player, false) && !herd.isLeader(player.getName())) {
	    sendMessage(player, "You cannot delete this herd.");
	    return false;
	}
	final BukkitScheduler scheduler = Bukkit.getScheduler();
	scheduler.runTaskAsynchronously(Mane.getInstance(), new Runnable() {
	    public void run() {
		String answer = questioner.ask(player, Ponies.Applejack.says() + "Are you sure you want to delete this herd?", "yes", "no");
		if (answer.equals("no")) {
		    sendSyncMessage(player, "Aborting herd deletion!");
		    return;
		}
		answer = questioner.ask(player, Ponies.Applejack.says() + "Final confirmation: Are you ABSOLUTELY sure you want to delete this herd?" + ChatColor.DARK_RED + ChatColor.BOLD + "THIS CANNOT BE REVERSED!!", "yes", "no");
		if (answer.equals("no")) {
		    sendSyncMessage(player, "Aborting herd deletion!");
		    return;
		}
		scheduler.runTask(Mane.getInstance(), new Runnable() {
		    public void run() {
			sendMessage(player, "Deleting herd..");
			deleteHerd(herd, true);// TODO //TODO find out why i put
					       // a TODO here.
		    }
		});
	    }
	});
	return true;
    }

    public boolean createherdcmd(Player player, String herdname) {
	if (getPlayerHerd(player) != null) {
	    sendMessage(player, "You're already in a herd! You cannot be in two herds at once!");
	    return false;
	}
	if (!createHerd(herdname, player.getName())) {
	    sendMessage(player, "Looks like a herd with that name already exists! Try another one, or join them!");
	    return false;
	}
	sendMessage(player, "Congratulations! You've created a herd! You're now free to customize it, and recruit members!");
	return true;
    }

    public boolean setherdprotection(CuboidRegion sel, Herd herd) {
	if (sel == null || herd == null) {
	    return false;
	}
	for (Herd herd2 : herds.values()) {
	    if (!herd2.equals(herd)) {
		Region r1, r2;
		r1 = herd2.protection;
		r2 = sel;
		if (r1 == null) {
		    continue;
		}
		if (Utils.intersects(r1, r2)) {
		    return false;
		}
	    }
	}
	herd.setProtection(sel);
	saveHerd(herd);
	return true;
    }

    public boolean joincmd(Herd herd, Player player) {
	if (getPlayerHerd(player) != null) {
	    sendMessage(player, "You're already a member of a herd! Please leave it to join a new one!");
	    return false;
	}
	if (herd.isGeneralMember(player.getName())) {
	    sendMessage(player, "You're already a member of that herd!");
	    return false;
	}
	if (herd.isRecruit(player.getName())) {
	    sendMessage(player, "You've already applied to that herd! Please wait for the leaders to approve your membership!");
	    return false;
	}
	if (!herd.addRecruit(player.getName())) {
	    System.out.println("recruit fail");
	}
	sendMessage(player, "Hooray! You've applied to join " + herd.displayName);
	herd.broadcast("You've got a new applicant to the herd! " + player.getDisplayName() + ChatColor.LIGHT_PURPLE + " wishes to join the herd!", true, true, true);
	saveHerd(herd);
	return true;
    }

    public boolean leavecmd(final Player player) {
	final Herd herd = getPlayerHerd(player);
	if (herd != null) {
	    Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), new Runnable() {
		public void run() {
		    String answer = questioner.ask(player, Ponies.Applejack.says() + "Are you sure you want to leave your herd?", "yes", "no");
		    if (answer.equals("no")) {
			sendSyncMessage(player, "Okay, you wont be leaving your herd!");
			return;
		    }
		    if (answer.equals("yes")) {
			Bukkit.getScheduler().runTask(Mane.getInstance(), new Runnable() {
			    public void run() {
				if (!herd.quit(player.getName())) {
				    sendMessage(player, "Looks like you're the last leader of the herd! To leave the herd, you'll need to either disband the herd, or using /herd setleader <name>!");
				} else {
				    sendMessage(player, "You've left " + herd.displayName + ChatColor.LIGHT_PURPLE + "!");
				    saveHerd(herd);
				}
			    }
			});
		    }
		}
	    });
	    return true;
	}
	sendMessage(player, "You're not in a herd!");
	return false;
    }

    public boolean acceptcmd(String player, Player accepter, Herd herd) {
	if (!herd.hasModPrivs(accepter, true)) {
	    return false;
	}
	if (!herd.isRecruit(player)) {
	    sendMessage(accepter, player + " has not applied for your herd, or is already a part of the herd!");
	    return false;
	}
	herd.promoteRecruit(player);
	Player player1 = Bukkit.getPlayerExact(player);
	if (player1 != null) {
	    sendMessage(player1, "Your request has been accepted, welcome to " + herd.displayName + ChatColor.LIGHT_PURPLE + "!");
	    sendMessage(player1, herd.motd);
	} else {
	    PostOffice.sendSystemMessage(Ponyville.getPony(player), "Your request has been accepted, welcome to " + herd.displayName + ChatColor.WHITE + "!", true);
	}
	sendMessage(accepter, "You've accepted " + player + " into the herd!");
	saveHerd(herd);
	return true;
    }

    public boolean denycmd(String player, Player denier, Herd herd) {
	if (!herd.hasModPrivs(denier, true)) {
	    return false;
	}
	if (!herd.isRecruit(player)) {
	    sendMessage(denier, player + " has not applied for your herd!");
	    return false;
	}
	herd.denyRecruit(player);
	Player player1 = Bukkit.getPlayerExact(player);
	if (player1 != null) {
	    sendMessage(player1, "Your request to join " + herd.displayName + ChatColor.LIGHT_PURPLE + " has been denied :c");
	} else {
	    PostOffice.sendSystemMessage(Ponyville.getPony(player), "Your request to join " + herd.displayName + ChatColor.WHITE + " has been denied :c", false);
	}
	sendMessage(denier, "You've denied " + player + " from joining the herd.");
	saveHerd(herd);
	return true;
    }

    public boolean promotecmd(String player, Player promoter, Herd herd) {
	if (!herd.canPromote(promoter.getName(), player, false)) {
	    if (herd.getRank(player) == Rank.MODERATOR && herd.getRank(promoter.getName()) == Rank.LEADER) {
		sendMessage(promoter, "The next promotion that user would get is to swap leadership with you! If you'd like to promote them to herd leader, please type /herd setleader " + player);
		return false;
	    }
	    sendMessage(promoter, "You cannot promote that user! Their promoted rank would be " + herd.getRank(player).getPromotion().name() + ", and your rank is currently " + herd.getRank(promoter.getName()).name());
	    System.out.println(promoter.getName());
	    return false;
	}
	herd.promote(player, promoter);
	Player p = Bukkit.getPlayerExact(player);
	if (p != null) {
	    sendMessage(p, "Congratulations! You've been promoted to " + herd.getRank(player).name() + " in your herd!");
	} else {
	    PostOffice.sendSystemMessage(Ponyville.getPony(player), "Congratulations! You've been promoted to " + herd.getRank(player).name() + " in your herd!", false);
	}
	sendMessage(promoter, "You've promoted " + (p != null ? p.getDisplayName() + ChatColor.LIGHT_PURPLE : player) + " to " + herd.getRank(player).name() + " in your herd!");
	saveHerd(herd);
	return true;
    }

    public boolean demotecmd(String player, Player demoter, Herd herd) {
	if (!herd.canDemote(demoter.getName(), player, false)) {
	    sendMessage(demoter, "Oh no! You can't do this.. :c");
	    return false;
	}
	herd.demote(player);
	Player p = Bukkit.getPlayerExact(player);
	if (p != null) {
	    sendMessage(p, "Your rank in the herd is now " + herd.getRank(player) + ".");
	} else {
	    PostOffice.sendSystemMessage(Ponyville.getPony(player), "Your rank in the herd is now " + herd.getRank(player) + ".", false);
	}
	sendMessage(demoter, "You've demoted " + (p != null ? p.getDisplayName() + ChatColor.LIGHT_PURPLE : player) + " to " + herd.getRank(player).name() + " in your herd!");
	saveHerd(herd);
	return true;
    }

    public boolean setleadercmd(final String name, final Player currentleader, final Herd herd) {
	if (herd.getRank(currentleader.getName()) != Rank.LEADER) {
	    sendMessage(currentleader, "Oh no! You can't do this.. :c");
	    return false;
	}
	if (herd.getRank(name) != Rank.MODERATOR) {
	    sendMessage(currentleader, "You cannot promote that user, please promote a moderator!");
	    return false;
	}
	final BukkitScheduler s = Bukkit.getScheduler();
	s.runTaskAsynchronously(Mane.getInstance(), new Runnable() {
	    public void run() {
		String answer = questioner.ask(currentleader, ChatColor.LIGHT_PURPLE + "Pinkie Pie: You are about to give up leadership to " + name + ". Are you SURE you want to do this?", "yes", "no");
		if (answer.equals("no")) {
		    sendSyncMessage(currentleader, "Okay, aborting leader swap!");
		    return;
		}
		s.runTask(Mane.getInstance(), new Runnable() {
		    public void run() {
			herd.promote(name, currentleader);
			saveHerd(herd);
			sendMessage(currentleader, "You have given " + name + " Leadership of " + herd.displayName + ChatColor.LIGHT_PURPLE + "!");
			Player newleader = Bukkit.getPlayerExact(name);
			if (newleader != null) {
			    sendMessage(newleader, "CONGRATULATIONS! You've been promoted to leader of the herd!");
			} else {
			    PostOffice.sendSystemMessage(Ponyville.getPony(name), "CONGRATULATIONS! You've been promoted to leader of the herd!", true);
			}
		    }
		});
	    }
	});
	return true;
    }

    public boolean kickcmd(String name, Player kicker, Herd herd) {
	if (!herd.hasModPrivs(kicker, true)) {
	    return false;
	}
	if (!herd.canDemote(kicker.getName(), name, false)) {
	    sendMessage(kicker, "Oh no! You can't do this.. :c");
	    return false;
	}
	if (!herd.isGeneralMember(name)) {
	    sendMessage(kicker, "That player is not a member of your herd!");
	    return false;
	}
	herd.members.remove(name);
	Player player = Bukkit.getPlayerExact(name);
	if (player != null) {
	    sendMessage(player, "You have been kicked from the herd! D:");
	} else {
	    PostOffice.sendSystemMessage(Ponyville.getPony(name), "You have been kicked from the herd! D:", true);
	}
	herd.broadcast(name + " has been kicked from the herd by " + kicker.getDisplayName() + ChatColor.LIGHT_PURPLE + "!", true, true, false);
	saveHerd(herd);
	return true;
    }

    public boolean setdisplaynamecmd(String displayName, Player setter, Herd herd) {
	if (!herd.isLeader(setter.getName())) {
	    sendMessage(setter, "Oh no! You can't do this.. :c");
	    return false;
	}
	{
	    String baseName = ChatColor.stripColor(displayName).toLowerCase();
	    for (Herd h : herds.values()) {
		if (h.equals(herd)) {
		    continue;
		}
		if (h.name.toLowerCase().equals(baseName) || ChatColor.stripColor(h.displayName).toLowerCase().equals(baseName)) {
		    sendMessage(setter, "Ack! Looks like that name is already taken by another herd!");
		    return false;
		}
	    }
	}
	displayName = ChatColor.translateAlternateColorCodes('&', displayName);
	herd.displayName = displayName;
	saveHerd(herd);
	herd.broadcast("The herd is now known as: " + herd.displayName + ChatColor.LIGHT_PURPLE + "!", true, false, true);
	return true;
    }

    public boolean listcmd(Player sender, Herd herd) {
	HashMap<Rank, ArrayList<String>> members = new HashMap<Rank, ArrayList<String>>();
	int online = 0, total = 0;
	for (String player : herd.members.keySet()) {
	    OfflinePlayer op = Bukkit.getOfflinePlayer(player);
	    if (op.isOnline()) {
		online++;
		Rank rank = herd.getRank(player);
		ArrayList<String> byRanks = members.get(rank);
		if (byRanks == null) {
		    byRanks = new ArrayList<String>();
		}
		byRanks.add(player);
		members.put(rank, byRanks);
	    }
	    total++;
	}
	sendMessage(sender, "Members online for " + herd.displayName + ChatColor.LIGHT_PURPLE + " - " + online + "/" + total + " online!");
	Pony pony = Ponyville.getPony(herd.herdLeader);
	sender.sendMessage(ChatColor.LIGHT_PURPLE + "Leader: " + pony.getNickname() + (pony.getPlayer().isOnline() ? " (Online)" : " (Offline)"));
	ArrayList<String> mod, member, guest, recruit;
	mod = members.get(Rank.MODERATOR);
	member = members.get(Rank.MEMBER);
	guest = members.get(Rank.GUEST);
	recruit = members.get(Rank.RECRUIT);
	if (mod != null && mod.size() > 0)
	    sender.sendMessage(ChatColor.DARK_GREEN + "Mods: " + listcmdstringparse(mod, ChatColor.DARK_GREEN));
	if (member != null && member.size() > 0)
	    sender.sendMessage(ChatColor.AQUA + "Members: " + listcmdstringparse(member, ChatColor.AQUA));
	if (guest != null && guest.size() > 0)
	    sender.sendMessage(ChatColor.YELLOW + "Guests: " + listcmdstringparse(guest, ChatColor.YELLOW));
	if (herd.hasModPrivs(sender, false) && (recruit != null && recruit.size() > 0))
	    sender.sendMessage(ChatColor.DARK_GRAY + "Recruits: " + listcmdstringparse(recruit, ChatColor.DARK_GRAY));
	return true;
    }

    private String listcmdstringparse(ArrayList<String> list, ChatColor color) {
	String listStr = "";
	for (String playerString : list) {
	    Player player = Bukkit.getPlayerExact(playerString);
	    if (player != null) {
		listStr += color + player.getDisplayName() + color + ", ";
	    }
	}
	return listStr.substring(0, listStr.length() - 4);
    }

    public boolean setmotdcmd(Player setter, String newmotd, Herd herd) {
	if (!herd.hasModPrivs(setter, true)) {
	    return false;
	}
	newmotd = ChatColor.translateAlternateColorCodes('&', newmotd);
	herd.motd = newmotd;
	herd.broadcast("New herd message of the day! " + herd.motd, true, false, false);
	saveHerd(herd);
	return true;
    }

    public boolean setentercmd(Player setter, String newEnter, Herd herd) {
	if (!herd.hasModPrivs(setter, true)) {
	    return false;
	}
	newEnter = ChatColor.translateAlternateColorCodes('&', newEnter);
	herd.enterMessage = newEnter;
	saveHerd(herd);
	sendMessage(setter, "Youve set the enter message to: " + newEnter);
	return true;
    }

    public boolean setleavecmd(Player setter, String newLeave, Herd herd) {
	if (!herd.hasModPrivs(setter, true)) {
	    return false;
	}
	newLeave = ChatColor.translateAlternateColorCodes('&', newLeave);
	herd.leaveMessage = newLeave;
	saveHerd(herd);
	sendMessage(setter, "Youve set the leave message to: " + newLeave);
	return true;
    }

    public boolean toggleprotectioncmd(Player player, Herd herd) {
	if (!herd.hasModPrivs(player, true)) {
	    return false;
	}
	herd.setProtection(!herd.protectionEnabled());
	return true;
    }

    /*
     * Bookmark Listeners
     */
    @PonyEvent
    public void onJoin(JoinEvent event) {
	if (event.isJoining()) {
	    Player player = event.getPlayer();
	    initPlayerLocation(player);
	    Herd h = getPlayerHerd(player);
	    if (h != null) {
		player.sendMessage(ChatColor.AQUA + "Herd MOTD: " + ChatColor.WHITE + h.motd);
	    }
	}
    }

    @PonyEvent
    public void onQuit(QuitEvent event) {
	if (event.isQuitting()) {
	    playerLocation.remove(event.getPlayer().getName());
	}
    }

    /*
     * Bookmark Utility methods
     */
    public Herd getPlayerHerd(Player p) {
	return getPlayerHerd(p.getName());
    }

    public Herd getPlayerHerd(String name) {
	for (Herd h : herds.values()) {
	    if (h.isGeneralMember(name)) {
		return h;
	    }
	}
	return null;
    }

    public Herd getHerd(String name) {
	return herds.get(name.toLowerCase());
    }

    public Herd getCmdHerd(Player player) {
	for (Herd h : herds.values()) {
	    if (h.isGeneralMember(player.getName())) {
		return h;
	    }
	}
	sendMessage(player, "You're not in a herd!");
	return null;
    }

    public void saveAllHerds() {
	for (Herd h : herds.values()) {
	    saveHerd(h);
	}
	saveConfig();
    }

    private void saveHerd(Herd h) {
	h.save(getConfig());
	saveConfig();
    }

    private void deleteHerd(Herd h, boolean notifyMembers) {
	if (notifyMembers) {
	    h.broadcast("The herd \"" + h.displayName + ChatColor.LIGHT_PURPLE + "\" has been dissolved. You are now free to join another herd :C", true, false, true);
	}
	getConfig().set(h.name, null);
	herds.remove(h.name);
	saveConfig();
    }

    private boolean createHerd(String herdName, String owner) {
	herdName = herdName.toLowerCase();
	for (Herd herd : herds.values()) {
	    if (herd.name.equals(herdName) || ChatColor.stripColor(herd.displayName).toLowerCase().equals(herdName) || getPlayerHerd(owner) != null) {
		return false;
	    }
	}
	Herd herd = new Herd(herdName, owner);
	herd.members.put(owner, Rank.LEADER);
	herds.put(herdName, herd);
	saveHerd(herd);
	return true;
    }

    public void deactivate() {
	saveAllHerds();
    }
}
