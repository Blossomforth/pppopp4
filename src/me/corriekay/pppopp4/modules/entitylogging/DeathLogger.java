package me.corriekay.pppopp4.modules.entitylogging;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.utilities.Utils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.entity.Sheep;
import org.bukkit.entity.Tameable;
import org.bukkit.entity.Villager;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.ItemDespawnEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

public class DeathLogger extends PSCmdExe {

    private final HashMap<ItemStack, String> itemDeathLogger = new HashMap<ItemStack, String>();

    public DeathLogger() {
	super("DeathLogger", Ponies.Fluttershy);
    }

    private String logDeath(Player player, PlayerDeathEvent event) {
	File directory = new File(Mane.getInstance().getDataFolder() + File.separator + "Death Logger" + File.separator + player.getName());
	if (!directory.exists()) {
	    directory.mkdirs();
	}
	int numberOfLogs = directory.listFiles().length + 1;
	File file = new File(directory + File.separator + numberOfLogs + ".yml");
	try {
	    file.createNewFile();
	} catch (IOException e) {
	    e.printStackTrace();
	    return null;
	}
	FileConfiguration deathConfig = YamlConfiguration.loadConfiguration(file);
	deathConfig.set("Deceased", player.getName());

	LivingEntity ent = event.getEntity();
	Location entloc = ent.getLocation();
	String coords = "x: " + (int) entloc.getX() + " y: " + (int) entloc.getY() + " z: " + (int) entloc.getZ();
	String entitytype = ent.getType().name().toLowerCase();
	String attacktype;
	try {
	    attacktype = ent.getLastDamageCause().getCause().name().toLowerCase();
	} catch (NullPointerException e) {
	    attacktype = "unknown";
	}
	String whokilled;
	if (ent.getLastDamageCause() instanceof EntityDamageByEntityEvent) {
	    EntityDamageByEntityEvent edbee = (EntityDamageByEntityEvent) ent.getLastDamageCause();
	    DamageCause dc = edbee.getCause();
	    if (dc == DamageCause.PROJECTILE) {
		Projectile p = (Projectile) edbee.getDamager();
		LivingEntity le = p.getShooter();
		if (le instanceof Player) {
		    whokilled = ((Player) le).getName();
		} else {
		    try {
			whokilled = le.getType().name().toLowerCase();
		    } catch (NullPointerException e) {
			whokilled = "Unknown";
		    }
		}
	    } else if (dc == DamageCause.ENTITY_ATTACK) {
		if (edbee.getDamager() instanceof Player) {
		    whokilled = ((Player) edbee.getDamager()).getName();
		} else {
		    whokilled = dc.name().toLowerCase();
		}
	    }/*
	      * else if (dc == DamageCause.ENTITY_EXPLOSION){ String whotarget =
	      * ""; if(((Creature)edbee.getDamager()).getTarget() instanceof
	      * Player){ whotarget =
	      * ((Player)(((Creature)edbee.getDamager()).getTarget
	      * ())).getName(); } else { TODO fix this damn block! D: whotarget
	      * = ((Creature)edbee.getDamager()).getTarget().getType().name(); }
	      * whokilled =
	      * edbee.getDamager().getType().name()+" exploded while targetting"
	      * + whotarget; }
	      */else {
		whokilled = dc.name().toLowerCase();
	    }
	} else {
	    whokilled = "a block of " + ent.getEyeLocation().getBlock().getType().name().toLowerCase();
	}
	if (ent instanceof Sheep) {
	    entitytype = ((Sheep) ent).getColor().name().toLowerCase() + " sheep";
	} else if (ent instanceof Villager) {
	    entitytype = ((Villager) ent).getProfession().name().toLowerCase() + " villager";
	} else if (ent instanceof Tameable) {
	    if (((Tameable) ent).isTamed()) {
		entitytype = ((Tameable) ent).getOwner().getName() + "\'s " + ent.getType().name().toLowerCase();
	    }
	} else if (ent instanceof Player) {
	    entitytype = ((Player) ent).getName();
	}

	try {
	    deathConfig.set("Death Cause", whokilled + " killed " + entitytype + " with " + attacktype + " at " + coords);
	} catch (NullPointerException e) {
	    deathConfig.set("Death Cause", "Unknown");
	}
	deathConfig.set("Time of Death", Utils.getDate(System.currentTimeMillis()));
	deathConfig.set("location", toStringListLoc(player.getLocation()));
	saveInventoryToStringList(deathConfig, player.getInventory().getContents());
	deathConfig.set("armor", player.getInventory().getArmorContents());
	deathConfig.set("Item Pickup", new ArrayList<String>());
	try {
	    deathConfig.save(file);
	} catch (IOException e) {
	    e.printStackTrace();
	    return null;
	}
	return directory + File.separator + numberOfLogs + ".yml";
    }

    private Object toStringListLoc(Location l) {
	ArrayList<String> list = new ArrayList<String>();
	list.add(l.getWorld().getName());
	list.add(l.getX() + "");
	list.add(l.getY() + "");
	list.add(l.getZ() + "");
	list.add(l.getPitch() + "");
	list.add(l.getYaw() + "");
	return list;
    }

    @PonyEvent
    public void onDeath(PlayerDeathEvent event) {
	String string = logDeath(event.getEntity(), event);
	for (ItemStack stack : event.getDrops()) {
	    itemDeathLogger.put(stack, string);
	}
	event.setDeathMessage(event.getDeathMessage().replace(event.getEntity().getName(), event.getEntity().getDisplayName() + ChatColor.WHITE));
	Bukkit.getConsoleSender().sendMessage(event.getDeathMessage());
	Player player = event.getEntity();
	Location loc = player.getLocation();
	sendMessage(player, "Oh gosh! Are you okay? ..Shoot, silly me, of course youre not okay... Well, for future reference, heres your location: x: " + loc.getBlockX() + " y: " + loc.getBlockY() + " z: " + loc.getBlockZ() + "! Dont lose your stuff!!");
    }

    @PonyEvent
    public void onPickup(PlayerPickupItemEvent event) {
	ItemStack item = event.getItem().getItemStack();
	if (itemDeathLogger.containsKey(item)) {
	    FileConfiguration config = YamlConfiguration.loadConfiguration(new File(itemDeathLogger.get(item)));
	    java.util.List<String> itemLog = config.getStringList("Item Pickup");
	    itemLog.add(item.getType() + "(" + item.getAmount() + ") was picked up by " + event.getPlayer().getName());
	    config.set("Item Pickup", itemLog);
	    try {
		config.save(new File(itemDeathLogger.get(item)));
		itemDeathLogger.remove(item);
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
    }

    /*
     * @EventHandler public void entityDeath(EntityDeathEvent event){ Entity e =
     * event.getEntity(); if(e instanceof Item){ Item i = (Item)e; ItemStack is
     * = i.getItemStack(); if(itemDeathLogger.containsKey(is)){
     * FileConfiguration config = YamlConfiguration.loadConfiguration(new
     * File(itemDeathLogger.get(is))); List<String> itemLog =
     * config.getStringList("Item Pickup");
     * itemLog.add(is.getType()+"("+is.getAmount()+") has died somehow!");
     * config.set("Item Pickup",itemLog); try { config.save(new
     * File(itemDeathLogger.get(is))); itemDeathLogger.remove(is); } catch
     * (IOException ee) { ee.printStackTrace(); } } } }
     */
    @PonyEvent
    public void itemDespawn(ItemDespawnEvent event) {
	ItemStack item = event.getEntity().getItemStack();
	if (itemDeathLogger.containsKey(item)) {
	    FileConfiguration config = YamlConfiguration.loadConfiguration(new File(itemDeathLogger.get(item)));
	    List<String> itemLog = config.getStringList("Item Pickup");
	    itemLog.add(item.getType() + "(" + item.getAmount() + ") Despawned!");
	    config.set("Item Pickup", itemLog);
	    try {
		config.save(new File(itemDeathLogger.get(item)));
		itemDeathLogger.remove(item);
	    } catch (IOException e) {
		e.printStackTrace();
	    }
	}
    }

    private void saveInventoryToStringList(FileConfiguration config, ItemStack[] iss) {
	config.set("inventory", toStringList(iss));
    }

    @SuppressWarnings("deprecation")
    private List<String> toStringList(ItemStack[] inventory) {
	List<String> stringInventory = new ArrayList<String>();
	for (ItemStack stack : inventory) {
	    if (stack == null) {
		stringInventory.add("air");
	    } else {
		String invToString = "";
		invToString += stack.getTypeId();
		invToString += " " + stack.getAmount();
		invToString += " " + stack.getDurability();
		invToString += " " + stack.getData().getData();
		for (Enchantment enchant : stack.getEnchantments().keySet()) {
		    invToString += " " + enchant.getId() + " " + stack.getEnchantmentLevel(enchant);
		}
		stringInventory.add(invToString);
	    }
	}
	return stringInventory;
    }
}
