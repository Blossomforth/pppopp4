package me.corriekay.pppopp4.modules;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PonyEvent {
    EventPriority priority() default EventPriority.NORMAL;

    Class<? extends Event>[] eventTypes() default {};
}
