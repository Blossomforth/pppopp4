package me.corriekay.pppopp4.modules.equestria;

import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

public class PonyUniverse {

    private PonyWorld overworld, nether, end;
    private String name;
    private Location spawn;

    public PonyUniverse(String name, PonyWorld overworld, PonyWorld nether,
	    PonyWorld end, Location spawn, boolean enableRemotechest,
	    boolean enableEnderchest) {
	this.name = name;
	if (overworld == null) {
	    throw new NullPointerException("Overworld for universe " + name + " cannot be null.");
	}
	overworld.setUniverse(this);
	overworld.setRemotechest(enableRemotechest);
	overworld.setEnderchest(enableEnderchest);
	if (nether != null) {
	    nether.setUniverse(this);
	    nether.setRemotechest(enableRemotechest);
	    nether.setEnderchest(enableEnderchest);
	}
	if (end != null) {
	    end.setUniverse(this);
	    end.setRemotechest(enableRemotechest);
	    end.setEnderchest(enableEnderchest);
	}
	this.overworld = overworld;
	this.nether = nether;
	this.end = end;
	this.spawn = spawn;
    }

    public PonyWorld getOverworld() {
	return overworld;
    }

    public boolean hasOverworld() {
	return true;
    }

    public PonyWorld getNether() {
	return nether;
    }

    public boolean hasNether() {
	return nether != null;
    }

    public PonyWorld getEnd() {
	return end;
    }

    public boolean hasEnd() {
	return end != null;
    }

    public String getName() {
	return name;
    }

    public Location getSpawn() {
	return spawn;
    }

    protected void setSpawn(Location l) {
	spawn = l;
    }

    protected void setOverworld(PonyWorld overworld) {
	if (overworld == null) {
	    throw new NullPointerException("Attempted to set overworld for " + name + " to null.");
	}
	this.overworld = overworld;
	overworld.setUniverse(this);
    }

    protected void setNether(PonyWorld nether) {
	this.nether = nether;
	nether.setUniverse(this);
    }

    protected void setEnd(PonyWorld end) {
	this.end = end;
	end.setUniverse(this);
    }

    public boolean equals(Object obj) {
	if (obj instanceof PonyUniverse) {
	    PonyUniverse other = (PonyUniverse) obj;
	    return other.getName().equals(getName());
	} else {
	    return false;
	}
    }

    public void save(FileConfiguration config) {
	PonyWorld world = getOverworld();
	config.set(name + ".type", world.getWorld().getEnvironment().toString());
	config.set(name + ".gamemode", world.getGameMode().toString());
	config.set(name + ".gametype", world.getGameType().toString());
	config.set(name + ".enableEnderchest", world.enabledEnderchest());
	config.set(name + ".enableRemotechest", world.enabledRemotechest());
	config.set(name + ".seed", world.getWorld().getSeed());
	config.set(name + ".flatworld", world.isFlatworld());
	config.set(name + ".spawn", Utils.getArrayLoc(world.getSpawn()));
	config.set(name + ".end", getEnd() != null);
	config.set(name + ".nether", getNether() != null);
    }
}
