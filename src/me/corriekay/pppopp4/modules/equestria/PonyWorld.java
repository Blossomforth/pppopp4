package me.corriekay.pppopp4.modules.equestria;

import java.util.Collection;
import java.util.HashMap;

import org.bukkit.*;
import org.bukkit.World.Environment;

public class PonyWorld {

    private static final HashMap<String, PonyWorld> worlds = new HashMap<String, PonyWorld>();
    private World w;
    private GameMode gm;
    private PonyWorld overWorld, netherWorld, endWorld;
    private GameType gt;
    private PonyUniverse universe;
    private boolean enableRemotechest, enableEnderchest;
    private boolean flatworld;

    public PonyWorld(World w, boolean flatworld) {
	this.w = w;
	this.flatworld = flatworld;
	worlds.put(w.getName(), this);
    }

    protected void setUniverse(PonyUniverse universe) {
	this.universe = universe;
    }

    public PonyUniverse getUniverse() {
	return universe;
    }

    public GameMode getGameMode() {
	return gm;
    }

    public void setGameMode(GameMode g) {
	gm = g;
    }

    public World getWorld() {
	return w;
    }

    public void setDifficulty(Difficulty d) {
	getWorld().setDifficulty(d);
    }

    public Location getSpawn() {
	return getUniverse().getSpawn();
    }

    public static PonyWorld getPonyWorld(Location l) {
	return getPonyWorld(l.getWorld());
    }

    public static PonyWorld getPonyWorld(World w) {
	return getPonyWorld(w.getName());
    }

    public static PonyWorld getPonyWorld(String worldname) {
	return worlds.get(worldname);
    }

    public static Collection<PonyWorld> getPonyWorlds() {
	return worlds.values();
    }

    public PonyWorld getNether() {
	return netherWorld;
    }

    public void setNether(PonyWorld netherWorld) {
	this.netherWorld = netherWorld;
    }

    public PonyWorld getOverworld() {
	return overWorld;
    }

    public void setOverworld(PonyWorld overWorld) {
	this.overWorld = overWorld;
    }

    public PonyWorld getEnd() {
	return endWorld;
    }

    public void setEnd(PonyWorld endWorld) {
	this.endWorld = endWorld;
    }

    public GameType getGameType() {
	return gt;
    }

    public void setGameType(GameType gt) {
	this.gt = gt;
    }

    public boolean isPVE() {
	return gt == GameType.PVE;
    }

    public boolean isPVP() {
	return gt == GameType.PVP;
    }

    public boolean isCreative() {
	return gt == GameType.CREATIVE;
    }

    public boolean isNether() {
	return getWorld().getEnvironment() == Environment.NETHER;
    }

    public boolean isOverworld() {
	return getWorld().getEnvironment() == Environment.NORMAL;
    }

    public boolean isEnd() {
	return getWorld().getEnvironment() == Environment.THE_END;
    }

    public String getName() {
	return getWorld().getName();
    }

    public void setEnderchest(boolean flag) {
	enableEnderchest = flag;
    }

    public boolean enabledEnderchest() {
	return enableEnderchest;
    }

    public void setRemotechest(boolean flag) {
	enableRemotechest = flag;
    }

    public boolean enabledRemotechest() {
	return enableRemotechest;
    }

    public boolean isFlatworld() {
	return flatworld;
    }

    public void nullify() {
	worlds.clear();
	w = null;
	setOverworld(null);
	setNether(null);
	setEnd(null);
    }
}
