package me.corriekay.pppopp4.modules.equestria;

public enum GameType {
    PVP("(PVP)"), PVE("(PVE)"), CREATIVE("(Creative)");

    String toString;

    GameType(String toString) {
	this.toString = toString;
    }

    public String toString() {
	return toString;
    }
}
