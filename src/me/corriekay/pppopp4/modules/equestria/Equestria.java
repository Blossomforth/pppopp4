package me.corriekay.pppopp4.modules.equestria;

import java.util.ArrayList;
import java.util.HashMap;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.invsee.InvSee;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.PonyStats;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.*;
import org.bukkit.World.Environment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.player.PlayerPortalEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.PlayerInventory;

public class Equestria extends PSCmdExe {

    // world1: subworld. world2: parent world
    private HashMap<String, PonyUniverse> universes = new HashMap<String, PonyUniverse>();
    protected static Equestria equestria;

    public Equestria() {
	super("Equestria", Ponies.PinkiePie);
	equestria = this;
	if (loadConfig("equestria.yml")) {
	    for (World w : Bukkit.getWorlds()) {
		if (w.getEnvironment() == Environment.NORMAL) {
		    PonyWorld world = new PonyWorld(w, false), end = new PonyWorld(Bukkit.getWorld(w.getName() + "_the_end"), false), nether = new PonyWorld(Bukkit.getWorld(w.getName() + "_nether"), false);
		    world.setDifficulty(w.getDifficulty());
		    world.setEnd(end);
		    world.setNether(nether);
		    world.setGameMode(GameMode.SURVIVAL);
		    world.setGameType(GameType.PVE);
		    world.setRemotechest(true);
		    world.setEnderchest(true);
		    PonyUniverse universe = new PonyUniverse(w.getName(), world, nether, end, w.getSpawnLocation(), true, true);
		    universes.put(universe.getName(), universe);
		    universe.save(getConfig());
		    saveConfig();
		}
	    }
	}
	for (String world : getConfig().getKeys(false)) {
	    Environment e = Environment.NORMAL;
	    WorldType wt = WorldType.NORMAL;
	    GameMode gm = GameMode.SURVIVAL;
	    GameType gt = GameType.PVE;
	    boolean enableEnderchest, enableRemotechest;
	    Location spawn;
	    try {
		wt = WorldType.valueOf(getConfig().getString(world + ".type", "NORMAL"));
	    } catch (Exception e1) {
		System.out.println(world + " type is invalid, defaulting to normal");
	    }
	    try {
		gm = GameMode.valueOf(getConfig().getString(world + ".gamemode", "SURVIVAL"));
	    } catch (Exception e1) {
		System.out.println(world + " gamemode is invalid, defaulting to survival");
	    }
	    try {
		gt = GameType.valueOf(getConfig().getString(world + ".gametype", "PVE").toUpperCase());
	    } catch (Exception e1) {
		System.out.println(world + " gametype is invalid, defaulting to PVE");
	    }
	    enableEnderchest = (getConfig().contains(world + ".enableEnderchest") ? getConfig().getBoolean(world + ".enableEnderchest", false) : false);
	    enableRemotechest = (getConfig().contains(world + ".enableRemotechest") ? getConfig().getBoolean(world + ".enableRemotechest", false) : false);
	    String seed = getConfig().getString(world + ".seed");
	    boolean flatworld = getConfig().getBoolean(world + ".flatworld", false);
	    PonyWorld w = loadWorld(world, e, wt, seed, flatworld);
	    spawn = Utils.getLoc((ArrayList<String>) getConfig().getStringList(world + ".spawn"));
	    if (spawn == null) {
		spawn = w.getWorld().getSpawnLocation();
		getConfig().set(world + ".spawn", Utils.getArrayLoc(spawn));
		saveConfig();
	    }
	    w.setGameType(gt);
	    Difficulty d = Difficulty.NORMAL;
	    try {
		d = Difficulty.valueOf(getConfig().getString(world + ".difficulty", "NORMAL"));
	    } catch (Exception e1) {
		System.out.println(world + " difficulty is invalid, defaulting to normal");
	    }
	    w.setDifficulty(d);
	    w.setOverworld(w);
	    w.setGameMode(gm);
	    PonyWorld end = null, nether = null;
	    if (getConfig().getBoolean(world + ".end", false)) {
		end = loadWorld(world + "_the_end", Environment.THE_END, wt, seed, flatworld);
		end.setDifficulty(d);
		end.setGameType(gt);
		end.setGameMode(gm);
		end.setOverworld(w);

	    }
	    if (getConfig().getBoolean(world + ".nether", false)) {
		nether = loadWorld(world + "_nether", Environment.NETHER, wt, seed, flatworld);
		nether.setDifficulty(d);
		nether.setGameType(gt);
		nether.setGameMode(gm);
		nether.setOverworld(w);
	    }
	    if (nether != null) {
		nether.setNether(nether);
		nether.setEnd(end);
	    }
	    if (end != null) {
		end.setNether(nether);
		end.setEnd(end);
	    }
	    PonyUniverse universe = new PonyUniverse(world, w, nether, end, spawn, enableRemotechest, enableEnderchest);
	    universes.put(world, universe);
	}
	for (PonyWorld pw : PonyWorld.getPonyWorlds()) {
	    if (pw.getGameType() == GameType.CREATIVE) {
		for (Entity e : pw.getWorld().getEntities()) {
		    if (e instanceof LivingEntity && !(e instanceof Player)) {
			e.remove();
		    }
		}
	    }
	}
	for (World w : Bukkit.getWorlds()) {
	    PonyWorld world = PonyWorld.getPonyWorld(w);
	    if (world == null) {
		throw new NullPointerException("World " + w.getName() + " not accounted for! Please add this world to the equestria.yml configuration file!");
	    }
	}
    }

    private PonyWorld loadWorld(String worldname, Environment e, WorldType wt, String seed, boolean flatworld) {
	World w = Bukkit.getWorld(worldname);
	if (w != null) {
	    PonyWorld pw = PonyWorld.getPonyWorld(w);
	    if (pw == null) {
		pw = new PonyWorld(w, flatworld);
	    }
	    return pw;
	}
	WorldCreator wc = new WorldCreator(worldname);
	if (flatworld) {
	    wc.generator(new FlatWorldGenerator());
	    wc.generateStructures(false);
	}
	wc.environment(e).type(wt);
	long s;
	try {
	    s = Long.parseLong(seed);
	} catch (Exception ex) {
	    s = seed.hashCode();
	}
	wc.seed(s);
	World world = wc.createWorld();
	PonyWorld pw = new PonyWorld(world, flatworld);
	return pw;
    }

    public boolean setUniverseSpawnLocation(Location l, PonyUniverse universe) {
	PonyUniverse targetUniverse = PonyWorld.getPonyWorld(l).getUniverse();
	if (!universe.equals(targetUniverse)) {
	    return false;
	}
	universe.setSpawn(l);
	getConfig().set(universe.getName() + ".spawn", Utils.getArrayLoc(l));
	saveConfig();
	return true;
    }

    public static Equestria getHandler() {
	return equestria;
    }

    @PonyEvent(priority = EventPriority.HIGHEST)
    public void spawnEvent(CreatureSpawnEvent event) {
	try {
	    PonyWorld world = PonyWorld.getPonyWorld(event.getLocation());
	    if (world.isCreative()) {
		event.setCancelled(true);
	    }
	} catch (Exception e) {
	    event.setCancelled(true);
	}
    }

    @PonyEvent(priority = EventPriority.HIGHEST)
    public void onTeleport(PlayerTeleportEvent event) {
	Player player = event.getPlayer();
	PonyWorld fromWorld, toWorld;
	fromWorld = PonyWorld.getPonyWorld(event.getFrom()).getOverworld();
	toWorld = PonyWorld.getPonyWorld(event.getTo()).getOverworld();
	if (!fromWorld.equals(toWorld)) {
	    if (InvSee.handler.isInvseeing(player.getName())) {
		sendMessage(player, "Please don't move worlds if you're invseeing!");
		event.setCancelled(true);
	    }
	    Pony pony = Ponyville.getPony(player);
	    pony.setInventory(player.getInventory(), fromWorld);
	    pony.setWorldStats(new PonyStats(player), fromWorld);
	    PlayerInventory pi = pony.getInventory(toWorld);
	    player.getInventory().setContents(pi.getContents());
	    player.getInventory().setArmorContents(pi.getArmorContents());
	    pony.getWorldStats(player, toWorld);
	    pony.save();
	    player.setGameMode(toWorld.getGameMode());
	}
    }

    @PonyEvent
    public void onPortalEnter(PlayerPortalEvent event) {
	PonyWorld pw = PonyWorld.getPonyWorld(event.getPlayer().getWorld());
	World toWorld;
	double ratio = 0;
	if (pw.isOverworld()) {
	    ratio = 0.125;
	    if (!pw.getUniverse().hasNether()) {
		return;
	    } else {
		toWorld = pw.getNether().getWorld();
	    }
	} else if (pw.isNether()) {
	    ratio = 8.0;
	    if (!pw.getUniverse().hasOverworld()) {
		return;
	    } else {
		toWorld = pw.getOverworld().getWorld();
	    }
	} else {
	    return;
	}
	Location from = event.getFrom();
	Location finalLoc = new Location(toWorld, from.getX() * ratio, from.getY(), from.getZ() * ratio);
	event.setTo(finalLoc);
    }
}
