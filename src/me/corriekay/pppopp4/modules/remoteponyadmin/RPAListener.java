package me.corriekay.pppopp4.modules.remoteponyadmin;

import java.security.KeyPair;
import java.security.interfaces.RSAPublicKey;
import java.util.HashSet;

import me.corriekay.ponipackets.PacketUtils;
import me.corriekay.ponipackets.client.ClientAlertResponsePacket;
import me.corriekay.ponipackets.client.ClientChatPacket;
import me.corriekay.ponipackets.client.ClientHoofshake;
import me.corriekay.ponipackets.server.PlayerListPacket;
import me.corriekay.ponipackets.server.Playur;
import me.corriekay.ponipackets.server.ServerHoofshake;
import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.modules.chat.Channel;
import me.corriekay.pppopp4.modules.chat.ChannelHandler;
import me.corriekay.pppopp4.modules.ponymanager.PonyManager;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.utilities.PonyLogger;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.FrameworkMessage.KeepAlive;
import com.esotericsoftware.kryonet.Listener;

public class RPAListener extends Listener {

    RemotePonyAdmin rpa;

    public RPAListener(RemotePonyAdmin rpa) {
	this.rpa = rpa;
    }

    // Method called when a new connection is incoming
    public void connected(Connection c) {
	try {
	    for (Connection connection : rpa.server.getConnections()) {
		if (!connection.isConnected()) {
		    continue;
		}
		if (!(c.equals(connection)) && (connection.getRemoteAddressTCP().getHostName().equals(c.getRemoteAddressTCP().getHostName()))) {
		    System.out.println("Disconnecting RPA client: Logged in from another client on the same host.");
		    killClient(connection, "Logged in from another client on the same host!");
		}
	    }
	} catch (Exception e) {}
	KeyPair kp;
	try {
	    kp = PacketUtils.keyPairGen();
	} catch (Exception e) {
	    e.printStackTrace();
	    killClient(c, "Exception thrown serverside generating RSA keypair");
	    return;
	}
	rpa.connecting.put(c, kp.getPrivate());
	byte[] pubMod, pubExp;
	RSAPublicKey pubkey = (RSAPublicKey) kp.getPublic();
	pubMod = pubkey.getModulus().toByteArray();
	pubExp = pubkey.getPublicExponent().toByteArray();
	ServerHoofshake shs = new ServerHoofshake();
	shs.ver = PacketUtils.version;
	shs.pubMod = pubMod;
	shs.pubExp = pubExp;
	try {
	    c.sendTCP(shs);
	} catch (Exception e) {
	    killClient(c, "An exception was caught trying to log in. The little bastard. Try again!");
	    e.printStackTrace();
	}
    }

    // Method called when a packet is received
    public void received(Connection c, Object p) {
	try {
	    if (p instanceof KeepAlive) {
		return;
	    }
	    if (p instanceof ClientHoofshake) {
		ClientHoofshake chs = (ClientHoofshake) p;
		byte[] pw = chs.pw;
		String pass;
		try {
		    pass = new String(PacketUtils.rsaDecrypt(pw, rpa.connecting.get(c)));
		} catch (Exception e) {
		    System.out.println("Disconnecting RPA client: Exception decrypting password!");
		    e.printStackTrace();
		    killClient(c, "Serverside exception decrypting password");
		    return;
		} finally {
		    rpa.connecting.remove(c);
		}
		Pony pony = Ponyville.getPony(chs.name);
		if (pony == null) {
		    System.out.println("Disconnecting RPA client: 404 - Pony not found - " + chs.name);
		    killClient(c, "Username not found!");
		    return;
		}
		boolean isOp = PonyManager.ponyManager.getGroup(pony.getGroup()).isOPType();
		if (!isOp) {
		    System.out.println("Disconnecting RPA Client: is not OP - " + chs.name + " " + pony.getGroup());
		    killClient(c, "User is not OP");
		    return;
		}
		Integer filePass = pony.getRPAPassword();
		if (filePass == null) {
		    System.out.println("Disconnecting RPA Client: Config password does not exist - " + chs.name);
		    killClient(c, "Please go into the minecraft server and set your password with /setpassword <password> before attempting to log in.");
		    return;
		}
		try {
		    if (filePass.intValue() != Integer.parseInt(pass)) {
			System.out.println("Disconnecting RPA client: Incorrect Password - " + chs.name);
			killClient(c, "Password does not match!");
			return;
		    }
		} catch (NumberFormatException e) {
		    System.out.println("Disconnecting RPA client: Password Hash malformed - " + chs.name);
		    killClient(c, "Please delete your preferences file and have the system fill out your password again!");
		    return;
		}
		for (Connection connection : rpa.server.getConnections()) {
		    if (!connection.isConnected()) {
			continue;
		    }
		    if (connection.equals(c)) {
			continue;
		    }
		    if (connection.isConnected()) {
			RemotePony pony2 = rpa.remotePonies.get(connection);
			if (pony2 == null) {
			    continue;
			}
			if (pony2.getName().equals(chs.name)) {
			    System.out.println("Disconnecting RPA client: logged in from another client! - " + chs.name);
			    killClient(connection, "Logged in from another client!");
			}
		    }
		}
		RemotePony rp = new RemotePony(pony, c);
		rpa.connected.put(rp, c);
		rpa.remotePonies.put(c, rp);

		// Send data
		System.out.println(pony.getName() + " has connected to RPA");
		updateAllPlayerLists();
		rpa.updateAlerts(c, rpa.getAlertPacket());
		return;
	    }
	    RemotePony pony = rpa.remotePonies.get(c);
	    if (pony == null) {
		return;
	    }
	    if (p instanceof ClientChatPacket) {
		ClientChatPacket ccp = (ClientChatPacket) p;
		Channel channel = ChannelHandler.getHandler().getChannel(pony.getPony().getChatChannel());
		if (ccp.msg.startsWith("/")) {
		    RemotePony rp = rpa.remotePonies.get(c);
		    org.bukkit.Server s = Bukkit.getServer();
		    if (rp.getPony().getPlayer().isOnline()) {
			s.dispatchCommand(rp.getPony().getPlayer().getPlayer(), ccp.msg.substring(1));
		    } else {
			s.dispatchCommand(rp, ccp.msg.substring(1));
		    }
		    return;
		}
		if (channel != null) {
		    channel.broadcastToChannel(pony.getName(), ccp.msg, true, true);
		}
	    }
	    if (p instanceof ClientAlertResponsePacket) {
		ClientAlertResponsePacket carp = (ClientAlertResponsePacket) p;
		rpa.receiveAlertResponse(carp, c);
	    }
	} catch (Exception e) {
	    Pony pony = rpa.remotePonies.get(c).getPony();
	    String user = "Unknown user";
	    if (pony != null) {
		user = pony.getName();
	    }
	    PonyLogger.logListenerException(e, "UNHANDLED EXCEPTION THROWN RECIEVING A PACKET!! rpa user: " + user, rpa.name, p.getClass().getSimpleName());
	}
    }

    // Method called when a client disconnects
    public void disconnected(Connection c) {
	RemotePony pony = rpa.remotePonies.get(c);
	System.out.println(pony != null ? pony.getName() : "An unknown user" + " has disconnected from RPA.");
	HashSet<RemotePony> removePony = new HashSet<RemotePony>();
	for (RemotePony ponyy : rpa.connected.keySet()) {
	    if (rpa.connected.get(ponyy).equals(c)) {
		removePony.add(ponyy);
	    }
	}
	for (RemotePony ponyy : removePony) {
	    rpa.connected.remove(ponyy);
	}
	rpa.connected.remove(c);
	rpa.remotePonies.remove(c);
    }

    private void killClient(Connection c, String msg) {
	rpa.killClient(c, msg);
    }

    // Update methods

    public void updateAllPlayerLists() {
	for (Connection c : rpa.server.getConnections()) {
	    if (c.isConnected()) {
		updatePlayerList(c);
	    }
	}
    }

    public void updatePlayerList(final Connection c) {
	Bukkit.getScheduler().runTaskLater(Mane.getInstance(), new Runnable() {
	    public void run() {
		PlayerListPacket plp = new PlayerListPacket();
		RemotePony rpony = rpa.remotePonies.get(c);
		for (Player player : Bukkit.getOnlinePlayers()) {
		    Pony pony = Ponyville.getPony(player);
		    if (!rpony.getPony().canSeeInvisible(pony)) {
			continue;
		    }
		    Playur p = new Playur();
		    p.group = pony.getGroup();
		    p.isInRPA = rpa.connected.containsKey(pony);
		    p.name = pony.getName();
		    p.nickname = pony.getNickname();
		    plp.players.put(pony.getName(), p);
		}
		for (RemotePony pony : rpa.connected.keySet()) {
		    Playur p = new Playur();
		    p.group = pony.getPony().getGroup();
		    p.isInRPA = rpa.connected.containsKey(pony);
		    p.name = pony.getName();
		    p.nickname = pony.getPony().getNickname();
		    plp.players.put(pony.getName(), p);
		}
		try {
		    c.sendTCP(plp);
		} catch (Exception e) {}
	    }
	}, 1);
    }
}
