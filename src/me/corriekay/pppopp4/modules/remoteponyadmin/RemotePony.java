package me.corriekay.pppopp4.modules.remoteponyadmin;

import java.util.Set;

import me.corriekay.ponipackets.server.BroadcastMessage;
import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.modules.ponymanager.PonyManager;
import me.corriekay.pppopp4.modules.ponyville.Pony;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Server;
import org.bukkit.command.CommandSender;
import org.bukkit.permissions.PermissibleBase;
import org.bukkit.permissions.Permission;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.permissions.PermissionAttachmentInfo;
import org.bukkit.plugin.Plugin;

import com.esotericsoftware.kryonet.Connection;

public class RemotePony implements CommandSender {

    private final PermissibleBase perm = new PermissibleBase(this);
    private PermissionAttachment pppoppAttach;

    private final Connection c;
    private final Pony pony;

    public RemotePony(Pony pony, Connection c) {
	this.c = c;
	this.pony = pony;
	PonyManager.ponyManager.setRemotePonyPermissions(this);
    }

    public PermissionAttachment getAttachment() {
	return pppoppAttach;
    }

    public void setPPPoPPAttach(PermissionAttachment a) {
	pppoppAttach = a;
    }

    public Pony getPony() {
	return pony;
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin) {
	return perm.addAttachment(plugin);
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, int ticks) {
	return perm.addAttachment(plugin, ticks);
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value) {
	return perm.addAttachment(plugin, name, value);
    }

    @Override
    public PermissionAttachment addAttachment(Plugin plugin, String name, boolean value, int ticks) {
	return perm.addAttachment(plugin, name, value, ticks);
    }

    @Override
    public Set<PermissionAttachmentInfo> getEffectivePermissions() {
	return perm.getEffectivePermissions();
    }

    @Override
    public boolean hasPermission(String permission) {
	return perm.hasPermission(permission);
    }

    @Override
    public boolean hasPermission(Permission permission) {
	return perm.hasPermission(permission);
    }

    @Override
    public boolean isPermissionSet(String permission) {
	return perm.isPermissionSet(permission);
    }

    @Override
    public boolean isPermissionSet(Permission permission) {
	return perm.isPermissionSet(permission);
    }

    @Override
    public void recalculatePermissions() {
	perm.recalculatePermissions();
    }

    @Override
    public void removeAttachment(PermissionAttachment attachment) {
	perm.removeAttachment(attachment);
    }

    @Override
    public boolean isOp() {
	return false;
    }

    @Override
    public void setOp(boolean flag) {
	perm.setOp(flag);
    }

    @Override
    public String getName() {
	return pony.getName();
    }

    @Override
    public Server getServer() {
	return Bukkit.getServer();
    }

    @Override
    public void sendMessage(final String msg) {
	Runnable r = new Runnable() {
	    public void run() {
		BroadcastMessage bm = new BroadcastMessage();
		bm.message = ChatColor.stripColor(msg);
		if (c.isConnected()) {
		    c.sendTCP(bm);
		}
	    }
	};
	Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), r);
    }

    @Override
    public void sendMessage(String[] msgs) {
	BroadcastMessage bm = new BroadcastMessage();
	for (String msg : msgs) {
	    bm.message += msg + "\n";
	}
	try {
	    bm.message = bm.message.substring(0, bm.message.length() - 2);
	} catch (Exception e) {}
	c.sendTCP(bm);
    }

}
