package me.corriekay.pppopp4.modules.remoteponyadmin;

import java.security.Key;
import java.util.HashMap;
import java.util.HashSet;

import me.corriekay.ponipackets.PacketUtils;
import me.corriekay.ponipackets.client.ClientAlertResponsePacket;
import me.corriekay.ponipackets.server.Alert;
import me.corriekay.ponipackets.server.AssassinateClient;
import me.corriekay.ponipackets.server.OPonyAlertPacket;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.ChannelBroadcastEvent;
import me.corriekay.pppopp4.events.JoinEvent;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.chat.Channel;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Server;
import com.esotericsoftware.minlog.Log;

public class RemotePonyAdmin extends PSCmdExe {

    private int port;
    protected Server server;
    protected HashMap<Connection, Key> connecting = new HashMap<Connection, Key>();
    protected HashMap<RemotePony, Connection> connected = new HashMap<RemotePony, Connection>();
    protected HashMap<Connection, RemotePony> remotePonies = new HashMap<Connection, RemotePony>();
    protected HashMap<String, Alert> alerts = new HashMap<String, Alert>();
    protected RPAListener listener;
    public static RemotePonyAdmin rpa;

    public RemotePonyAdmin() throws Exception {
	super("RemotePonyAdmin", Ponies.PinkiePie, "alert", "setpassword", "rpadebug");
	rpa = this;
	if (loadConfig("rpa.yml")) {
	    getConfig().set("port", 25566);
	    getConfig().set("debug", false);
	    saveConfig();
	}
	if (getConfig().getBoolean("debug")) {
	    Log.DEBUG();
	}
	port = getConfig().getInt("port");
	server = new Server(100000000, 100000000);
	PacketUtils.registerServerPackets(server);
	server.start();
	System.out.println("RPA server booting");
	server.bind(port);
	System.out.println("RPA server binding port " + port);
	listener = new RPAListener(this);
	server.addListener(listener);
    }

    protected void killClient(Connection c, String message) {
	for (StackTraceElement ste : Thread.currentThread().getStackTrace()) {
	    System.out.println(ste);
	}
	AssassinateClient ac = new AssassinateClient();
	ac.confirm = true;
	ac.note = message;
	c.sendTCP(ac);
	c.close();
    }

    public void sendMessage(RemotePony pony, String message) {
	Connection c = connected.get(pony);
	if (c == null) {
	    return;
	}
	if (!c.isConnected()) {
	    return;
	}
	pony.sendMessage(message); // DEBUG Ive changed this from sending
				   // directly, to using the pony.sendMessage()
				   // method. May cause issues, but seems more
				   // efficient, considering that i added the
				   // threading safety to the pony object, but
				   // itw as never here.
    }

    public void sendMessageAll(String message) {
	for (RemotePony rp : connected.keySet()) {
	    sendMessage(rp, message);
	}
    }

    protected OPonyAlertPacket getAlertPacket() {
	OPonyAlertPacket opap = new OPonyAlertPacket();
	opap.alerts = new HashSet<Alert>();
	for (Alert alert : alerts.values()) {
	    opap.alerts.add(alert);
	}
	return opap;
    }

    private int updateAlerts(Alert newAlert) {
	OPonyAlertPacket opap = getAlertPacket();
	opap.newAlert = newAlert;
	int count = 0;
	for (Connection c : connected.values()) {
	    if (updateAlerts(c, opap)) {
		count++;
	    }
	}
	return count;
    }

    protected boolean updateAlerts(Connection c, OPonyAlertPacket packet) {
	if (c.isConnected()) {
	    c.sendTCP(packet);
	    return true;
	}
	return false;
    }

    protected void receiveAlertResponse(ClientAlertResponsePacket carp, Connection c) {
	String playername = carp.playerName;
	Pony pony = Ponyville.getPony(playername);
	if (pony == null) {
	    return;
	}
	Alert alert = alerts.get(pony.getName());
	if (alert == null) {
	    updateAlerts(null);
	    return;
	}
	RemotePony responder = remotePonies.get(c);
	if (responder == null) {
	    return;
	}
	alert.responder = responder.getName();
	Player player = Bukkit.getPlayerExact(pony.getName());
	if (player != null) {
	    sendMessage(player, "You've got the attention of an OPony! " + responder.getPony().getNickname() + " is on their way!");
	}
	updateAlerts(null);
    }

    // BOOKMARK Command

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case setpassword: {
	    Pony pony = Ponyville.getPony(player);
	    int pass = args[0].hashCode();
	    pony.setRPAPassword(pass);
	    pony.save();
	    System.out.println("Saving password for " + pony.getName());
	    sendMessage(player, "Password saved! it should now be possible to log into RPA using that password!");
	    return true;
	}
	case alert: {
	    Alert alert = alerts.get(player.getName());
	    String msg = "Help! I need an OPony!";
	    if (args.length > 0) {
		msg = Utils.joinStringArray(args, " ");
	    }
	    if (alert != null) {
		if (alert.timestamp + (1000 * 60 * 5) >= System.currentTimeMillis()) {
		    long timeleft = alert.timestamp + (1000 * 60 * 5);
		    timeleft -= System.currentTimeMillis();
		    int minutesleft = 0, secondsleft = 0;
		    while (timeleft >= 1000 * 60) {
			minutesleft++;
			timeleft -= 1000 * 60;
		    }
		    while (timeleft >= 1000) {
			secondsleft++;
			timeleft -= 1000;
		    }
		    sendMessage(player, "Please be patient, you've got an alert issued already! You've got " + minutesleft + ":" + secondsleft + " left before you can /alert again!");
		    return true;
		}
	    }
	    alert = new Alert();
	    alert.alerter = player.getName();
	    alert.timestamp = System.currentTimeMillis();
	    alert.message = msg;
	    alert.responder = null;
	    alerts.put(player.getName(), alert);
	    int alerted = updateAlerts(alert);
	    sendMessage(player, "Alert sent out! " + alerted + " Oponies have been alerted!");
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    public boolean optionalCommand(CommandSender sender, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case rpadebug: {
	    boolean debug = !getConfig().getBoolean("debug");
	    if (debug) {
		Log.DEBUG();
		sendMessage(sender, "RPA debug on!");
	    } else {
		Log.NONE();
		sendMessage(sender, "RPA debug off!");
	    }
	    getConfig().set("debug", debug);
	    saveConfig();
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    // Bookmark Listeners
    @PonyEvent
    public void join(JoinEvent event) {
	listener.updateAllPlayerLists();
    }

    @PonyEvent
    public void quit(QuitEvent event) {
	listener.updateAllPlayerLists();
    }

    @PonyEvent
    public void msg(ChannelBroadcastEvent event) {
	Channel c = event.getChannel();
	for (RemotePony rp : connected.keySet()) {
	    Pony pony = rp.getPony();
	    if (pony.getListeningChannels().contains(c.getName())) {
		rp.sendMessage(event.getMessage());
	    }
	}
    }

    public void deactivate() {
	for (Connection c : connected.values()) {
	    killClient(c, "The server has closed! Please reboot.");
	}
	server.close();
    }
}
