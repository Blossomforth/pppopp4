package me.corriekay.pppopp4.modules.celestialmanagement;

import java.util.HashSet;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.JoinEvent;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.ponymanager.PonyPermission;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;

public class PrincessCelestia extends PSCmdExe {

    private final HashSet<String> celestials = new HashSet<String>();

    public PrincessCelestia() {
	super("PrincessCelestia", Ponies.PinkiePie, "gm", "celestia");
	for (Player p : Bukkit.getOnlinePlayers()) {
	    login(p, false);
	}
    }

    private void login(Player player, boolean notify) {
	Pony pony = Ponyville.getPony(player);
	if (pony.isGodMode() && player.hasPermission(PonyPermission.IS_OPERATOR.getPerm())) {
	    celestials.add(player.getName());
	    if (notify) {
		sendMessage(player, "Yay! You're a pretty princess now!");
	    }
	}
    }

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case celestia: {
	    boolean isgod = celestials.contains(player.getName());
	    Pony pony = Ponyville.getPony(player);
	    if (isgod) {
		pony.setGodMode(false);
		celestials.remove(player.getName());
		sendMessage(player, "Awh, Okay! Taking off the crown now..");
	    } else {
		pony.setGodMode(true);
		celestials.add(player.getName());
		sendMessage(player, "Yay! youre a pretty princess now!");
	    }
	    pony.save();
	    return true;
	}
	case gm: {
	    GameMode gm = player.getGameMode();
	    if (gm == GameMode.SURVIVAL) {
		player.setGameMode(GameMode.CREATIVE);
	    } else {
		player.setGameMode(GameMode.SURVIVAL);
	    }
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    @PonyEvent
    public void onJoin(JoinEvent event) {
	if (event.isJoining()) {
	    login(event.getPlayer(), true);
	}
    }

    @PonyEvent
    public void onQuit(QuitEvent event) {
	if (event.isQuitting()) {
	    celestials.remove(event.getPlayer().getName());
	}
    }

    @PonyEvent(priority = EventPriority.HIGHEST, eventTypes = { EntityDamageByEntityEvent.class, EntityDamageByBlockEvent.class })
    public void dmgEvent(EntityDamageEvent event) {
	if ((event.getEntity() instanceof Player) && (celestials.contains(((Player) event.getEntity()).getName()))) {
	    event.setCancelled(true);
	}
    }

    @PonyEvent(priority = EventPriority.HIGHEST)
    public void foodChange(FoodLevelChangeEvent event) {
	if ((event.getEntity() instanceof Player) && (celestials.contains(((Player) event.getEntity()).getName()))) {
	    event.setCancelled(true);
	}
    }
}
