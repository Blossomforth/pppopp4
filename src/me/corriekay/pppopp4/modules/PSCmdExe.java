package me.corriekay.pppopp4.modules;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.command.CommandMetadata.CommandType;
import me.corriekay.pppopp4.modules.invisibilityhandler.InvisibilityHandler;
import me.corriekay.pppopp4.modules.ponymanager.PonyPermission;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.modules.remoteponyadmin.RemotePony;
import me.corriekay.pppopp4.utilities.PonyLogger;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_7_R1.CraftOfflinePlayer;
import org.bukkit.craftbukkit.v1_7_R1.CraftServer;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.EventException;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.plugin.EventExecutor;
import org.bukkit.plugin.IllegalPluginAccessException;

import de.diddiz.LogBlockQuestioner.LogBlockQuestioner;

public abstract class PSCmdExe implements EventExecutor, Listener {

    public final String name;

    public final Ponies pony;

    protected final HashMap<Class<? extends Event>, Method> methodMap = new HashMap<Class<? extends Event>, Method>();

    protected static ConsoleCommandSender console = Bukkit.getConsoleSender();

    protected final String notPlayer = "Silly console, You need to be a player to do that!";

    protected final String notEnoughArgs = "Uh oh, youre gonna need to provide more arguments for that command than that!";

    protected final String cantFindPlayer = "I looked high and low, but I couldnt find that pony! :C";

    protected final LogBlockQuestioner questioner;

    private FileConfiguration config = null;

    private String configName = null;

    @SuppressWarnings("unchecked")
    public PSCmdExe(String name, Ponies pony, String... cmds) {
	this.name = name;
	this.pony = pony;
	questioner = (LogBlockQuestioner) Bukkit.getPluginManager().getPlugin("LogBlockQuestioner");
	for (Method method : this.getClass().getMethods()) {
	    method.setAccessible(true);
	    Annotation eh = method.getAnnotation(PonyEvent.class);
	    if (eh != null) {
		Class<?>[] params = method.getParameterTypes();
		if (params.length == 1) {
		    PonyEvent pe = (PonyEvent) eh;
		    registerEvent((Class<? extends Event>) params[0], pe.priority(), pe.eventTypes(), method);
		}
	    }
	}
	if (cmds != null) {
	    registerCommands(cmds);
	}
    }

    private void registerEvent(Class<? extends Event> event, EventPriority priority, Class<? extends Event>[] eventTypes, Method method) {
	try {
	    Bukkit.getPluginManager().registerEvent(event, this, priority, this, Mane.getInstance());
	    methodMap.put(event, method);
	    for (Class<? extends Event> eventType : eventTypes) {
		methodMap.put(eventType, method);
	    }
	} catch (NullPointerException e) {
	    Bukkit.getLogger().severe("Illegal event registration!");
	} catch (IllegalPluginAccessException e) {
	    Bukkit.getLogger().severe("Illegal plugin access exception!");
	    Bukkit.getLogger().severe(e.getMessage());
	    Bukkit.getLogger().severe("Tried to register illegal event: " + event.getCanonicalName());
	}
    }

    private void registerCommands(String[] cmds) {
	for (String cmd : cmds) {
	    if (!Mane.getCmdMap().registerCommand(cmd, this)) {
		Mane.getInstance().getLogger().severe("Attempted command register failed: \"" + cmd + "\" is not registered.");
	    }
	}
    }

    @Override
    public void execute(Listener arg0, Event arg1) throws EventException {
	Method method = methodMap.get(arg1.getClass());
	if (method == null) {
	    return;
	}
	try {
	    method.invoke(this, arg1);
	} catch (Exception e) {
	    if (e instanceof InvocationTargetException) {
		InvocationTargetException ite = (InvocationTargetException) e;
		e.setStackTrace(ite.getCause().getStackTrace());
	    }
	    String eventName = arg1.getClass().getCanonicalName();
	    PonyLogger.logListenerException(e, "Error on event: " + e.getMessage(), name, eventName);
	    sendMessage(console, "Unhandled exception in listener " + name + "! Please check the error logs for more information: " + name + ":" + e.getClass().getCanonicalName());
	}

    }

    public final boolean onCommand(CommandSender sender, CommandMetadata cmd, String label, String[] args, CommandType type) {
	try {
	    switch (type) {
	    case DIFFERENTIATE:
		if (sender instanceof Player) {
		    return playerCommand((Player) sender, cmd, label, args);
		} else if (sender instanceof ConsoleCommandSender) {
		    return consoleCommand((ConsoleCommandSender) sender, cmd, label, args);
		} else if (sender instanceof RemotePony) {
		    return remotePonyCommand((RemotePony) sender, cmd, label, args);
		}
	    case REQUIRES_CONSOLE:
		if (!(sender instanceof ConsoleCommandSender)) {
		    sendMessage(sender, "Silly not-console! You need to be the console to do that!");
		    return true;
		} else {
		    return consoleCommand((ConsoleCommandSender) sender, cmd, label, args);
		}
	    case REQUIRES_PLAYER:
		boolean isPlayerOrRPA = false;
		if (sender instanceof Player) {
		    isPlayerOrRPA = true;
		}
		if (sender instanceof RemotePony) {
		    isPlayerOrRPA = true;
		}
		if (!isPlayerOrRPA) {
		    sendMessage(sender, "Silly not-player! You need to be a player to do that!");
		    return true;
		} else {
		    return (sender instanceof Player) ? playerCommand((Player) sender, cmd, label, args) : remotePonyCommand((RemotePony) sender, cmd, label, args);
		}
	    case OPTIONAL:
		return optionalCommand(sender, cmd, label, args);
	    default:
		return false;
	    }
	} catch (Exception e) {
	    String message = "Exception thrown on command: " + cmd.getCommandName() + "\nLabel: " + label;
	    message += "\nArgs: ";
	    for (String arg : args) {
		message += arg + " ";
	    }
	    PonyLogger.logCmdException(e, message, name);
	    sendMessage(console, "Unhandled exception on Command! Please check the error log for more information!");
	    sendMessage(sender, "OH SWEET CELESTIA SOMETHING WENT HORRIBLY HORRIBLY WRONG! YOU GOTTA TELL THE SERVER ADMINS AS SOON AS YOU CAN D:");
	    return false;
	}
    }

    public boolean optionalCommand(CommandSender sender, CommandMetadata cmd, String label, String[] args) throws Exception {
	throwUnhandledCommandException(cmd);
	return true;
    }

    public boolean playerCommand(Player sender, CommandMetadata cmd, String label, String[] args) throws Exception {
	throwUnhandledCommandException(cmd);
	return true;
    }

    public boolean remotePonyCommand(RemotePony sender, CommandMetadata cmd, String label, String[] args) throws Exception {
	sendMessage(sender, "Sorry! that command hasn't been implemented yet!");
	return true;
    }

    public boolean consoleCommand(ConsoleCommandSender sender, CommandMetadata cmd, String label, String[] args) throws Exception {
	throwUnhandledCommandException(cmd);
	return true;
    }

    public void sendMessage(CommandSender sender, String message) {
	sendMessage(sender, message, pony);
    }

    public void sendMessage(CommandSender sender, String message, Ponies pony) {
	sender.sendMessage(pony.says() + message);
    }

    public void sendSyncMessage(final CommandSender sender, final String message) {
	Bukkit.getScheduler().scheduleSyncDelayedTask(Mane.getInstance(), new Runnable() {
	    public void run() {
		sendMessage(sender, message);
	    }
	});
    }

    public void sendSyncMessage(final CommandSender sender, final String message, final Ponies pony) {
	Bukkit.getScheduler().scheduleSyncDelayedTask(Mane.getInstance(), new Runnable() {
	    public void run() {
		sendMessage(sender, message, pony);
	    }
	});
    }

    /* methods for getting players */
    private void tooManyMatches(ArrayList<String> playerNames, CommandSender requester) {
	String message = pony.says() + "There were too many matches!!";
	for (String string : playerNames) {
	    message += string + ChatColor.LIGHT_PURPLE + " ";
	}
	requester.sendMessage(message);
    }

    /**
     * Player get methods
     */
    protected Player getOnlinePlayer(String pName, CommandSender requester) {
	return getOnlinePlayer(pName, requester, false);
    }

    protected Player getOnlinePlayer(String pName, CommandSender requester, boolean canNull) {
	Pony senderPony = null;
	if (requester instanceof Player) {
	    senderPony = Ponyville.getPony((Player) requester);
	}
	pName = pName.toLowerCase();
	ArrayList<String> players = new ArrayList<String>();
	for (Player player : Bukkit.getOnlinePlayers()) {
	    String playername = player.getName().toLowerCase();
	    String playerDname = ChatColor.stripColor(player.getDisplayName()).toLowerCase();
	    boolean isHidden = false;
	    if (senderPony != null && InvisibilityHandler.handler.isHidden(player.getName())) {
		Pony pony = Ponyville.getPony(player);
		isHidden = !senderPony.canSeeInvisible(pony);
	    }
	    if ((playername.equals(pName) || playerDname.equals(pName)) && !isHidden) {
		return player;
	    } else {
		if (playername.contains(pName) || playerDname.contains(pName)) {
		    if (!isHidden) {
			players.add(player.getName());
		    }
		}
	    }
	}
	if (players.size() > 1) {
	    tooManyMatches(players, requester);
	    return null;
	} else if (players.size() < 1) {
	    if (!canNull) {
		sendMessage(requester, "I looked high and low, but I couldnt find that pony! :C");
	    }
	    return null;
	} else {
	    return Bukkit.getPlayerExact(players.get(0));
	}
    }

    protected String getOfflinePlayer(String pName, CommandSender requester) {
	pName = pName.toLowerCase();
	ArrayList<String> player = new ArrayList<String>();
	File dir = new File(Mane.getInstance().getDataFolder() + File.separator + "Players");
	if (!dir.isDirectory()) {
	    sendMessage(requester, "I looked high and low, but I couldnt find that pony! :C");
	    return null;
	}
	File[] files = dir.listFiles();
	for (File file : files) {
	    String fname = file.getName();
	    if (fname.toLowerCase().equals(pName)) {
		return fname;
	    }
	    if (fname.toLowerCase().contains(pName)) {
		player.add(fname);
	    }
	}
	if (player.size() == 0) {
	    sendMessage(requester, "I looked high and low, but I couldnt find that pony! :C");
	    return null;
	}
	if (player.size() > 1) {
	    tooManyMatches(player, requester);
	    return null;
	}
	return player.get(0);
    }

    protected OfflinePlayer getOnlineOfflinePlayer(String pName, CommandSender requester) {
	pName = pName.toLowerCase();
	Player p = getOnlinePlayer(pName, requester, true);
	if (p != null) {
	    return p;
	} else {
	    File dir = new File(Mane.getInstance().getDataFolder() + File.separator + "Players");
	    ArrayList<String> player = new ArrayList<String>();
	    if (!dir.isDirectory()) {
		sendMessage(requester, "I looked high and low, but I couldnt find that pony! :C");
		return null;
	    }
	    File[] files = dir.listFiles();
	    for (File file : files) {
		String name = file.getName().toLowerCase();
		if (name.contains(pName)) {
		    if (name.equals(pName)) {
			return Bukkit.getOfflinePlayer(file.getName());
		    } else {
			player.add(file.getName());
		    }
		}
	    }
	    if (player.size() > 1) {
		tooManyMatches(player, requester);
		return null;
	    } else if (player.size() < 1) {
		sendMessage(requester, cantFindPlayer);
		return null;
	    } else {
		OfflinePlayer op = Bukkit.getOfflinePlayer(player.get(0));
		if (op.isOnline()) {
		    op = new CraftOfflinePlayer((CraftServer) Bukkit.getServer(), player.get(0)) {
			public Player getPlayer() {
			    return null;
			}
		    };
		}
		return op;
	    }
	}
    }

    public static String getPlayerSilent(String pName) {
	pName = pName.toLowerCase();
	File dir = new File(Mane.getInstance().getDataFolder() + File.separator + "Players");
	ArrayList<String> player = new ArrayList<String>();
	if (!dir.isDirectory()) {
	    return null;
	}
	File[] files = dir.listFiles();
	for (File file : files) {
	    String name = file.getName().toLowerCase();
	    if (name.contains(pName)) {
		if (name.equals(pName)) {
		    return file.getName();
		} else {
		    player.add(file.getName());
		}
	    }
	}
	if (player.size() > 1) {
	    return "TOO_MANY_MATCHES";
	} else if (player.size() < 1) {
	    return null;
	}
	return player.get(0);
    }

    /**
     * Sends a message to all Players connected, including the console.
     * 
     * @param message
     *            Message to send.
     */
    protected void broadcastMessage(String message) {
	for (Player p : Bukkit.getOnlinePlayers()) {
	    p.sendMessage(message);
	}
	console.sendMessage(message);
	// RemotePonyAdmin.rpa.message(message); TODO send RPA message
    }

    protected void broadcastMessage(String message, PonyPermission permission) {
	String perm = permission.getPermString();
	for (Player player : Bukkit.getOnlinePlayers()) {
	    if (player.hasPermission(perm)) {
		player.sendMessage(message);
	    }
	}
	console.sendMessage(message);
	// TODO RPA
    }

    public void saveConfig() {
	try {
	    config.save(new File(Mane.getInstance().getDataFolder(), configName));
	} catch (Exception e) {
	    e.printStackTrace();
	}
    }

    public boolean loadConfig(String name) {
	File file = new File(Mane.getInstance().getDataFolder(), name);
	boolean created = false;
	if (!file.exists()) {
	    try {
		file.createNewFile();
	    } catch (IOException e) {}
	    created = true;
	}
	config = YamlConfiguration.loadConfiguration(file);
	configName = name;
	return created;
    }

    public FileConfiguration getConfig() {
	return config;
    }

    public void throwUnhandledCommandException(CommandMetadata cmd) throws UnhandledCommandException {
	throw new UnhandledCommandException("Command not handled: " + cmd.getCommandName());
    }

    public boolean checkArgs(Player player, String[] args, int minCount) {
	boolean enough = args.length >= minCount;
	if (!enough) {
	    sendMessage(player, notEnoughArgs);
	}
	return enough;
    }

    public void tabComplete(PlayerChatTabCompleteEvent event) {}

    public void deactivate() {}
}
