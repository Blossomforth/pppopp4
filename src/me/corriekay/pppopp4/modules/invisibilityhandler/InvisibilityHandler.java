package me.corriekay.pppopp4.modules.invisibilityhandler;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.JoinEvent;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Creature;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityTargetEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;

public class InvisibilityHandler extends PSCmdExe {

    private final List<String> invisiblePlayers = new ArrayList<String>();
    private final List<String> noPickup = new ArrayList<String>();
    private final List<String> pickupFlagAutoModified = new ArrayList<String>();
    public static InvisibilityHandler handler;

    public InvisibilityHandler() {
	super("InvisibilityHandler", Ponies.PinkiePie, "hide", "nopickup", "setinvislevel");
	handler = this;
	init();
    }

    private void init() {
	for (Player player : Bukkit.getOnlinePlayers()) {
	    calculateInvisibility(player, false);
	}
    }

    /*
     * Bookmark Getters and Checkers
     */
    public boolean isPickingUp(String player) {
	if (noPickup.contains(player)) {
	    return false;
	} else
	    return true;
    }

    public void togglePickup(Player player) {
	pickupFlagAutoModified.remove(player.getName());
	if (isPickingUp(player.getName())) {
	    pickupOff(player);
	} else
	    pickupOn(player);
    }

    private void pickupOff(Player player) {
	noPickup.add(player.getName());
	sendMessage(player, "Disabling pickup!");
    }

    private void pickupOn(Player player) {
	noPickup.remove(player.getName());
	sendMessage(player, "Enabling pickup!");
    }

    public boolean isHidden(String player) {
	if (invisiblePlayers.contains(player)) {
	    return true;
	} else
	    return false;
    }

    /*
     * Bookmark Command
     */
    public boolean consoleCommand(ConsoleCommandSender sender, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case setinvislevel: {
	    String target = getPlayerSilent(args[0]);
	    if (target == null) {
		return true;
	    }
	    int level;
	    try {
		level = Integer.parseInt(args[1]);
	    } catch (NumberFormatException e) {
		sendMessage(sender, "Thats not a number!");
		return true;
	    }
	    Pony pony = Ponyville.getPony(target);
	    pony.setInvisibleLevel(level);
	    pony.save();
	    sendMessage(sender, "Youve set " + pony.getNickname() + ChatColor.LIGHT_PURPLE + "'s invisibility level to " + level + "!");
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case hide: {
	    if (label.equals("fakehide")) {
		toggleInvisibility(player, false, true);
	    } else {
		toggleInvisibility(player, true, true);
	    }
	    return true;
	}
	case nopickup: {
	    togglePickup(player);
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    public void toggleInvisibility(Player player, boolean silent, boolean notify) {
	if (isHidden(player.getName())) {
	    turnOff(player, silent, notify);
	} else {
	    turnOn(player, silent, notify);
	}
    }

    /*
     * Bookmark Listeners
     */

    @PonyEvent
    public void onTarget(EntityTargetEvent event) {
	if (event.getTarget() instanceof Player) {
	    if (isHidden(((Player) event.getTarget()).getName())) {
		event.setCancelled(true);
	    }
	}
    }

    @PonyEvent
    public void onJoin(JoinEvent event) {
	if (event.isJoining()) {
	    Player player = event.getPlayer();
	    Pony pony = event.getPony();
	    if (pony.isInvisible()) {
		turnOn(player, true, true);
		event.setCancelled(true);
		for (Player player2 : Bukkit.getOnlinePlayers()) {
		    Pony pony2 = Ponyville.getPony(player2);
		    if (pony2.canSeeInvisible(pony)) {
			player2.sendMessage(ChatColor.DARK_GRAY + player.getDisplayName() + ChatColor.DARK_GRAY + " has logged in silently!");
			// TODO RPA MESSAGE HERE
		    }
		}
	    } else {
		turnOff(player, true, true);
	    }
	    for (Player player2 : Bukkit.getOnlinePlayers()) {
		Pony pony2 = Ponyville.getPony(player2);
		if (!isHidden(player2.getName())) {
		    player.showPlayer(player2);
		} else if (!pony.canSeeInvisible(pony2)) {
		    player.hidePlayer(player2);
		}
	    }
	}
    }

    @PonyEvent
    public void onQuit(QuitEvent event) {
	Player player = event.getPlayer();
	if (invisiblePlayers.contains(player.getName()) && event.isQuitting()) {
	    event.setCancelled(true);
	    invisiblePlayers.remove(player.getName());
	    noPickup.remove(player.getName());
	    pickupFlagAutoModified.remove(player.getName());
	}
    }

    @PonyEvent
    public void onPickup(PlayerPickupItemEvent event) {
	if (noPickup.contains(event.getPlayer().getName())) {
	    event.setCancelled(true);
	}
    }

    /*
     * Bookmark Utils
     */
    private void calculateInvisibility(Player player, boolean notify) {
	Pony pony = Ponyville.getPony(player);
	if (pony.isInvisible()) {
	    turnOn(player, true, notify);
	} else {
	    turnOff(player, true, notify);
	}
    }

    private void turnOn(Player player, boolean silent, boolean notify) {
	invisiblePlayers.add(player.getName());
	Pony pony = Ponyville.getPony(player);
	if (notify) {
	    sendMessage(player, "Shhh!~ You're hidden now!");
	}
	if (isPickingUp(player.getName())) {
	    pickupFlagAutoModified.add(player.getName());
	    pickupOff(player);
	}
	for (Player player2 : Bukkit.getOnlinePlayers()) {
	    if (player2.equals(player)) {
		continue;
	    }
	    Pony pony2 = Ponyville.getPony(player2);
	    if (!pony2.canSeeInvisible(pony)) {
		player2.hidePlayer(player);
	    }
	}
	QuitEvent qe = new QuitEvent(player, pony, false);
	qe.setCancelled(silent);
	Bukkit.getPluginManager().callEvent(qe);
	if (!qe.isCancelled()) {
	    for (Player player2 : Bukkit.getOnlinePlayers()) {
		player2.sendMessage(qe.getMsg());
	    }
	}
	List<Entity> nearby = player.getNearbyEntities(100, 100, 100);
	for (Entity entity : nearby) {
	    if (entity instanceof Creature) {
		Creature creature = (Creature) entity;
		Entity entity2 = creature.getTarget();
		if (entity2 instanceof Player) {
		    Player player2 = (Player) entity2;
		    if (player.equals(player2)) {
			creature.setTarget(null);
		    }
		}
	    }
	}
	pony.setInvisible(true);
	pony.save();
    }

    private void turnOff(Player player, boolean silent, boolean notify) {
	invisiblePlayers.remove(player.getName());
	Pony pony = Ponyville.getPony(player);
	if (pickupFlagAutoModified.contains(player.getName())) {
	    pickupFlagAutoModified.remove(player.getName());
	    pickupOn(player);
	}
	invisiblePlayers.remove(player.getName());
	if (player.hasPermission(CommandMetadata.hide.getPermission())) {
	    if (notify) {
		sendMessage(player, "You're now visible!");
	    }
	}
	for (Player player2 : Bukkit.getOnlinePlayers()) {
	    player2.showPlayer(player);
	}
	JoinEvent je = new JoinEvent(player, pony, false);
	je.setCancelled(silent);
	Bukkit.getPluginManager().callEvent(je);
	if (!je.isCancelled()) {
	    for (Player player2 : Bukkit.getOnlinePlayers()) {
		player2.sendMessage(je.getMsg());
	    }
	}
	pony.setInvisible(false);
	pony.save();
    }
}
