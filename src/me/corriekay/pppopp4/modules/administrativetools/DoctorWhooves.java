package me.corriekay.pppopp4.modules.administrativetools;

import java.util.HashMap;
import java.util.Map;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.equestria.PonyWorld;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.modules.warp.WarpHandler;
import me.corriekay.pppopp4.utilities.IOP;
import me.corriekay.pppopp4.utilities.PonyLogger;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.OfflinePlayer;
import org.bukkit.conversations.*;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class DoctorWhooves extends PSCmdExe {

    private final RollbackCommandFactory tardis = new RollbackCommandFactory();

    public DoctorWhooves() {
	super("DoctorWhooves", Ponies.DoctorWhooves, "rollback");
    }

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) {
	OfflinePlayer target = getOnlineOfflinePlayer(args[0], player);
	if (target == null) {
	    return true;
	}
	target = Bukkit.getOfflinePlayer(target.getName());
	tardis.startRollbackFactory(player, target);
	return true;
    }

    public class RollbackCommandFactory extends ConversationFactory {
	public RollbackCommandFactory() {
	    super(Mane.getInstance());
	}

	private void startRollbackFactory(Player player, OfflinePlayer target) {
	    Map<Object, Object> map = new HashMap<Object, Object>();
	    map.put("opony", player);
	    map.put("target", target); // OfflinePlayer
	    map.put("since", 7); // since in days
	    map.put("area", 100); // area in number
	    map.put("sel", false); // using selection?
	    if (target.isOnline()) {
		map.put("player", target.getPlayer());
	    }
	    Conversation conv = withFirstPrompt(new StartPrompt()).withEscapeSequence("/exit").withLocalEcho(false).withModality(false).withInitialSessionData(map).buildConversation(player);
	    conv.begin();
	}

	public class StartPrompt extends MessagePrompt {
	    public String getPromptText(ConversationContext arg0) {
		return pony.says() + "Starting the rollback function...";
	    }

	    @Override
	    protected Prompt getNextPrompt(ConversationContext arg0) {
		return new FullrollbackPrompt();
	    }
	}

	public class FullrollbackPrompt extends BooleanPrompt {

	    @Override
	    public String getPromptText(ConversationContext context) {
		return pony.says() + "Would you like to wipe their gameplay information? (doing so will delete their inventories, ender chests, remote chests, any and all warps (private, home, back, etc) reset their health, remove all exp, and set their location at the server spawn. Yes/No?";
	    }

	    @Override
	    protected Prompt acceptValidatedInput(final ConversationContext arg0, boolean arg1) {
		if (arg1) {
		    Runnable r = new Runnable() {
			public void run() {
			    Pony targetpony = Ponyville.getPony((OfflinePlayer) arg0.getSessionData("target"));
			    final Player targetplayer = ((OfflinePlayer) arg0.getSessionData("target")).getPlayer();
			    if (targetplayer != null) {
				Bukkit.getScheduler().runTask(Mane.getInstance(), new Runnable() {
				    public void run() {
					targetplayer.kickPlayer("Your account has been fully rolled back. Because of this, you are required to rejoin the server.");
				    }
				});
			    }
			    IOP op = new IOP(targetpony.getName());
			    for (PonyWorld world : PonyWorld.getPonyWorlds()) {
				if (world.isOverworld()) {
				    if (world.enabledEnderchest()) {
					targetpony.getEnderChest(world).clear();
					targetpony.saveEnderChest(world);
				    }
				    if (world.enabledRemotechest()) {
					targetpony.getRemoteChest(world).clear();
					targetpony.saveRemoteChest(world);
				    }
				    PlayerInventory pi = targetpony.getInventory(world);
				    pi.clear();
				    pi.setArmorContents(new ItemStack[4]);
				    targetpony.setInventory(pi, world);
				}
			    }
			    op.setLocation(WarpHandler.handler.spawn);
			    op.setGameMode(GameMode.SURVIVAL);
			    op.savePlayerData();
			    targetpony.setGodMode(false);
			    targetpony.setBackWarp(null);
			    targetpony.setHomeWarp(null);
			    targetpony.setOfflineWarp(null);
			    for (String warp : targetpony.getAllNamedWarps().keySet()) {
				targetpony.removeNamedWarp(warp);
			    }
			    targetpony.wipeWorldStats();
			    targetpony.save();
			    Player opony = (Player) arg0.getSessionData("opony");
			    PonyLogger.logAdmin(opony.getName() + " wiped " + targetpony.getName() + "'s game data");
			}
		    };
		    Bukkit.getScheduler().runTask(Mane.getInstance(), r);
		}
		return new LWCWipe(arg1);
	    }
	}

	public class LWCWipe extends BooleanPrompt {
	    private final boolean bool;

	    public LWCWipe(Boolean b) {
		bool = b;
	    }

	    @Override
	    public String getPromptText(ConversationContext context) {
		String msg = pony.says() + "Do you wish to wipe this users LWC locks?";
		if (bool) {
		    msg = pony.says() + "Users game data has been wiped.\n" + msg;
		} else {
		    msg = pony.says() + "Users game data has not been wiped.\n" + msg;
		}
		return msg;
	    }

	    @Override
	    protected Prompt acceptValidatedInput(ConversationContext context, boolean input) {
		if (input) {
		    final Player player = (Player) context.getSessionData("opony");
		    final OfflinePlayer target = (OfflinePlayer) context.getSessionData("target");
		    Bukkit.getScheduler().runTask(Mane.getInstance(), new Runnable() {
			public void run() {
			    player.performCommand("/lwc admin purge " + target.getName());
			}
		    });
		}
		return new LogblockPrompt();
	    }
	}

	public class LogblockPrompt extends BooleanPrompt {

	    @Override
	    public String getPromptText(ConversationContext arg0) {

		return pony.says() + "Do you wish to set up a logblock rollback?";
	    }

	    @Override
	    protected Prompt acceptValidatedInput(ConversationContext arg0, boolean arg1) {
		if (arg1) {
		    return new LogblockDayRollbackPrompt();
		} else {
		    return new EndPrompt();
		}
	    }
	}

	public class LogblockDayRollbackPrompt extends NumericPrompt {

	    @Override
	    public String getPromptText(ConversationContext context) {
		return pony.says() + "How many days should we roll this player back?";
	    }

	    @Override
	    protected Prompt acceptValidatedInput(ConversationContext arg0, Number arg1) {
		arg0.setSessionData("since", arg1.intValue());
		return new LogblockAreaRollbackPrompt();
	    }

	}

	public class LogblockAreaRollbackPrompt extends StringPrompt {

	    @Override
	    public Prompt acceptInput(ConversationContext context, String input) {
		if (input.equals("sel")) {
		    context.setSessionData("sel", true);
		    return new ConfirmLogblockCommandPrompt();
		}
		try {
		    int area = Integer.parseInt(input);
		    context.setSessionData("area", area);
		    return new ConfirmLogblockCommandPrompt();
		} catch (NumberFormatException e) {
		    return this;
		}
	    }

	    @Override
	    public String getPromptText(ConversationContext context) {
		return pony.says() + "Please indicate the radius of the rollback area. If you've got a world edit selection and wish to use that, type \"sel\".";
	    }
	}

	public class ConfirmLogblockCommandPrompt extends BooleanPrompt {

	    @Override
	    public String getPromptText(ConversationContext context) {
		String msg = pony.says() + "Please review this carefully! This is your logblock rollback command:\n";
		msg += parseContextForLogblockCommand(context);
		msg += "\nAre you sure you wish to do this?";
		return msg;
	    }

	    @Override
	    protected Prompt acceptValidatedInput(final ConversationContext context, boolean input) {
		if (input) {
		    Bukkit.getScheduler().runTask(Mane.getInstance(), new Runnable() {
			public void run() {
			    ((Player) context.getSessionData("opony")).performCommand(parseContextForLogblockCommand(context));
			}
		    });
		}
		return new EndPrompt();
	    }

	}

	public class EndPrompt extends MessagePrompt {

	    @Override
	    public String getPromptText(ConversationContext context) {
		return pony.says() + "Rollback process completed! If you've chosen to execute a logblock rollback, please wait for the system to gather the rollback data.";
	    }

	    @Override
	    protected Prompt getNextPrompt(ConversationContext arg0) {
		return null;
	    }
	}
    }

    private String parseContextForLogblockCommand(ConversationContext context) {
	return "/lb rollback player " + ((OfflinePlayer) context.getSessionData("target")).getName() + " since " + context.getSessionData("since") + "d " + ((Boolean) context.getSessionData("sel") ? "sel" : ("area " + context.getSessionData("area")));
    }
}
