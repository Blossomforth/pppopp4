package me.corriekay.pppopp4.modules.administrativetools;

import java.io.File;
import java.util.*;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.modules.remoteponyadmin.RemotePonyAdmin;
import me.corriekay.pppopp4.utilities.Utils;
import net.minecraft.server.v1_7_R1.NBTTagCompound;

import org.bukkit.*;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.*;
import org.bukkit.event.player.PlayerInteractEntityEvent;

public class AdminTools extends PSCmdExe {

    private final int spawncount = 50;

    private final HashMap<String, Location> roundup = new HashMap<String, Location>();

    public AdminTools() {
	super("AdminTools", Ponies.PinkiePie, "playerinformation", "rcv", "runcustomtask", "runalttask", "extinguish", "spawnmob", "killmob", "heal", "sudo", "roundup", "matchip");
    }

    public boolean optionalCommand(final CommandSender sender, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case sudo: {
	    ArrayList<Player> targets = new ArrayList<Player>();
	    if (args[0].equals("all")) {
		for (Player players : Bukkit.getOnlinePlayers()) {
		    targets.add(players);
		}
	    } else {
		Player target = getOnlinePlayer(args[0], sender);
		if (target == null) {
		    return true;
		}
		targets.add(target);
	    }
	    String cmdString = "/" + Utils.joinStringArray(args, " ", 1);
	    for (Player player : targets) {
		player.chat(cmdString);
	    }
	    return true;
	}
	case playerinformation: {
	    OfflinePlayer target = getOnlineOfflinePlayer(args[0], sender);
	    if (target == null) {
		return true;
	    }
	    Pony pony = Ponyville.getPony(target);
	    sender.sendMessage(ChatColor.DARK_RED + "===== " + ChatColor.WHITE + pony.getName() + "'s info" + ChatColor.DARK_RED + " =====");
	    sender.sendMessage(ChatColor.DARK_RED + "Nickname: " + ChatColor.WHITE + pony.getNickname());
	    ArrayList<String> nicks = pony.getNickHistory();
	    if (nicks.size() > 0) {
		sender.sendMessage(ChatColor.DARK_RED + "Nickname History:");
		for (int i = nicks.size() - 1; i > nicks.size() - 6; i--) {
		    sender.sendMessage("- " + nicks.get(i));
		    if (i == 0) {
			break;
		    }
		}
	    }
	    sender.sendMessage(ChatColor.DARK_RED + "First seen: " + ChatColor.WHITE + pony.getFirstLogon());
	    sender.sendMessage(ChatColor.DARK_RED + "Last seen: " + ChatColor.WHITE + pony.getLastLogout());
	    sender.sendMessage(ChatColor.DARK_RED + "Last login: " + ChatColor.WHITE + pony.getLastLogon());
	    sender.sendMessage(ChatColor.DARK_RED + "Online: " + ChatColor.WHITE + target.isOnline());
	    sender.sendMessage(ChatColor.DARK_RED + "Ip Addresses:");
	    ArrayList<String> ips = pony.getIps();
	    for (int i = ips.size() - 1; i > ips.size() - 6; i--) {
		sender.sendMessage(ChatColor.DARK_RED + "- " + ips.get(i));
		if (i == 0) {
		    break;
		}
	    }
	    sender.sendMessage(ChatColor.DARK_RED + "Muted: " + ChatColor.WHITE + pony.isMuted());
	    sender.sendMessage(ChatColor.DARK_RED + "God: " + ChatColor.WHITE + pony.isGodMode());
	    sender.sendMessage(ChatColor.DARK_RED + "Chatting in: " + ChatColor.WHITE + pony.getChatChannel());
	    sender.sendMessage(ChatColor.DARK_RED + "Listening channels:");
	    for (String chan : pony.getListeningChannels()) {
		sender.sendMessage("- " + chan);
	    }
	    sender.sendMessage(ChatColor.DARK_RED + "PonySpy: " + ChatColor.WHITE + pony.isPonySpy());
	    sender.sendMessage(ChatColor.DARK_RED + "PonyManager group: " + ChatColor.WHITE + pony.getGroup());
	    sender.sendMessage(ChatColor.DARK_RED + "Banned: " + ChatColor.WHITE + pony.isBanned());
	    if (pony.isBanned()) {
		sender.sendMessage(ChatColor.DARK_RED + "Ban Type: " + ChatColor.WHITE + (pony.getBanType() == 1 ? "Tempban" : "Permaban"));
		if (pony.getBanType() == 1) {
		    boolean isExpired = pony.getUnbanTime() < System.currentTimeMillis();
		    sender.sendMessage(ChatColor.DARK_RED + "Tempban Expired: " + ChatColor.WHITE + isExpired);
		}
	    }
	    sender.sendMessage(ChatColor.DARK_RED + "====================");
	    return true;
	}
	case runcustomtask: {
	    Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), new Runnable() {
		@SuppressWarnings("unchecked")
		public void run() {
		    File dir = new File(Mane.getInstance().getDataFolder() + File.separator + "Players");
		    int errors = 0;
		    int total = dir.listFiles().length + 1;
		    int current = 1;
		    int percentage = 0;
		    for (File file : dir.listFiles()) {
			int thisPercentage = (int) Math.floor((current / total) * 100);
			if (thisPercentage != percentage) {
			    System.out.println("Files parsed: " + thisPercentage + "%");
			    percentage = thisPercentage;
			}
			current++;
			try {
			    String name = file.getName();
			    Pony pony = Ponyville.getPony(name);
			    NBTTagCompound c = pony.c.getCompound("warps");
			    ArrayList<String> removeme = new ArrayList<String>();
			    for (NBTTagCompound s : (Collection<NBTTagCompound>) c.c()) {
				if (!s.getName().equals("other")) {
				    String worldname = s.getString("world");
				    if (worldname.equals("world_the_end") || worldname.equals("world_nether")) {
					removeme.add(s.getName());

				    }
				}
			    }
			    for (String remove : removeme) {
				pony.removeNamedWarp(remove);
			    }
			    pony.save();
			    // CODE
			} catch (Exception e) {
			    errors++;
			    Mane.getInstance().getLogger().severe("Error on parsing config: " + file.getName());
			    e.printStackTrace();
			}
		    }
		    sendMessage(sender, "Configs converted! " + errors + " errors!");
		}
	    });
	    return true;
	}
	case runalttask: {
	    Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), new Runnable() {
		public void run() {
		    File dir = new File(Mane.getInstance().getDataFolder() + File.separator + "Players");
		    int errors = 0;
		    int total = dir.listFiles().length + 1;
		    int current = 1;
		    int percentage = 0;
		    for (File file : dir.listFiles()) {
			int thisPercentage = (int) Math.floor((current / total) * 100);
			if (thisPercentage != percentage) {
			    System.out.println("Files parsed: " + thisPercentage + "%");
			    percentage = thisPercentage;
			}
			current++;
			try {
			    String name = file.getName();
			    Pony pony = Ponyville.getPony(name);
			    // CODE
			    if (pony.getGroup().equals("commandless")) {
				pony.setGroup("filly");
				pony.addNote("CLEANUP: Pony added to group filly after commandless group was decommissioned.", "CONSOLE", null);
			    }
			    // CODE
			    pony.save();
			} catch (Exception e) {
			    errors++;
			    Mane.getInstance().getLogger().severe("Error on parsing config: " + file.getName());
			    System.out.println(e.getMessage());
			}
		    }
		    sendMessage(sender, "Configs converted! " + errors + " errors!");
		}
	    });
	    return true;
	}
	case matchip: {
	    final String ip = args[0];
	    Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), new Runnable() {
		public void run() {
		    ArrayList<String> playersFound = new ArrayList<String>();
		    for (String ponyname : Ponyville.getAllPonyNames()) {
			try {
			    Pony pony = Ponyville.getPony(ponyname);
			    if (pony.getIps().contains(ip)) {
				playersFound.add(ponyname);
			    }
			} catch (Exception e) {
			    continue;
			}
		    }
		    String sendme = "Logged ip's match these players:\n" + Utils.joinStringArray(playersFound.toArray(new String[playersFound.size()]), ", ");
		    sendSyncMessage(sender, sendme);
		}
	    });
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    @SuppressWarnings("deprecation")
    public boolean playerCommand(final Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case rcv: {
	    String MESSAGE = "";
	    for (String WORD : args) {
		MESSAGE += WORD.toUpperCase() + " ";
	    }
	    MESSAGE = ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "[ROYALCANTERLOTVOICE](" + ChatColor.RESET + player.getDisplayName() + ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "): " + ChatColor.BLUE + "" + ChatColor.BOLD + MESSAGE;
	    Bukkit.broadcastMessage(MESSAGE);
	    RemotePonyAdmin.rpa.sendMessageAll(MESSAGE);
	    return true;
	}
	case spawnmob: {
	    int number = 1;
	    EntityType et;
	    String mtString = args[0].toLowerCase();
	    if (mtString.equals("zombie")) {
		et = EntityType.ZOMBIE;
	    } else if (mtString.equals("skeleton")) {
		et = EntityType.SKELETON;
	    } else if (mtString.equals("spider")) {
		et = EntityType.SPIDER;
	    } else if (mtString.equals("creeper") || mtString.equals("chargedcreeper")) {
		et = EntityType.CREEPER;
	    } else if (mtString.equals("cavespider")) {
		et = EntityType.CAVE_SPIDER;
	    } else if (mtString.equals("sheep")) {
		et = EntityType.SHEEP;
	    } else if (mtString.equals("pig")) {
		et = EntityType.PIG;
	    } else if (mtString.equals("cow")) {
		et = EntityType.COW;
	    } else if (mtString.equals("wolf")) {
		et = EntityType.WOLF;
	    } else if (mtString.equals("ocelot")) {
		et = EntityType.OCELOT;
	    } else if (mtString.equals("chicken")) {
		et = EntityType.CHICKEN;
	    } else if (mtString.equals("squid")) {
		et = EntityType.SQUID;
	    } else {
		sendMessage(player, "Uh uh! thats not a spawnable mob!");
		return true;
	    }
	    Location loc;
	    if (label.equals("spawnmobtarget")) {
		if (args.length < 2) {
		    sendMessage(player, notEnoughArgs);
		    return true;
		} else {
		    Player p = getOnlinePlayer(args[1], player);
		    if (p == null) {
			return true;
		    } else {
			loc = p.getLocation();
		    }
		    if (args.length >= 3) {
			try {
			    number = Integer.parseInt(args[2]);
			} catch (NumberFormatException e) {
			    sendMessage(player, "Thats not a number!");
			    return true;
			}
		    }
		}
	    } else {
		if (!(player instanceof Player)) {
		    sendMessage(player, notPlayer);
		    return true;
		} else {
		    loc = ((Player) player).getTargetBlock(null, 100).getLocation();
		    loc.setY(loc.getY() + 1);
		    if (args.length >= 2) {
			try {
			    number = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
			    sendMessage(player, "Thats not a number!");
			    return true;
			}
		    }
		}
	    }
	    if (number > spawncount)
		number = spawncount;
	    for (int i = 0; i < number; i++) {
		Entity e = loc.getWorld().spawnEntity(loc, et);
		if (mtString.equals("chargedcreeper")) {
		    Creeper c = (Creeper) e;
		    c.setPowered(true);
		}
	    }
	    return true;
	}
	case extinguish: {
	    int x = 25;
	    if (args.length > 0) {
		try {
		    x = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
		    sendMessage(player, "Thats not a number! defaulting to 25 radius!");
		    x = 25;
		}
	    }
	    if (x > 100) {
		x = 100;
	    }
	    HashSet<Block> blocks = Utils.getBlocks(x, x, x, player.getLocation());
	    for (Block block : blocks) {
		if (block.getType() == Material.FIRE) {
		    block.setType(Material.AIR);
		}
	    }
	    return true;
	}
	case killmob: {
	    int radius = 50;
	    if (args.length > 1) {
		try {
		    radius = Integer.parseInt(args[1]);
		} catch (NumberFormatException e) {
		    sendMessage(player, "Thats not a number!");
		    return true;
		}
	    }
	    if (radius > 100) {
		radius = 100;
	    }
	    if (args[0].equals("all")) {
		for (Entity ent : player.getNearbyEntities(radius, radius, radius)) {
		    if (!(ent instanceof Player)) {
			ent.remove();
		    }
		}
		sendMessage(player, "All entities removed!");
		return true;
	    } else {
		EntityType et;
		try {
		    et = EntityType.valueOf(args[0].toUpperCase());
		} catch (Exception e) {
		    sendMessage(player, "Invalid mob type silly!");
		    return true;
		}
		for (Entity ent : player.getNearbyEntities(radius, radius, radius)) {
		    if (ent.getType() == et) {
			ent.remove();
		    }
		}
		sendMessage(player, "Entities of type " + args[0] + " removed!");
		return true;
	    }
	}
	case heal: {
	    if (args.length <= 0) {
		player.setHealth(20);
		player.setFoodLevel(20);
		sendMessage(player, "You have healed yourself!");
		return true;
	    }
	    Player target = getOnlinePlayer(args[0], console);
	    if (target != null) {
		target.setHealth(20);
		target.setFoodLevel(20);
		sendMessage(target, "You have been healed!");
		sendMessage(player, "You have healed " + target.getDisplayName() + ChatColor.LIGHT_PURPLE + "!");
	    }
	    return true;
	}
	case roundup: {
	    if (roundup.containsKey(player.getName())) {
		sendMessage(player, "Roundup finished!", Ponies.Applejack);
		roundup.remove(player.getName());
	    } else {
		sendMessage(player, "Let's get this rodeo underway! Roundup started!", Ponies.Applejack);
		roundup.put(player.getName(), player.getLocation());
	    }
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    public boolean consoleCommand(ConsoleCommandSender console, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case rcv: {
	    String MESSAGE = "";
	    for (String WORD : args) {
		MESSAGE += WORD.toUpperCase() + " ";
	    }
	    MESSAGE = ChatColor.DARK_BLUE + "" + ChatColor.BOLD + "[ROYALCANTERLOTVOICE](SERVER): " + ChatColor.BLUE + MESSAGE.trim();
	    Bukkit.broadcastMessage(MESSAGE);
	    RemotePonyAdmin.rpa.sendMessageAll(MESSAGE);
	    return true;
	}
	case heal: {
	    if (args.length < 1) {
		sendMessage(console, notEnoughArgs);
		return true;
	    }
	    Player target = getOnlinePlayer(args[0], console);
	    if (target != null) {
		target.setHealth(20);
		target.setFoodLevel(20);
		sendMessage(target, "You have been healed!");
		sendMessage(console, "You have healed " + target.getDisplayName() + ChatColor.LIGHT_PURPLE + "!");
	    }
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    @PonyEvent
    public void interactEvent(PlayerInteractEntityEvent event) {
	if (roundup.containsKey(event.getPlayer().getName())) {
	    if (event.getRightClicked() instanceof LivingEntity && !(event.getRightClicked() instanceof Player)) {
		event.getRightClicked().teleport(roundup.get(event.getPlayer().getName()));
	    }
	}
    }
}
