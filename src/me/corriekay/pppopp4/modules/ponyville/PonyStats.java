package me.corriekay.pppopp4.modules.ponyville;

import java.util.ArrayList;

import net.minecraft.server.v1_7_R1.NBTTagCompound;
import net.minecraft.server.v1_7_R1.NBTTagList;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;

public class PonyStats {
    private final NBTTagCompound compound = new NBTTagCompound();

    @SuppressWarnings("deprecation")
    public PonyStats(Player p) {
	// potion effects
	NBTTagList effectList = new NBTTagList();
	ArrayList<PotionEffect> effects = (ArrayList<PotionEffect>) p.getActivePotionEffects();
	for (PotionEffect pe : effects) {
	    NBTTagCompound eCompound = new NBTTagCompound();
	    eCompound.setByte("Amplifier", (byte) (pe.getAmplifier()));
	    eCompound.setByte("Id", (byte) (pe.getType().getId()));
	    eCompound.setInt("Duration", (int) (pe.getDuration()));
	    effectList.add(eCompound);
	}
	compound.set("potioneffects", effectList);

	// hunger, health, saturation, etc.
	float exhaustion, exp, saturation;
	int foodlvl, level, totalxp;
	double health;
	exhaustion = p.getExhaustion();
	exp = p.getExp();
	saturation = p.getSaturation();
	foodlvl = p.getFoodLevel();
	health = p.getHealth();
	if (health < 0)
	    health = 0;
	level = p.getLevel();
	totalxp = p.getTotalExperience();
	compound.setFloat("exhaustion", exhaustion);
	compound.setFloat("exp", exp);
	compound.setFloat("saturation", saturation);
	compound.setInt("foodlvl", foodlvl);
	compound.setInt("health", (int) health);
	compound.setInt("level", level);
	compound.setInt("totalxp", totalxp);
    }

    public NBTTagCompound getCompound() {
	return compound;
    }
}
