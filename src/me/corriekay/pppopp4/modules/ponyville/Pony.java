package me.corriekay.pppopp4.modules.ponyville;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.modules.chat.ChannelHandler;
import me.corriekay.pppopp4.modules.classroom.MissCheerilee;
import me.corriekay.pppopp4.modules.equestria.PonyWorld;
import me.corriekay.pppopp4.modules.ponymanager.PonyPermission;
import me.corriekay.pppopp4.modules.warp.Warp;
import me.corriekay.pppopp4.utilities.PonyLogger;
import me.corriekay.pppopp4.utilities.Utils;
import net.minecraft.server.v1_7_R1.ItemStack;
import net.minecraft.server.v1_7_R1.NBTBase;
import net.minecraft.server.v1_7_R1.NBTCompressedStreamTools;
import net.minecraft.server.v1_7_R1.NBTTagByte;
import net.minecraft.server.v1_7_R1.NBTTagCompound;
import net.minecraft.server.v1_7_R1.NBTTagFloat;
import net.minecraft.server.v1_7_R1.NBTTagInt;
import net.minecraft.server.v1_7_R1.NBTTagList;
import net.minecraft.server.v1_7_R1.NBTTagLong;
import net.minecraft.server.v1_7_R1.NBTTagString;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.v1_7_R1.inventory.CraftInventoryPlayer;
import org.bukkit.craftbukkit.v1_7_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class Pony {

    private final File datFile;

    public final NBTTagCompound c;

    private HashMap<String, Inventory> remoteChests = new HashMap<String, Inventory>();

    private HashMap<String, Inventory> enderChests = new HashMap<String, Inventory>();

    private HashMap<Inventory, String> rcWorlds = new HashMap<Inventory, String>();

    private HashMap<Inventory, String> ecWorlds = new HashMap<Inventory, String>();

    protected Pony(Player pone) throws FileNotFoundException {
	this(pone.getName());
    }

    protected Pony(String pone) throws FileNotFoundException {
	datFile = new File(Mane.getInstance().getDataFolder() + File.separator + "Players", pone);
	c = NBTCompressedStreamTools.a(new FileInputStream(datFile));
	loadRemoteChest();
	loadEnderChest();
    }

    protected Pony(File file) throws Exception {
	datFile = file;
	c = NBTCompressedStreamTools.a(new FileInputStream(datFile));
	loadRemoteChest();
	loadEnderChest();
    }

    public PonyWorld getRCWorld(Inventory i) {
	return PonyWorld.getPonyWorld(rcWorlds.get(i));
    }

    public PonyWorld getECWorld(Inventory i) {
	return PonyWorld.getPonyWorld(ecWorlds.get(i));
    }

    /* getters */
    public OfflinePlayer getPlayer() {
	return Bukkit.getOfflinePlayer(getName());
    }

    public String getName() {
	return c.getString("name");
    }

    public boolean isMuted() {
	return c.getByte("muted") == 1;
    }

    public boolean isNsfwBlocked() {
	return c.getByte("nsfwblocked") == 1;
    }

    public boolean isNsfwAccepted() {
	return c.getByte("nsfwaccept") == 1;
    }

    public HashSet<String> getSilenced() {
	HashSet<String> silenced = new HashSet<String>();
	NBTTagList list = c.getList("silenced");
	for (int i = 0; i < list.size(); i++) {
	    silenced.add(((NBTTagString) list.get(i)).toString());
	}
	return silenced;
    }

    public boolean isGodMode() {
	return c.getByte("god") == 1;
    }

    public boolean isInvisible() {
	return c.getByte("invisible") == 1;
    }

    public int getInvisibleLevel() {
	return c.getInt("invisiblelevel");
    }

    public boolean canSeeInvisible(Pony otherPony) {
	int myLevel, theirLevel;
	myLevel = getInvisibleLevel();
	theirLevel = otherPony.getInvisibleLevel();
	return myLevel >= theirLevel;
    }

    public String getNickname() {
	return c.getString("nickname");
    }

    public ArrayList<String> getNickHistory() {
	ArrayList<String> nicks = new ArrayList<String>();
	NBTTagList list = c.getList("nickhistory");
	for (int i = 0; i < list.size(); i++) {
	    nicks.add(((NBTTagString) list.get(i)).toString());
	}
	return nicks;
    }

    public String getChatChannel() {
	return c.getString("chatchannel");
    }

    public ChatColor getChannelColor(String channel) {
	try {
	    ChatColor color = ChatColor.valueOf(c.getCompound("channelcolors").getString(channel));
	    return color;
	} catch (Exception e) {
	    return null;
	}
    }

    public HashSet<String> getListeningChannels() {
	HashSet<String> chans = new HashSet<String>();
	NBTTagList list = c.getList("listenchannels");
	for (int i = 0; i < list.size(); i++) {
	    chans.add(((NBTTagString) list.get(i)).toString());
	}
	return chans;
    }

    public boolean isPonySpy() {
	return c.getByte("ponyspy") == 1;
    }

    public String getGroup() {
	return c.getString("group");
    }

    public ArrayList<String> getPerms() {
	ArrayList<String> perms = new ArrayList<String>();
	NBTTagList list = c.getList("perms");
	for (int i = 0; i < list.size(); i++) {
	    perms.add(((NBTTagString) list.get(i)).toString());
	}
	return perms;
    }

    public ArrayList<String> getIps() {
	ArrayList<String> ips = new ArrayList<String>();
	NBTTagList list = c.getList("ipaddress");
	for (int i = 0; i < list.size(); i++) {
	    ips.add(((NBTTagString) list.get(i)).toString());
	}
	return ips;
    }

    public boolean getAdvancedXrayLogging() {
	return c.getBoolean("advancedXrayLogging");
    }

    public boolean hasTutorialIdFinished(int i) {
	return getCompletedTutorialIds().contains(i);
    }

    public String getHornLeft() {
	return getHorn().getString("left");
    }

    public String getHornRight() {
	return getHorn().getString("right");
    }

    public boolean getHornOn() {
	return getHorn().getByte("ison") == 1;
    }

    public org.bukkit.inventory.PlayerInventory getInventory(PonyWorld world) {
	PlayerInventory inv = new PlayerInventory(null);
	NBTTagList list = c.getCompound("inventories").getList(world.getOverworld().getName());
	if (list != null) {
	    inv.b(list);
	}
	return new CraftInventoryPlayer(inv);
    }

    @SuppressWarnings("deprecation")
    public void getWorldStats(Player p, PonyWorld world) {
	Pony pony = Ponyville.getPony(p);
	world = world.getOverworld();
	String worldname = world.getName();
	NBTTagCompound compound = pony.c.getCompound("worlds").getCompound(worldname);
	if (!pony.c.getCompound("worlds").hasKey(worldname)) {
	    for (PotionEffect e : p.getActivePotionEffects()) {
		p.removePotionEffect(e.getType());
	    }
	    p.setHealth(20);
	    p.setFoodLevel(20);
	    p.setLevel(0);
	    p.setTotalExperience(0);
	    p.setExhaustion(0);
	    return;
	}

	// set potion effects
	NBTTagList effects = compound.getList("potioneffects");
	ArrayList<PotionEffect> effectsList = new ArrayList<PotionEffect>();
	for (int i = 0; i < effects.size(); i++) {
	    NBTTagCompound effect = (NBTTagCompound) effects.get(i);
	    byte amp = effect.getByte("Amplifier");
	    byte id = effect.getByte("Id");
	    int time = effect.getInt("Duration");
	    effectsList.add(new PotionEffect(PotionEffectType.getById(id), time, amp));
	}
	for (PotionEffect pe : p.getActivePotionEffects()) {
	    p.removePotionEffect(pe.getType());
	}
	p.addPotionEffects(effectsList);

	float exhaustion, exp, saturation;
	int foodlvl, health, level, totalxp;
	exhaustion = compound.getFloat("exhaustion");
	exp = compound.getFloat("exp");
	saturation = compound.getFloat("saturation");
	foodlvl = compound.getInt("foodlvl");
	health = compound.getInt("health");
	level = compound.getInt("level");
	totalxp = compound.getInt("totalxp");
	p.setExhaustion(exhaustion);
	p.setExp(exp);
	p.setSaturation(saturation);
	p.setFoodLevel(foodlvl);
	p.setHealth(health);
	p.setLevel(level);
	p.setTotalExperience(totalxp);
    }

    public void loadRemoteChest() {
	NBTTagCompound rcCompound = c.getCompound("remotechest");
	List<String> worlds = new ArrayList<String>();
	for (PonyWorld world : PonyWorld.getPonyWorlds()) {
	    if (world.isOverworld() && world.enabledRemotechest()) {
		worlds.add(world.getName());
	    }
	}
	boolean save = false;
	for (String world : worlds) {
	    NBTTagList invlist = rcCompound.getList(world);
	    Inventory inv = Bukkit.createInventory(Bukkit.getPlayerExact(getName()), 54);
	    if (!rcCompound.hasKey(world)) {
		invlist = new NBTTagList(world);
		rcCompound.set(world, invlist);
		save = true;
	    } else {
		for (int i = 0; i < invlist.size(); i++) {
		    NBTTagCompound itemC = (NBTTagCompound) invlist.get(i);
		    int index = itemC.getInt("index");
		    ItemStack is = ItemStack.createStack(itemC);
		    CraftItemStack cis = CraftItemStack.asCraftMirror(is);
		    inv.setItem(index, cis);
		}
	    }
	    rcWorlds.put(inv, world);
	    remoteChests.put(world, inv);
	}
	if (save) {
	    save();
	}
    }

    public void loadEnderChest() {
	boolean save = false;
	NBTTagCompound ecCompound = c.getCompound("enderchest");
	List<String> worlds = new ArrayList<String>();
	for (PonyWorld world : PonyWorld.getPonyWorlds()) {
	    if (world.isOverworld() && world.enabledEnderchest()) {
		worlds.add(world.getName());
	    }
	}
	for (String world : worlds) {
	    Inventory inv = Bukkit.createInventory(Bukkit.getPlayerExact(getName()), 27);
	    NBTTagList invlist;
	    if (!ecCompound.hasKey(world)) {
		System.out.println("enderchest for " + world + " null, loading a brand new enderchest");
		invlist = new NBTTagList(world);
		ecCompound.set(world, invlist);
		save = true;
	    } else {
		invlist = ecCompound.getList(world);
		for (int i = 0; i < invlist.size(); i++) {
		    NBTTagCompound itemC = (NBTTagCompound) invlist.get(i);
		    int index = itemC.getInt("index");
		    ItemStack is = ItemStack.createStack(itemC);
		    CraftItemStack cis = CraftItemStack.asCraftMirror(is);
		    inv.setItem(index, cis);
		}
	    }
	    ecWorlds.put(inv, world);
	    enderChests.put(world, inv);
	}
	if (save) {
	    save();
	}

    }

    public Inventory getRemoteChest(PonyWorld w) {
	return remoteChests.get(w.getOverworld().getName());
    }

    public Inventory getEnderChest(PonyWorld w) {
	return enderChests.get(w.getOverworld().getName());
    }

    public String getEmoteName() {
	return getEmote().getString("name");
    }

    public String getEmoteSender() {
	return getEmote().getString("sender");
    }

    public String getEmoteReceiver() {
	return getEmote().getString("receiver");
    }

    public String getEmoteServer() {
	return getEmote().getString("server");
    }

    public boolean getEmotePrivate() {
	return getEmote().getByte("private") == 1;
    }

    public boolean getEmoteSilent() {
	return getEmote().getByte("silent") == 1;
    }

    public boolean getEmoteNsfw() {
	return getEmote().getByte("nsfw") == 1;
    }

    public boolean getEmoteIsBanned() {
	return getEmote().getByte("banned") == 1;
    }

    public String getFirstLogon() {
	return c.getString("firstlogon");
    }

    public String getLastLogon() {
	return c.getString("lastlogon");
    }

    public String getLastLogout() {
	return c.getString("lastlogout");
    }

    public Location getBackWarp() {
	return getWarp(getOtherWarps().getCompound("back"));
    }

    public Location getHomeWarp() {
	return getWarp(getOtherWarps().getCompound("home"));
    }

    public Location getOfflineWarp() {
	return getWarp(getOtherWarps().getCompound("offline"));
    }

    public Location getNamedWarp(String warp) {
	return getWarp(getWarps().getCompound(warp));
    }

    @SuppressWarnings("unchecked")
    public HashMap<String, Location> getAllNamedWarps() {
	HashMap<String, Location> locs = new HashMap<String, Location>();
	for (NBTTagCompound s : (Collection<NBTTagCompound>) getWarps().c()) {
	    String name = s.getName();
	    if (name.equals("other")) {
		continue;
	    }
	    locs.put(name, getWarp(s));
	}
	return locs;
    }

    @SuppressWarnings("unchecked")
    public HashMap<String, Warp> getAllNamedWarpsAsWarps() {
	HashMap<String, Warp> locs = new HashMap<String, Warp>();
	for (NBTTagCompound s : (Collection<NBTTagCompound>) getWarps().c()) {
	    String name = s.getName();
	    if (name.equals("other")) {
		continue;
	    }
	    locs.put(name, new Warp(name, getWarp(s)));
	}
	return locs;
    }

    public boolean isBanned() {
	return getBans().getByte("banned") == 1;
    }

    /**
     * @return 0: not banned/unbanned 1: tempbanned 2: permabanned
     */
    public int getBanType() {
	return getBans().getInt("bantype");
    }

    public long getUnbanTime() {
	return getBans().getLong("unbantime");
    }

    public ArrayList<String> getNotes() {
	ArrayList<String> notes = new ArrayList<String>();
	NBTTagList list = c.getList("notes");
	for (int i = 0; i < list.size(); i++) {
	    notes.add(((NBTTagString) list.get(i)).toString());
	}
	return notes;
    }

    public ArrayList<Integer> getCompletedTutorialIds() {
	ArrayList<Integer> ids = new ArrayList<Integer>();
	NBTTagList list = c.getList("finishedTutorials");
	for (int i = 0; i < list.size(); i++) {
	    ids.add(((NBTTagInt) list.get(i)).data);
	}
	return ids;
    }

    public Integer getRPAPassword() {
	if (c.hasKey("rpapassword")) {
	    return c.getInt("rpapassword");
	} else {
	    return null;
	}
    }

    public ArrayList<String> getMail() {
	NBTTagList list = c.getList("mail");
	ArrayList<String> mails = new ArrayList<String>();
	for (int i = 0; i < list.size(); i++) {
	    NBTBase base = list.get(i);
	    if (base instanceof NBTTagString) {
		mails.add(((NBTTagString) base).data);
	    }
	}
	return mails;
    }

    /* private getters */
    private NBTTagCompound getHorn() {
	return c.getCompound("horn");
    }

    private NBTTagCompound getEmote() {
	return c.getCompound("emote");
    }

    private NBTTagCompound getWarps() {
	return c.getCompound("warps");
    }

    private NBTTagCompound getOtherWarps() {
	return getWarps().getCompound("other");
    }

    private NBTTagCompound getBans() {
	return c.getCompound("ban");
    }

    private Location getWarp(NBTTagCompound compound) {
	try {
	    String w;
	    long x, y, z;
	    float p, yaw;
	    if (!(compound.hasKey("world") || compound.hasKey("x") || compound.hasKey("y") || compound.hasKey("z") || compound.hasKey("pitch") || compound.hasKey("yaw"))) {
		return null;
	    }
	    w = compound.getString("world");
	    x = compound.getLong("x");
	    y = compound.getLong("y");
	    z = compound.getLong("z");
	    p = c.getFloat("pitch");
	    yaw = c.getFloat("yaw");
	    Location l = new Location(Bukkit.getWorld(w), x, y, z);
	    l.setPitch(p);
	    l.setYaw(yaw);
	    return l;
	} catch (Exception e) {
	    return null;
	}
    }

    private NBTTagCompound getLocationCompound(Location l) {
	NBTTagCompound loc = new NBTTagCompound();
	if (l != null) {
	    String w;
	    long x, y, z;
	    float p, yaw;
	    w = l.getWorld().getName();
	    x = (long) l.getX();
	    y = (long) l.getY();
	    z = (long) l.getZ();
	    p = l.getPitch();
	    yaw = l.getYaw();
	    loc.set("world", new NBTTagString("world", w));
	    loc.set("x", new NBTTagLong("x", x));
	    loc.set("y", new NBTTagLong("y", y));
	    loc.set("z", new NBTTagLong("z", z));
	    loc.set("pitch", new NBTTagFloat("pitch", p));
	    loc.set("yaw", new NBTTagFloat("yaw", yaw));
	}
	return loc;
    }

    /* Setters */

    public void setName(String arg) {
	c.set("name", new NBTTagString("name", arg));
    }

    public void setMuted(boolean flag) {
	c.set("muted", new NBTTagByte("muted", getBoolByte(flag)));
    }

    public void setNsfwBlocked(boolean flag) {
	c.set("nsfwblocked", new NBTTagByte("nsfwblocked", getBoolByte(flag)));
    }

    public void setNsfwAccept(boolean flag) {
	c.set("nsfwaccept", new NBTTagByte("nsfwaccept", getBoolByte(flag)));
    }

    public void setSilenced(HashSet<String> set) {
	c.set("silenced", getList(set));
    }

    public void setGodMode(boolean flag) {
	c.set("god", new NBTTagByte("god", getBoolByte(flag)));
    }

    public void setInvisible(boolean flag) {
	c.set("invisible", new NBTTagByte("invisible", getBoolByte(flag)));
    }

    public void setInvisibleLevel(int level) {
	c.set("invisiblelevel", new NBTTagInt("invisiblelevel", level));
    }

    public void setNickname(String arg) {
	c.set("nickname", new NBTTagString("nickname", arg));
    }

    public void setNickHistory(ArrayList<String> set) {
	c.set("nickhistory", getList(set));
    }

    public void setChatChannel(String arg) {
	c.set("chatchannel", new NBTTagString("chatchannel", arg));
    }

    public void setChannelColor(String channel, ChatColor color) {
	c.getCompound("channelcolors").setString(channel, color.name());
    }

    public void setListenChannels(HashSet<String> set) {
	c.set("listenchannels", getList(set));
    }

    public void setPonySpy(boolean flag) {
	c.set("ponyspy", new NBTTagByte("ponyspy", getBoolByte(flag)));
    }

    public void setGroup(String arg) {
	c.set("group", new NBTTagString("group", arg));
    }

    public void setPerms(ArrayList<String> set) {
	c.set("perms", getList(set));
    }

    public void setIps(ArrayList<String> set) {
	c.set("ipaddress", getList(set));
    }

    public void setAdvancedXrayLogging(boolean flag) {
	c.set("advancedXrayLogging", new NBTTagByte("advancedXrayLogging", getBoolByte(flag)));
    }

    public void addFinishedTutorialId(final MissCheerilee.TutorialIds tutorial) {
	int i = tutorial.getId();
	ArrayList<Integer> ints = getCompletedTutorialIds();
	if (!ints.contains(i)) {
	    ints.add(i);
	    try {
		final Player player = Bukkit.getPlayer(getName());
		if (player != null) {
		    Bukkit.getScheduler().runTask(Mane.getInstance(), new Runnable() {
			public void run() {
			    player.sendMessage(Ponies.Cheerilee.says() + "Congratulations! You've completed the course: " + tutorial.getName() + "!");
			}
		    });
		}
	    } catch (Exception e) {}
	}
	c.set("finishedTutorials", getIntList(ints));
    }

    public void clearTutorials() {
	ArrayList<Integer> ints = new ArrayList<Integer>();
	c.set("finishedTutorials", getIntList(ints));
    }

    public void setInventory(org.bukkit.inventory.PlayerInventory inv, PonyWorld world) {
	String name = world.getOverworld().getName();
	c.getCompound("inventories").set(name, ((CraftInventoryPlayer) inv).getInventory().a(new NBTTagList()));
    }

    public void wipeWorldStats() {
	c.set("worlds", new NBTTagCompound());
    }

    public void setWorldStats(PonyStats stats, PonyWorld world) {
	c.getCompound("worlds").set(world.getOverworld().getName(), stats.getCompound());
    }

    public void saveRemoteChest(PonyWorld w) {
	if (w == null) {
	    System.out.println("WARNING WORLD IS NULL! player: " + getPlayer().getName());
	}
	NBTTagList remotechest = new NBTTagList();
	Inventory inv = getRemoteChest(w);
	for (int index = 0; index < inv.getContents().length; index++) {
	    CraftItemStack cis = (CraftItemStack) inv.getItem(index);
	    if (cis != null) {
		ItemStack is = CraftItemStack.asNMSCopy(cis);
		NBTTagCompound c = new NBTTagCompound();
		c = is.save(c);
		c.set("index", new NBTTagInt("index", index));
		remotechest.add(c);
	    }
	}
	c.getCompound("remotechest").set(w.getOverworld().getName().toLowerCase(), remotechest);
    }

    public void saveEnderChest(PonyWorld w) {
	if (w == null) {
	    System.out.println("WARNING WORLD IS NULL! player: " + getPlayer().getName());
	}
	NBTTagList enderChest = new NBTTagList();
	Inventory inv = getEnderChest(w);
	for (int index = 0; index < inv.getContents().length; index++) {
	    CraftItemStack cis = (CraftItemStack) inv.getItem(index);
	    if (cis != null) {
		ItemStack is = CraftItemStack.asNMSCopy(cis);
		NBTTagCompound c = new NBTTagCompound();
		c = is.save(c);
		c.set("index", new NBTTagInt("index", index));
		enderChest.add(c);
	    }
	}
	c.getCompound("enderchest").set(w.getOverworld().getName().toLowerCase(), enderChest);
    }

    public void setEmoteName(String arg) {
	getEmote().set("name", new NBTTagString("name", arg));
    }

    public void setEmoteSender(String arg) {
	getEmote().set("sender", new NBTTagString("name", arg));
    }

    public void setEmoteReceiver(String arg) {
	getEmote().set("receiver", new NBTTagString("name", arg));
    }

    public void setEmoteServer(String arg) {
	getEmote().set("server", new NBTTagString("name", arg));
    }

    public void setEmotePrivate(boolean flag) {
	getEmote().set("private", new NBTTagByte("private", getBoolByte(flag)));
    }

    public void setEmoteSilent(boolean flag) {
	getEmote().set("silent", new NBTTagByte("silent", getBoolByte(flag)));
    }

    public void setEmoteNsfw(boolean flag) {
	getEmote().set("nsfw", new NBTTagByte("nsfw", getBoolByte(flag)));
    }

    public void setEmoteBanned(boolean flag) {
	getEmote().set("banned", new NBTTagByte("banned", getBoolByte(flag)));
    }

    public void setHornLeft(String s) {
	getHorn().set("left", new NBTTagString("left", s));
    }

    public void setHornRight(String s) {
	getHorn().set("right", new NBTTagString("right", s));
    }

    public void setHornOn(boolean flag) {
	getHorn().set("ison", new NBTTagByte("ison", getBoolByte(flag)));
    }

    public void setFirstLogon(String arg) {
	c.set("firstlogon", new NBTTagString("firstlogon", arg));
    }

    public void setLastLogon(String arg) {
	c.set("lastlogon", new NBTTagString("lastlogon", arg));
    }

    public void setLastLogout(String arg) {
	c.set("lastlogout", new NBTTagString("lastlogout", arg));
    }

    public void setBackWarp(Location loc) {
	getOtherWarps().set("back", getLocationCompound(loc));
    }

    public void setHomeWarp(Location loc) {
	getOtherWarps().set("home", getLocationCompound(loc));
    }

    public void setOfflineWarp(Location loc) {
	getOtherWarps().set("offline", getLocationCompound(loc));
    }

    public void setNamedWarp(String warpname, Location loc) {
	getWarps().set(warpname, getLocationCompound(loc));
    }

    public boolean removeNamedWarp(String warpname) {
	if (getWarps().hasKey(warpname)) {
	    getWarps().remove(warpname);
	    return true;
	} else {
	    return false;
	}
    }

    public void setMail(ArrayList<String> mail) {
	c.set("mail", getList(mail));
    }

    public void setBanned(boolean flag) {
	getBans().set("banned", new NBTTagByte("banned", getBoolByte(flag)));
    }

    /**
     * @param bantype
     *            0: not banned/unbanned 1: tempbanned 2: permabanned
     */
    public void setBanType(int bantype) {
	getBans().set("bantype", new NBTTagInt("bantype", bantype));
    }

    public void setUnbanTime(long arg) {
	getBans().set("unbantime", new NBTTagLong("unbantime", arg));
    }

    public void setNotes(ArrayList<String> set) {
	c.set("notes", getList(set));
    }

    public void addNote(String whatHappened, String who, String note) {
	ArrayList<String> notes = getNotes();
	String noteToAdd = who + " " + whatHappened + " at " + Utils.getFileDate(System.currentTimeMillis());
	if (note != null) {
	    noteToAdd += " - " + note;
	}
	Bukkit.broadcast(ChatColor.LIGHT_PURPLE + "Pinkie Pie: New note added for player " + getName() + "! \n" + noteToAdd, PonyPermission.ALERT_NOTE.getPermString());
	notes.add(noteToAdd);
	setNotes(notes);
    }

    public void setRPAPassword(Integer i) {
	c.set("rpapassword", new NBTTagInt("rpapassword", i));
    }

    /* Utility methods */

    private byte getBoolByte(boolean arg) {
	if (arg) {
	    return 1;
	} else {
	    return 0;
	}
    }

    private NBTTagList getList(Collection<String> col) {
	NBTTagList list = new NBTTagList();
	for (String s : col) {
	    NBTTagString ts = new NBTTagString("", s);
	    list.add(ts);
	}
	return list;
    }

    private NBTTagList getIntList(Collection<Integer> col) {
	NBTTagList list = new NBTTagList();
	for (int i : col) {
	    NBTTagInt intt = new NBTTagInt("", i);
	    list.add(intt);
	}
	return list;
    }

    public boolean save() {
	try {
	    NBTCompressedStreamTools.a(c, new FileOutputStream(datFile));
	} catch (FileNotFoundException e) {
	    e.printStackTrace();
	    return false;
	}
	return true;
    }

    public void saveAllChests() {
	for (String worldname : enderChests.keySet()) {
	    PonyWorld world = PonyWorld.getPonyWorld(worldname);
	    saveEnderChest(world);
	}
	for (String worldname : remoteChests.keySet()) {
	    PonyWorld world = PonyWorld.getPonyWorld(worldname);
	    saveRemoteChest(world);
	}
    }

    public static Pony moveToPonyville(Player pone) {
	return moveToPonyville(pone.getName(), getIp(pone), pone.getFirstPlayed());
    }

    public static Pony moveToPonyville(String pone, String ip, long firstPlayed) {
	File datFile = new File(Mane.getInstance().getDataFolder() + File.separator + "Players", pone);
	if (datFile.exists()) {
	    try {
		return new Pony(pone);
	    } catch (Exception e) {
		PonyLogger.logListenerException(e, "File says exists, but does not!", "Pony (NOT A HANDLER!)", "Player login event");
		return null;
	    }
	}
	try {
	    datFile.createNewFile();
	} catch (IOException e) {
	    PonyLogger.logListenerException(e, "New player file cannot be created!", "Pony (NOT A HANDLER!)", "Player Join Event");
	    return null;
	}
	NBTTagCompound c = new NBTTagCompound();
	// set basics
	c.set("name", new NBTTagString("name", pone));
	c.set("muted", new NBTTagByte("muted", (byte) 0));
	c.set("silenced", new NBTTagList());
	c.set("nsfwblocked", new NBTTagByte("nsfwblocked", (byte) 0));
	c.set("nsfwaccept", new NBTTagByte("nsfwaccept", (byte) 0));
	c.set("god", new NBTTagByte("god", (byte) 0));
	c.set("invisible", new NBTTagByte("invisible", (byte) 0));
	c.set("invisiblelevel", new NBTTagInt("invisiblelevel", 0));
	c.set("nickname", new NBTTagString("nickname", pone));
	c.set("nickhistory", new NBTTagList());
	c.set("chatchannel", new NBTTagString("chatchannel", ChannelHandler.getHandler().getData().getDefaultChannel().getName()));
	c.set("channelcolors", new NBTTagCompound());
	NBTTagList lc = new NBTTagList();
	lc.add(new NBTTagString("channel", "equestria"));
	c.set("listenchannels", lc);
	c.set("ponyspy", new NBTTagByte("ponyspy", (byte) 0));
	c.set("group", new NBTTagString("group", "filly"));
	c.set("perms", new NBTTagList());
	NBTTagList ips = new NBTTagList();
	ips.add(new NBTTagString("ip", ip));
	c.set("ipaddress", ips);
	c.set("advancedXrayLogging", new NBTTagByte("advancedXrayLogging", (byte) 0));
	c.set("finishedTutorials", new NBTTagList());

	// Horn
	NBTTagCompound horn = new NBTTagCompound();
	horn.set("left", new NBTTagString("left", "horn help"));
	horn.set("right", new NBTTagString("right", "horn help"));
	horn.set("ison", new NBTTagByte("ison", (byte) 0));
	c.set("horn", horn);

	// set inventories
	c.set("inventories", new NBTTagCompound());

	// set world settings
	c.set("worlds", new NBTTagCompound());

	// set Remote Chest
	NBTTagCompound rcCompound = new NBTTagCompound();
	c.set("remotechest", rcCompound);
	NBTTagCompound ecCompound = new NBTTagCompound();
	c.set("enderchest", ecCompound);

	// emote
	NBTTagCompound emote = new NBTTagCompound();
	emote.set("name", new NBTTagString("name", "none"));
	emote.set("server", new NBTTagString("server", "none"));
	emote.set("sender", new NBTTagString("sender", "none"));
	emote.set("receiver", new NBTTagString("receiver", "none"));
	emote.set("private", new NBTTagByte("private", (byte) 1));
	emote.set("silent", new NBTTagByte("silent", (byte) 1));
	emote.set("nsfw", new NBTTagByte("nsfw", (byte) 0));
	emote.set("banned", new NBTTagByte("banned", (byte) 0));
	c.set("emote", emote);

	// login
	c.set("firstlogon", new NBTTagString("firstlogon", Utils.getDate(firstPlayed)));
	c.set("lastlogon", new NBTTagString("lastlogon", "n/a"));
	c.set("lastlogout", new NBTTagString("lastlogout", "n/a"));

	// warps
	NBTTagCompound warps, other, back, home, offline;
	warps = new NBTTagCompound();
	other = new NBTTagCompound();
	back = new NBTTagCompound();
	home = new NBTTagCompound();
	offline = new NBTTagCompound();
	other.set("back", back);
	other.set("home", home);
	other.set("offline", offline);
	warps.set("other", other);
	c.set("warps", warps);

	// mail
	c.set("mail", new NBTTagList());

	// ban
	NBTTagCompound ban = new NBTTagCompound();
	ban.set("banned", new NBTTagByte("banned", (byte) 0));
	ban.set("bantype", new NBTTagByte("bantype", (byte) 0));
	ban.set("unbantime", new NBTTagLong("unbantime", 0));
	c.set("ban", ban);
	c.set("notes", new NBTTagList());

	// save
	try {
	    NBTCompressedStreamTools.a(c, new FileOutputStream(datFile));
	} catch (Exception e) {
	    return null;
	}
	try {
	    return new Pony(pone);
	} catch (Exception e) {
	    return null;
	}
    }

    private static String getIp(Player player) {
	return player.getAddress().toString().substring(1, player.getAddress().toString().indexOf(":"));
    }
}
