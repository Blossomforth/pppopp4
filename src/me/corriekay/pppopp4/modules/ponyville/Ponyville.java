package me.corriekay.pppopp4.modules.ponyville;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.WeakHashMap;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.JoinEvent;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.UnhandledCommandException;
import me.corriekay.pppopp4.modules.equestria.PonyWorld;
import me.corriekay.pppopp4.modules.warp.WarpHandler;
import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.PlayerInventory;

public final class Ponyville extends PSCmdExe {

    private final HashMap<String, Pony> ponies = new HashMap<String, Pony>();
    private final WeakHashMap<String, Pony> weakPonies = new WeakHashMap<String, Pony>();
    private static Ponyville pv;
    private final boolean dohas = false;

    public Ponyville() {
	super("Ponyville", Ponies.PinkiePie, "motd", "rules");
	pv = this;
	File dir = new File(Mane.getInstance().getDataFolder() + File.separator + "Players");
	if (!dir.isDirectory()) {
	    dir.mkdirs();
	}
	loadConfig("rules.yml");
	init();
    }

    private void init() {
	for (Player player : Bukkit.getOnlinePlayers()) {
	    addPony(player);
	}
	Bukkit.getScheduler().runTaskTimer(Mane.getInstance(), new Runnable() {
	    public void run() {
		for (Player player : Bukkit.getOnlinePlayers()) {
		    displayWarning(player);
		}
	    }
	}, 0, 20 * 60 * 10);
    }

    private void displayWarning(Player player) {
	if (dohas)
	    sendMessage(player, ChatColor.DARK_RED + "ATTENTION: THIS SERVER IS IN A BETA STATE, AND YOU ARE A BETA TESTER. YOU WILL ENCOUNTER BUGS, BROKEN COMMANDS, AND GENERAL NOT-WORKING-NESS. THERE WILL ALSO BE RANDOM, UNSCHEDULED REBOOTS, WITH ZERO WARNING. CHOOSE TO PLAY AT YOUR RISK. IF YOU ENCOUNTER A BUG PLEASE LET US KNOW BY EMAILING THE ADMINISTRATOR AT\n\"corriekay.pinkie@outlook.com\"\nTHANK YOU FOR YOUR CO-OPERATION\nTHE BUG THREAD CAN BE FOUND HERE:\nhttp://tinyurl.com/pppopp4bugs");
    }

    /*
     * Remove Pony
     */
    public static void removePony(Player p) {
	removePony(p.getName());
    }

    public static void removePony(Pony p) {
	removePony(p.getName());
    }

    public static void removePony(String s) {
	Pony p = getPony(s);
	p.save();
	pv.ponies.remove(s);
    }

    /*
     * Add Pony
     */
    public static Pony addPony(Player p) {
	Pony pony = Pony.moveToPonyville(p);
	pv.ponies.put(p.getName(), pony);
	pv.setWeakPony(pony);
	return pony;
    }

    private void setWeakPony(Pony pony) {
	weakPonies.put(pony.getName(), pony);
    }

    /*
     * Get Pony
     */
    private static Pony getOnlinePony(String p) {
	Pony pony = pv.getWeakPony(p);
	if (pony != null) {
	    return pony;
	}
	return pv.ponies.get(p);
    }

    public static Pony getPony(Player p) {
	return getPony(p.getName());
    }

    public static Pony getPony(OfflinePlayer p) {
	return getPony(p.getName());
    }

    public static Pony getPony(String p) {
	Pony pony = getOnlinePony(p);
	if (pony == null) {
	    try {
		pony = new Pony(p);
		pv.setWeakPony(pony);
		return pony;
	    } catch (FileNotFoundException e) {
		return null;
	    }
	} else {
	    return pony;
	}
    }

    public static ArrayList<Pony> getOnlinePonies() {
	ArrayList<Pony> ponies = new ArrayList<Pony>();
	for (Pony pony : pv.ponies.values()) {
	    ponies.add(pony);
	}
	return ponies;
    }

    private Pony getWeakPony(String name) {
	return weakPonies.get(name);
    }

    public static ArrayList<String> getAllPonyNames() {
	ArrayList<String> ponylist = new ArrayList<String>();
	for (File file : new File(Mane.getInstance().getDataFolder() + File.separator + "Players").listFiles()) {
	    ponylist.add(file.getName());
	}
	return ponylist;
    }

    /*
     * Listeners
     */
    @PonyEvent
    public void onJoin(PlayerJoinEvent event) {
	if (!event.getPlayer().hasPlayedBefore()) {
	    event.getPlayer().teleport(WarpHandler.handler.spawn);
	}
	Player player = event.getPlayer();
	PonyWorld world = PonyWorld.getPonyWorld(player.getWorld());
	showMOTD(player);
	Pony pony = getPony(player);
	JoinEvent je;
	boolean newfilly = pony == null;
	if (newfilly) {
	    pony = addPony(player);
	    je = new JoinEvent(player, pony, true);
	    je.setJoinMessage(ChatColor.AQUA + "Please welcome " + ChatColor.RED + player.getName() + ChatColor.AQUA + " to Equestria!");
	} else {
	    je = new JoinEvent(player, pony, true);
	}
	{// Load information as the player joins.
	    PlayerInventory i = player.getInventory(), i2 = pony.getInventory(world);
	    i.setContents(i2.getContents());
	    i.setArmorContents(i2.getArmorContents());
	    pony.setLastLogon(Utils.getSystemTime());
	}
	pony.save();
	player.setDisplayName(pony.getNickname());
	Bukkit.getPluginManager().callEvent(je);
	if (newfilly) {// Perform actions that require stuff to be set up for a
		       // new player
	    Location spawn = WarpHandler.handler.spawn;
	    if (spawn == null) {
		spawn = player.getLocation();
	    }
	    player.teleport(spawn);
	}
	pony.getWorldStats(player, PonyWorld.getPonyWorld(player.getWorld()));
	event.setJoinMessage(je.isCancelled() ? null : je.getMsg());
	if (event.getJoinMessage() != null) {
	    Bukkit.getConsoleSender().sendMessage(event.getJoinMessage());
	}
	displayWarning(player);
    }

    @PonyEvent
    public void onQuit(PlayerQuitEvent event) {
	Player player = event.getPlayer();
	Pony pony = getPony(player);
	PonyWorld pworld = PonyWorld.getPonyWorld(player.getWorld());
	QuitEvent qe = new QuitEvent(player, pony, true);
	Bukkit.getPluginManager().callEvent(qe);
	{// Saved info on quit
	    pony.setLastLogout(Utils.getSystemTime());
	    for (PonyWorld world : PonyWorld.getPonyWorlds()) {
		if (pony.getRemoteChest(world) != null) {
		    pony.saveRemoteChest(world);
		}
	    }
	    pony.setWorldStats(new PonyStats(player), pworld);
	    pony.setInventory(player.getInventory(), pworld);
	}
	pony.save();
	event.setQuitMessage(qe.isCancelled() ? null : qe.getMsg());
	removePony(player);
	if (event.getQuitMessage() != null) {
	    Bukkit.getConsoleSender().sendMessage(event.getQuitMessage());
	}
    }

    /*
     * Commands
     */
    @Override
    public boolean optionalCommand(CommandSender sender, CommandMetadata cmd, String label, String[] args) throws UnhandledCommandException {
	switch (cmd) {
	case rules: {
	    showRules(sender);
	    return true;
	}
	case motd: {
	    showMOTD(sender);
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    public void showRules(CommandSender sender) {
	FileConfiguration config = getConfig();
	for (String s : config.getStringList("rules")) {
	    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
	}
    }

    public void showMOTD(CommandSender sender) {
	FileConfiguration config = getConfig();
	String name = sender.getName();
	if (sender instanceof Player) {
	    name = ((Player) sender).getDisplayName();
	}
	for (String s : config.getStringList("motd")) {
	    s = s.replaceAll("<player>", name);
	    sender.sendMessage(ChatColor.translateAlternateColorCodes('&', s));
	}
    }

    private void savePony(Player player) {
	Pony pony = Ponyville.getPony(player);
	savePony(player, pony);
    }

    private void savePony(Player player, Pony pony) {
	pony.setInventory(player.getInventory(), PonyWorld.getPonyWorld(player.getWorld()));
	pony.saveAllChests();
	pony.save();
    }

    /*
     * Deactivate Method
     */
    public void deactivate() {
	for (Player player : Bukkit.getOnlinePlayers()) {
	    savePony(player);
	}
    }
}
