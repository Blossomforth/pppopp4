package me.corriekay.pppopp4.modules.postoffice;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;

public class PonyMailbox {
    public final String owner;
    public List<Mail> inbox = new ArrayList<Mail>();

    public PonyMailbox(Pony pony) {
	owner = pony.getName();
	for (String letter : pony.getMail()) {
	    try {
		Mail mail = new Mail(letter);
		inbox.add(mail);
	    } catch (Exception e) {
		Bukkit.getLogger().severe("MAIL CORRUPT - Player " + owner + " - Mail text: " + letter);
		e.printStackTrace();
		continue;
	    }
	}
    }

    /*
     * Bookmark Checkers
     */
    public boolean hasMail() {
	return inbox.size() > 0;
    }

    public boolean hasUnreadMail() {
	for (Mail mail : inbox) {
	    if (!mail.isRead) {
		return true;
	    }
	}
	return false;
    }

    /*
     * Bookmark Getters
     */
    public Mail getMail(int i) {
	int counter = 0;
	for (Mail mail : inbox) {
	    counter++;
	    if (counter < i) {
		continue;
	    }
	    return mail;
	}
	return null;
    }

    public int totalCount() {
	return inbox.size();
    }

    public int unreadCount() {
	int count = 0;
	for (Mail mail : inbox) {
	    if (!mail.isRead) {
		count++;
	    }
	}
	return count;
    }

    public int readCount() {
	return totalCount() - unreadCount();
    }

    public String renderMailbox(boolean read, int page) {
	if (page == 0) {
	    page = 1;
	}
	if (inbox.size() < 1) {
	    return "No mail!\n";
	}
	StringBuilder builder = new StringBuilder();
	int start = (page - 1) * 5 + 1;
	int end = page * 5;
	int totalpages = (int) (Math.ceil((double) inbox.size() / 5.0));
	builder.append(ChatColor.WHITE + "Showing page " + page + " of " + totalpages + "\n");
	for (int i = start; i <= end; i++) {
	    Mail mail = getMail(i);
	    if (mail == null) {
		continue;
	    }
	    builder.append(ChatColor.WHITE + "" + i + " " + mail.renderInboxText());
	}
	return builder.toString();
    }

    /*
     * Bookmark Utils
     */
    public void deliverMail(Mail mail) {
	ArrayList<Mail> newmailbox = new ArrayList<Mail>();
	newmailbox.add(mail);
	newmailbox.addAll(inbox);
	inbox = newmailbox;
    }

    public boolean deleteMail(int mailNumber) {
	Mail mail = getMail(mailNumber);
	if (mail == null) {
	    return false;
	}
	inbox.remove(mail);
	return true;
    }

    public boolean markAllRead() {
	for (Mail mail : inbox) {
	    mail.isRead = true;
	}
	return true;
    }

    public boolean deleteAll() {
	inbox.clear();
	return true;
    }

    public void saveToFile() {
	Pony pony = Ponyville.getPony(owner);
	ArrayList<String> mailbox = pony.getMail();
	mailbox.clear();
	for (Mail mail : inbox) {
	    mailbox.add(mail.toString());
	}
	pony.setMail(mailbox);
	pony.save();
    }
}
