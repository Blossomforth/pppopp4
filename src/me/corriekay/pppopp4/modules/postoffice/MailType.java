package me.corriekay.pppopp4.modules.postoffice;

public enum MailType {
    FROM_PLAYER(0), SYSTEM_MESSAGE(1);

    private final int type;

    MailType(int i) {
	type = i;
    }

    public static MailType getType(int i) {
	for (MailType type : values()) {
	    if (type.getType() == i) {
		return type;
	    }
	}
	return null;
    }

    public int getType() {
	return type;
    }
}
