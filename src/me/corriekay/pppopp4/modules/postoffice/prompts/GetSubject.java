package me.corriekay.pppopp4.modules.postoffice.prompts;

import static me.corriekay.pppopp4.Ponies.DerpyHooves;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

public class GetSubject extends StringPrompt {

    @Override
    public Prompt acceptInput(ConversationContext context, String input) {
	context.setSessionData("subject", input);
	return new GetMessage(DerpyHooves.says() + "Okay, heres the tricky part. You're gonna type out a letter. I'll sit here and wait for you to finish. You can type more than one line. I'll paste together the entire mail at the end. When youre finished, type /accept to submit the mail, or type /erase to start over!");
    }

    @Override
    public String getPromptText(ConversationContext context) {
	return DerpyHooves.says() + "Okay, i'm gonna need a subject! This needs to be short!";
    }

}
