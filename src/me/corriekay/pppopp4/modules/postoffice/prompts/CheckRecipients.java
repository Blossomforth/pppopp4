package me.corriekay.pppopp4.modules.postoffice.prompts;

import static me.corriekay.pppopp4.Ponies.DerpyHooves;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.modules.postoffice.PostOffice;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

public class CheckRecipients extends StringPrompt {

    private final String promptText;

    public CheckRecipients(String promptText) {
	this.promptText = promptText;
    }

    @Override
    public String getPromptText(ConversationContext context) {
	return promptText;
    }

    protected boolean isInputValid(ConversationContext context, String input) {
	return true;
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input) {
	String[] recipientstxt = input.split(", ");
	Recipients recipients = new Recipients();
	for (String player : recipientstxt) {
	    player = player.trim();
	    String op = PostOffice.get().getOfflinePlayer(player);
	    if (op == null) {
		return new CheckRecipients(DerpyHooves.says() + "Uh oh, I couldn't find the address for the pony " + player + "! Give me another list of ponies?");
	    }
	    if (op.equals("TOO_MANY_MATCHES")) {
		return new CheckRecipients(DerpyHooves.says() + "Hmm. Not sure which one that is! Theres a lot of ponies that go by that name! Can you be more specific about this \"" + player + "\" pony?");
	    }
	    Pony pony = Ponyville.getPony(op);
	    recipients.addRecipients(pony.getName());
	}
	context.setSessionData("recipients", recipients);
	return new GetUrgency();
    }
}
