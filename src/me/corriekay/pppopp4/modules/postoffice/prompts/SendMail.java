package me.corriekay.pppopp4.modules.postoffice.prompts;

import static me.corriekay.pppopp4.Ponies.DerpyHooves;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.modules.postoffice.Mail;
import me.corriekay.pppopp4.modules.postoffice.MailType;
import me.corriekay.pppopp4.modules.postoffice.PostOffice;
import me.corriekay.pppopp4.utilities.PonyLogger;

import org.bukkit.ChatColor;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.MessagePrompt;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

public class SendMail extends MessagePrompt {

    @Override
    public String getPromptText(ConversationContext context) {
	return DerpyHooves.says() + "Okay... Give me one moment! This is the part where things can get a little derpy!!";
    }

    @Override
    protected Prompt getNextPrompt(ConversationContext arg0) {
	Recipients r = (Recipients) arg0.getSessionData("recipients");
	int counter = 0;
	int total = r.getRecipients().size();

	try {
	    String subject, message, sender;
	    boolean urgent;
	    MailType type = MailType.FROM_PLAYER;
	    subject = (String) arg0.getSessionData("subject");
	    message = ChatColor.translateAlternateColorCodes('&', (String) arg0.getSessionData("message"));
	    sender = ((Player) arg0.getForWhom()).getName();
	    urgent = (Boolean) arg0.getSessionData("urgent");

	    Mail mail = new Mail(subject, message, sender, type, urgent, false);

	    for (String reciever : r.getRecipients()) {
		try {
		    Pony pony = Ponyville.getPony(reciever);
		    PostOffice.get().deliverMail(mail, pony);
		    counter++;
		} catch (Exception e) {
		    continue;
		}
	    }
	    return new MailSent(DerpyHooves.says() + "Yay! Ive sent out " + counter + " of " + total + " messages successfully!");
	} catch (Exception e1) {
	    PonyLogger.logCmdException(e1, "Error while parsing Mail in SendMail.class", "DerpyHooves");
	    return new DERP();
	}
    }
}
