package me.corriekay.pppopp4.modules.postoffice.prompts;

import java.util.HashSet;

public class Recipients {
    private HashSet<String> recipients = new HashSet<String>();

    public void addRecipients(String... recipients) {
	for (String string : recipients) {
	    this.recipients.add(string);
	}
    }

    public HashSet<String> getRecipients() {
	return recipients;
    }
}
