package me.corriekay.pppopp4.modules.postoffice.prompts;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.MessagePrompt;
import org.bukkit.conversations.Prompt;

public class MailSent extends MessagePrompt {

    String msg;

    public MailSent(String msg) {
	this.msg = msg;
    }

    @Override
    public String getPromptText(ConversationContext arg0) {
	return msg;
    }

    @Override
    protected Prompt getNextPrompt(ConversationContext arg0) {
	return null;
    }

}
