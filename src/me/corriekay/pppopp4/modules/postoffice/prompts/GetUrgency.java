package me.corriekay.pppopp4.modules.postoffice.prompts;

import static me.corriekay.pppopp4.Ponies.DerpyHooves;

import org.bukkit.conversations.BooleanPrompt;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;

public class GetUrgency extends BooleanPrompt {

    @Override
    public String getPromptText(ConversationContext arg0) {
	return DerpyHooves.says() + "Oki doki!! Is this an urgent letter? :O";
    }

    @Override
    protected Prompt acceptValidatedInput(ConversationContext arg0, boolean arg1) {
	arg0.setSessionData("urgent", arg1);
	return new GetSubject();
    }

}
