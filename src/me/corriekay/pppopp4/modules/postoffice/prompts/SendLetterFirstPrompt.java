package me.corriekay.pppopp4.modules.postoffice.prompts;

import static me.corriekay.pppopp4.Ponies.DerpyHooves;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.MessagePrompt;
import org.bukkit.conversations.Prompt;

public class SendLetterFirstPrompt extends MessagePrompt {

    @Override
    public String getPromptText(ConversationContext context) {
	return DerpyHooves.says() + "Ohai there! Looks like you wanna send a letter! Go ahead and tell me who this letter is for! Seperate player names by commas! Also, you can exit this menu at any time, by typing /exit! :D";
    }

    @Override
    protected Prompt getNextPrompt(ConversationContext arg0) {
	return new CheckRecipients(DerpyHooves.says() + "Okay, so who's the letter for?");
    }

}
