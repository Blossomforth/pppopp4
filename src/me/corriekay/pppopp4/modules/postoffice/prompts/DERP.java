package me.corriekay.pppopp4.modules.postoffice.prompts;

import static me.corriekay.pppopp4.Ponies.DerpyHooves;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.MessagePrompt;
import org.bukkit.conversations.Prompt;

public class DERP extends MessagePrompt {

    @Override
    public String getPromptText(ConversationContext context) {
	return DerpyHooves.says() + "OH NO!!! I... I just don't know what went wrong! I was just sitting here trying to do the mail and put your letter together again and so i could be like \"I brought you a letter!\" to your friendpony and then... and.. and... I JUST DONT KNOW WHAT WENT WRONG! You gotta tell the administrators! They'll know what to do... I know... Ive kept the error! They'll know where it is. QUICKLY! TELL THEM D:!";
    }

    @Override
    protected Prompt getNextPrompt(ConversationContext context) {
	return null;
    }

}
