package me.corriekay.pppopp4.modules.postoffice.prompts;

import static me.corriekay.pppopp4.Ponies.DerpyHooves;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.StringPrompt;

public class GetMessage extends StringPrompt {

    final String displayme;

    public GetMessage(String displayme) {
	this.displayme = displayme;
    }

    @Override
    public Prompt acceptInput(ConversationContext context, String input) {
	String messageSoFar = (String) context.getSessionData("message");
	if (input.equals("/accept")) {
	    System.out.println("accept");
	    return new SendMail();
	}
	if (input.equals("/erase")) {
	    System.out.println("erase");
	    context.setSessionData("message", "");
	    return new GetMessage(DerpyHooves.says() + "Oh gosh! I must have dropped your message... Can you write that again? ...whats that, you erased it? Oh... Okay. Ready to accept your message again! Remember, type /accept when you're finished writing!");
	}
	if (!(messageSoFar.endsWith(" ") && input.startsWith(" "))) {
	    messageSoFar += " ";
	}
	messageSoFar += input;
	context.setSessionData("message", messageSoFar);
	return new GetMessage(messageSoFar);
    }

    @Override
    public String getPromptText(ConversationContext context) {
	return displayme;
    }

}
