package me.corriekay.pppopp4.modules.postoffice.prompts;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class DerpyHooves extends ConversationFactory {

    public DerpyHooves(Plugin plugin) {
	super(plugin);
    }

    public void startLetter(Player player) {
	Map<Object, Object> map = new HashMap<Object, Object>();
	map.put("urgency", false);
	map.put("message", "");
	Conversation conv = withFirstPrompt(new SendLetterFirstPrompt()).withEscapeSequence("/exit").withInitialSessionData(map).withLocalEcho(false).withModality(false).buildConversation(player);
	conv.begin();
    }
}
