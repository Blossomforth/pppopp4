package me.corriekay.pppopp4.modules.postoffice;

import java.util.HashMap;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.JoinEvent;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.modules.postoffice.prompts.DerpyHooves;
import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class PostOffice extends PSCmdExe {

    private HashMap<String, PonyMailbox> mailboxes = new HashMap<String, PonyMailbox>();
    public final DerpyHooves derpyHooves;
    private static PostOffice office;
    private BukkitTask task;

    public PostOffice() {
	super("DerpyHooves", Ponies.DerpyHooves, "sendletter", "composeletter", "viewinbox", "viewletter", "deleteletter", "markunread", "respond", "forward", "markallread", "deleteallmail");
	office = this;
	init();
	derpyHooves = new DerpyHooves(Mane.getInstance());
    }

    private void init() {
	for (Player player : Bukkit.getOnlinePlayers()) {
	    initPlayer(player, false);
	}
	task = Bukkit.getScheduler().runTaskTimer(Mane.getInstance(), new Runnable() {
	    public void run() {
		for (String player : mailboxes.keySet()) {
		    Player playerr = Bukkit.getPlayerExact(player);
		    if (playerr != null) {
			mailboxAlertPlayer(playerr);
		    }
		}
	    }
	}, 1, 15 * 60 * 20);
    }

    private void initPlayer(Player player, boolean showinbox) {
	PonyMailbox pmb = new PonyMailbox(Ponyville.getPony(player));
	mailboxes.put(player.getName(), pmb);
	if (showinbox) {
	    mailboxAlertPlayer(player);
	}
    }

    /*
     * Bookmark Getters
     */
    public PonyMailbox getMailbox(Player player) {
	return getMailbox(player.getName());
    }

    public PonyMailbox getMailbox(String player) {
	return mailboxes.get(player);
    }

    /*
     * Bookmark Command
     */
    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case sendletter: {
	    OfflinePlayer op = getOnlineOfflinePlayer(args[0], player);
	    if (op != null) {
		Pony pony = Ponyville.getPony(op.getName());
		String letter = getColoredMessageFromArgs(args, 1);
		sendLetterCmd(player, pony, letter);
	    }
	    return true;
	}
	case composeletter: {
	    derpyHooves.startLetter(player);
	    return true;
	}
	case viewinbox: {
	    try {
		showMailbox(player, (args.length > 0 ? Integer.parseInt(args[0]) : 1));
	    } catch (NumberFormatException e) {
		sendMessage(player, "I... I dont think thats a number..");
	    }
	    return true;
	}
	case viewletter: {
	    try {
		int letter = Integer.parseInt(args[0]);
		showLetter(player, letter);
	    } catch (NumberFormatException e) {
		sendMessage(player, "I... I dont think thats a number..");
	    }
	    return true;
	}
	case deleteletter: {
	    try {
		int letterNumber = Integer.parseInt(args[0]);
		deleteLetterCmd(player, letterNumber);
	    } catch (NumberFormatException e) {
		sendMessage(player, "I... I dont think thats a number..");
	    }
	    return true;
	}
	case markunread: {
	    try {
		int letterNumber = Integer.parseInt(args[0]);
		markUnreadCmd(player, letterNumber);
	    } catch (NumberFormatException e) {
		sendMessage(player, "I... I dont think thats a number..");
	    }
	    return true;
	}
	case respond: {
	    try {
		int letterNumber = Integer.parseInt(args[0]);
		respondLetterCmd(player, letterNumber, Utils.joinStringArray(args, " ", 1));
	    } catch (NumberFormatException e) {
		sendMessage(player, "I... I dont think thats a number..");
	    }
	    return true;
	}
	case forward: {
	    try {
		int letterNumber = Integer.parseInt(args[0]);
		OfflinePlayer op = getOnlineOfflinePlayer(args[1], player);
		if (op == null) {
		    return true;
		}
		Pony pony = Ponyville.getPony(op.getName());
		String additionalMessage = null;
		if (args.length > 2) {
		    additionalMessage = Utils.joinStringArray(args, " ", 2);
		}
		forwardLetterCmd(player, letterNumber, pony, additionalMessage);
		return true;
	    } catch (NumberFormatException e) {
		sendMessage(player, "I... I dont think thats a number..");
	    }
	    return true;
	}
	case markallread: {
	    Pony pony = Ponyville.getPony(player);
	    markAllReadCmd(pony);
	    sendMessage(player, "All mail marked as read :3");
	    return true;
	}
	case deleteallmail: {
	    Pony pony = Ponyville.getPony(player);
	    deleteAllMailCmd(pony);
	    sendMessage(player, "Pfft, who needs that pesky mail anyways. It was probably spam or junk or something...");
	    return true;
	}
	default: {
	    this.throwUnhandledCommandException(cmd);
	}
	}
	return true;
    }

    /*
     * Bookmark Command Methods
     */
    public boolean sendLetterCmd(Player sender, Pony reciever, String message) {
	String subject = "Message from " + sender.getName();
	Mail mail = new Mail(subject, message, sender.getName(), MailType.FROM_PLAYER, false, false);
	deliverMail(mail, reciever);
	sendMessage(sender, "Okay! Sending a letter to " + reciever.getNickname() + ChatColor.YELLOW + "!");
	return true;
    }

    public boolean respondLetterCmd(Player sender, int letterNumber, String message) {
	PonyMailbox pmb = mailboxes.get(sender.getName());
	Mail mail = pmb.getMail(letterNumber);
	if (mail == null) {
	    sendMessage(sender, "Oh no! I cant find that letter... Maybe it got lost in my bag...");
	    return false;
	}
	if (mail.type == MailType.SYSTEM_MESSAGE) {
	    sendMessage(sender, "Silly filly! This doesnt have a return address!");
	    return true;
	}
	String subject = mail.subject;
	subject = (!ChatColor.stripColor(subject).startsWith("RE:") ? "RE: " + subject : subject);
	// subject = ((!ChatColor.stripColor(subject).startsWith("RE:") ? "RE: "
	// + subject : subject).length() > 25 ? subject.substring(0, 22) + "..."
	// : subject);
	Mail mail2 = new Mail(subject, message, sender.getName(), MailType.FROM_PLAYER, mail.isUrgent, false);
	Pony pony = Ponyville.getPony(mail.senderName);
	deliverMail(mail2, pony);
	sendMessage(sender, "Okay! Sending a letter to " + pony.getNickname() + ChatColor.YELLOW + "!");
	return true;
    }

    public boolean forwardLetterCmd(Player sender, int letterNumber, Pony to, String additionalMessage) {
	PonyMailbox mailbox = mailboxes.get(sender.getName());
	Mail mail = mailbox.getMail(letterNumber);
	if (mail == null) {
	    sendMessage(sender, "Oh no! I cant find that letter... Maybe it got lost in my bag...");
	    return false;
	}
	if (mail.type == MailType.SYSTEM_MESSAGE) {
	    sendMessage(sender, "Uh oh, you cant forward a system message like that!");
	    return false;
	}
	String message = "";
	if (additionalMessage != null) {
	    message += additionalMessage += "\n---------------\n";
	}
	message += "FWD:\nfrom: " + sender.getName() + "\nOriginally sent by: " + mail.senderName + "\nOriginal Text:\n" + mail.message;
	mail = new Mail(mail.subject, message, sender.getName(), MailType.FROM_PLAYER, mail.isUrgent, false);
	deliverMail(mail, to);
	sendMessage(sender, "Okay! forwarding a letter to " + to.getNickname() + ChatColor.YELLOW + "!");
	return true;
    }

    public boolean showLetter(Player player, int letterNumber) {
	PonyMailbox pmb = mailboxes.get(player.getName());
	Mail mail = pmb.getMail(letterNumber);
	if (mail == null) {
	    sendMessage(player, "Oh no! I cant find that letter... Maybe it got lost in my bag...");
	    return false;
	}
	sendMessage(player, "I brought you a letter!\n" + mail.renderViewText());
	mail.isRead = true;
	pmb.saveToFile();
	return true;
    }

    public boolean deleteLetterCmd(Player sender, int mail) {
	PonyMailbox pmb = mailboxes.get(sender.getName());
	if (!pmb.deleteMail(mail)) {
	    sendMessage(sender, "Hmm... I couldnt find that letter... Are you sure you... *looks in her mailbag* I CANT FIND IT!! D:");
	    return false;
	}
	pmb.saveToFile();
	sendMessage(sender, "Whoosh! Mail deleted!");
	return true;
    }

    public boolean markUnreadCmd(Player sender, int mailNumber) {
	PonyMailbox pmb = mailboxes.get(sender.getName());
	Mail mail = pmb.getMail(mailNumber);
	if (mail == null) {
	    sendMessage(sender, "Hmm... I couldnt find that letter... Are you sure you... *looks in her mailbag* I CANT FIND IT!! D:");
	    return false;
	}
	mail.isRead = false;
	pmb.saveToFile();
	sendMessage(sender, "Woosh! Mail un-read-ified!");
	return true;
    }

    public boolean markAllReadCmd(Pony pony) {
	PonyMailbox pmb = mailboxes.get(pony.getName());
	if (pmb == null) {
	    pmb = new PonyMailbox(pony);
	}
	pmb.markAllRead();
	pmb.saveToFile();
	return true;
    }

    public boolean deleteAllMailCmd(Pony pony) {
	PonyMailbox pmb = mailboxes.get(pony.getName());
	if (pmb == null) {
	    pmb = new PonyMailbox(pony);
	}
	pmb.deleteAll();
	pmb.saveToFile();
	return true;
    }

    /*
     * Bookmark Listeners
     */

    @PonyEvent
    public void login(JoinEvent event) {
	if (event.isJoining()) {
	    initPlayer(event.getPlayer(), true);
	}
    }

    @PonyEvent
    public void logout(QuitEvent event) {
	if (event.isQuitting()) {
	    mailboxes.remove(event.getPlayer().getName());
	}
    }

    /*
     * Bookmark Utils
     */

    public static void sendSystemMessage(Pony pony, String message, boolean urgent) {
	sendSystemMessage(pony, message, "System Message", urgent);
    }

    public static void sendSystemMessage(Pony pony, String message, String subject, boolean urgent) {
	Mail mail = new Mail(subject, message, "SYSTEM MESSAGE", MailType.SYSTEM_MESSAGE, urgent, false);
	office.deliverMail(mail, pony);
    }

    public String getOfflinePlayer(String pName) {
	return getPlayerSilent(pName);
    }

    public void deliverMail(Mail mail, Pony player) {
	PonyMailbox mailbox = mailboxes.get(player.getName());
	if (mailbox == null) {
	    mailbox = new PonyMailbox(player);
	}
	mailbox.deliverMail(mail);
	Player playerr = Bukkit.getPlayerExact(player.getName());
	if (playerr != null) {
	    sendMessage(playerr, "I brought you a letter from " + (mail.type == MailType.FROM_PLAYER ? Ponyville.getPony(mail.senderName).getNickname() + ChatColor.YELLOW : "SYSTEM") + "! Check your inbox with /inbox! *flies away*");
	}
	mailbox.saveToFile();
    }

    private void mailboxAlertPlayer(Player player) {
	PonyMailbox mailbox = getMailbox(player);
	if (mailbox.hasUnreadMail()) {
	    sendSyncMessage(player, "I brought you a letter! You have " + mailbox.unreadCount() + " unread letters!");
	}
    }

    private void showMailbox(Player player, int page) {
	PonyMailbox mailbox = getMailbox(player);
	String letters = mailbox.renderMailbox(true, page);
	sendMessage(player, "Heres your mailbox!\nType /letter <number> to view a letter!\nColor code: " + ChatColor.WHITE + "unread " + ChatColor.GRAY + " read " + ChatColor.RED + " !urgent " + ChatColor.WHITE + "\nid - Sender - Subject\n===============\n" + letters + ChatColor.WHITE + "===============");
    }

    public String getColoredMessageFromArgs(String[] args, int index) {
	return ChatColor.translateAlternateColorCodes('&', Utils.getStringFromIndex(index, args));
    }

    public static PostOffice get() {
	return office;
    }

    public void deactivate() {
	task.cancel();
    }
}
