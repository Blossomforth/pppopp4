package me.corriekay.pppopp4.modules.postoffice;

import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.ChatColor;

public class Mail {

    public MailType type;
    public String senderName;
    public String subject;
    public String message;
    public boolean isUrgent, isRead;
    private static final String delimiter = "��";

    public Mail(String rawmail) {
	String[] parts = rawmail.split(delimiter);
	type = MailType.getType(Integer.parseInt(parts[0]));
	senderName = parts[1];
	subject = parts[2];
	message = ChatColor.translateAlternateColorCodes('&', parts[3]);
	isUrgent = parts[4].equals("0") ? false : true;
	isRead = parts[5].equals("0") ? false : true;
    }

    public Mail(String subject, String message, String sender, MailType type,
	    boolean urgent, boolean isRead) {
	this.subject = subject;
	this.message = message;
	senderName = sender;
	this.type = type;
	isUrgent = urgent;
	this.isRead = isRead;
    }

    public String renderInboxText() {
	return (isUrgent ? ChatColor.RED + "" + ChatColor.BOLD + "!" + ChatColor.RESET : "") + (isRead ? ChatColor.GRAY : ChatColor.WHITE) + (type == MailType.SYSTEM_MESSAGE ? "SYSTEM" : senderName) + (isRead ? ChatColor.GRAY : ChatColor.WHITE) + " - " + (subject.length() > 25 ? subject.substring(0, 22) + "..." : subject) + ChatColor.WHITE + "\n";
    }

    public String renderViewText() {
	return ChatColor.WHITE + "From: " + (type == MailType.FROM_PLAYER ? Ponyville.getPony(senderName).getNickname() + ChatColor.WHITE : "SYSTEM") + "\nSubject: " + subject + "\n====================\n" + message + ChatColor.WHITE + "\n====================\n";
    }

    public String toString() {
	return type.getType() + delimiter + senderName + delimiter + subject + delimiter + message + delimiter + (isUrgent ? "1" : "0") + delimiter + (isRead ? "1" : "0");
    }
}
