package me.corriekay.pppopp4.modules.lockdown;

import java.io.File;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.ponymanager.PonyPermission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;

public class Lockdown extends PSCmdExe {

    private boolean lockdown = false;

    public Lockdown() {
	super("Lockdown", Ponies.PinkiePie, "lockdown");
	Bukkit.getScheduler().runTaskTimer(Mane.getInstance(), new Runnable() {
	    public void run() {
		if (lockdown) {
		    Bukkit.broadcast(ChatColor.DARK_RED + "ATTENTION: THE SERVER IS IN LOCKDOWN!", "pppopp3.alert");
		}
	    }
	}, 0, 5 * 20 * 60);
    }

    @Override
    public boolean optionalCommand(CommandSender sender, CommandMetadata cmd, String label, String[] args) {
	if (cmd == CommandMetadata.lockdown) {
	    if (lockdown) {
		lockdown = false;
		broadcastMessage(ChatColor.GREEN + "Lockdown disabled!", PonyPermission.IS_OPERATOR);
		// RemotePonyAdmin.rpa.message("Lockdown disabled!");
		// RemotePonyAdmin.rpa.sendCustomAlertAllClients("Lockdown Disabled!");
	    } else {
		lockdown = true;
		broadcastMessage(ChatColor.DARK_RED + "Lockdown enabled!", PonyPermission.IS_OPERATOR);
		// RemotePonyAdmin.rpa.message("Lockdown enabled!");
		// RemotePonyAdmin.rpa.sendCustomAlertAllClients("Lockdown Enabled!");
		Bukkit.getScheduler().scheduleSyncDelayedTask(Mane.getInstance(), new Runnable() {
		    @Override
		    public void run() {
			lockdown = false;
			broadcastMessage(ChatColor.GREEN + "Lockdown disabled!", PonyPermission.IS_OPERATOR);
			// RemotePonyAdmin.rpa.message("Lockdown disabled!");
			// RemotePonyAdmin.rpa.sendCustomAlertAllClients("Lockdown Disabled!");
		    }
		}, 50 * 60 * 60 * 12);
	    }
	    return true;
	}
	return true;
    }

    @EventHandler
    public void join(final AsyncPlayerPreLoginEvent event) {
	if (event.isAsynchronous()) {
	    Future<Boolean> kick = Bukkit.getScheduler().callSyncMethod(Mane.getInstance(), new Callable<Boolean>() {
		public Boolean call() {
		    if (lockdown) {
			File file = new File(Mane.getInstance().getDataFolder() + File.separator + "Players", event.getName());
			OfflinePlayer player = Bukkit.getOfflinePlayer(event.getName());
			if (!player.hasPlayedBefore() && !file.exists()) {
			    return true;
			}
		    }
		    return false;
		}
	    });
	    try {
		if (kick.get()) {
		    event.disallow(Result.KICK_OTHER, "The server is unavailable right now, Please try again later!");
		    return;
		} else {
		    return;
		}
	    } catch (Exception e) {
		e.printStackTrace();
		return;
	    }
	}
	if (lockdown) {
	    File file = new File(Mane.getInstance().getDataFolder() + File.separator + "Players", event.getName());
	    OfflinePlayer player = Bukkit.getOfflinePlayer(event.getName());
	    if (!player.hasPlayedBefore() && !file.exists()) {
		event.disallow(Result.KICK_OTHER, "The server is unavailable right now, Please try again later!");

	    }
	}
    }
}
