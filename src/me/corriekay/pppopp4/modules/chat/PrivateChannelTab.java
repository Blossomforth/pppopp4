package me.corriekay.pppopp4.modules.chat;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.command.validation.tabbing.BasicTabRule;

import org.bukkit.command.CommandSender;

public class PrivateChannelTab implements BasicTabRule {

    @Override
    public List<String> tab(CommandSender sender, CommandMetadata cmd, String argBit, String[] args) {
	String[] s = new String[] { "createchannel", "setcolor", "setpassword", "setmoderator", "removemoderator", "removepassword", "setadmin", "ban", "kick", "unban", "mute", "closechannel", "setquick", "help", "setnsfw" };
	List<String> returnMe = new ArrayList<String>();
	for (String ss : s) {
	    returnMe.add(ss);
	}
	return returnMe;
    }

}
