package me.corriekay.pppopp4.modules.chat;

import java.util.HashMap;
import java.util.HashSet;

import org.bukkit.ChatColor;

public class ChannelData {

    private HashMap<String, Channel> channels = new HashMap<String, Channel>();
    private HashMap<String, HashSet<String>> ignores = new HashMap<String, HashSet<String>>();
    private HashMap<String, HashMap<Channel, ChatColor>> channelColors = new HashMap<String, HashMap<Channel, ChatColor>>();
    private HashSet<String> muted = new HashSet<String>();
    private Channel defaultChannel = null;
    private final HashMap<String, String> pmTargets = new HashMap<String, String>();
    private long expiretime;

    public Channel getDefaultChannel() {
	return defaultChannel;
    }

    public void setDefaultChannel(Channel chan) {
	defaultChannel = chan;
    }

    public HashMap<String, Channel> getChannels() {
	return channels;
    }

    public void addChannel(Channel chan) {
	channels.put(chan.getName(), chan);
    }

    public void removeChannel(Channel chan) {
	channels.remove(chan.getName());
    }

    public HashMap<String, HashSet<String>> getIgnores() {
	return ignores;
    }

    public void addIgnores(String name, HashSet<String> ignored) {
	ignores.put(name, ignored);
    }

    public void removeIgnores(String name) {
	ignores.remove(name);
    }

    public HashMap<String, HashMap<Channel, ChatColor>> getChannelColors() {
	return channelColors;
    }

    public void addChannelColor(String player, Channel chan, ChatColor color) {
	HashMap<Channel, ChatColor> colors = channelColors.get(player);
	if (colors == null) {
	    colors = new HashMap<Channel, ChatColor>();
	}
	colors.put(chan, color);
	channelColors.put(player, colors);
    }

    public HashSet<String> getMuted() {
	return muted;
    }

    public void setMuted(String player, boolean flag) {
	if (flag) {
	    muted.add(player);
	} else {
	    muted.remove(player);
	}
    }

    public HashMap<String, String> getPmTargets() {
	return pmTargets;
    }

    public long getExpiretime() {
	return expiretime;
    }

    public void setExpiretime(long expiretime) {
	this.expiretime = expiretime;
    }

}
