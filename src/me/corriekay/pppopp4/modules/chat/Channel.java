package me.corriekay.pppopp4.modules.chat;

import static me.corriekay.pppopp4.modules.chat.ChannelHandler.regex;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.events.ChannelBroadcastEvent;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.utilities.PonyLogger;
import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;

public class Channel {

    private String name, quick, icon, permission;
    protected ChatColor color;
    private final HashSet<String> chatters = new HashSet<String>();
    private final HashSet<String> listeners = new HashSet<String>();
    private final ChannelHandler handler;
    private final ChatColor w = ChatColor.WHITE;
    private boolean nsfw = false;

    public Channel(String channel, String quick, String icon,
	    String permission, ChatColor color, boolean nsfw,
	    ChannelHandler handler) {
	name = channel;
	this.quick = quick;
	this.icon = icon;
	this.permission = permission;
	this.color = color;
	this.handler = handler;
	this.nsfw = nsfw;
    }

    /*
     * Bookmark Broadcasters
     */
    public String broadcastToChannel(String who, String message, boolean log, boolean rpa) {
	Pony pony = Ponyville.getPony(who);
	message = parseForLinks(message);
	if (message.startsWith(">")) {
	    message = ChatColor.GREEN + message;
	}
	String formattedWho = who.equals("Server") ? "Server" : pony.getNickname();
	String consoleMessage = color + icon + " " + w + formattedWho + w + ": " + color + message;
	String rpaMessage = icon + " " + formattedWho + ": " + message;
	for (String name : listeners) {
	    ChatColor finalColor = getColor(name);
	    String finalMessage = finalColor + icon + " " + w + formattedWho + w + ": " + finalColor + message;
	    if (rpa || handler.doSendMessage(who, name)) {
		Player player = Bukkit.getPlayerExact(name);
		if (player == null) {
		    Bukkit.getLogger().severe("Chat desync - Player null: " + name);
		    continue;
		}
		player.sendMessage(finalMessage);
	    }
	}
	Bukkit.getConsoleSender().sendMessage(consoleMessage);
	ChannelBroadcastEvent cbe = new ChannelBroadcastEvent(this, rpaMessage);
	Bukkit.getPluginManager().callEvent(cbe);
	if (log) {
	    PonyLogger.logMessage("ChatLogs" + File.separator + Utils.getFileDate(), getName(), "[" + Utils.getTimeStamp() + "] [" + who + "|" + ChatColor.stripColor(formattedWho) + "]: " + consoleMessage);
	    PonyLogger.logMessage("ChatLogs" + File.separator + Utils.getFileDate(), "allchat", "[" + Utils.getTimeStamp() + "] [" + who + "|" + ChatColor.stripColor(formattedWho) + "]: " + consoleMessage);
	}
	return consoleMessage;
    }

    public void broadcastRawMessage(String message, boolean log) {
	message = parseForLinks(message);
	for (String name : listeners) {
	    Player player = Bukkit.getPlayerExact(name);
	    if (player != null) {
		player.sendMessage(message);
	    }
	}
	if (log) {
	    PonyLogger.logMessage("ChatLogs" + File.separator + Utils.getFileDate(), getName(), "[" + Utils.getTimeStamp() + "]: " + message);
	    PonyLogger.logMessage("ChatLogs" + File.separator + Utils.getFileDate(), "allchat", "[" + Utils.getTimeStamp() + "]: " + message);
	}
	Bukkit.getConsoleSender().sendMessage(message);
	// TODO Send messages to RPA clients.
    }

    public void broadcastEmote(String message, String name, boolean log) {
	message = parseForLinks(message);
	for (String listener : listeners) {
	    Player player = Bukkit.getPlayerExact(listener);
	    if (player != null) {
		ChatColor c = getColor(player.getName());
		player.sendMessage(c + "* " + name + " " + c + message);
	    }
	}
    }

    /*
     * Bookmark Checkers
     */
    public boolean canJoin(Permissible p) {
	if (p instanceof Player) {
	    Pony pony = Ponyville.getPony((Player) p);
	    if (nsfw && pony.isNsfwBlocked()) {
		return false;
	    }
	    if (nsfw && !pony.isNsfwAccepted()) {
		return false;
	    }
	}
	if (permission == null) {
	    return true;
	}
	return p.hasPermission(permission);
    }

    public boolean isChatting(Player p) {
	return chatters.contains(p.getName());
    }

    public boolean isListening(Player p) {
	return listeners.contains(p.getName());
    }

    public boolean isPrivateChannel() {
	return false;
    }

    public boolean isNsfwFlagged() {
	return nsfw;
    }

    /*
     * Bookmark Joiners and Leavers
     */
    public boolean joinChat(Player p, boolean notify) {
	if (canJoin(p) && !isChatting(p)) {
	    chatters.add(p.getName());
	    if (notify) {
		p.sendMessage(getColor(p.getName()) + "Now chatting in channel " + getName() + "!");
	    }
	    if (!isListening(p)) {
		joinListening(p, false);
	    }
	    return true;
	}
	return false;
    }

    public boolean joinListening(Player p, boolean notify) {
	if (canJoin(p) && !isListening(p)) {
	    listeners.add(p.getName());
	    if (notify) {
		p.sendMessage(getColor(p.getName()) + "Now listening to channel " + getName() + "!");
	    }
	    return true;
	}
	return false;
    }

    public boolean leaveChat(Player p, boolean notify, boolean leaveListen) {
	if (isChatting(p)) {
	    chatters.remove(p.getName());
	    if (notify) {
		p.sendMessage(getColor(p.getName()) + "No longer chatting in channel " + getName() + "!");
	    }
	    if (leaveListen) {
		leaveListen(p, false);
	    }
	    return true;
	} else if (leaveListen) {
	    leaveListen(p, notify);
	}
	return false;
    }

    public boolean leaveListen(Player p, boolean notify) {
	if (isListening(p)) {
	    listeners.remove(p.getName());
	    if (notify) {
		p.sendMessage(getColor(p.getName()) + "No longer listening to channel " + getName() + "!");
	    }
	    return true;
	} else {
	    return false;
	}
    }

    /*
     * Bookmark Getters
     */
    protected HashSet<String> getChatters() {
	return chatters;
    }

    protected HashSet<String> getListeners() {
	return listeners;
    }

    public ChatColor getColor(String name) {
	ChatColor cColor = handler.getPlayerColor(name, this);
	if (cColor == null) {
	    cColor = color;
	}
	return cColor;
    }

    public String getName() {
	return name;
    }

    public String getQuick() {
	return quick;
    }

    public String toString() {
	return getName();
    }

    public String getIcon() {
	return icon;
    }

    /*
     * Bookmark Setters
     */

    protected void setName(String nname) {
	name = nname;
    }

    protected void setQuick(String qquick) {
	Mane.getCmdMap().removeAlias(quick);
	quick = qquick;
	setIcon("[" + quick + "]");
	Mane.getCmdMap().setAlias("channel", quick);
    }

    private void setIcon(String iicon) {
	icon = iicon;
    }

    protected boolean setNsfw(boolean flag) {
	if (flag) {
	    for (String listener : listeners) {
		Pony pony = Ponyville.getPony(listener);
		if (pony.isNsfwBlocked() || !pony.isNsfwAccepted()) {
		    if (pony.getPlayer().isOnline()) {
			if (!leaveChat(pony.getPlayer().getPlayer(), false, true)) {
			    leaveListen(pony.getPlayer().getPlayer(), false);
			}
		    }
		}
	    }
	}
	nsfw = flag;
	return flag;
    }

    /*
     * Bookmark Utils
     */
    protected String parseForLinks(String msg) {
	String msg2 = "";
	String[] msgArray = msg.split(" ");
	for (String s : msgArray) {
	    if (s.matches(regex) || s.startsWith("www.")) {
		String site = "http://mlpf.im/yourls-api.php?signature=2a0114bbbb&action=shorturl&format=simple&url=" + s;
		URL fURL;
		try {
		    fURL = new URL(site);
		    BufferedReader in = new BufferedReader(new InputStreamReader(fURL.openStream()));
		    s = in.readLine();
		    in.close();
		} catch (Exception e) {
		    if (s.startsWith("https")) {
			s = s.replaceFirst("https", "http");
		    }
		    site = "http://mlpf.im/yourls-api.php?signature=2a0114bbbb&action=shorturl&format=simple&url=" + s;
		    try {
			fURL = new URL(site);
			BufferedReader in = new BufferedReader(new InputStreamReader(fURL.openStream()));
			s = in.readLine();
			in.close();
		    } catch (Exception e2) {}
		}
	    }
	    msg2 += s + " ";
	}
	return msg2;
    }

    public ArrayList<Player> getListenerPlayers() {
	ArrayList<Player> players = new ArrayList<Player>();
	for (String string : listeners) {
	    Player player = Bukkit.getPlayerExact(string);
	    if (player != null) {
		players.add(player);
	    }
	}
	return players;
    }
}
