package me.corriekay.pppopp4.modules.chat;

import java.util.Collection;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import me.corriekay.pppopp4.Mane;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;

public class PrivateChannel extends Channel {

    protected static ConcurrentHashMap<String, PrivateChannel> privateChannels = new ConcurrentHashMap<String, PrivateChannel>();
    static {
	Bukkit.getScheduler().runTaskTimer(Mane.getInstance(), new Runnable() {
	    public void run() {
		for (PrivateChannel pc : privateChannels.values()) {
		    pc.saveChannel();
		    long current = System.currentTimeMillis(), last = pc.getLastActivity(), expire = ChannelHandler.getHandler().getData().getExpiretime();
		    if (current - expire > last && pc.getListeners().size() < 1) {
			ChannelHandler.getHandler().retireChannel(pc);
			privateChannels.remove(pc.getName());
		    }
		}
	    }
	}, 20 * 5, 20 * 30);
    }

    public static Collection<PrivateChannel> getChannels() {
	return privateChannels.values();
    }

    private List<String> muted, banned, moderators;
    private String admin, password;
    private long lastActivity;

    protected PrivateChannel(String name, String quick, String icon,
	    ChatColor color, boolean nsfw, ChannelHandler handler,
	    String password, String admin, List<String> moderators,
	    List<String> banned, List<String> muted, long lastActivity) {
	super(name, quick, icon, null, color, nsfw, handler);
	this.muted = muted;
	this.banned = banned;
	this.moderators = moderators;
	this.admin = admin;
	this.password = password;
	this.setLastActivity(lastActivity);
	privateChannels.put(getName(), this);
    }

    protected PrivateChannel(String name, FileConfiguration config,
	    ChannelHandler handler) {
	this(config.getConfigurationSection("privateChannels." + name), handler);
    }

    private PrivateChannel(ConfigurationSection section, ChannelHandler handler) {
	this(section.getName(), section.getString("quick"), section.getString("icon"), ChatColor.valueOf(section.getString("color")), section.getBoolean("nsfw"), handler, section.getString("password"), section.getString("administrator"), section.getStringList("moderators"), section.getStringList("banned"), section.getStringList("muted"), section.getLong("lastActivity"));
    }

    /*
     * Broadcast override
     */
    public String broadcastToChannel(String who, String message, boolean log, boolean rpa) {
	if (!isMuted(who)) {
	    updateLastActivity();
	    return super.broadcastToChannel(who, message, log, rpa);
	} else {
	    return null;
	}
    }

    public void broadcastRawMessage(String message, boolean log) {
	super.broadcastRawMessage(message, log);
	updateLastActivity();
    }

    public void broadcastEmote(String message, String name, boolean log) {
	super.broadcastEmote(message, name, log);
	updateLastActivity();
    }

    /*
     * checker overrides
     */
    public boolean canJoin(Permissible p) {
	if (p instanceof Player && isBanned(((Player) p).getName())) {
	    return false;
	} else {
	    return super.canJoin(p);
	}
    }

    /*
     * Checkers
     */
    public boolean isMuted(String name) {
	return muted.contains(name);
    }

    public boolean isBanned(String name) {
	return banned.contains(name);
    }

    public boolean isModerator(Player player) {
	return isModerator(player.getName());
    }

    public boolean isModerator(String name) {
	return moderators.contains(name) || isAdmin(name);
    }

    public boolean isAdmin(Player player) {
	return isAdmin(player.getName());
    }

    public boolean isAdmin(String name) {
	return name.equals(admin);
    }

    public boolean checkPassword(String challange, String user) {
	if (isAdmin(user) || isModerator(user)) {
	    return true;
	}
	if (password == null) {
	    return true;
	} else if (challange == null && password != null) {
	    return false;
	}
	return challange.equals(password);
    }

    public boolean isPrivateChannel() {
	return true;
    }

    public long getLastActivity() {
	return lastActivity;
    }

    public void updateLastActivity() {
	setLastActivity(System.currentTimeMillis());
    }

    public void setLastActivity(long lastActivity) {
	this.lastActivity = lastActivity;
    }

    public void setColor(ChatColor color) {
	this.color = color;
    }

    public void setPassword(String pw) {
	password = pw;
    }

    public void addModerator(String mod) {
	moderators.add(mod);
    }

    public void removeModerator(String mod) {
	moderators.remove(mod);
    }

    public void setAdministrator(String admin) {
	this.admin = admin;
    }

    public void addBan(String name) {
	banned.add(name);
    }

    public void removeBan(String name) {
	banned.remove(name);
    }

    public boolean toggleMute(String name) {
	if (isMuted(name)) {
	    muted.remove(name);
	    return false;
	}
	muted.add(name);
	return true;
    }

    public void saveChannel() {
	FileConfiguration config = ChannelHandler.getHandler().getConfig();
	ConfigurationSection section = config.getConfigurationSection("privateChannels");
	section = section.createSection(getName());
	section.set("color", color.name());
	section.set("icon", this.getIcon());
	section.set("quick", this.getQuick());
	section.set("administrator", admin);
	section.set("moderators", moderators);
	section.set("banned", banned);
	section.set("muted", muted);
	section.set("password", password);
	section.set("lastActivity", lastActivity);
	section.set("nsfw", isNsfwFlagged());
	ChannelHandler.getHandler().saveConfig();
    }
}
