package me.corriekay.pppopp4.modules.chat;

import java.io.File;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.command.PonyCommandMap;
import me.corriekay.pppopp4.events.ChannelMessageEvent;
import me.corriekay.pppopp4.events.JoinEvent;
import me.corriekay.pppopp4.events.PrivateMessageEvent;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.UnhandledCommandException;
import me.corriekay.pppopp4.modules.ponymanager.PonyManager;
import me.corriekay.pppopp4.modules.ponymanager.PonyPermission;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.modules.remoteponyadmin.RemotePony;
import me.corriekay.pppopp4.utilities.PonyLogger;
import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

public class ChannelHandler extends PSCmdExe {

    protected final static String regex = "^(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]";
    private final ChannelData data = new ChannelData();
    private static ChannelHandler handler;
    private final HashSet<String> unavailableChannelNames = new HashSet<String>();

    public ChannelHandler() {
	super("ChannelHandler", Ponies.PinkiePie, "join", "leave", "channel", "say", "me", "mute", "pm", "r", "silence", "silenced", "channelcolor", "toggleplayernsfw", "pc", "chatmacros");
	handler = this;
	if (loadConfig("channels.yml")) {
	    getConfig().createSection("publicChannels");
	    getConfig().createSection("privateChannels");
	    getConfig().set("publicChannels.general.color", "WHITE");
	    getConfig().set("publicChannels.general.icon", "[general]");
	    getConfig().set("publicChannels.general.default", true);
	    getConfig().set("publicChannels.general.quick", "g");
	    getConfig().set("publicChannels.general.nsfw", false);
	}
	data.setExpiretime(getConfig().getLong("expiretime", 1000 * 60 * 60 * 24 * 2));
	initializeChannels();
	initializePlayers();
	saveConfig();
    }

    private void initializeChannels() {
	PonyCommandMap pcm = Mane.getCmdMap();
	for (String channel : getConfig().getConfigurationSection("publicChannels").getKeys(false)) {
	    ChatColor color = ChatColor.valueOf(getConfig().getString("publicChannels." + channel + ".color"));
	    String icon = getConfig().getString("publicChannels." + channel + ".icon");
	    String permission = getConfig().getString("publicChannels." + channel + ".permission", null);
	    String quick = getConfig().getString("publicChannels." + channel + ".quick");
	    boolean setDefault = getConfig().getBoolean("publicChannels." + channel + ".default", false);
	    boolean nsfw = getConfig().getBoolean("publicChannels." + channel + ".nsfw");
	    if (setDefault) {
		if (data.getDefaultChannel() != null) {
		    Bukkit.getLogger().severe("Channel " + channel + " attempted to register itsself as default, but there is already a default channel!: " + data.getDefaultChannel().getName());
		    continue;
		}
		if (permission != null) {
		    Bukkit.getLogger().severe("Default channel cannot have a permission! Permission removed from channel " + channel);
		    permission = null;
		}
	    }
	    Channel c = new Channel(channel, quick, icon, permission, color, nsfw, this);
	    if (setDefault) {
		data.setDefaultChannel(c);
	    }
	    data.addChannel(c);
	    pcm.setAlias("channel", c.getQuick());
	    pcm.setAlias("channel", c.getName());
	}
	for (String pcname : getConfig().getConfigurationSection("privateChannels").getKeys(false)) {
	    Channel pc = new PrivateChannel(pcname, getConfig(), this);
	    data.addChannel(pc);
	    pcm.setAlias("channel", pc.getQuick());
	    pcm.setAlias("channel", pc.getName());
	}
	if (data.getDefaultChannel() == null) {
	    throw new NullPointerException("Null Default Channel. Please provide a default channel");
	}
    }

    private void initializePlayers() {
	for (Player player : Bukkit.getOnlinePlayers()) {
	    initializePlayer(player);
	}
    }

    private void initializePlayer(Player player) {
	Pony pony = Ponyville.getPony(player);
	setMuted(player.getName(), pony.isMuted());
	Channel chan = getChannel(pony.getChatChannel());
	if (chan != null && !chan.joinChat(player, false)) {
	    data.getDefaultChannel().joinChat(player, false);
	}
	for (String listenChannel : pony.getListeningChannels()) {
	    Channel listen = getChannel(listenChannel);
	    if (listen != null) {
		listen.joinListening(player, false);
	    }
	}
	HashSet<String> listenChans = new HashSet<String>();
	for (Channel c : getListeningChannels(player)) {
	    listenChans.add(c.getName());
	}
	pony.setListenChannels(listenChans);
	pony.save();
	data.addIgnores(player.getName(), pony.getSilenced());
    }

    public void retireChannel(final PrivateChannel pc) {
	unavailableChannelNames.add(pc.getName());
	Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), new Runnable() {
	    public void run() {
		System.out.println("STARTING CHANNEL RETIREMENT TASK - Channel " + pc.getName());
		for (String name : Ponyville.getAllPonyNames()) {
		    Pony pony = Ponyville.getPony(name);
		    if (pony != null) {
			if (pony.getPlayer().isOnline()) {
			    Player player = (Player) pony.getPlayer();
			    if (pc.isChatting(player) || pc.isListening(player)) {
				sendMessage(player, "The channel you were in, " + pc.getName() + ", has closed down! If you're no longer in a channel, please join a new channel!");
				pc.leaveChat(player, false, true);
				ArrayList<Channel> c = getListeningChannels(player);
				if (c.size() >= 1) {
				    c.get(0).joinChat(player, true);
				}
			    }
			}
			if (pony.getChatChannel().equals(pc.getName())) {
			    pony.setChatChannel(data.getDefaultChannel().getName());
			}
			HashSet<String> lcs = pony.getListeningChannels();
			lcs.remove(pc.getName());
			pony.setListenChannels(lcs);
			pony.save();
		    }
		}
		unavailableChannelNames.remove(pc.getName());
		System.out.println("CHANNEL RETIREMENT TASK COMPLETE - Channel " + pc.getName());
	    }
	});
	ChannelHandler handler = getHandler();
	handler.getData().removeChannel(pc);
	handler.getConfig().set("privateChannels." + pc.getName(), null);
	handler.saveConfig();
	PonyCommandMap map = Mane.getCmdMap();
	map.removeAlias(pc.getName());
	map.removeAlias(pc.getQuick());
	PrivateChannel.privateChannels.remove(pc.getName());
    }

    /*
     * Bookmark Checkers
     */
    public boolean doSendMessage(String sender, String reciever) {
	boolean senderBlock, recieverBlock;
	senderBlock = data.getIgnores().get(sender).contains(reciever);
	recieverBlock = data.getIgnores().get(reciever).contains(sender);
	return !(senderBlock || recieverBlock);
    }

    public boolean isMuted(Player player) {
	return data.getMuted().contains(player.getName());
    }

    /*
     * Bookmark Getters
     */

    public ChatColor getPlayerColor(String name, Channel channel) {
	try {
	    return data.getChannelColors().get(name).get(channel);
	} catch (NullPointerException e) {
	    return null;
	}
    }

    public Channel getChannel(String name) {
	for (Channel chan : data.getChannels().values()) {
	    if (chan.getName().equalsIgnoreCase(name) || chan.getQuick().equalsIgnoreCase(name)) {
		return chan;
	    }
	}
	return null;
    }

    public Channel getChattingChannel(Player player) {
	for (Channel c : data.getChannels().values()) {
	    if (c.isChatting(player)) {
		return c;
	    }
	}
	return null;
    }

    public ArrayList<Channel> getListeningChannels(Player player) {
	ArrayList<Channel> channelz = new ArrayList<Channel>();
	for (Channel channel : data.getChannels().values()) {
	    if (channel.isListening(player)) {
		channelz.add(channel);
	    }
	}
	return channelz;
    }

    public static ChannelHandler getHandler() {
	return handler;
    }

    public ChannelData getData() {
	return data;
    }

    /*
     * Bookmark Setters
     */
    public void setMuted(String name, boolean flag) {
	if (flag) {
	    data.getMuted().add(name);
	} else {
	    data.getMuted().remove(name);
	}
    }

    private void updatePMs(String name, String name2) {
	data.getPmTargets().put(name, name2);
	data.getPmTargets().put(name2, name);
    }

    private void updateChannels(Player player) {
	Pony pony = Ponyville.getPony(player);
	Channel chan = getChattingChannel(player);
	if (chan != null) {
	    pony.setChatChannel(chan.getName());
	} else {
	    pony.setChatChannel("null");
	}
	HashSet<String> channels = new HashSet<String>();
	for (Channel chann : getListeningChannels(player)) {
	    channels.add(chann.getName());
	}
	pony.setListenChannels(channels);
	pony.save();
    }

    /*
     * Bookmark Commands
     */
    public boolean optionalCommand(CommandSender sender, CommandMetadata cmd, String label, String[] args) throws UnhandledCommandException {
	switch (cmd) {
	case mute: {
	    OfflinePlayer target = getOnlineOfflinePlayer(args[0], sender);
	    if (target != null) {
		mute(sender, target);
	    }
	    return true;
	}
	case toggleplayernsfw: {
	    OfflinePlayer target = getOnlineOfflinePlayer(args[0], sender);
	    if (target != null) {
		if (togglePlayerNsfw(Ponyville.getPony(target))) {
		    sendMessage(sender, "This player is now blocked from joining nsfw channels!");
		} else {
		    sendMessage(sender, "This player is now allowed to join nsfw channels!");
		}
	    }
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    public boolean playerCommand(final Player player, CommandMetadata cmd, String label, String[] args) throws UnhandledCommandException {
	switch (cmd) {
	case channel: {
	    if (label.equals("channel")) {
		sendMessage(player, channel(player));
	    } else {
		if (args.length <= 0) {
		    sendMessage(player, notEnoughArgs);
		    return true;
		}
		quickMessage(player, Utils.joinStringArray(args, " "), label);
	    }
	    return true;
	}
	case join: {
	    final Channel channel = getChannel(args[0]);
	    if (channel == null) {
		sendMessage(player, "Uh oh, i couldnt find that channel!");
		return true;
	    }
	    if (channel instanceof PrivateChannel && !player.hasPermission(PonyPermission.IS_OPERATOR.getPerm())) {
		PrivateChannel pc = (PrivateChannel) channel;
		String pw = null;
		if (args.length >= 2) {
		    pw = args[1];
		}
		if (!pc.checkPassword(pw, player.getName())) {
		    sendMessage(player, "Incorrect password! type /join <channel> <password>!");
		    return true;
		}
	    }
	    final Pony pony = Ponyville.getPony(player);
	    if (channel.isNsfwFlagged() && !pony.isNsfwAccepted()) {
		Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), new Runnable() {
		    @Override
		    public void run() {
			String answer = questioner.ask(player, Ponies.PinkiePie.says() + "The channel you're attempting to join is flagged as NSFW. Are you sure you wish to join this channel?", "yes", "no");
			if (answer.equals("no")) {
			    sendSyncMessage(player, "Okay! You wont be joining this channel!");
			    return;
			}
			answer = questioner.ask(player, Ponies.PinkiePie.says() + "Just to reiterate: This channel is flagged as NSFW!! These channels are meant to have an adult audience who can talk about adult topics maturely. You " + ChatColor.BOLD + "WILL" + ChatColor.RESET + "" + ChatColor.LIGHT_PURPLE + " see violence, rudeness, possibly gore, and DEFINITELY porn. " + ChatColor.DARK_RED + ChatColor.BOLD + " You have been warned, and should you agree, you WILL NOT BE WARNED AGAIN. For this channel and any other channel. Ever. Are you sure you wish to join this channel?.", "yes", "no");
			if (answer.equals("no")) {
			    sendSyncMessage(player, "Okay! You wont be joining this channel!");
			    return;
			}
			Bukkit.getScheduler().runTask(Mane.getInstance(), new Runnable() {
			    public void run() {
				sendMessage(player, "Oh I just KNEW you'd make the right choice! You make this pretty pink party pony pleasently pleased~ ;3\nIts a real party in here!");
				pony.setNsfwAccept(true);
				pony.save();
				join(player, channel);
			    }
			});
		    }
		});
		return true;
	    }
	    join(player, channel);
	    return true;
	}
	case leave: {
	    Channel channel;
	    if (args.length > 0) {
		channel = getChannel(args[0]);
	    } else {
		channel = getChattingChannel(player);
	    }
	    if (channel == null) {
		sendMessage(player, "Silly, you're not in that channel!");
		return true;
	    }
	    leave(player, channel);
	    return true;
	}
	case me: {
	    if (isMuted(player)) {
		sendMessage(player, "Naughty naughty! You're muted!");
		return true;
	    }
	    Channel c = getChattingChannel(player);
	    if (c == null) {
		sendMessage(player, "You're not in a channel!");
		return true;
	    }
	    c.broadcastEmote(Utils.joinStringArray(args, " "), player.getDisplayName(), true);
	    return true;
	}
	case chatmacros: {
	    Channel c = getChattingChannel(player);
	    if (isMuted(player)) {
		sendMessage(player, "Naughty naughty! You're muted!");
		return true;
	    }
	    if (c == null) {
		sendMessage(player, "You're not in a channel!");
	    }
	    if (label.equals("chatmacros")) {
		String sendme = "Heres a list of chatmacros!: ";
		for (String cmmd : cmd.getLabels()) {
		    sendme += "/" + cmmd + ", ";
		}
		sendMessage(player, sendme.substring(0, sendme.length() - 2));
	    } else {
		String extra = "";
		if (args.length > 0) {
		    extra = " " + Utils.joinStringArray(args, " ");
		}
		if (label.equals("wat")) {
		    c.broadcastToChannel(player.getName(), "ಠ_ಠ" + extra, true, true);
		} else if (label.equals("tableflip") || label.equals("fliptable")) {
		    c.broadcastToChannel(player.getName(), "(╯°□°）╯︵ ┻━┻" + extra, true, true);
		} else if (label.equals("glasses")) {
		    c.broadcastToChannel(player.getName(), "•_•)  ( •_•)>⌐■-■  (⌐■_■)" + extra, true, true);
		} else {
		    sendMessage(player, "That macro has not been implemented yet!");
		}
	    }
	    return true;
	}
	case pm: {
	    if (isMuted(player)) {
		sendMessage(player, "Naughty naughty! You're muted!");
		return true;
	    }
	    Player receiver = getOnlinePlayer(args[0], player);
	    if (receiver == null) {
		return true;
	    }
	    if (!pm(player, receiver, Utils.joinStringArray(args, " ", 1))) {
		sendMessage(player, "The private message didnt go through! Most likely that player has blocked you :c");
	    }
	    return true;
	}
	case r: {
	    if (isMuted(player)) {
		sendMessage(player, "Naughty naughty! You're muted!");
		return true;
	    }
	    Player target = null;
	    String tarstring = data.getPmTargets().get(player.getName());
	    if (tarstring == null) {
		sendMessage(player, "Hmm, looks like you dont have a player to respond to! They most likely went offline, or you've never had anyone message you!");
		return true;
	    }
	    target = Bukkit.getPlayerExact(tarstring);
	    if (target == null) {
		sendMessage(player, cantFindPlayer);
		return true;
	    }
	    if (!pm(player, target, Utils.joinStringArray(args, " "))) {
		sendMessage(player, "The private message didnt go through! Most likely that player has blocked you :c");
	    }
	    return true;
	}
	case silence: {
	    OfflinePlayer target = getOnlineOfflinePlayer(args[0], player);
	    if (target != null) {
		silence(player, target);
	    }
	    return true;
	}
	case silenced: {
	    silenced(player);
	    return true;
	}
	case channelcolor: {
	    Channel c = getChattingChannel(player);
	    ChatColor cc = c.color;
	    try {
		cc = ChatColor.valueOf(args[0].toUpperCase());
	    } catch (IllegalArgumentException e) {
		sendMessage(player, "Thats not a real color!");
		return true;
	    }
	    if (args.length < 1) {
		c = getChannel(args[1]);
		if (c == null) {
		    sendMessage(player, "I can't find that channel!");
		    return true;
		}
	    }
	    Pony pony = Ponyville.getPony(player);
	    pony.setChannelColor(c.getName(), cc);
	    pony.save();
	    data.addChannelColor(player.getName(), c, cc);
	    sendMessage(player, "You've changed the channel color to " + cc + cc.name() + ChatColor.LIGHT_PURPLE + " for channel " + c.getName() + "!");
	    return true;
	}
	case pc: {
	    if (args.length > 1 && args[0].equals("createchannel")) {
		for (PrivateChannel pc : PrivateChannel.getChannels()) {
		    if (pc.isAdmin(player)) {
			sendMessage(player, "You already have a private channel: " + pc.getName() + "!");
			return true;
		    }
		}
		if (unavailableChannelNames.contains(args[1])) {
		    sendMessage(player, "That channel name is unavailable at the moment, as we are preparing it for you. Please re-issue the command in a few minutes!");
		    return true;
		}
		if (createChannel(args[1], player)) {
		    sendMessage(player, "Private channel created! Type /join " + args[1] + " to join that channel!");
		} else {
		    sendMessage(player, "That channel name has already been taken!");
		}
		return true;
	    }
	    if (args[0].equals("help")) {
		sendMessage(player, "To use a private channel command (save for createchannel and help) you must be inside of the channel you wish to modify! Here are a list of private channel subcommands (use them as such: /pc <subcommand>): createchannel, setquick (this is the channel quick message code, for instance, Equestrias quick is \"eq\"), setcolor, setpassword, setmoderator, removemoderator, setadmin, ban, kick, unban, mute, closechannel, removepassword, help. ");
		return true;
	    }
	    Channel channel = getChattingChannel(player);
	    if (channel == null) {
		sendMessage(player, "You're not in a channel!");
		return true;
	    }
	    if (!(channel instanceof PrivateChannel)) {
		sendMessage(player, "You're not in a private channel!");
		return true;
	    }
	    PrivateChannel pc = (PrivateChannel) channel;
	    String arg = args[0];
	    if (pc.isAdmin(player)) {
		if (arg.equals("setquick")) {
		    if (!checkArgs(player, args, 2)) {
			return true;
		    }
		    if (!setQuick(args[1], pc)) {
			sendMessage(player, "That quick has already been claimed, or is a command alias!");
		    } else {
			sendMessage(player, "Quick for this channel has been changed to " + pc.getQuick() + "!");
		    }
		    return true;
		}
		if (arg.equals("setcolor")) {
		    if (!checkArgs(player, args, 2)) {
			return true;
		    }
		    try {
			ChatColor c = ChatColor.valueOf(args[1].toUpperCase());
			setColor(c, pc);
			sendMessage(player, "Channel color changed to " + c + c.name() + "!");
		    } catch (IllegalArgumentException e) {
			sendMessage(player, "That color cannot be found!");
		    }
		    return true;
		}
		if (arg.equals("setpassword")) {
		    if (!checkArgs(player, args, 2)) {
			return true;
		    }
		    setPassword(args[1], pc);
		    sendMessage(player, "password changed!");
		    return true;
		}
		if (arg.equals("setmoderator")) {
		    if (!checkArgs(player, args, 2)) {
			return true;
		    }
		    OfflinePlayer target = getOnlineOfflinePlayer(args[1], player);
		    if (target == null) {
			return true;
		    }
		    if (setModerator(player, target.getName(), pc)) {
			sendMessage(player, "You've added " + target.getName() + " as a moderator to this channel!");
		    }
		    return true;
		}
		if (arg.equals("removemoderator")) {
		    if (!checkArgs(player, args, 2)) {
			return true;
		    }
		    OfflinePlayer target = getOnlineOfflinePlayer(args[1], player);
		    if (target == null) {
			return true;
		    }
		    if (removeModerator(player, target.getName(), pc)) {
			sendMessage(player, "You've removed " + target.getName() + " as a moderator to this channel!");
		    }
		    return true;
		}
		if (arg.equals("setadmin")) {
		    if (!checkArgs(player, args, 2)) {
			return true;
		    }
		    final Player newAdmin = getOnlinePlayer(args[1], player);
		    if (newAdmin == null) {
			return true;
		    }
		    final PrivateChannel fpc = pc;
		    final Player finalplayer = player;
		    Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), new Runnable() {
			public void run() {
			    String answer = questioner.ask(finalplayer, Ponies.PinkiePie.says() + "Are you ABSOLUTELY sure you want to give this player adminship? We wont be able to reverse it!", "yes", "no");
			    if (answer.equals("no")) {
				sendSyncMessage(finalplayer, "Aborting admin change!");
			    }
			    if (answer.equals("yes")) {
				Bukkit.getScheduler().runTask(Mane.getInstance(), new Runnable() {
				    public void run() {
					if (!setAdmin(newAdmin, fpc)) {
					    sendMessage(finalplayer, "That user is already the admin!");
					} else {
					    sendMessage(finalplayer, "You've given " + newAdmin.getDisplayName() + ChatColor.LIGHT_PURPLE + " adminship of this channel!");
					}
				    }
				});
			    }
			}
		    });
		    return true;
		}
		if (arg.equals("closechannel")) {
		    final Player fp = player;
		    final PrivateChannel c = pc;
		    Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), new Runnable() {
			public void run() {
			    String answer = questioner.ask(fp, Ponies.PinkiePie.says() + "You are about to delete channel " + c.getName() + ". Are you sure you want to delete this channel?", "yes", "no");
			    if (answer.equals("no")) {
				sendSyncMessage(fp, "Channel delete aborted!");
				return;
			    }
			    if (answer.equals("yes")) {
				sendSyncMessage(fp, "Deleting channel!");
				retireChannel(c);
				return;
			    }
			}
		    });
		    return true;
		}
		if (arg.equals("removepassword")) {
		    removePassword(pc);
		    sendMessage(player, "You've removed the password for channel " + pc.getName() + "!");
		    return true;
		}
	    }
	    if (pc.isModerator(player)) {
		if (arg.equals("ban")) {
		    if (!checkArgs(player, args, 2)) {
			return true;
		    }
		    OfflinePlayer target = getOnlineOfflinePlayer(args[1], player);
		    if (!ban(target.getName(), pc)) {
			sendMessage(player, "Unable to ban that person? Are they already banned? Are they a moderator?");
		    }
		    return true;
		}
		if (arg.equals("kick")) {
		    if (!checkArgs(player, args, 2)) {
			return true;
		    }
		    Player kickme = getOnlinePlayer(args[1], player);
		    if (kickme == null) {
			return true;
		    }
		    if (!kick(kickme, pc)) {
			sendMessage(player, "That player is not in that channel, or cannot be kicked!");
		    }
		    return true;
		}
		if (arg.equals("unban")) {
		    if (!checkArgs(player, args, 2)) {
			return true;
		    }
		    OfflinePlayer target = getOnlineOfflinePlayer(args[1], player);
		    if (target == null) {
			return true;
		    }
		    if (!unban(target.getName(), pc)) {
			sendMessage(player, "That player is not banned!");
		    } else {
			sendMessage(player, "You've unbanned " + target.getName() + " from this channel!");
		    }
		    return true;
		}
		if (arg.equals("mute")) {
		    if (!checkArgs(player, args, 2)) {
			return true;
		    }
		    OfflinePlayer target = getOnlineOfflinePlayer(args[1], player);
		    if (target == null) {
			return true;
		    }
		    boolean muted = privateChannelMute(target.getName(), pc);
		    sendMessage(player, "You've " + (muted ? "muted " : "unmuted ") + target.getName() + " from this channel!");
		    if (target.isOnline()) {
			sendMessage(target.getPlayer(), "You've been " + (muted ? "muted " : "unmuted ") + " from the private channel " + pc.getName() + "!");
		    }
		    return true;
		}
		if (arg.equals("setnsfw")) {
		    if (!checkArgs(player, args, 2)) {
			return true;
		    }
		    boolean bool = Boolean.parseBoolean(args[1]);
		    if (channel.setNsfw(bool)) {
			channel.broadcastRawMessage("This channel has now been flagged as NSFW. NSFW emotes and topics are now allowed!", false);
		    } else {
			channel.broadcastRawMessage("This channel has now been flagged as SFW. NSFW emotes and topics are now disallowed!", false);
		    }
		    ((PrivateChannel) channel).saveChannel();
		    return true;
		}
	    }
	    sendMessage(player, "It doesnt look like you have permission to do that!");
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    public boolean remotePonyCommand(RemotePony rp, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case channel: {
	    if (args.length <= 0) {
		sendMessage(rp, notEnoughArgs);
		return true;
	    }
	    String msg = Utils.joinStringArray(args, " ");
	    Pony pony = rp.getPony();
	    if (pony.getListeningChannels().contains(label)) {
		Channel c = getChannel(label);
		if (c == null) {
		    sendMessage(rp, "Uh oh, i couldnt find that channel!");
		    return true;
		}
		c.broadcastToChannel(rp.getName(), msg, true, true);
		return true;
	    } else {
		sendMessage(rp, "You're not chatting in that channel!");
		return true;
	    }
	}
	default: {
	    sendMessage(rp, "Sorry! that command hasn't been implemented yet!");
	    return true;
	}
	}
    }

    public boolean consoleCommand(ConsoleCommandSender console, CommandMetadata cmd, String label, String[] args) throws UnhandledCommandException {
	switch (cmd) {
	case me: {
	    data.getDefaultChannel().broadcastEmote(Utils.joinStringArray(args, " "), "Server", true);
	    return true;
	}
	case say: {
	    return true;
	}
	case channel: {
	    if (args.length < 1) {
		sendMessage(console, notEnoughArgs);
		return false;
	    } else if (label.equals("channel")) {
		sendMessage(console, channel(console));
	    } else {
		Channel c = getChannel(label);
		if (c == null) {
		    sendMessage(console, "Wat. Channel not found D:");
		} else {
		    c.broadcastToChannel("Server", Utils.joinStringArray(args, " "), true, true);
		}
	    }
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    public String channel(Player player) {
	String channels = "Heres a list of channels!\n";
	Iterator<Channel> channelIt = data.getChannels().values().iterator();
	while (channelIt.hasNext()) {
	    Channel chan = channelIt.next();
	    channels += (chan.isNsfwFlagged() ? " " + ChatColor.DARK_RED + "(nsfw)" : " ") + chan.getColor(player.getName()) + chan;
	    if (channelIt.hasNext()) {
		channels += ",";
	    }
	}
	return channels;
    }

    public String channel(ConsoleCommandSender console) {
	String channels = "Heres a list of channels!\n";
	Iterator<Channel> channelIt = data.getChannels().values().iterator();
	while (channelIt.hasNext()) {
	    Channel chan = channelIt.next();
	    channels += (chan.isNsfwFlagged() ? " " + ChatColor.DARK_RED + "(nsfw)" : " ") + chan.color + chan;
	    if (channelIt.hasNext()) {
		channels += ",";
	    }
	}
	return channels;
    }

    public boolean quickMessage(Player player, String message, String quick) {
	Channel channel = getChannel(quick);
	if (channel == null) {
	    sendMessage(player, "Uh oh, i couldnt find that channel!");
	    return false;
	}
	if (channel.isListening(player)) {
	    chat(channel, player, message, true);
	    return true;
	} else {
	    sendMessage(player, "You're not chatting in that channel!");
	    return false;
	}
    }

    public boolean join(Player player, Channel channel) {
	if (channel.isChatting(player)) {
	    sendMessage(player, "Pfft, you're already chatting there silly!");
	    return false;
	}
	if (channel.canJoin(player)) {
	    try {
		getChattingChannel(player).leaveChat(player, false, false);
	    } catch (NullPointerException e) {}
	    channel.joinChat(player, true);
	    updateChannels(player);
	    return true;
	} else {
	    sendMessage(player, "You can't join that channel!");
	    return false;
	}
    }

    public boolean leave(Player player, Channel channel) {
	if (channel == null) {
	    return false;
	}
	boolean chatting = channel.isChatting(player);
	if (chatting) {
	    channel.leaveChat(player, true, true);
	    ArrayList<Channel> chans = getListeningChannels(player);
	    if (chans.size() == 0) {
		sendMessage(player, "You're no longer in any chat channels!");
	    } else {
		Channel chan = chans.get(0);
		chan.joinChat(player, true);
	    }
	} else {
	    channel.leaveListen(player, true);
	}
	updateChannels(player);
	return true;
    }

    public boolean pm(Player sender, Player reciever, String message) {
	if (!doSendMessage(sender.getName(), reciever.getName())) {
	    return false;
	}
	PrivateMessageEvent pme = new PrivateMessageEvent(sender, reciever, message);
	Bukkit.getPluginManager().callEvent(pme);
	if (!pme.isCancelled()) {
	    sender.sendMessage(ChatColor.RED + "[PM][You > " + reciever.getDisplayName() + ChatColor.RED + "]" + ChatColor.GRAY + ": " + pme.getMsg());
	    reciever.sendMessage(ChatColor.RED + "[PM][" + sender.getDisplayName() + ChatColor.RED + " > You]" + ChatColor.GRAY + ": " + pme.getMsg());
	    this.updatePMs(sender.getName(), reciever.getName());
	    return true;
	} else {
	    return false;
	}
    }

    public boolean silence(Player silencer, OfflinePlayer silenced) {
	String group = PonyManager.ponyManager.getGroup(silenced.getName()).getName();
	if (group.equalsIgnoreCase("admin") || group.equalsIgnoreCase("opony")) {
	    sendMessage(silencer, "You can't silence that pony!");
	    return true;
	}
	Pony pony = Ponyville.getPony(silencer);
	HashSet<String> sl = pony.getSilenced();
	if (sl.contains(silenced.getName())) {
	    sendMessage(silencer, "Unsilencing player " + silenced.getName() + "!");
	    sl.remove(silenced.getName());
	} else {
	    sendMessage(silencer, "Silencing player " + silenced.getName() + "!");
	    sl.add(silenced.getName());
	}
	pony.setSilenced(sl);
	data.getIgnores().put(silencer.getName(), sl);
	pony.save();
	return true;
    }

    public boolean silenced(Player player) {
	HashSet<String> silenced = Ponyville.getPony(player).getSilenced();
	if (silenced.isEmpty()) {
	    sendMessage(player, "Silly willy, you havnt silenced anyone!");
	} else {
	    Iterator<String> s = silenced.iterator();
	    StringBuilder sb = new StringBuilder();
	    String p = null;
	    while (s.hasNext()) {
		p = s.next();
		sb.append(p);
		if (s.hasNext()) {
		    sb.append(", ");
		}
	    }
	    sendMessage(player, "These are the players you have silenced!\n" + sb);
	}
	return true;
    }

    public boolean channelcolor() {
	return true;
    }

    public boolean mute(CommandSender sender, OfflinePlayer target) {
	Pony pony = Ponyville.getPony(target);
	if (pony.isMuted()) {
	    pony.setMuted(false);
	    data.setMuted(pony.getName(), false);
	    if (target.isOnline()) {
		sendMessage(target.getPlayer(), "You're a good pony! You've been unmuted!");
	    }
	    sendMessage(sender, "Yay! you unmuted " + target.getName() + ChatColor.LIGHT_PURPLE + "!");
	} else {
	    pony.setMuted(true);
	    data.setMuted(pony.getName(), true);
	    if (target.isOnline()) {
		sendMessage(target.getPlayer(), "Naughty naughty! You've been muted!");
	    }
	    sendMessage(sender, "Awh, You muted " + target.getName() + ChatColor.LIGHT_PURPLE + "...");
	}
	pony.save();
	return true;
    }

    public boolean say() {
	return true;
    }

    /*
     * Bookmark channel commands
     */
    public boolean createChannel(String name, Player player) {
	if (!Mane.getCmdMap().checkAliasAvailable(name)) {
	    return false;
	}
	Channel c = getChannel(name);
	if (c != null) {
	    return false;
	}
	List<String> moderators = new ArrayList<String>();
	moderators.add(player.getName());
	PrivateChannel pc = new PrivateChannel(name, name, "[" + name + "]", ChatColor.WHITE, false, this, null, player.getName(), moderators, new ArrayList<String>(), new ArrayList<String>(), System.currentTimeMillis());
	pc.saveChannel();
	data.addChannel(pc);
	PonyCommandMap pcm = Mane.getCmdMap();
	pcm.setAlias("channel", pc.getName());
	pcm.setAlias("channel", pc.getQuick());
	return true;
    }

    public boolean setColor(ChatColor color, PrivateChannel c) {
	c.setColor(color);
	getConfig().set("privateChannels." + c.getName() + ".color", color.name());
	saveConfig();
	return true;
    }

    public boolean setPassword(String pw, PrivateChannel c) {
	c.setPassword(pw);
	c.saveChannel();
	return true;
    }

    public boolean setModerator(Player setter, String mod, PrivateChannel c) {
	if (c.isAdmin(mod)) {
	    sendMessage(setter, mod + " is the admin!");
	    return false;
	}
	if (c.isModerator(mod)) {
	    sendMessage(setter, mod + " is already a moderator!");
	    return false;
	} else {
	    c.addModerator(mod);
	    c.saveChannel();
	    Player player = Bukkit.getPlayerExact(mod);
	    if (player != null) {
		sendMessage(player, "You've been promoted to moderator for channel " + c.getName() + "!");
	    }
	    return true;
	}
    }

    public boolean removeModerator(Player setter, String mod, PrivateChannel c) {
	if (c.isAdmin(mod)) {
	    sendMessage(setter, mod + " is the admin!");
	    return false;
	}
	if (!c.isModerator(mod)) {
	    sendMessage(setter, mod + " isn't a moderator!");
	    return false;
	} else {
	    c.removeModerator(mod);
	    c.saveChannel();
	    Player player = Bukkit.getPlayerExact(mod);
	    if (player != null) {
		sendMessage(player, "You've been demoted from moderator for channel " + c.getName() + "!");
	    }
	    return true;
	}
    }

    public boolean removePassword(PrivateChannel c) {
	c.setPassword(null);
	c.saveChannel();
	return true;
    }

    public boolean setAdmin(Player player, PrivateChannel c) {
	if (c.isAdmin(player)) {
	    return false;
	}
	c.setAdministrator(player.getName());
	c.saveChannel();
	sendMessage(player, "Congratulations! You're now the admin for the channel " + c.getName() + "!");
	return true;
    }

    public boolean ban(String name, PrivateChannel c) {
	if (c.isAdmin(name) || c.isModerator(name) || c.isBanned(name)) {
	    return false;
	}
	c.addBan(name);
	c.saveChannel();
	Player player = Bukkit.getPlayerExact(name);
	if (player != null) {
	    c.leaveChat(player, false, true);
	    sendMessage(player, "You were banned from channel " + c.getName() + "!");
	} else {
	    Pony pony = Ponyville.getPony(name);
	    if (pony != null) {
		if (pony.getChatChannel().equals(c.getName())) {
		    pony.setChatChannel(data.getDefaultChannel().getName());
		}
		HashSet<String> set = pony.getListeningChannels();
		set.remove(c.getName());
		pony.setListenChannels(set);
		pony.save();
	    }
	}
	c.broadcastRawMessage(name + "was banned from this channel!", true);
	return true;
    }

    public boolean kick(Player player, PrivateChannel c) {
	if (c.isModerator(player.getName()) || c.isAdmin(player.getName())) {
	    return false;
	}
	if (c.isListening(player) || c.isChatting(player)) {
	    c.leaveChat(player, false, true);
	    sendMessage(player, "You were kicked from channel " + c.getName() + "!");
	    c.broadcastRawMessage(player.getDisplayName() + " was kicked from the channel!", true);
	    ArrayList<Channel> chans = getListeningChannels(player);
	    if (chans.size() > 0) {
		chans.get(0).joinChat(player, true);
	    }
	    return true;
	}
	return false;
    }

    public boolean unban(String name, PrivateChannel c) {
	if (c.isBanned(name)) {
	    c.removeBan(name);
	    c.saveChannel();
	    return true;
	}
	return false;
    }

    public boolean privateChannelMute(String name, PrivateChannel c) {
	boolean returnme = c.toggleMute(name);
	c.saveChannel();
	return returnme;
    }

    public boolean setQuick(String quick, PrivateChannel c) {
	if (!Mane.getCmdMap().checkAliasAvailable(quick)) {
	    return false;
	}
	Channel chan = getChannel(quick);
	if (chan != null && !chan.equals(c)) {
	    return false;
	}
	c.setQuick(quick);
	c.saveChannel();
	return true;
    }

    public boolean togglePlayerNsfw(Pony pony) {
	boolean nsfw = !pony.isNsfwBlocked();
	if (nsfw) {
	    if (pony.getPlayer().isOnline()) {
		Player player = pony.getPlayer().getPlayer();
		for (Channel c : getListeningChannels(player)) {
		    System.out.println("Checking channel " + c.getName());
		    if (c.isNsfwFlagged()) {
			c.leaveChat(player, false, true);
		    }
		}
	    }
	}
	pony.setNsfwBlocked(nsfw);
	pony.save();
	return nsfw;
    }

    /*
     * Bookmark Util methods
     */
    public boolean chat(Channel channel, Player player, String message, boolean log) {
	if (isMuted(player)) {
	    sendMessage(player, "Naughty naughty! You're muted!");
	    return false;
	}
	if (channel == null) {
	    sendMessage(player, "You're not chatting in a channel! Get a list of channels (/channel) and join one (/join <channel>)!");
	    return false;
	}
	ChannelMessageEvent cme = new ChannelMessageEvent(message, channel.getName(), player);
	Bukkit.getPluginManager().callEvent(cme);
	if (channel.broadcastToChannel(player.getName(), cme.getMessage(), true, false) == null && channel.isPrivateChannel()) {
	    sendMessage(player, "You're muted from that channel!");
	    return false;
	}
	return true;
    }

    /*
     * Bookmark Listeners
     */
    @PonyEvent(priority = EventPriority.LOW)
    public void onChat(AsyncPlayerChatEvent event) {
	if (event.isCancelled()) {
	    return;
	}
	event.setCancelled(true);
	Player player = event.getPlayer();
	Channel channel = getChattingChannel(player);
	chat(channel, player, event.getMessage(), true);
    }

    @PonyEvent
    public void onJoin(JoinEvent event) {
	if (event.isJoining()) {
	    initializePlayer(event.getPlayer());
	}
    }

    @PonyEvent
    public void onQuit(QuitEvent event) {
	Player player = event.getPlayer();
	data.getPmTargets().remove(player.getName());
	for (String key : data.getPmTargets().keySet()) {
	    String value = data.getPmTargets().get(key);
	    if (value == null) {
		continue;
	    }
	    if (value.equals(player.getName())) {
		data.getPmTargets().put(key, null);
	    }
	}
	if (event.isQuitting()) {
	    for (Channel chan : data.getChannels().values()) {
		chan.leaveChat(event.getPlayer(), false, true);
	    }
	}
    }

    @PonyEvent
    public void onCommand(PlayerCommandPreprocessEvent event) {
	String msg = "[" + Utils.getTimeStamp() + "]" + event.getPlayer().getName() + ": " + event.getMessage();
	PonyLogger.logCommand(Mane.getInstance().getDataFolder() + File.separator + "ChatLogs" + File.separator + Utils.getFileDate(System.currentTimeMillis()), "commandlog", msg);
    }

    /*
     * Bookmark Deactivate
     */
    public void deactivate() {
	for (Channel c : data.getChannels().values()) {
	    if (c instanceof PrivateChannel) {
		PrivateChannel pc = (PrivateChannel) c;
		getConfig().set("privateChannels." + pc.getName() + ".lastActivity", pc.getLastActivity());
	    }
	}
	saveConfig();
    }
}
