package me.corriekay.pppopp4.modules.chat;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.command.validation.tabbing.BasicTabRule;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class LeaveTab implements BasicTabRule {

    @Override
    public List<String> tab(CommandSender sender, CommandMetadata cmd, String argBit, String[] args) {
	List<String> returnMe = new ArrayList<String>();
	if (!(sender instanceof Player)) {
	    returnMe.add("You need to be a player to use this command!");
	    return returnMe;
	}
	for (Channel c : ChannelHandler.getHandler().getListeningChannels((Player) sender)) {
	    returnMe.add(c.getName());
	}
	return returnMe;
    }
}
