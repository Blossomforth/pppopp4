package me.corriekay.pppopp4.modules.chat;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.command.validation.tabbing.BasicTabRule;

import org.bukkit.command.CommandSender;

public class JoinTab implements BasicTabRule {

    @Override
    public List<String> tab(CommandSender sender, CommandMetadata cmd, String argBit, String[] args) {
	List<String> returnMe = new ArrayList<String>();
	returnMe.addAll(ChannelHandler.getHandler().getData().getChannels().keySet());
	return returnMe;
    }

}
