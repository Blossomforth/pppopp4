package me.corriekay.pppopp4.modules.chat;

import java.io.File;
import java.util.HashSet;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.JoinEvent;
import me.corriekay.pppopp4.events.PrivateMessageEvent;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.ponymanager.PonyPermission;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.utilities.PonyLogger;
import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class PonySpy extends PSCmdExe {

    private final HashSet<String> spies = new HashSet<String>();

    public PonySpy() {
	super("PonySpy", Ponies.PinkiePie, "ponyspy");
	init();
    }

    private void init() {
	for (Player player : Bukkit.getOnlinePlayers()) {
	    initPlayer(player);
	}
    }

    private void initPlayer(Player player) {
	Pony pony = Ponyville.getPony(player);
	if (pony.isPonySpy()) {
	    if (player.hasPermission(PonyPermission.CAN_PONYSPY.getPerm())) {
		spies.add(player.getName());
	    } else {
		pony.setPonySpy(false);
		pony.save();
	    }
	}
    }

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) {
	Pony pony = Ponyville.getPony(player);
	boolean ps = pony.isPonySpy();
	if (ps) {
	    spies.remove(player.getName());
	    sendMessage(player, "Ponyspy deactivated!");
	    pony.setPonySpy(false);
	} else {
	    spies.add(player.getName());
	    sendMessage(player, "Ponyspy activated!");
	    pony.setPonySpy(true);
	}
	pony.save();
	return true;
    }

    @PonyEvent
    public void onPM(PrivateMessageEvent event) {
	Player sender, reciever;
	sender = event.getSender();
	reciever = event.getReciever();
	String message = ChatColor.RED + "[PS][" + event.getSender().getDisplayName() + ChatColor.RED + " > " + event.getReciever().getDisplayName() + ChatColor.RED + "]" + ChatColor.GRAY + ": " + event.getMsg();
	Bukkit.getConsoleSender().sendMessage(message);
	PonyLogger.logMessage("ChatLogs" + File.separator + Utils.getFileDate(System.currentTimeMillis()), "Private Messages", ChatColor.stripColor(message));
	// TODO send RPA pm message to "ponyspy" channel
	for (String spyString : spies) {
	    if (sender.getName().equals(spyString) || reciever.getName().equals(spyString)) {
		continue;
	    }
	    Player spy = Bukkit.getPlayerExact(spyString);
	    if (spy != null) {
		spy.sendMessage(message);
	    }
	}
    }

    @PonyEvent
    public void onJoin(JoinEvent event) {
	if (event.isJoining()) {
	    initPlayer(event.getPlayer());
	}
    }

    @PonyEvent
    public void onQuit(QuitEvent event) {
	if (event.isQuitting()) {
	    spies.remove(event.getPlayer().getName());
	}
    }
}
