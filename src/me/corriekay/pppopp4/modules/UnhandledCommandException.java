package me.corriekay.pppopp4.modules;

public class UnhandledCommandException extends Exception {
    public UnhandledCommandException(String string) {
	super(string);
    }

    @SuppressWarnings("unused")
    private UnhandledCommandException() {}

    private static final long serialVersionUID = 824927863496200504L;

}
