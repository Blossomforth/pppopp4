package me.corriekay.pppopp4.modules.classroom;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class MissCheerilee extends PSCmdExe {

    private final LessonPlan cheerileesLessonPlan = new LessonPlan();

    public MissCheerilee() {
	super("MissCheerilee", Ponies.Cheerilee, "tutorial");
    }

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) {
	if (args.length == 0) {
	    sendMessage(player, renderTutorialList(player));
	    return true;
	}
	int tutId;
	try {
	    tutId = Integer.parseInt(args[0]);
	} catch (NumberFormatException e) {
	    if (args[0].equals("cleartutorials")) {
		Pony pony = Ponyville.getPony(player);
		pony.clearTutorials();
		pony.save();
		sendMessage(player, "Your report card has been cleared!");
		return true;
	    }
	    sendMessage(player, "Hmm. That's not a number! Do I have to send you back to fillyschool?");
	    return false;
	}
	TutorialIds tutorial;
	if (tutId == 0) {
	    tutorial = TutorialIds.Introduction;
	} else if (tutId == 1) {
	    tutorial = TutorialIds.ServerEtiquette;
	} else if (tutId == 2) {
	    tutorial = TutorialIds.Griefing;
	} else if (tutId == 3) {
	    tutorial = TutorialIds.CheatingAndMods;
	} else if (tutId == 4) {
	    tutorial = TutorialIds.PersonalResponsibility;
	} else if (tutId == 5) {
	    tutorial = TutorialIds.PopQuiz;
	} else if (tutId == 6) {
	    tutorial = TutorialIds.BuildRights;
	} else {
	    sendMessage(player, "I'm terribly sorry! That course has not been prepared yet :<");
	    return true;
	}
	Pony pony = Ponyville.getPony(player);
	if (!tutorial.canhasTutorial(pony)) {
	    sendMessage(player, "Hey now! You can't take that course! You're not prepared for it! You should view the other courses, by typing /tutorial, and enroll for a more appropriate course!");
	    return true;
	}
	// TODO more validation of course selection

	cheerileesLessonPlan.enrollInClass(tutorial, player);
	return true;
    }

    private String renderTutorialList(Player player) {
	Pony pony = Ponyville.getPony(player);
	String list = "I have several lessons for you to read, so please choose one and we can get started! Here's my lesson book that we can use to go over things! Type /tutorial <id> to enroll in a specific course.\n" + ChatColor.GRAY + "Finished, " + ChatColor.GREEN + "Available, " + ChatColor.RED + "Unavailable" + ChatColor.WHITE + "\n\n";

	// Beginner tutorials
	list += ChatColor.GREEN + "Beginner Courses" + ChatColor.WHITE + ":\n" + TutorialIds.Introduction.renderTutorial(pony) + (pony.hasTutorialIdFinished(TutorialIds.Introduction.getId()) ? "" : ChatColor.WHITE + " Do this tutorial to unlock the others!") + "\n" + TutorialIds.ServerEtiquette.renderTutorial(pony) + "\n" + TutorialIds.Griefing.renderTutorial(pony) + "\n" + TutorialIds.CheatingAndMods.renderTutorial(pony) + "\n" + TutorialIds.PersonalResponsibility.renderTutorial(pony) + "\n\n";

	// Intermediate courses
	list += ChatColor.GOLD + "Intermediate Courses" + ChatColor.WHITE + ":\n" + TutorialIds.PopQuiz.renderTutorial(pony) + (!pony.hasTutorialIdFinished(TutorialIds.Introduction.getId()) ? "" : (pony.hasTutorialIdFinished(TutorialIds.PopQuiz.getId()) ? "" : ChatColor.WHITE + " Complete the quiz to unlock the build rights course!")) + "\n" + TutorialIds.BuildRights.renderTutorial(pony);
	return list;
    }

    public enum TutorialIds {

	Introduction("Introduction to the Tutorial", 0),
	ServerEtiquette("Server Etiquette", 1, 0),
	Griefing("Griefing", 2, 0),
	CheatingAndMods("Cheating and Client Mods", 3, 0),
	PersonalResponsibility("Personal Responsibility", 4, 0),
	PopQuiz("Pop Quiz!", 5, 0),
	BuildRights("How to obtain Build Rights", 6, 5);

	private String displayName;
	private int tutorialId;
	private int[] requiredIds;

	TutorialIds(String name, int i, int... requireds) {
	    displayName = name;
	    tutorialId = i;
	    requiredIds = requireds;
	}

	public int getId() {
	    return tutorialId;
	}

	public String getName() {
	    return displayName;
	}

	public boolean canhasTutorial(Pony pony) {
	    for (int required : requiredIds) {
		if (!pony.hasTutorialIdFinished(required)) {
		    return false;
		}
	    }
	    return true;
	}

	public String renderTutorial(Pony pony) {
	    ChatColor color = ChatColor.GREEN;
	    if (!canhasTutorial(pony)) {
		color = ChatColor.RED;
	    } else {
		if (pony.hasTutorialIdFinished(getId())) {
		    color = ChatColor.GRAY;
		}
	    }
	    return ChatColor.WHITE + "" + getId() + " - " + color + displayName;
	}
    }
}
