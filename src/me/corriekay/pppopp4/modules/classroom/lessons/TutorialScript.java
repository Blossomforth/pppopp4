package me.corriekay.pppopp4.modules.classroom.lessons;

import java.util.ArrayList;
import java.util.HashSet;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.modules.classroom.MissCheerilee;
import me.corriekay.pppopp4.modules.classroom.ScriptComponent;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.ValidatingPrompt;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitTask;

public class TutorialScript extends ValidatingPrompt {

    protected final MissCheerilee.TutorialIds tutorial;
    protected final Player player;
    protected boolean canContinue = false;
    protected boolean initiallyDisplayed = false;
    protected final Pony pony;
    protected final ArrayList<ScriptComponent> script;
    protected final HashSet<BukkitTask> scripttasks = new HashSet<BukkitTask>();

    public TutorialScript(Player player, ArrayList<ScriptComponent> script,
	    MissCheerilee.TutorialIds tutorial) {
	this.player = player;
	this.tutorial = tutorial;
	this.script = script;
	this.pony = Ponyville.getPony(player);
    }

    protected void scheduleScript(ConversationContext context, int initialDelay, boolean addContinue) {
	scheduleScript(context, script, initialDelay, addContinue);
    }

    private void scheduleScript(final ConversationContext context, ArrayList<ScriptComponent> script, int initialDelay, final boolean addContinue) {
	int delay = initialDelay;
	for (int i = 0; i < script.size(); i++) {
	    ScriptComponent comp = script.get(i);
	    scheduleText(context, comp.getPony().says() + comp.getText(), delay);
	    delay += comp.getSeconds() * 20;
	}

	scripttasks.add(Bukkit.getScheduler().runTaskLater(Mane.getInstance(), new Runnable() {
	    public void run() {
		canContinue = true;
		if (!addContinue) {
		    return;
		}
		context.getForWhom().sendRawMessage(Ponies.Cheerilee.says() + "Please type /continue, to continue!");
	    }
	}, delay + 20));
    }

    private void scheduleText(final ConversationContext context, final String text, int delay) {
	scripttasks.add(Bukkit.getScheduler().runTaskLater(Mane.getInstance(), new Runnable() {
	    public void run() {
		context.getForWhom().sendRawMessage(text);
	    }
	}, delay));
    }

    protected void terminateScript() {
	for (BukkitTask task : scripttasks) {
	    task.cancel();
	}
    }

    @Override
    public String getPromptText(ConversationContext context) {
	if (!initiallyDisplayed) {
	    initiallyDisplayed = true;
	    scheduleScript(context, script, 100, true);
	    return Ponies.Cheerilee.says() + "Attention fillies! Please open your textbooks to chapter " + tutorial.getId() + ", \"" + tutorial.getName() + "\", please!";
	}
	return "";
    }

    @Override
    protected boolean isInputValid(ConversationContext arg0, String arg1) {
	if (canContinue == false) {
	    arg0.getForWhom().sendRawMessage(ChatColor.GRAY + "Please dont interrupt the class! If you need to excuse yourself, please type /exit!");
	    return false;
	}
	return true;
    }

    @Override
    protected Prompt acceptValidatedInput(ConversationContext context, String input) {
	pony.addFinishedTutorialId(tutorial);
	pony.save();
	return null;
    }

}
