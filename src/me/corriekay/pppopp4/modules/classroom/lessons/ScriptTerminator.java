package me.corriekay.pppopp4.modules.classroom.lessons;

import org.bukkit.conversations.ConversationAbandonedEvent;
import org.bukkit.conversations.ConversationAbandonedListener;

public class ScriptTerminator implements ConversationAbandonedListener {
    TutorialScript script;

    public ScriptTerminator(TutorialScript script) {
	this.script = script;
    }

    @Override
    public void conversationAbandoned(ConversationAbandonedEvent abandonedEvent) {
	script.terminateScript();
    }

}
