package me.corriekay.pppopp4.modules.classroom.lessons;

import java.util.ArrayList;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.modules.classroom.MissCheerilee.TutorialIds;
import me.corriekay.pppopp4.modules.classroom.ScriptComponent;

import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.entity.Player;

public class Introduction extends TutorialScript {

    public Introduction(Player player, ArrayList<ScriptComponent> script,
	    TutorialIds tutorial) {
	super(player, script, tutorial);
    }

    @Override
    public String getPromptText(ConversationContext context) {
	if (!initiallyDisplayed) {
	    initiallyDisplayed = true;
	    scheduleScript(context, 3, false);
	    return Ponies.Cheerilee.says() + "Hello, " + player.getDisplayName() + Ponies.Cheerilee.c() + "! Welcome to my classroom! My name is Miss Cheerilee and -";
	}
	return "";
    }

    @Override
    protected Prompt acceptValidatedInput(ConversationContext arg0, String arg1) {
	pony.addFinishedTutorialId(tutorial);
	pony.save();
	return null;
    }

}
