package me.corriekay.pppopp4.modules.classroom.lessons;

import java.util.ArrayList;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.modules.classroom.MissCheerilee;
import me.corriekay.pppopp4.modules.classroom.MissCheerilee.TutorialIds;
import me.corriekay.pppopp4.modules.classroom.ScriptComponent;

import org.bukkit.conversations.BooleanPrompt;
import org.bukkit.conversations.ConversationContext;
import org.bukkit.conversations.Prompt;
import org.bukkit.conversations.ValidatingPrompt;
import org.bukkit.entity.Player;

public class PopQuiz extends TutorialScript {

    int question = 1;
    private int wrongAnswers = 0;
    private ArrayList<QuizQuestion> wrongAnswers2 = new ArrayList<QuizQuestion>();
    private final Prompt quiz;
    private GradeQuiz gradeQuiz;

    public PopQuiz(Player player, ArrayList<ScriptComponent> script,
	    TutorialIds tutorial) {
	super(player, script, tutorial);
	quiz = prepareQuiz();
    }

    public void terminateScript() {
	super.terminateScript();
	try {
	    gradeQuiz.terminateScript();
	} catch (Exception e) {}
    }

    @Override
    public String getPromptText(ConversationContext context) {
	if (!initiallyDisplayed) {
	    initiallyDisplayed = true;
	    scheduleScript(context, 75, false);
	    return Ponies.Cheerilee.says() + "Okay fillies! It's time for a Pop Quiz!";
	}
	return "";
    }

    // @formatter:off
    private Prompt prepareQuiz() {
	gradeQuiz = new GradeQuiz(player, new ArrayList<ScriptComponent>(), tutorial);
	Prompt p = new TrueOrFalse("Ettiquette is how you conduct yourself on the server, so treat others how you want to be treated!", "Server Etiquette is a fancy dish served at fancy parties?", false, new MultipleChoiceQuestion("An /alert will get an Opony's attention the quickest!", "How do you summon an OPony who is not in-game?", new String[] { "Make a wish on a star.", "By typing /alert.", "By waiting for one to show up.", "Leave! I don't have time for this!" }, 'b', new MultipleChoiceQuestion("Oponies are best pony for answers!", "If you have a question, who should you ask?", new String[] { "The sky, it has all the answers!", "Veteran players, they know everything!", "Oponies, their wisdom is great!", "Nopony, cuz nopony knows anything!" }, 'c', new MultipleChoiceQuestion("Oponies understand that accidents happen, so you shouldn't be scared!", "What should you do if you accidentally break something you didn't indend to?", new String[] { "RUN! They can't accuse me if I wasn't at the scene of the crime!", "Leave a note.", "Inform an OPony, it's just an accident!", "Leave forever." }, 'c', new TrueOrFalse("I should make sure that that the chest in question is marked saying I can take things before taking them!", "If a chest is unlocked, you can just take anything you want from it, no questions asked.", false, new MultipleChoiceQuestion("If it is done with ill intent, it is considered griefing", "What is Griefing?", new String[] { "Maliciously and purposely destroying another players home, city, or project.", "An act that is very embarassing to another pony.", "Everypony's favorite pasttime!", "The best way to get attention, I don't care what kind!" }, 'a', new MultipleChoiceQuestion("Mini-map! The other mods are forbidden on this server! Unless an OPony says you can use a specific mod not listed, it's best not to use it if you want to play on this server", "Which of these mods are allowed on the server?", new String[] { "Smart movement.", "X-ray.", "Mini-map.", "Flight." }, 'c', new MultipleChoiceQuestion("Cheating has consequences, so don't cheat!", "Why did Applebloom get the Cutie Pox?", new String[] { "Because she's a bad pony!", "She picked at that scab.", "Cutie Pox is like a cold, everypony gets it!", "She tried to cheat her way to a cutie mark." }, 'd', new TrueOrFalse("You should work for what you want, you'll feel better about it!", "X-raying is the only way I'll get any good materials!", false, new MultipleChoiceQuestion("Saying someone else did something while on your account means you are still accountable, \"My friend did it!\" is not an acceptable excuse!", "Complete the phrase: \"You are responsible for what happens on your _____.\"", new String[] { "kitchen table", "planet", "lawn", "account" }, 'd', new TrueOrFalse("Make sure your password is secure and is not easy to guess! Keep it in a safe place or with a trusted adult!", "Keeping your password safe from others is the best way to secure your account.", true, new MultipleChoiceQuestion("Your account is always your responsibility! Make sure nopony you dont absolutely trust plays on it!", "What happens if someone uses my account without my permission?", new String[] { "I'm not responsible for a thing!", "Nothing, I really don't care what happens.", "I'm still accountable for making it easy to access and should make my password hard to guess!", "I love lamp." }, 'c', new TrueOrFalse("If that were the case you wouldn't need this tutorial!", "You can build right away on this server.", false, new MultipleChoiceQuestion("When everyone reads and understands the rules, everyone can have an eaiser time on this server!", "Why do we need this tutorial?", new String[] { "So everypony understand the rules of the server!", "Because you're mean ponies who want to watch new players suffer!", "This is annoying, I'm leaving." }, 'a', new TrueOrFalse("The Oponies know who has and who has not read the tutorial. They're not easily fooled!", "Telling new players the magic phrase gets them through the system faster, without them having to read this tutorial!", false, gradeQuiz)))))))))))))));
	return p;
    }

    // @formatter:on

    @Override
    protected Prompt acceptValidatedInput(ConversationContext context, String input) {
	return quiz;
    }

    public interface QuizQuestion {
	public void setIncorrectMessage(String msg);

	public String getIncorrectMessage();

	public String getQuestion();

	public String renderIncorrect();
    }

    public class MultipleChoiceQuestion extends ValidatingPrompt implements
	    QuizQuestion {

	private final String query;
	private final String[] answers;
	private final char correctAnswer;
	private final Prompt nextPrompt;
	private String incorrectMessage;
	private char answer;

	public MultipleChoiceQuestion(String incorrectMessage, String query,
		String[] answers, char correctAnswer, Prompt nextPrompt) {
	    this.query = query;
	    this.answers = answers;
	    this.correctAnswer = correctAnswer;
	    this.nextPrompt = nextPrompt;
	    setIncorrectMessage(incorrectMessage);
	}

	@Override
	public String getPromptText(ConversationContext arg0) {
	    String returnme = Ponies.Cheerilee.says() + "Question " + question + ":\n";
	    returnme += query;
	    int cint = 97;
	    for (String answer : answers) {
		returnme += "\n" + ((char) cint) + ") " + answer;
		cint++;
	    }
	    return returnme;
	}

	@Override
	protected boolean isInputValid(ConversationContext arg0, String input) {
	    char[] carray = input.toCharArray();
	    boolean returnme = ((int) carray[0] - 96) <= answers.length;
	    if (carray.length > 1 || !returnme) {
		arg0.getForWhom().sendRawMessage(Ponies.Cheerilee.says() + "Thats an invalid answer!");
		return false;
	    }
	    return returnme;
	}

	@Override
	protected Prompt acceptValidatedInput(ConversationContext context, String input) {
	    answer = input.toCharArray()[0];
	    if (answer != correctAnswer) {
		wrongAnswers++;
		System.out.println("+1 wrong answer"); // DEBUG REMOVEME
		wrongAnswers2.add(this);
	    }
	    question++;
	    return nextPrompt;
	}

	@Override
	public void setIncorrectMessage(String msg) {
	    incorrectMessage = msg;
	}

	@Override
	public String getIncorrectMessage() {
	    return incorrectMessage;
	}

	@Override
	public String getQuestion() {
	    return query;
	}

	@Override
	public String renderIncorrect() {
	    String returnme = "Question: \"" + getQuestion() + "\"\nAnswers:\n";
	    int cint = 97;
	    for (String answer : answers) {
		returnme += "\n" + ((char) cint) + ") " + answer;
		cint++;
	    }
	    returnme += "\n\nYour answer: " + answer + "\n" + incorrectMessage;
	    return returnme;
	}
    }

    public class TrueOrFalse extends BooleanPrompt implements QuizQuestion {

	private final String query;
	private final boolean correctAnswer;
	private boolean answer;
	private final Prompt nextPrompt;
	private String incorrectMessage;

	public TrueOrFalse(String incorrectMessage, String query,
		boolean answer, Prompt nextPrompt) {
	    this.query = query;
	    this.correctAnswer = answer;
	    this.nextPrompt = nextPrompt;
	    setIncorrectMessage(incorrectMessage);
	}

	@Override
	public String getPromptText(ConversationContext context) {
	    String returnme = Ponies.Cheerilee.says() + "Question " + question + "\nTrue or false: " + query;
	    return returnme;
	}

	@Override
	protected Prompt acceptValidatedInput(ConversationContext arg0, boolean answer) {
	    this.answer = answer;
	    if (this.answer != correctAnswer) {
		wrongAnswers++;
		System.out.println("+1 wrong answer"); // DEBUG REMOVEME
		wrongAnswers2.add(this);
	    }
	    question++;
	    return nextPrompt;
	}

	@Override
	public void setIncorrectMessage(String msg) {
	    incorrectMessage = msg;
	}

	@Override
	public String getIncorrectMessage() {
	    return incorrectMessage;
	}

	@Override
	public String getQuestion() {
	    return "True or false: " + query;
	}

	@Override
	public String renderIncorrect() {
	    return "Question: \"" + getQuestion() + "\"\nYour Answer: " + answer + "\n" + incorrectMessage;
	}
    }

    public class GradeQuiz extends TutorialScript {

	private boolean pass = false;

	public GradeQuiz(Player player, ArrayList<ScriptComponent> script,
		TutorialIds tutorial) {
	    super(player, script, tutorial);
	}

	private void completeScript() {
	    Ponies cheerilee = Ponies.Cheerilee;
	    ScriptComponent pass = new ScriptComponent(cheerilee, "Type /tutorial " + MissCheerilee.TutorialIds.BuildRights.getId() + " to continue on to learn how to earn your building rights on the server!", 5);
	    System.out.println("wrong answers: " + wrongAnswers); // DEBUG
								  // REMOVEME
	    if (wrongAnswers <= 0) {
		script.add(new ScriptComponent(cheerilee, "Wow, congratulations! you scored a 100%, all questions were answered correctly! Good job!", 5));
		script.add(pass);
		this.pass = true;
	    } else {
		for (QuizQuestion incorrect : wrongAnswers2) {
		    script.add(new ScriptComponent(cheerilee, incorrect.renderIncorrect(), 10));
		}
		if (wrongAnswers > 2) {
		    script.add(new ScriptComponent(cheerilee, "I'm terribly sorry, but you missed more than two questions. I'll have to have you retake the quiz! Type /tutorial " + tutorial.getId() + " to retake the quiz!", 5));
		} else {
		    script.add(new ScriptComponent(cheerilee, "Congratulations! You only missed " + wrongAnswers + " question" + (wrongAnswers > 1 ? "s" : "") + "! You pass the quiz, good job!", 5));
		    script.add(pass);
		    this.pass = true;
		}
	    }
	}

	@Override
	public String getPromptText(ConversationContext context) {
	    if (!initiallyDisplayed) {
		initiallyDisplayed = true;
		completeScript();
		scheduleScript(context, 100, true);
		return Ponies.Cheerilee.says() + "Okay, give me just a moment, and I'll go over your quiz, grade it, and give you your score back!";
	    }
	    return "";
	}

	@Override
	protected Prompt acceptValidatedInput(ConversationContext context, String input) {
	    if (pass) {
		pony.addFinishedTutorialId(tutorial);
		pony.save();
	    }
	    return null;
	}

    }
}
