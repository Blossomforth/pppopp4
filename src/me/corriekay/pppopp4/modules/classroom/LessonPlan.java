package me.corriekay.pppopp4.modules.classroom;

import java.util.ArrayList;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.modules.classroom.lessons.Introduction;
import me.corriekay.pppopp4.modules.classroom.lessons.PopQuiz;
import me.corriekay.pppopp4.modules.classroom.lessons.ScriptTerminator;
import me.corriekay.pppopp4.modules.classroom.lessons.TutorialScript;

import org.bukkit.ChatColor;
import org.bukkit.conversations.Conversation;
import org.bukkit.conversations.ConversationFactory;
import org.bukkit.entity.Player;

public class LessonPlan extends ConversationFactory {

    Ponies cheerilee = Ponies.Cheerilee, applebloom = Ponies.Applebloom,
	    scootaloo = Ponies.Scootaloo, sweetiebelle = Ponies.SweetieBelle;

    public LessonPlan() {
	super(Mane.getInstance());
    }

    protected void enrollInClass(MissCheerilee.TutorialIds tutorial, Player player) {
	switch (tutorial) {
	case Introduction: {
	    enrollIntroduction(player);
	    return;
	}
	case ServerEtiquette: {
	    enrollEtiquette(player);
	    return;
	}
	case Griefing: {
	    enrollGriefing(player);
	    return;
	}
	case CheatingAndMods: {
	    enrollCheating(player);
	    return;
	}
	case PersonalResponsibility: {
	    enrollPersonalResponsibility(player);
	    return;
	}
	case PopQuiz: {
	    takeQuiz(player);
	    return;
	}
	case BuildRights: {
	    enrollBuildRights(player);
	    return;
	}
	default: {
	    return;
	}
	}
    }

    private Conversation createTutorial(TutorialScript script, Player player) {
	return withFirstPrompt(script).withEscapeSequence("/exit").addConversationAbandonedListener(new ScriptTerminator(script)).withLocalEcho(false).withModality(true).buildConversation(player);
    }

    private void enrollIntroduction(Player player) {
	ArrayList<ScriptComponent> script = new ArrayList<ScriptComponent>();
	script.add(new ScriptComponent(applebloom, "AND AH'M APPLEBLOOM!", .03));
	script.add(new ScriptComponent(scootaloo, "Scootaloo here!", 0.3));
	script.add(new ScriptComponent(sweetiebelle, "I'm Sweetie Bo- I mean Sweetie Belle!", 5));
	script.add(new ScriptComponent(cheerilee, "...Thank you, you three, for that wonderful introduction! Today, I'll be teaching you many things about the MLP:FiM Steam Group Minecraft Server! Theres a LOT to learn, so let's begin!", 8));
	script.add(new ScriptComponent(cheerilee, "First things first, If you ever need to leave the classroom for any reason, any reason at all, just type /exit. It will forfeit your progress through the current lesson, but you'll be free to type to other ponies!", 9));
	script.add(new ScriptComponent(cheerilee, "During the duration of these courses, you will be unable to speak to other players, or execute server commands! So please don't forget about /exit!", 8));
	script.add(new ScriptComponent(cheerilee, "Some of the courses will be unavailable to you to start with. Just work through the ones that are available, and you'll eventually unlock them all!", 7.8));
	script.add(new ScriptComponent(cheerilee, "If you're confident enough that you think you can pass the quiz at the end to obtain build rights, by all means, you may attempt the quiz!", 4));
	script.add(new ScriptComponent(cheerilee, "This concludes the course tutorial!", 3));
	script.add(new ScriptComponent(applebloom, "See yah next time!", 4));
	script.add(new ScriptComponent(cheerilee, "Please type /continue to finish this course, and then /tutorial to view the other courses you may enroll in! I hope to see you in my next class!", 0.1));
	Introduction intro = new Introduction(player, script, MissCheerilee.TutorialIds.Introduction);
	createTutorial(intro, player).begin();
    }

    private void enrollEtiquette(Player player) {
	ArrayList<ScriptComponent> script = new ArrayList<ScriptComponent>();
	script.add(new ScriptComponent(cheerilee, "Alright, everypony, settle down please! We have an important lesson to get to! Today's lesson will be on Server Etiquette!", 4));
	script.add(new ScriptComponent(applebloom, "What's Etiquette mean?", 8));
	script.add(new ScriptComponent(cheerilee, "Etiquette is a set of rules on how to best behave yourself. This server has a set of rules to follow, as how to act. Being rude or mean to other ponies isn't acceptable!", 4));
	script.add(new ScriptComponent(scootaloo, "Right! So mind your manners!", 8));
	script.add(new ScriptComponent(cheerilee, "Exactly, Scootaloo. So if you're talking about things that you don't want to say to Granny Smith out loud, it doesn't belong in the public channels!", 6));
	script.add(new ScriptComponent(applebloom, "Okay, I think I get the idea. Don't be rude or mean. What else is there to know?", 10));
	script.add(new ScriptComponent(cheerilee, "I'm so glad you're full of questions today, Applebloom! Let's see... Oh, I've been asked by the moderators of this server, who call themselves Oponies, that you should listen to them, especially when they address you!", 8));
	script.add(new ScriptComponent(sweetiebelle, "I see! They're like the big brothers and sisters around here, right?", 12));
	script.add(new ScriptComponent(cheerilee, "Right! So please be co-operative when they ask or tell you to do, or not to do something! They also told me that they're always open to answer questions, so don't hesitate to ask away. The Oponies also told me that if you start any trouble, they will intervene.", 8));
	script.add(new ScriptComponent(applebloom, "Thats good to know! So what do I do if I see someone being a mean pony?", 10));
	script.add(new ScriptComponent(cheerilee, "You can check to see if there are any Oponies online, by typing /list. they'll be listed under Oponies, Administrators, or the Owner. They'll be happy to assist with dealing with naughty ponies!", 8));
	script.add(new ScriptComponent(scootaloo, "Well, what if there are no Oponies to help out? What then?", 15));
	script.add(new ScriptComponent(cheerilee, "Great question. If there are no Oponies online, you can type /alert to attempt to inform an Opony that they're needed. That command will summon them online to come to your side. But, they told me that using /alert is like calling the police, so don't use it wihtout a good reason!", 7));
	script.add(new ScriptComponent(scootaloo, "Summon? They can't be there all the time, can they?", 9));
	script.add(new ScriptComponent(cheerilee, "Not always! If there are no responses to the /alert, unfortunately that means that there are no Oponies available to assist. Though, they have good coverage, and this is typically very rare that there are none at all to assist!", 7));
	script.add(new ScriptComponent(cheerilee, "Anyways, that concludes this lesson!", 5));
	TutorialScript etiquette = new TutorialScript(player, script, MissCheerilee.TutorialIds.ServerEtiquette);
	createTutorial(etiquette, player).begin();
    }

    private void enrollGriefing(Player player) {
	ArrayList<ScriptComponent> script = new ArrayList<ScriptComponent>();
	script.add(new ScriptComponent(cheerilee, "Today's lesson covers what we call \"Griefing\". Griefing is when you purposely destroy something that doesn't belong to you.", 5.5));
	script.add(new ScriptComponent(applebloom, "Why would anypony want to do that to another pony?", 9));
	script.add(new ScriptComponent(cheerilee, "Some ponies do it to be mean to other ponies. It's not very nice to the ponies who get their projects destroyed because another pony wanted to be mean for no good reason.", 7));
	script.add(new ScriptComponent(scootaloo, "What a jerk thing to do! I'm not gonna do that, but what happens if it was just an accident?", 13));
	script.add(new ScriptComponent(cheerilee, "Well, you should come clean that you did it, but that it was an accident! The Oponies are understanding, and realize accidents do happen. If you say you did it on accident, and it WAS an accident, they'll work with you to make thinks right again, and you wont be in trouble!", 7));
	script.add(new ScriptComponent(cheerilee, "However, if you did do it on purpose, you will be held accountable for your actions!", 4.5));
	script.add(new ScriptComponent(applebloom, "What about ponies who take things from other ponies?", 8));
	script.add(new ScriptComponent(cheerilee, "Stealing things from other ponies is wrong! You wouldn't like it if someone came along and stole all of the things from your house, right?", 4));
	script.add(new ScriptComponent(sweetiebelle, "That counts as griefing as well, right?", 7));
	script.add(new ScriptComponent(cheerilee, "In a way, yes. If you're stealing things from another player, that makes them sad and angry.", 6));
	script.add(new ScriptComponent(applebloom, "What about these \"public\" chests that i see all over the place?", 14.5));
	script.add(new ScriptComponent(cheerilee, "Those chests are okay to take from. The contents are for everypony who wants something from it, so it's okay to take things from a chest marked as public. If there's no marking on it saying that there are free items, or that its public, don't take from it! It's very rude!", 4));
	script.add(new ScriptComponent(applebloom, "Ah understand now, Miss Cheerilee!", 8));
	script.add(new ScriptComponent(cheerilee, "Very good! That concludes this lesson for today! Again, if you have any other questions, please feel free to ask an Opony!", 5));
	TutorialScript griefing = new TutorialScript(player, script, MissCheerilee.TutorialIds.Griefing);
	createTutorial(griefing, player).begin();
    }

    private void enrollCheating(Player player) {
	ArrayList<ScriptComponent> script = new ArrayList<ScriptComponent>();
	script.add(new ScriptComponent(cheerilee, "Good morning, everypony! Let's get into today's lesson, which covers Modifications and Cheating.", 3));
	script.add(new ScriptComponent(scootaloo, "Cheating?", 6));
	script.add(new ScriptComponent(cheerilee, "Yes, cheating. Some ponies like to take the easy way to earning things. Like applebloom did, when she made that one potion, and got the Cutie Pox!", 5));
	script.add(new ScriptComponent(applebloom, "Don't remind me...", 8));
	script.add(new ScriptComponent(cheerilee, "It's a very good example though! You wanted your Cutie Mark so badly, that you took Zecora's potion ingredients and you gave yourself Cutie Pox! Did you learn something from that?", 5));
	script.add(new ScriptComponent(applebloom, "Yes, Ah did. Taking a shortcut because something is hard, isn't the right way to do things!", 6));
	script.add(new ScriptComponent(cheerilee, "Exactly, ponies who cheat their way to valuable items using x-ray texture packs, or mods, are not welcome on this server.", 5));
	script.add(new ScriptComponent(sweetiebelle, "What about other mods? Theres a lot of really neat ones out there!", 15));
	script.add(new ScriptComponent(cheerilee, "The Oponies told me that only a few mods are allowed on this server. They gave me a list, and I'll show it to you: Optifine, Mine Little Pony, and Minimap mods are all allowed. They say that other mods are not allowed, such as movement mods that allow you to fly or run fast.", 7));
	script.add(new ScriptComponent(scootaloo, "Hmph. Figures they wont let me fly.", 12));
	script.add(new ScriptComponent(cheerilee, "Well, if everypony was a pegasus, who would make the crops grow? Who would make things like clothing or paper, like unicorns? If everypony was a pegasus, not a lot of adventuring woulds be done!", 9));
	script.add(new ScriptComponent(applebloom, "Sounds borin' if ya ask me! So, cheaters don't prosper and mods that give you unnatural advantages are not allowed!", 15));
	script.add(new ScriptComponent(cheerilee, "Yes, that is correct. One last thing that I'd like to cover in this lesson: Mob Farms. Mob farms use a mob spawner or and enclosed dark area to obtain items from monsters that attack you! You can have one, but there are a few guidelines you have to follow to use one!", 15));
	script.add(new ScriptComponent(cheerilee, "You have to keep the fight fair; That means that the monster can't be hurt by the environment before you fight it. Drop traps, lava traps, cacti, drowning, and trapping them so they can't fight is not allowed if you'd like to use a mob farm! You have to fight them fairly!", 10));
	script.add(new ScriptComponent(cheerilee, "You can move them around, but you can't trap them, It's not very fair to those who do so already without using a farm!", 7));
	script.add(new ScriptComponent(sweetiebelle, "I'm not sure I understand, whats the point of using a mob farm then?", 9));
	script.add(new ScriptComponent(cheerilee, "The mob farm is still extremely useful, in that they generate a lot of mobs to attack without you having to hunt them down outside!", 6));
	script.add(new ScriptComponent(scootaloo, "Hmm, that does still sound really useful!", 10));
	script.add(new ScriptComponent(cheerilee, "Yes, it very much so is! Anyways, that concludes the lesson on cheating and mods! If you're still unsure about any of these, please ask an Opony to clarify, they'll be happy to do so!", 5));
	TutorialScript cheating = new TutorialScript(player, script, MissCheerilee.TutorialIds.CheatingAndMods);
	createTutorial(cheating, player).begin();
    }

    private void enrollPersonalResponsibility(Player player) {
	ArrayList<ScriptComponent> script = new ArrayList<ScriptComponent>();
	script.add(new ScriptComponent(applebloom, "Whats personal responsibility mean, Miss Cheerilee?", 8));
	script.add(new ScriptComponent(cheerilee, "Personal responsibility means that you are held accountable for your actions! But even moreso, you have to answer for the things that happen on your account!", 5));
	script.add(new ScriptComponent(applebloom, "So, if I didn't do my homework, I'd be responsible for it?", 9));
	script.add(new ScriptComponent(cheerilee, "That's right, Applebloom! Now, when I talk about Personal Responsibility, and how it applies to this server, I mean that anything that happens while your account is logged in, you are responsible for!", 7));
	script.add(new ScriptComponent(applebloom, "So, if I let babs use my account, and she did something that's against the rules, I'd have to take the blame?", 14));
	script.add(new ScriptComponent(cheerilee, "Yes, because you made the choice to let babs use your account. If she used it without your permission, you're still responsible for letting her access it, by not keeping your password secure, or to yourself. If you share an account with another pony, anything they do alsco reflects on you!", 9));
	script.add(new ScriptComponent(applebloom, "So, I should make sure that if I don't want anypony else using my account, I should make my password only known to me, and make sure I keep it safe!", 5));
	script.add(new ScriptComponent(cheerilee, "Yes! Thats the gist of it, and that concludes todays lesson on Personal Responsibility!", 5));
	TutorialScript responsibility = new TutorialScript(player, script, MissCheerilee.TutorialIds.PersonalResponsibility);
	createTutorial(responsibility, player).begin();
    }

    private void takeQuiz(Player player) {
	ArrayList<ScriptComponent> script = new ArrayList<ScriptComponent>();
	script.add(new ScriptComponent(applebloom, "Awh!!", 0.3));
	script.add(new ScriptComponent(scootaloo, "Hmph..", 0.7));
	script.add(new ScriptComponent(sweetiebelle, "I love quizzes!", 5));
	script.add(new ScriptComponent(cheerilee, "Now now, my little ponies, You won't ever earn your cutie marks with that attitude! Let us begin... Please type /continue when you're ready!", 0));
	PopQuiz quiz = new PopQuiz(player, script, MissCheerilee.TutorialIds.PopQuiz);
	createTutorial(quiz, player).begin();
    }

    private void enrollBuildRights(Player player) {
	ArrayList<ScriptComponent> script = new ArrayList<ScriptComponent>();
	script.add(new ScriptComponent(cheerilee, "Thank you for coming, everypony, let's begin. Today's lesson is quite short, so please pay attention! Today, we'll be discussing how to get the ability to build on this server!", 5));
	script.add(new ScriptComponent(applebloom, "You mean we can't just build right away? That's not fair!", 10));
	script.add(new ScriptComponent(cheerilee, "That may be so, but it also wouldn't be fair to you if ponies who didn't read the rules just come in and start making a mess of things and then be mean to everypony else, right?", 11.5));
	script.add(new ScriptComponent(applebloom, "Hmm.. I guess you're right! We need rules so everypony can get along! Anypony can review the server rules by typing /rules, so it's important that everypony can review things if they're not sure of something!", 10));
	script.add(new ScriptComponent(cheerilee, "Absolutely! We have rules so everypony that plays on this server can have a good time! Now, when somepony has read and understands all of the rules, they're responsible for obeying the rules!", 6));
	script.add(new ScriptComponent(scootaloo, "Wait, so how do we let the Oponies know that we read and understand the rules?", 15));
	script.add(new ScriptComponent(cheerilee, "That's a simple one, Scootaloo! All you have to do to let them know that you've read and accept the rules, just say " + ChatColor.LIGHT_PURPLE + ChatColor.BOLD + "\"I want to be a pretty pony, please!\"" + ChatColor.RESET + cheerilee.c() + " in the chat, after the lesson is over!", 6));
	script.add(new ScriptComponent(sweetiebelle, "I.. What? Thats it? That's simple!", 18.5));
	script.add(new ScriptComponent(cheerilee, "Yes! It's really that easy. But, once you get your build rights, you can't tell anypony else about this magical phrase! It's a secret to everypony! Once you get your build rights, you can explore the world properly, get materials, and start building a home. This also gives you access to all of the normal server commands, such as LWC access, and some other things like teleportation!", 10));
	script.add(new ScriptComponent(applebloom, "Cutie Mark Crusader Pretty Ponies!!", 0.3));
	script.add(new ScriptComponent(sweetiebelle, "Cutie Mark Crusader Pretty Ponies!!", 0.1));
	script.add(new ScriptComponent(scootaloo, "Cutie Mark Crusader Pretty Ponies!!", 5));
	script.add(new ScriptComponent(cheerilee, "That concludes this short lesson! Remember, " + player.getDisplayName() + ChatColor.RESET + cheerilee.c() + ", ask into chat to become a pretty pony, and dont forget your please and thank-yous!", 2));
	TutorialScript buildRights = new TutorialScript(player, script, MissCheerilee.TutorialIds.BuildRights);
	createTutorial(buildRights, player).begin();

    }
}
