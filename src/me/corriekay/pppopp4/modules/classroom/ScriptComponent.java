package me.corriekay.pppopp4.modules.classroom;

import me.corriekay.pppopp4.Ponies;

public class ScriptComponent {

    private final Ponies pony;
    private final String text;
    private final double secondsToWait;

    public ScriptComponent(Ponies pony, String text, double secondsToWait) {
	this.pony = pony;
	this.text = text;
	this.secondsToWait = secondsToWait;
    }

    public Ponies getPony() {
	return pony;
    }

    public String getText() {
	return text;
    }

    public double getSeconds() {
	return secondsToWait;
    }

}
