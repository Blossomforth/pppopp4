package me.corriekay.pppopp4.modules.invsee;

import java.util.HashMap;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.equestria.PonyUniverse;
import me.corriekay.pppopp4.modules.equestria.PonyWorld;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class InvSee extends PSCmdExe {

    private final HashMap<String, InvSeePackage> invseerInvs = new HashMap<String, InvSeePackage>();
    public static InvSee handler;

    public InvSee() {
	super("InvSee", Ponies.PinkiePie, "invsee", "commitinventorychange", "invupdate", "clearinventory");
	handler = this;
    }

    /*
     * Bookmark Command
     */

    public boolean playerCommand(final Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case invsee: {
	    Pony targetPony;
	    PonyUniverse targetUniverse;
	    if (invseerInvs.containsKey(player.getName())) { // invseeing
		if (args.length > 0) { // targetting a player while invseeing
		    OfflinePlayer op = getOnlineOfflinePlayer(args[0], player);
		    if (op == null) {
			return true;
		    }
		    targetPony = Ponyville.getPony(op);
		    PonyWorld world = PonyWorld.getPonyWorld(player.getWorld()).getOverworld();
		    if (args.length > 1) { // targetting a specific world
			world = PonyWorld.getPonyWorld(args[1]);
			if (world == null) {
			    sendMessage(player, "World not found!");
			    return true;
			}
			world = world.getOverworld();
		    }
		    targetUniverse = world.getUniverse();
		    InvSeePackage invsee = invseerInvs.get(player.getName());
		    invsee.changeTarget(targetPony, targetUniverse);
		    invsee.copyInventory();
		    return true;
		} else { // not targetting a player while invseeing
		    InvSeePackage invsee = invseerInvs.get(player.getName());
		    if (invsee.restoreInventory(true)) {
			sendMessage(player, "Inventory returned!");
		    }
		    return true;
		}
	    } else { // not invseeing
		if (args.length > 0) { // targetting a player while not
				       // invseeing
		    OfflinePlayer op = getOnlineOfflinePlayer(args[0], player);
		    if (op == null) {
			return true;
		    }
		    targetPony = Ponyville.getPony(op);
		    PonyWorld world = PonyWorld.getPonyWorld(player.getWorld()).getOverworld();
		    if (args.length > 1) { // targetting a specific world
			world = PonyWorld.getPonyWorld(args[1]);
			if (world == null) {
			    sendMessage(player, "World not found!");
			    return true;
			}
			world = world.getOverworld();
		    }
		    targetUniverse = world.getUniverse();
		    InvSeePackage invsee = new InvSeePackage(player, targetPony, targetUniverse);
		    invseerInvs.put(player.getName(), invsee);
		    invsee.copyInventory();
		    return true;
		} else { // not targetting a player while not invseeing
		    sendMessage(player, "Already viewing your own inventory!");
		    return true;
		}
	    }
	}
	case commitinventorychange: {
	    if (!isInvseeing(player.getName())) {
		sendMessage(player, "Hey, you need to be viewing an inventory to do that!");
		return true;
	    }
	    InvSeePackage invsee = invseerInvs.get(player.getName());
	    invsee.commitInventoryChange();
	    invsee.setModified(false);
	    return true;
	}
	case invupdate: {
	    if (!isInvseeing(player.getName())) {
		sendMessage(player, "Hey, you need to be viewing an inventory to do that!");
		return true;
	    }
	    InvSeePackage invsee = invseerInvs.get(player.getName());
	    invsee.copyInventory();
	    invsee.setModified(false);
	    return true;
	}
	case clearinventory: {
	    Runnable r = new Runnable() {
		public void run() {
		    String answer = questioner.ask(player, Ponies.PinkiePie.says() + "Are you sure you want to delete your inventory? This is irreversable!!", "yes", "no");
		    if (answer.equals("no")) {
			sendSyncMessage(player, "Okay! We wont be deleting your inventory!");
			return;
		    }
		    if (answer.equals("yes")) {
			Runnable r = new Runnable() {
			    public void run() {
				player.getInventory().clear();
				sendMessage(player, "Your inventory was sent to the mooooonaaah!!");
			    }
			};
			Bukkit.getScheduler().runTask(Mane.getInstance(), r);
			return;
		    }
		}
	    };
	    Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), r);
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    /*
     * Bookmark Listeners
     */
    @PonyEvent(priority = EventPriority.LOWEST)
    public void onQuit(QuitEvent event) {
	if (event.isQuitting() && isInvseeing(event.getPlayer().getName())) {
	    InvSeePackage invsee = invseerInvs.get(event.getPlayer().getName());
	    invsee.restoreInventory(false);
	    invseerInvs.remove(event.getPlayer().getName());
	}
    }

    @PonyEvent
    public void onInv1(InventoryClickEvent event) {
	if (event.getWhoClicked() instanceof Player) {
	    onEvent((Player) event.getWhoClicked());
	}
    }

    @PonyEvent(priority = EventPriority.HIGHEST)
    public void onInv2(PlayerPickupItemEvent event) {
	onEvent(event.getPlayer());
    }

    @PonyEvent
    public void onInv3(PlayerDropItemEvent event) {
	onEvent(event.getPlayer());
    }

    private void onEvent(Player player) {
	if (isInvseeing(player.getName())) {
	    InvSeePackage invsee = invseerInvs.get(player.getName());
	    invsee.setModified(true);
	}
    }

    /*
     * Bookmark Utils
     */

    public boolean isInvseeing(String playername) {
	return invseerInvs.containsKey(playername);
    }

    public void deactivate() {
	for (InvSeePackage invsee : invseerInvs.values()) {
	    try {
		invsee.restoreInventory(false);
		sendMessage(invsee.invseeingPony.getPlayer().getPlayer(), "SERVER RELOAD - Restoring inventory!");
	    } catch (Exception e) {
		continue;
	    }
	}
    }

    /*
     * Bookmark Invseer Class
     */

    private class InvSeePackage {
	private final ItemStack[] items, armor;
	private PonyUniverse universe;
	private Pony targetPony, invseeingPony;
	private boolean isModified = false;

	public InvSeePackage(Player player, Pony targetPony,
		PonyUniverse universe) {
	    items = player.getInventory().getContents();
	    armor = player.getInventory().getArmorContents();
	    this.targetPony = targetPony;
	    this.universe = universe;
	    invseeingPony = Ponyville.getPony(player);
	}

	public void changeTarget(Pony pony, PonyUniverse universe) {
	    targetPony = pony;
	    this.universe = universe;
	}

	public void copyInventory() {
	    PlayerInventory inv = invseeingPony.getPlayer().getPlayer().getInventory();
	    PlayerInventory targetInv;
	    if (isInUniverse()) {
		targetInv = targetPony.getPlayer().getPlayer().getInventory();
	    } else {
		targetInv = targetPony.getInventory(universe.getOverworld());
	    }
	    inv.setArmorContents(targetInv.getArmorContents());
	    inv.setContents(targetInv.getContents());
	    sendMessage(invseeingPony.getPlayer().getPlayer(), "Viewing inventory for player " + targetPony.getNickname() + ChatColor.LIGHT_PURPLE + " - world " + universe.getName() + "!");
	}

	public boolean restoreInventory(boolean ask) {
	    if (ask && isModified) {
		final InventoryRestoreTask task = new InventoryRestoreTask(this);
		Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), new Runnable() {
		    public void run() {
			Player player = invseeingPony.getPlayer().getPlayer();
			String answer = questioner.ask(player, Ponies.PinkiePie.says() + "You've changed your inventory without committing it! Would you like to commit before you quit?", "yes", "no");
			if (answer.equals("no")) {
			    sendSyncMessage(player, "Okay! You haven't saved the inventory!");
			    Bukkit.getScheduler().runTask(Mane.getInstance(), task);
			} else {
			    sendSyncMessage(player, "Okay, saving the inventory!");
			    Bukkit.getScheduler().runTask(Mane.getInstance(), new Runnable() {
				public void run() {
				    commitInventoryChange();
				    task.run();
				}
			    });
			}
		    }
		});
		return false;
	    }
	    PlayerInventory inv = invseeingPony.getPlayer().getPlayer().getInventory();
	    inv.setArmorContents(armor);
	    inv.setContents(items);
	    invseerInvs.remove(invseeingPony.getPlayer().getPlayer().getName());
	    return true;
	}

	public void commitInventoryChange() {
	    PlayerInventory i = invseeingPony.getPlayer().getPlayer().getInventory();
	    ItemStack[] armor, backpack;
	    armor = i.getArmorContents();
	    backpack = i.getContents();
	    OfflinePlayer op = targetPony.getPlayer();
	    sendMessage(invseeingPony.getPlayer().getPlayer(), "You've saved the inventory of " + targetPony.getNickname() + ChatColor.LIGHT_PURPLE + " in the world " + universe.getName() + "!");
	    if (op.isOnline()) {
		Player targetPlayer = op.getPlayer();
		if (isInUniverse()) {
		    System.out.println("Setting inventory");
		    PlayerInventory targetInv = targetPlayer.getInventory();
		    targetInv.setArmorContents(armor);
		    targetInv.setContents(backpack);
		    return;
		}
	    }
	    PlayerInventory targetInv = targetPony.getInventory(universe.getOverworld());
	    targetInv.setArmorContents(armor);
	    targetInv.setContents(backpack);
	    targetPony.setInventory(targetInv, universe.getOverworld());
	    targetPony.save();
	}

	private boolean isInUniverse() {
	    if (!targetPony.getPlayer().isOnline()) {
		return false;
	    }
	    PonyUniverse universeIn = PonyWorld.getPonyWorld(targetPony.getPlayer().getPlayer().getWorld()).getUniverse();
	    if (!universeIn.equals(universe)) {
		return false;
	    }
	    return true;
	}

	public void setModified(boolean flag) {
	    isModified = flag;
	}

	public class InventoryRestoreTask implements Runnable {
	    private InvSeePackage invsee;

	    public InventoryRestoreTask(InvSeePackage invsee) {
		this.invsee = invsee;
	    }

	    public void run() {
		invsee.restoreInventory(false);
	    }
	}
    }
}
