package me.corriekay.pppopp4.modules.miscellaneous;

import java.util.ArrayList;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.invisibilityhandler.InvisibilityHandler;
import me.corriekay.pppopp4.modules.ponymanager.PonyPermission;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class MiscCommands extends PSCmdExe {

    public MiscCommands() {
	super("MiscCommands", Ponies.PinkiePie, "nick", "whopony");
    }

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case nick: {
	    String nickname = Utils.joinStringArray(args, " ");
	    nickname = nickname.replaceAll("&k", "");
	    if (!player.hasPermission(PonyPermission.FORMAT_NICKNAME.getPerm())) {
		nickname = nickname.replaceAll("&l", "").replaceAll("&m", "").replaceAll("&n", "").replaceAll("&o", "").replaceAll("&0", "").replaceAll("&1", "");
		nickname = nickname.replaceAll("[^(a-zA-Z0-9 !?.&~)]", "");
	    }
	    nickname = ChatColor.translateAlternateColorCodes('&', nickname);
	    if (ChatColor.stripColor(nickname).length() > 20) {
		sendMessage(player, "Boy, thats a long nickname! You should shorten it a bit!");
		return true;
	    }
	    if (ChatColor.stripColor(nickname).length() < 3) {
		sendMessage(player, "Boy, thats a short nickname! You should lengthen it a bit!");
		return true;
	    }
	    Pony pony = Ponyville.getPony(player);
	    ArrayList<String> nicks = pony.getNickHistory();
	    String oldNick = pony.getNickname();
	    if (!nicks.contains(oldNick)) {
		nicks.add(oldNick);
	    }
	    pony.setNickname(nickname);
	    pony.setNickHistory(nicks);
	    pony.save();
	    player.setDisplayName(nickname);
	    // TODO update RPA
	    sendMessage(player, "Nickname changed to " + player.getDisplayName() + ChatColor.LIGHT_PURPLE + "!");
	    return true;
	}
	case whopony: {
	    String target = args[0].toLowerCase().replaceAll("_", " ");
	    ArrayList<Player> players = new ArrayList<Player>();
	    Pony seeingPony = Ponyville.getPony(player);
	    for (Player p : Bukkit.getOnlinePlayers()) {
		Pony lookinAtPony = Ponyville.getPony(p);
		if (p.getName().toLowerCase().contains(target) || ChatColor.stripColor(p.getDisplayName()).toLowerCase().contains(target)) {
		    if ((!InvisibilityHandler.handler.isHidden(p.getName())) || seeingPony.canSeeInvisible(lookinAtPony)) {
			players.add(p);
		    }
		}
	    }
	    if (players.size() < 1) {
		sendMessage(player, "No ponies under that name were found!");
		return true;
	    }
	    for (Player p : players) {
		sendMessage(player, "Silly, " + p.getDisplayName() + ChatColor.LIGHT_PURPLE + " is " + p.getName() + "!");
	    }
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }
}
