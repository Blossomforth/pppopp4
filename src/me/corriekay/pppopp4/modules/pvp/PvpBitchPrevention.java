package me.corriekay.pppopp4.modules.pvp;

import java.util.HashMap;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.equestria.PonyUniverse;
import me.corriekay.pppopp4.modules.equestria.PonyWorld;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.event.player.PlayerRespawnEvent;

public class PvpBitchPrevention extends PSCmdExe {

    private final HashMap<String, Integer> combattants = new HashMap<String, Integer>();

    public PvpBitchPrevention() throws SecurityException, NoSuchMethodException {
	super("PvpBitchPrevention", Ponies.PinkiePie);
	Bukkit.getScheduler().runTaskTimer(Mane.getInstance(), new Runnable() {
	    @SuppressWarnings("unchecked")
	    public void run() {
		for (String player : ((HashMap<String, Integer>) combattants.clone()).keySet()) {
		    int timer = combattants.get(player);
		    timer--;
		    combattants.put(player, timer);
		    if (timer <= 0) {
			combattants.remove(player);
			Player playerObj = Bukkit.getPlayerExact(player);
			if (playerObj != null) {
			    sendMessage(playerObj, "No longer in combat! Safe zones are now safe!");
			}
		    }
		}
	    }
	}, 0, 20);
    }

    @EventHandler(priority = EventPriority.HIGH, ignoreCancelled = false)
    public void onDamage(EntityDamageByEntityEvent event) {
	Entity e = event.getEntity();
	Entity d = event.getDamager();
	if (e instanceof Player && d instanceof Player) {
	    Player entity, damager;
	    entity = (Player) e;
	    damager = (Player) d;
	    if (!combattants.containsKey(entity.getName()) && event.isCancelled()) {
		return;
	    }
	    PonyWorld w = PonyWorld.getPonyWorld(event.getEntity().getWorld());
	    if (!w.isPVP()) {
		return;
	    }
	    addCombattant(entity);
	    addCombattant(damager);
	    if (event.isCancelled() && combattants.containsKey(entity.getName())) {
		event.setCancelled(false);
	    }
	}
    }

    private void addCombattant(Player p) {
	if (!combattants.containsKey(p.getName())) {
	    sendMessage(p, "Careful! Youre in combat now! Safe areas are no longer safe!");
	}
	combattants.put(p.getName(), 30);
    }

    @EventHandler
    public void realmChangeEvent(PlayerChangedWorldEvent event) {
	PonyUniverse from, to;
	from = PonyWorld.getPonyWorld(event.getFrom()).getUniverse();
	to = PonyWorld.getPonyWorld(event.getPlayer().getWorld()).getUniverse();
	if (from.getOverworld().isPVP() && !to.getOverworld().isPVP()) {
	    if (combattants.containsKey(event.getPlayer().getName())) {
		sendMessage(event.getPlayer(), "No longer in combat! Safe zones are now safe!");
		combattants.remove(event.getPlayer().getName());
	    }
	}
    }

    @EventHandler
    public void respawn(PlayerRespawnEvent event) {
	if (combattants.containsKey(event.getPlayer().getName())) {
	    sendMessage(event.getPlayer(), "No longer in combat! Safe zones are now safe!");
	    combattants.remove(event.getPlayer().getName());
	}
    }
}
