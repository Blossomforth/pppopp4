package me.corriekay.pppopp4.modules.pvp;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.equestria.PonyWorld;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.utilities.ExpUtil;

import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.block.BlockExpEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class PvpRules extends PSCmdExe {

    private final double modifier = 2.25;

    public PvpRules() {
	super("PvpRules", Ponies.PinkiePie);
    }

    @PonyEvent
    public void playerDeathEvent(PlayerDeathEvent event) {
	Player player = event.getEntity();
	if (!(player.getLastDamageCause() instanceof EntityDamageByEntityEvent)) {
	    return;
	}
	PonyWorld world = PonyWorld.getPonyWorld(player.getWorld());
	if (!world.isPVP()) {
	    return;
	}
	EntityDamageByEntityEvent edbee = (EntityDamageByEntityEvent) player.getLastDamageCause();
	Entity killer = edbee.getDamager();
	Player pKiller;
	if (killer instanceof Player) {
	    pKiller = (Player) killer;
	} else if (killer instanceof Projectile) {
	    Projectile projKiller = (Projectile) killer;
	    if (projKiller.getShooter() instanceof Player) {
		pKiller = (Player) projKiller.getShooter();
	    } else {
		return;
	    }
	} else {
	    return;
	}
	ExpUtil.giveExperience(pKiller, (ExpUtil.getTotalExperience(player) / 4) * 3);
	event.setNewExp(0);
	event.setDroppedExp(0);
	Pony pony = Ponyville.getPony(player);
	if (world.enabledRemotechest()) {
	    Inventory backpack = pony.getRemoteChest(world);
	    for (ItemStack is : backpack.getContents()) {
		event.getDrops().add(is);
	    }
	    backpack.clear();
	    pony.saveRemoteChest(world);
	}
	if (world.enabledEnderchest()) {
	    Inventory ec = pony.getEnderChest(world);
	    for (ItemStack is : ec.getContents()) {
		event.getDrops().add(is);
	    }
	    ec.clear();
	    pony.saveEnderChest(world);
	}
	// TODO figure out a way to drop skulls without spamming the PVP realms
	// with them on every kill
    }

    @PonyEvent
    public void deathEvent(EntityDeathEvent event) {
	PonyWorld world = PonyWorld.getPonyWorld(event.getEntity().getWorld());
	if (world.isPVP()) {
	    if (!(event.getEntity() instanceof Player)) {
		event.setDroppedExp((int) (event.getDroppedExp() * modifier));
	    }
	}
    }

    @PonyEvent
    public void breakBlock(BlockExpEvent event) {
	PonyWorld world = PonyWorld.getPonyWorld(event.getBlock().getLocation());
	if (world.isPVP()) {
	    event.setExpToDrop((int) (event.getExpToDrop() * modifier));
	}
    }
}
