package me.corriekay.pppopp4.modules.remotechest;

import java.util.HashMap;
import java.util.HashSet;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.equestria.GameType;
import me.corriekay.pppopp4.modules.equestria.PonyWorld;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

public class RemoteChestManager extends PSCmdExe {

    private HashMap<String, Inventory> viewingRCInvs = new HashMap<String, Inventory>();

    private HashMap<String, Inventory> viewingECInvs = new HashMap<String, Inventory>();

    private HashMap<String, InventoryView> verView = new HashMap<String, InventoryView>();

    private HashMap<String, InventoryView> vecView = new HashMap<String, InventoryView>();

    public RemoteChestManager() {
	super("RemoteChestManager", Ponies.PinkiePie, "chest", "workbench", "transferchest", "viewenderchest", "viewremotechest");
    }

    @SuppressWarnings("deprecation")
    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case chest: {
	    Pony pony = Ponyville.getPony(player);
	    Inventory inv = pony.getRemoteChest(PonyWorld.getPonyWorld(player.getLocation()));
	    if (inv == null) {
		sendMessage(player, "There is no remote chest for this world!");
		return true;
	    }
	    player.openInventory(inv);
	    viewingRCInvs.put(player.getName(), inv);
	    return true;
	}
	case workbench: {
	    player.openWorkbench(null, true);
	    return true;
	}
	case transferchest: {
	    PonyWorld world = PonyWorld.getPonyWorld(player.getWorld());
	    Pony pony = Ponyville.getPony(player);
	    Inventory inv = pony.getRemoteChest(world);
	    if (inv == null) {
		sendMessage(player, "There is no remote chest for this world!");
		return true;
	    }
	    if (args.length == 0) {
		transferChest(player, player.getInventory(), inv, "null", false);
		sendMessage(player, "Items transferred!");
		return true;
	    }
	    if (args[0].equals("help")) {
		sendMessage(player, "transferchest is a powerful item transferral tool for the on-the-go miner! if you just want to dump your inventory into your chest, just type /transferchest. Simple as that! If you want to use a \"smart\" mode, type either /transferchest ore (to transfer ores and ingots) or /transferchest material (to transfer materials, such as dirt, gravel, sand, cobblestone) to your chest. If youre feeling adventurous, type /transferchest <material type (or) material id> to transfer a specific type of item to your inventory!");
		return true;
	    }
	    if (args[0].equals("ore") || args[0].equals("material")) {
		transferChest(player, player.getInventory(), inv, args[0], false);
		sendMessage(player, "Items Transferred!");
		return true;
	    }
	    boolean correct = true;
	    Material mat = null;
	    int matId;
	    mat = Material.matchMaterial(args[0]);
	    correct = true;
	    if (mat == null) {
		try {
		    matId = Integer.parseInt(args[0]);
		    mat = Material.getMaterial(matId);
		    correct = true;
		} catch (NumberFormatException e2) {
		    correct = false;
		}
	    }
	    if (correct) {
		transferChest(player, player.getInventory(), inv, mat.name(), true);
		sendMessage(player, "Items transferred!");
		return true;
	    } else {
		sendMessage(player, "Sorry... I couldnt find that item type!");
		return true;
	    }
	}
	case viewenderchest: {
	    PonyWorld world = PonyWorld.getPonyWorld(player.getLocation()).getOverworld();
	    if (args.length > 1) {
		world = PonyWorld.getPonyWorld(args[1]);
		if (world == null) {
		    sendMessage(player, "Hey, that world doesnt exist!");
		    return true;
		} else {
		    world = world.getOverworld();
		}
	    }
	    OfflinePlayer op = getOnlineOfflinePlayer(args[0], player);
	    if (op != null) {
		Pony pony = Ponyville.getPony(op);
		Inventory ec = pony.getEnderChest(world);
		if (ec == null) {
		    sendMessage(player, "There is no ender chest for the world " + world.getName() + "!");
		    return true;
		} else {
		    player.openInventory(ec);
		    InventoryView iv = new InventoryView();
		    iv.invOwner = pony;
		    iv.viewingInv = ec;
		    iv.world = world;
		    vecView.put(player.getName(), iv);
		    return true;
		}
	    }
	    return true;
	}
	case viewremotechest: {
	    PonyWorld world = PonyWorld.getPonyWorld(player.getLocation()).getOverworld();
	    if (args.length > 1) {
		world = PonyWorld.getPonyWorld(args[1]);
		if (world == null) {
		    sendMessage(player, "Hey, that world doesnt exist!");
		    return true;
		} else {
		    world = world.getOverworld();
		}
	    }
	    OfflinePlayer op = getOnlineOfflinePlayer(args[0], player);
	    if (op != null) {
		Pony pony = Ponyville.getPony(op);
		Inventory rc = pony.getRemoteChest(world);
		if (rc == null) {
		    sendMessage(player, "There is no remote chest for the world " + world.getName() + "!");
		    return true;
		} else {
		    player.openInventory(rc);
		    InventoryView iv = new InventoryView();
		    iv.invOwner = pony;
		    iv.viewingInv = rc;
		    iv.world = world;
		    verView.put(player.getName(), iv);
		    return true;
		}
	    }
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    protected static void transferChest(Player player, PlayerInventory playerInv, Inventory chestInv, String param, boolean isMat) {
	HashSet<Material> typesToTransfer = new HashSet<Material>();
	if (isMat) {
	    Material mat = Material.getMaterial(param);
	    typesToTransfer.add(mat);
	} else {
	    if (param.equals("ore")) {
		typesToTransfer.add(Material.COAL);
		typesToTransfer.add(Material.COAL_ORE);
		typesToTransfer.add(Material.DIAMOND);
		typesToTransfer.add(Material.DIAMOND_ORE);
		typesToTransfer.add(Material.DIAMOND_BLOCK);
		typesToTransfer.add(Material.IRON_INGOT);
		typesToTransfer.add(Material.IRON_ORE);
		typesToTransfer.add(Material.IRON_BLOCK);
		typesToTransfer.add(Material.GOLD_INGOT);
		typesToTransfer.add(Material.GOLD_ORE);
		typesToTransfer.add(Material.GOLD_BLOCK);
		typesToTransfer.add(Material.LAPIS_ORE);
		typesToTransfer.add(Material.LAPIS_BLOCK);
		typesToTransfer.add(Material.REDSTONE_ORE);
		typesToTransfer.add(Material.REDSTONE);
	    }
	    if (param.equals("material")) {
		typesToTransfer.add(Material.DIRT);
		typesToTransfer.add(Material.STONE);
		typesToTransfer.add(Material.COBBLESTONE);
		typesToTransfer.add(Material.SAND);
		typesToTransfer.add(Material.GRAVEL);
		typesToTransfer.add(Material.CLAY_BALL);
		typesToTransfer.add(Material.WOOD);
		typesToTransfer.add(Material.LOG);
		typesToTransfer.add(Material.SNOW_BALL);
		typesToTransfer.add(Material.SNOW_BLOCK);
	    }
	    if (param.equals("null")) {// dumb transfer/alltransfer
		for (Material material : Material.values()) {
		    typesToTransfer.add(material);
		}
	    }
	}
	for (int i = 9; i <= 35; i++) {
	    ItemStack playerIs = playerInv.getContents()[i];
	    if (playerIs == null) {
		playerIs = new ItemStack(Material.AIR);
	    }
	    if (typesToTransfer.contains(playerIs.getType())) {
		try {
		    HashMap<Integer, ItemStack> returnedItems = chestInv.addItem(playerInv.getItem(i));
		    playerInv.setItem(i, returnedItems.get(0));
		} catch (NullPointerException e) {}
	    }
	}
    }

    @PonyEvent
    public void onEnderchest(PlayerInteractEvent event) {
	Player player = event.getPlayer();
	Block b = event.getClickedBlock();
	if (b != null && b.getType() == Material.ENDER_CHEST && event.getAction() == Action.RIGHT_CLICK_BLOCK) {
	    event.setCancelled(true);
	    Pony pony = Ponyville.getPony(player);
	    Inventory inv = pony.getEnderChest(PonyWorld.getPonyWorld(player.getLocation()));
	    if (inv == null) {
		sendMessage(player, "This world doesnt support ender chests!");
		return;
	    }
	    player.openInventory(inv);
	    viewingECInvs.put(player.getName(), inv);
	}
    }

    @PonyEvent
    public void invclose(InventoryCloseEvent event) {
	if (event.getPlayer() instanceof Player) {
	    Player p = (Player) event.getPlayer();
	    Pony pony = Ponyville.getPony(p);
	    if (viewingRCInvs.containsKey(p.getName())) {
		Inventory inv2 = viewingRCInvs.get(p.getName());
		pony.saveRemoteChest(pony.getRCWorld(inv2));
		viewingRCInvs.remove(p.getName());
		pony.save();
		return;
	    }
	    if (viewingECInvs.containsKey(p.getName())) {
		Inventory inv2 = viewingECInvs.get(p.getName());
		pony.saveEnderChest(pony.getECWorld(inv2));
		viewingECInvs.remove(p.getName());
		pony.save();
		return;
	    }
	    if (verView.containsKey(p.getName())) {
		InventoryView iv = verView.get(p.getName());
		Pony ownerPony = iv.invOwner;
		ownerPony.saveEnderChest(iv.world);
		ownerPony.save();
		verView.remove(p.getName());
		return;
	    }
	    if (vecView.containsKey(p.getName())) {
		InventoryView iv = vecView.get(p.getName());
		Pony ownerPony = iv.invOwner;
		ownerPony.saveEnderChest(iv.world);
		ownerPony.save();
		vecView.remove(p.getName());
		return;
	    }
	}
    }

    @PonyEvent
    public void onQuit(QuitEvent event) {
	if (event.isQuitting()) {
	    Pony pony = event.getPony();
	    pony.saveAllChests();
	    pony.save();
	}
    }

    @PonyEvent
    public void onDeath(PlayerDeathEvent event) {
	Player player = event.getEntity();
	PonyWorld world = PonyWorld.getPonyWorld(player.getLocation());
	if (world.getGameType() == GameType.PVP) {
	    if (!(player.getLastDamageCause() instanceof EntityDamageByEntityEvent)) {
		return;
	    }
	    EntityDamageByEntityEvent edbee = (EntityDamageByEntityEvent) player.getLastDamageCause();
	    Player killer = null;
	    if (!(edbee.getDamager() instanceof Player)) {
		if (edbee.getDamager() instanceof Projectile) {
		    Projectile p = (Projectile) edbee.getDamager();
		    if (p.getShooter() instanceof Player) {
			killer = (Player) p.getShooter();
		    }
		}
	    } else {
		killer = (Player) edbee.getDamager();
	    }
	    if (killer == null) {
		return;
	    }
	    Pony pony = Ponyville.getPony(player);
	    Inventory rc, ec;
	    rc = pony.getRemoteChest(world);
	    ec = pony.getEnderChest(world);
	    if (rc != null) {
		ItemStack[] stack = rc.getContents();
		rc.clear();
		pony.saveRemoteChest(pony.getRCWorld(rc));
		for (ItemStack s : stack) {
		    event.getDrops().add(s);
		}
	    }
	    if (ec != null) {
		ItemStack[] stack = ec.getContents();
		ec.clear();
		pony.saveEnderChest(pony.getECWorld(ec));
		for (ItemStack s : stack) {
		    event.getDrops().add(s);
		}
	    }
	    pony.save();
	}
    }

    public class InventoryView {
	public Inventory viewingInv;

	public Pony invOwner;

	public PonyWorld world;
    }
}
