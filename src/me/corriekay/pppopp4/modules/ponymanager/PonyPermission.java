package me.corriekay.pppopp4.modules.ponymanager;

import org.bukkit.permissions.Permission;

public enum PonyPermission {

    ALERT_NOTE("pppopp.alertnote"),
    ALERT_GROUP_CHANGE("pppopp.alertgroupchange"),
    TP_BYPASS("pppopp.tpbypass"),
    IS_OPERATOR("pppopp.operator"),
    FORMAT_NICKNAME("pppopp.formattednickname"),
    CAN_PONYSPY("pppopp.ponyspy");

    private final String permission;
    private final Permission ppermission;

    PonyPermission(String perm) {
	permission = perm;
	ppermission = new Permission(perm);
    }

    public String getPermString() {
	return permission;
    }

    public Permission getPerm() {
	return ppermission;
    }

    public String toString() {
	return permission;
    }
}
