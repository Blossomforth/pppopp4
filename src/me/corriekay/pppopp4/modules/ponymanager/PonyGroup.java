package me.corriekay.pppopp4.modules.ponymanager;

import java.util.HashMap;
import java.util.HashSet;

import me.corriekay.pppopp4.modules.equestria.PonyWorld;

import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.permissions.Permissible;

public class PonyGroup {

    protected final HashMap<String, BunchOfPerms> worldPerms = new HashMap<String, BunchOfPerms>();
    protected final HashMap<String, Boolean> globalPerms;
    public final String perm2add2;
    private String name;
    private int invisLevel;
    private boolean isOPType;
    private boolean isFillyType;
    private String listName;

    public PonyGroup(FileConfiguration config, String name) {
	this.name = name;
	perm2add2 = config.getString("groups." + name + ".perm2add2");
	invisLevel = config.getInt("groups." + name + ".invisibilityLevel", 0);
	globalPerms = new HashMap<String, Boolean>();
	isOPType = config.getBoolean("groups." + name + ".isOPType", false);
	isFillyType = config.getBoolean("groups." + name + ".isFillyType", false);
	listName = ChatColor.translateAlternateColorCodes('&', config.getString("groups." + name + ".listName"));
	{// get global perms
	    HashSet<String> negPerms = new HashSet<String>();
	    for (String gperm : config.getStringList("groups." + name + ".globalPermissions")) {
		if (gperm.startsWith("-")) {
		    negPerms.add(gperm.substring(1, gperm.length()));
		} else {
		    globalPerms.put(gperm, true);
		}
	    }
	    for (String negPerm : negPerms) {
		globalPerms.put(negPerm, false);
	    }
	}
	{// get world perms, add to worldPerms
	    for (PonyWorld world : PonyWorld.getPonyWorlds()) {
		if (!world.isOverworld()) {
		    continue;
		}
		String worldString = world.getName();
		BunchOfPerms worldBunchPerms;
		HashMap<String, Boolean> finalWorldPerms = new HashMap<String, Boolean>();
		for (String perm : globalPerms.keySet()) {
		    finalWorldPerms.put(perm, globalPerms.get(perm));
		}
		if (config.contains("groups." + name + ".worldPermissions." + worldString)) {
		    HashSet<String> negPerms = new HashSet<String>();
		    for (String worldPermission : config.getStringList("groups." + name + ".worldPermissions." + worldString)) {
			if (worldPermission.startsWith("-")) {
			    negPerms.add(worldPermission.substring(1, worldPermission.length()));
			} else {
			    finalWorldPerms.put(worldPermission, true);
			}
		    }
		    for (String negPerm : negPerms) {
			finalWorldPerms.put(negPerm, false);
		    }
		}
		worldBunchPerms = new BunchOfPerms(finalWorldPerms);
		worldPerms.put(worldString, worldBunchPerms);
	    }
	}
    }

    protected HashMap<String, Boolean> getPermissions(PonyWorld w) {
	return worldPerms.get(w.getOverworld().getName()).getPerms();
    }

    protected boolean canMoveTo(Player asker) {
	return asker.hasPermission(perm2add2);
    }

    protected boolean canMoveTo(ConsoleCommandSender asker) {
	return asker.hasPermission(perm2add2);
    }

    protected boolean canMoveTo(Permissible asker) {
	return asker.hasPermission(perm2add2);
    }

    public String getName() {
	return name;
    }

    public int getDefaultInvisLevel() {
	return invisLevel;
    }

    public boolean isOPType() {
	return isOPType;
    }

    public boolean isFillyType() {
	return isFillyType;
    }

    public String getListName() {
	return listName;
    }
}
