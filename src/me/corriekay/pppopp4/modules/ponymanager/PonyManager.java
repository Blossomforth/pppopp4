package me.corriekay.pppopp4.modules.ponymanager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.JoinEvent;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.UnhandledCommandException;
import me.corriekay.pppopp4.modules.equestria.PonyUniverse;
import me.corriekay.pppopp4.modules.equestria.PonyWorld;
import me.corriekay.pppopp4.modules.invisibilityhandler.InvisibilityHandler;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.modules.remoteponyadmin.RemotePony;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.permissions.Permissible;
import org.bukkit.permissions.PermissionAttachment;

public class PonyManager extends PSCmdExe {

    public static PonyManager ponyManager;

    public HashMap<String, PonyGroup> groups = new HashMap<String, PonyGroup>();

    private final HashMap<String, HashSet<String>> groupsList = new HashMap<String, HashSet<String>>();

    private final HashMap<String, PermissionAttachment> playerPerms = new HashMap<String, PermissionAttachment>();

    private PermissionAttachment consolePerms = Bukkit.getConsoleSender().addAttachment(Mane.getInstance());

    private PonyGroup[] listGroups;

    public PonyManager() {
	super("PonyManager", Ponies.PinkiePie, "setgroup", "list", "groupaddperm", "groupdelperm", "useraddperm", "userdelperm", "testperm");
	ponyManager = this;
	if (loadConfig("permissions.yml")) {
	    getConfig().createSection("groups.filly.globalPermissions");
	    for (PonyWorld w : PonyWorld.getPonyWorlds()) {
		if (w.isOverworld()) {
		    getConfig().createSection("groups.filly.worldPermissions." + w.getName());
		}
	    }
	    getConfig().set("groups.filly.worldPermissions.perm2add2", "ponymanager.groupadd.player");
	    getConfig().set("groups.filly.isOPType", false);
	    getConfig().set("groups.filly.invisibilityLevel", 0);
	    getConfig().set("groups.filly.listName", "");
	    saveConfig();
	}
	reloadPerms();
    }

    /*
     * Permission loading and checking
     */
    private void reloadPerms() {
	groups.clear();
	groupsList.clear();
	int arraysize = getConfig().getConfigurationSection("groups").getKeys(false).size();
	System.out.println("Settting size to " + arraysize); // DEBUG REMOVEME
	listGroups = new PonyGroup[arraysize];
	int number = 0;
	for (String group : getConfig().getConfigurationSection("groups").getKeys(false)) {
	    PonyGroup ponygroup = new PonyGroup(getConfig(), group);
	    groups.put(group, ponygroup);
	    groupsList.put(group, new HashSet<String>());
	    listGroups[getConfig().getInt("groups." + group + ".listNumber", number)] = ponygroup;
	    number++;
	}
	calculateAllPerms();
	ConsoleCommandSender ccs = Bukkit.getConsoleSender();
	ccs.removeAttachment(consolePerms);
	consolePerms = ccs.addAttachment(Mane.getInstance());
	for (String perm : getConfig().getStringList("consolePerms")) {
	    boolean add = !perm.startsWith("-");
	    if (!add) {
		perm = perm.substring(1, perm.length());
	    }
	    consolePerms.setPermission(perm, add);
	}
    }

    private void calculateAllPerms() {
	for (Player player : Bukkit.getOnlinePlayers()) {
	    calculatePerms(player, null);
	}
    }

    @SuppressWarnings("unchecked")
    private void calculatePerms(Player player, PonyWorld w) {
	PermissionAttachment pa = playerPerms.get(player.getName());
	if (pa != null) {
	    try {
		player.removeAttachment(pa);
	    } catch (IllegalArgumentException e) {
		System.out.println("exception thrown: player does not have permission attachment.");
	    }
	}
	pa = player.addAttachment(Mane.getInstance());
	Pony p = Ponyville.getPony(player);
	PonyGroup group = groups.get(p.getGroup());
	HashMap<String, Boolean> perms = null;
	if (w == null) {
	    perms = (HashMap<String, Boolean>) group.getPermissions(PonyWorld.getPonyWorld(player.getWorld())).clone();
	} else {
	    perms = (HashMap<String, Boolean>) group.getPermissions(w).clone();
	}
	{// Calculate negative permissions
	    HashSet<String> negPerms = new HashSet<String>();
	    for (String perm : p.getPerms()) {
		if (perm.startsWith("-")) {
		    negPerms.add(perm.substring(1, perm.length()));
		} else {
		    perms.put(perm, true);
		}
	    }
	    for (String negPerm : negPerms) {
		perms.put(negPerm, false);
	    }
	}
	for (String perm : perms.keySet()) {
	    pa.setPermission(perm, perms.get(perm));
	}
	for (String g : groupsList.keySet()) {
	    groupsList.get(g).remove(player.getName());
	}
	groupsList.get(group.getName()).add(player.getName());
	playerPerms.put(player.getName(), pa);
    }

    @SuppressWarnings("unchecked")
    public void setRemotePonyPermissions(RemotePony rp) {
	PermissionAttachment pa = rp.getAttachment();
	if (pa != null) {
	    try {
		rp.removeAttachment(pa);
	    } catch (IllegalArgumentException e) {
		e.printStackTrace();
	    }
	}
	pa = rp.addAttachment(Mane.getInstance());
	rp.setPPPoPPAttach(pa);
	Pony p = rp.getPony();
	PonyGroup group = groups.get(p.getGroup());
	HashMap<String, Boolean> perms = (HashMap<String, Boolean>) group.globalPerms.clone();
	for (String s : perms.keySet()) {
	    pa.setPermission(s, perms.get(s));
	}
    }

    /*
     * Listeners
     */
    @PonyEvent
    public void onJoin(JoinEvent event) {
	if (event.isJoining()) {
	    calculatePerms(event.getPlayer(), null);
	}
    }

    @PonyEvent
    public void onQuit(QuitEvent event) {
	if (event.isQuitting()) {
	    for (String g : groupsList.keySet()) {
		groupsList.get(g).remove(event.getPlayer().getName());
	    }
	    playerPerms.remove(event.getPlayer().getName());
	}
    }

    @PonyEvent
    public void onTeleport(PlayerTeleportEvent event) {
	PonyUniverse to = PonyWorld.getPonyWorld(event.getTo()).getUniverse(), from = PonyWorld.getPonyWorld(event.getFrom()).getUniverse();
	if (!to.equals(from)) {
	    calculatePerms(event.getPlayer(), to.getOverworld());
	}
    }

    /*
     * Commands
     */
    @Override
    public boolean optionalCommand(CommandSender sender, CommandMetadata cmd, String label, String[] args) throws UnhandledCommandException {
	switch (cmd) {
	case setgroup: {
	    OfflinePlayer target = getOnlineOfflinePlayer(args[0], sender);
	    if (target == null) {
		return true;
	    }
	    Pony pony = Ponyville.getPony(target);
	    PonyGroup group = getGroup(args[1]);
	    if (group == null) {
		sendMessage(sender, "Hmm... I cant seem to find that group!");
		return true;
	    }
	    if (!canSetGroup(sender, group)) {
		sendMessage(sender, "Hey, you cant put them in that group! Whaddya try'na do, break the server??");
		return true;
	    }
	    if (!setGroup(pony, group)) {
		sendMessage(sender, "That user is already in that group!");
	    }
	    return true;
	}
	case list: {
	    showList(sender);
	    return true;
	}
	case groupaddperm: {
	    PonyUniverse universe = null;
	    if (args.length >= 3) {
		universe = getUniverse(args[2]);
		if (universe == null) {
		    sendMessage(sender, "That world could not be found!");
		    return true;
		}
	    }
	    PonyGroup group = getGroup(args[0]);
	    if (group == null) {
		sendMessage(sender, "Hmm... I cant seem to find that group!");
		return true;
	    }
	    String permission = args[1];
	    if (!groupAddPerm(permission, group, universe)) {
		sendMessage(sender, "Oh you... That group already has that permission for that universe!");
	    } else {
		sendMessage(sender, "Yay! You added " + permission + " to group " + group.getName() + "!");
	    }
	    return true;
	}
	case groupdelperm: {
	    PonyUniverse universe = null;
	    if (args.length >= 3) {
		universe = getUniverse(args[2]);
		if (universe == null) {
		    sendMessage(sender, "That world could not be found!");
		    return true;
		}
	    }
	    PonyGroup group = getGroup(args[0]);
	    if (group == null) {
		sendMessage(sender, "Hmm... I cant seem to find that group!");
		return true;
	    }
	    String permission = args[1];
	    if (!groupDelPerm(permission, group, universe)) {
		sendMessage(sender, "That group doesnt have that permission! What do you want me to do, make a DOUBLE NEGATIVE PERMISSION??");
		return true;
	    } else {
		sendMessage(sender, "Removed permission " + permission + " from group " + group.getName() + "!");
	    }
	    return true;
	}
	case useraddperm: {
	    OfflinePlayer target = getOnlineOfflinePlayer(args[0], sender);
	    if (target == null) {
		return true;
	    }
	    Pony pony = Ponyville.getPony(target);
	    String permission = args[1];
	    if (!userAddPerm(permission, pony)) {
		sendMessage(sender, "Huh. That user already has that permission! :D");
	    } else {
		sendMessage(sender, "You added " + permission + " to " + target.getName() + "'s permissions list!");
	    }
	    return true;
	}
	case userdelperm: {
	    OfflinePlayer target = getOnlineOfflinePlayer(args[0], sender);
	    if (target == null) {
		return true;
	    }
	    Pony pony = Ponyville.getPony(target);
	    String permission = args[1];
	    if (!userDelPerm(permission, pony)) {
		sendMessage(sender, "Huh. That user doesnt have that permission anyways! :D");
	    } else {
		sendMessage(sender, "You removed " + permission + " to " + target.getName() + "'s permissions list!");
	    }
	    return true;
	}
	case testperm: {
	    Player player = getOnlinePlayer(args[0], sender);
	    if (player != null) {
		sendMessage(sender, "Does the player have permission " + args[1] + "? " + testPerm(args[1], player));
	    }
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    /**
     * @param user
     * @param group
     * @return true if successful, false if user is already in the group.
     */
    public boolean setGroup(Pony user, PonyGroup group) {
	String groupName = user.getGroup();
	if (groupName.equals(group.getName())) {
	    return false;
	}
	user.setGroup(group.getName());
	user.setInvisibleLevel(group.getDefaultInvisLevel());
	user.save();
	OfflinePlayer op = user.getPlayer();
	if (op.isOnline()) {
	    Player player = (Player) op;
	    sendMessage(player, "Yay! You're a pretty " + group.getName() + "!!!");
	    calculatePerms(player, null);
	}
	broadcastMessage(pony.says() + user.getName() + " was moved to group " + group.getName(), PonyPermission.ALERT_GROUP_CHANGE);
	// TODO RPA message
	return true;
    }

    public void showList(CommandSender sender) {
	Pony pony = null;
	if (sender instanceof Player) {
	    pony = Ponyville.getPony((Player) sender);
	}
	if (sender instanceof RemotePony) {
	    pony = ((RemotePony) sender).getPony();
	}
	String finalDisplay = "";
	int onlineCount = 0;
	for (PonyGroup group : listGroups) {
	    int grouponlinecount = 0;
	    String display = group.getListName() + ChatColor.WHITE + ": " + ChatColor.WHITE;
	    HashSet<String> pones = groupsList.get(group.getName());
	    if (pones.size() < 1) {
		continue;
	    }
	    for (String name : groupsList.get(group.getName())) {
		Pony listpony = Ponyville.getPony(name);
		boolean showMe = true;
		if (InvisibilityHandler.handler.isHidden(listpony.getName())) {
		    showMe = false;
		    if (pony == null) {
			showMe = true;
		    } else {
			showMe = pony.canSeeInvisible(listpony);
		    }
		}
		if (showMe) {
		    onlineCount++;
		    grouponlinecount++;
		    display += listpony.getNickname() + ChatColor.WHITE + ", " + ChatColor.WHITE;
		}
	    }
	    if (grouponlinecount > 0) {
		finalDisplay += "\n" + display.substring(0, display.length() - 6);
	    }
	}
	sendMessage(sender, "There are " + onlineCount + " of " + Bukkit.getServer().getMaxPlayers() + " max players online!" + (onlineCount > 0 ? finalDisplay : ""));
    }

    /**
     * @param addPerm
     * @param group
     * @param universe
     * @return true if successful, false if group already has the permission
     */
    public boolean groupAddPerm(String addPerm, PonyGroup group, PonyUniverse universe) {
	String path = "groups." + group.getName() + ".";
	if (universe == null) {
	    path += "globalPermissions";
	} else {
	    path += universe.getName();
	}
	List<String> perms = getConfig().getStringList(path);
	if (perms == null) {
	    perms = new ArrayList<String>();
	}
	if (perms.contains(addPerm)) {
	    return false;
	}
	perms.add(addPerm);
	getConfig().set(path, perms);
	saveConfig();
	reloadPerms();
	return true;
    }

    /**
     * @param delPerm
     * @param group
     * @param universe
     * @return trueif successful, false if group did not have the permission
     */
    public boolean groupDelPerm(String delPerm, PonyGroup group, PonyUniverse universe) {
	String path = "groups." + group.getName() + ".";
	if (universe == null) {
	    path += "globalPermissions";
	} else {
	    path += universe.getName();
	}
	List<String> perms = getConfig().getStringList(path);
	if (perms == null || !perms.contains(delPerm)) {
	    return false;
	} else {
	    perms.remove(delPerm);
	    if (perms.size() == 0) {
		perms = null;
	    }
	    getConfig().set(path, perms);
	    saveConfig();
	    reloadPerms();
	}
	return true;
    }

    /**
     * @param addPerm
     * @param user
     * @return true if successful, false if user already has the permission
     */
    public boolean userAddPerm(String addPerm, Pony user) {
	ArrayList<String> perms = user.getPerms();
	if (perms.contains(addPerm)) {
	    return false;
	} else {
	    perms.add(addPerm);
	    user.setPerms(perms);
	    user.save();
	    reloadPerms(user, null);
	    return true;
	}
    }

    /**
     * @param delPerm
     * @param user
     * @return true if successful, false if user doesnt have the permission
     */
    public boolean userDelPerm(String delPerm, Pony user) {
	ArrayList<String> perms = user.getPerms();
	if (!perms.contains(delPerm)) {
	    return false;
	} else {
	    perms.remove(delPerm);
	    user.setPerms(perms);
	    user.save();
	    reloadPerms(user, null);
	    return true;
	}
    }

    /**
     * @param testPerm
     * @param user
     * @return true if has perm, false if not
     */
    public boolean testPerm(String testPerm, Player user) {
	return user.hasPermission(testPerm);
    }

    /*
     * Util Methods
     */
    public PonyGroup getGroup(String name) {
	return groups.get(name);
    }

    public boolean canSetGroup(Permissible tester, PonyGroup group) {
	return tester.hasPermission(group.perm2add2);
    }

    public PonyUniverse getUniverse(String arg) {
	PonyWorld world = PonyWorld.getPonyWorld(arg);
	if (world == null) {
	    return null;
	}
	return world.getUniverse();
    }

    /**
     * @param pony
     * @param world
     * @return true if the player was online and has had the permissions
     *         reloaded, false if they were offline and nothing happened
     */
    public boolean reloadPerms(Pony pony, PonyWorld world) {
	if (pony.getPlayer().isOnline()) {
	    Player player = (Player) pony.getPlayer();
	    calculatePerms(player, world);
	    return true;
	}
	return false;
    }
}
