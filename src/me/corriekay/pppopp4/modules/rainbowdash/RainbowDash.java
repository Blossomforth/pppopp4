package me.corriekay.pppopp4.modules.rainbowdash;

import java.util.HashMap;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.equestria.PonyWorld;
import net.minecraft.server.v1_7_R1.EntityLightning;
import net.minecraft.server.v1_7_R1.EntityPlayer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftLightningStrike;
import org.bukkit.craftbukkit.v1_7_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.weather.LightningStrikeEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class RainbowDash extends PSCmdExe {

    private static String rainbowSays = "�cR�6a�ei�an�9b�5o�cw �6D�ea�as�9h" + ChatColor.AQUA + ": ";

    private final HashMap<String, Boolean> zeusMap = new HashMap<String, Boolean>();

    public RainbowDash() {
	super("RainbowDash", Ponies.RainbowDash, "weather", "zeus", "time"); // TODO
									     // Figure
									     // out
									     // how
									     // to
									     // work
									     // the
									     // superstorm
									     // back
									     // into
									     // the
									     // server
									     // without
									     // everyone
									     // breaking
									     // their
									     // speakers.
    }

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case weather: {
	    World world = player.getWorld();
	    ;
	    if (args.length > 0) {
		world = Bukkit.getServer().getWorld(args[0]);
		if (world == null) {
		    sendMessage(player, "World not found!");
		    return true;
		}
		return true;
	    }
	    if (label.equals("sun")) {
		world.setWeatherDuration(10 * 60 * 20);
		world.setThundering(false);
		world.setStorm(false);
		sendMessage(player, "Weather set to sunny! I'll clear the skies in ten seconds flat!");
		return true;
	    }
	    if (label.equals("storm")) {
		world.setWeatherDuration(10 * 60 * 20);
		world.setStorm(true);
		world.setThundering(true);
		world.setThunderDuration(10 * 60 * 20);
		sendMessage(player, "I LOVE THUNDERSTORMS! Heres a good one!");
		return true;
	    }
	    if (label.equals("rain")) {
		world.setWeatherDuration(10 * 60 * 20);
		world.setStorm(true);
		sendMessage(player, "A rainstorm? alright! Lemme just get out my rainclouds!");
		return true;
	    }
	    if (label.equals("day")) {
		world.setTime(6000);
		player.sendMessage("Princess Celestia: " + ChatColor.GOLD + "Let there be light!");
		return true;
	    }
	    if (label.equals("night")) {
		world.setTime(18000);
		player.sendMessage(ChatColor.BLUE + "Princess Luna: " + ChatColor.DARK_BLUE + "I shall bring forth the moon!");
		return true;
	    }
	    return true;
	}
	case zeus: {
	    if (zeusMap.containsKey(player.getName())) {
		zeusMap.remove(player.getName());
		sendMessage(player, "Lightning disabled!");
		return true;
	    }
	    zeusMap.put(player.getName(), (args.length > 0 && args[0].equals("true")));
	    sendMessage(player, "Lightning enabled!");
	    return true;
	}
	case time: {
	    World world = player.getWorld();
	    ;
	    if (args.length > 0) {
		world = Bukkit.getServer().getWorld(args[0]);
		if (world == null) {
		    sendMessage(player, "World not found!");
		    return true;
		}
	    }
	    if (label.equals("time")) {
		long time = world.getTime() + 6000;
		if (time > 24000) {
		    time = time - 24000;
		}
		String half = "am";
		if (time > 12000) {
		    half = "pm";
		    time = time - 12000;
		}
		int minutes = (int) Math.floor(time / 16.66);
		int hour = 0;
		while (minutes > 59) {
		    hour++;
		    minutes = minutes - 60;
		}
		if (hour == 0) {
		    hour = 12;
		}
		String minuteString = minutes + "";
		if (minuteString.length() == 1) {
		    if (minuteString.equals("0")) {
			minuteString = "00";
		    } else {
			minuteString = "0" + minuteString;
		    }
		}
		sendMessage(player, "The time is " + hour + ":" + minuteString + " " + half);
		return true;
	    }
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    @PonyEvent
    public void onQuit(QuitEvent event) {
	if (event.isQuitting()) {
	    zeusMap.remove(event.getPlayer().getName());
	}
    }

    @SuppressWarnings("deprecation")
    @PonyEvent(priority = EventPriority.LOWEST)
    public void interactEvent(PlayerInteractEvent event) {
	Player player = event.getPlayer();
	if (zeusMap.containsKey(player.getName())) {
	    if (event.getAction() == Action.LEFT_CLICK_AIR || event.getAction() == Action.LEFT_CLICK_BLOCK) {
		event.setCancelled(true);
		Location l = player.getTargetBlock(null, 100).getLocation();
		if (zeusMap.get(player.getName())) {
		    player.getWorld().strikeLightning(l);
		} else {
		    player.getWorld().strikeLightningEffect(l);
		}
	    }
	}
    }

    @PonyEvent
    public void weatherChange(WeatherChangeEvent event) {
	PonyWorld world = PonyWorld.getPonyWorld(event.getWorld());
	if (world.isCreative() && event.toWeatherState()) {
	    event.setCancelled(true);
	    event.getWorld().setThundering(false);
	    event.getWorld().setStorm(false);
	}
    }

    // Method that fixes bukkits silly lightning bug
    // @PonyEvent (priority = EventPriority.HIGHEST)
    public void onLightning(LightningStrikeEvent event) {
	if (!event.isCancelled()) {
	    event.setCancelled(true);
	    Location l = event.getLightning().getLocation();
	    for (Player player : Bukkit.getOnlinePlayers()) {
		if (l.getWorld().equals(player.getWorld())) {
		    Location ll = player.getLocation();
		    // int x, z;
		    // x = Math.abs((l.getBlockX() - ll.getBlockX()));
		    // z = Math.abs((l.getBlockZ() - ll.getBlockZ()));
		    // if(invSqrt((x * x) + (z * z)) > 512) {
		    if (ll.distance(l) < 512) {
			EntityPlayer ep = ((CraftPlayer) player).getHandle();
			EntityLightning el = ((CraftLightningStrike) event.getLightning()).getHandle();
			Packet71Weather lstrike = new Packet71Weather(el);
			ep.playerConnection.sendPacket(lstrike);
		    }
		}
	    }
	}
    }

    // private float invSqrt(float x){
    // float xhalf = 0.5f * x;
    // int i = Float.floatToIntBits(x);
    // i = 0x5f3759df;
    // x = Float.intBitsToFloat(i);
    // x = x * (1.5f - xhalf * x * x);
    // return x;
    // }

    public void sendMessage(CommandSender sender, String message) {
	sender.sendMessage(rainbowSays + message);
    }
}
