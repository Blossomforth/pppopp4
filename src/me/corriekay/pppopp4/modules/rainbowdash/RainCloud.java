package me.corriekay.pppopp4.modules.rainbowdash;

import java.util.Random;

public class RainCloud {

    int x, y, z;
    Random r = new Random();

    public RainCloud(long seed) {
	r = new Random(seed);
	x = r.nextInt(14) + 1;
	y = r.nextInt(14) + 1;
	z = r.nextInt(14) + 1;
    }

    public boolean countDownX() {
	x--;
	if (x < 1) {
	    x = r.nextInt(14) + 1;
	    return true;
	}
	return false;
    }

    public boolean countDownY() {
	y--;
	if (y < 1) {
	    y = r.nextInt(14) + 1;
	    return true;
	}
	return false;
    }

    public boolean countDownZ() {
	z--;
	if (z < 1) {
	    z = r.nextInt(14) + 1;
	    return true;
	}
	return false;
    }

}
