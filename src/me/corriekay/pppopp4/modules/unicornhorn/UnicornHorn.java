package me.corriekay.pppopp4.modules.unicornhorn;

import java.util.HashMap;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.JoinEvent;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class UnicornHorn extends PSCmdExe {

    private HashMap<String, Horn> horns = new HashMap<String, Horn>();

    public UnicornHorn() {
	super("UnicornHorn", Ponies.Rarity, "horn");
	for (Player player : Bukkit.getOnlinePlayers()) {
	    Pony pony = Ponyville.getPony(player);
	    setPlayer(pony);
	}
    }

    private void setPlayer(Pony player) {
	Horn horn = new Horn();
	horn.left = player.getHornLeft();
	horn.right = player.getHornRight();
	horn.on = player.getHornOn();
	horns.put(player.getName(), horn);
    }

    private void removePlayer(Pony player) {
	horns.remove(player.getName());
    }

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case horn: {
	    String arg = args[0];
	    Horn h = horns.get(player.getName());
	    if (arg.equals("help")) {
		sendMessage(player, "Unicorn Horn is a plugin designed to let you bind commands to the left and right click actions of the stick.\nType /horn on/off to turn the use of the plugin on or off.\nType /horn left/right \"command blah blah\" to set a command.");
		return true;
	    }
	    if (arg.equals("on")) {
		h.on = true;
		sendMessage(player, "Setting horn on!");
		saveMacro(player.getName());
		return true;
	    }
	    if (arg.equals("off")) {
		h.on = false;
		sendMessage(player, "Setting horn off!");
		saveMacro(player.getName());
		return true;
	    }
	    if (args.length < 2) {
		sendMessage(player, notEnoughArgs);
		return true;
	    }
	    String macro = Utils.joinStringArray(args, " ", 1);
	    if (arg.equals("left")) {
		h.left = macro;
		sendMessage(player, "Left macro set!");
		saveMacro(player.getName());
		return true;
	    }
	    if (arg.equals("right")) {
		h.right = macro;
		sendMessage(player, "Right macro set!");
		saveMacro(player.getName());
		return true;
	    }
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    private void saveMacro(String player) {
	Pony pony = Ponyville.getPony(player);
	Horn h = horns.get(player);
	pony.setHornOn(h.on);
	pony.setHornLeft(h.left);
	pony.setHornRight(h.right);
	pony.save();
    }

    @PonyEvent
    public void quit(QuitEvent event) {
	if (event.isQuitting()) {
	    removePlayer(event.getPony());
	}
    }

    @PonyEvent
    public void join(JoinEvent event) {
	if (event.isJoining()) {
	    setPlayer(event.getPony());
	}
    }

    @PonyEvent
    public void interact(PlayerInteractEvent event) {
	Action a = event.getAction();
	if (a == Action.PHYSICAL) {
	    return;
	}
	if (event.getPlayer().getItemInHand().getType() == Material.STICK) {
	    Horn h = horns.get(event.getPlayer().getName());
	    if (!h.on) {
		return;
	    }
	    Player player = event.getPlayer();
	    event.setCancelled(true);
	    if (a == Action.LEFT_CLICK_AIR || a == Action.LEFT_CLICK_BLOCK) {
		String leftcmd = h.left;
		if (leftcmd != null) {
		    player.chat("/" + leftcmd);
		} else {
		    sendMessage(player, "Your left macro is not set! Set it with /horn left <cmd>!");
		}
		return;
	    }
	    if (a == Action.RIGHT_CLICK_AIR || a == Action.RIGHT_CLICK_BLOCK) {
		String rightcmd = h.right;
		if (rightcmd != null) {
		    player.chat("/" + rightcmd);
		} else {
		    sendMessage(player, "Your right macro is not set! Set it with /horn right <cmd>!");
		}
		return;
	    }
	}
    }
}
