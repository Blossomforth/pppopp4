package me.corriekay.pppopp4.modules.unicornhorn;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.command.validation.tabbing.BasicTabRule;

import org.bukkit.command.CommandSender;

public class HornTab implements BasicTabRule {

    private final List<String> tab = new ArrayList<String>();

    public HornTab() {
	tab.add("left");
	tab.add("right");
	tab.add("on");
	tab.add("off");
	tab.add("help");
    }

    @Override
    public List<String> tab(CommandSender sender, CommandMetadata cmd, String argBit, String[] args) {
	return tab;
    }

}
