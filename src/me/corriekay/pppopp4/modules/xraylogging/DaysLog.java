package me.corriekay.pppopp4.modules.xraylogging;

import java.util.HashMap;

public class DaysLog {

    public HashMap<String, PlayerBreaks> breaks = new HashMap<String, PlayerBreaks>();
    public HashMap<String, String> claimers = new HashMap<String, String>();

    public String renderPlayersList() {
	String s = "";
	for (String player : breaks.keySet()) {
	    PlayerBreaks bbreak = breaks.get(player);
	    s += "\n" + bbreak.logScopeRender();
	}
	return s;
    }

    public PlayerBreaks getClaimed(String claimer) {
	if (!hasClaimed(claimer)) {
	    return null;
	}
	return breaks.get(claimers.get(claimer));
    }

    public String getClaim(String claimer) {
	return claimers.get(claimer);
    }

    public boolean isFinished(String claim) {
	PlayerBreaks bbreak = breaks.get(claim);
	return (bbreak == null || bbreak.mark);
    }

    public boolean isClaimed(String claim) {
	return claimers.containsValue(claim);
    }

    public boolean hasClaimed(String claimer) {
	return claimers.containsKey(claimer);
    }

    public boolean isInList(String target) {
	return breaks.containsKey(target);
    }

    public String getCompletedSqlQuery() {
	String s = "";
	boolean sub = false;
	for (String breakss : breaks.keySet()) {
	    if (breaks.get(breakss).mark) {
		sub = true;
		s += breakss + ",";
	    }
	}
	if (sub) {
	    s = s.substring(0, s.length() - 1);
	}
	return s;
    }

    public boolean claim(String claimer, String claimee) {
	if (claimee == null) {
	    for (String toClaim : breaks.keySet()) {
		if (!isClaimed(toClaim) || !isFinished(toClaim)) {
		    if (claim(claimer, toClaim)) {
			return true;
		    }
		}
	    }
	    return false;
	}
	if (isClaimed(claimee) || isFinished(claimee)) {
	    return false;
	}
	PlayerBreaks bbreak = breaks.get(claimee);
	if (bbreak == null) {
	    return false;
	}
	bbreak.setClaimedBy(claimer);
	claimers.put(claimer, claimee);
	return true;
    }
}
