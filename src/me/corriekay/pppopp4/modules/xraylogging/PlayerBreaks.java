package me.corriekay.pppopp4.modules.xraylogging;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;

public class PlayerBreaks {

    public final String name;
    public HashMap<Location, Material> breakLocations;
    public ArrayList<Location> parsedLocations = new ArrayList<Location>();

    int current = 0;
    public boolean mark = false;
    private String claimedBy;

    public PlayerBreaks(String name, HashMap<Location, Material> locs) {
	this.name = name;
	breakLocations = locs;
	parseLocations();
    }

    public void parseLocations() {
	for (Location l1 : breakLocations.keySet()) {
	    boolean add = true;
	    Material type = breakLocations.get(l1);
	    for (Location l2 : parsedLocations) {
		Material type2 = breakLocations.get(l2);
		if (type.equals(type2)) {
		    if (l1.distance(l2) < 5) {
			add = false;
		    }
		}
	    }
	    if (add) {
		parsedLocations.add(l1);
	    }
	}
    }

    public Material getType(Location l) {
	return breakLocations.get(l);
    }

    public Location next() {
	current++;
	return parsedLocations.get(current);
    }

    public boolean hasNext() {
	return parsedLocations.size() - 1 >= current + 1;
    }

    public Location previous() {
	current--;
	return parsedLocations.get(current);
    }

    public boolean hasPrevious() {
	return current - 1 >= 0;
    }

    public Location current() {
	return parsedLocations.get(current);
    }

    public boolean hasIndex(int i) {
	return parsedLocations.size() - 1 >= i;
    }

    public Location getIndex(int i) {
	return parsedLocations.get(i);
    }

    public String claimedBy() {
	return claimedBy;
    }

    public void setClaimedBy(String name) {
	claimedBy = name;
    }

    public String logScopeRender() {
	String returnMe = name;
	if (mark) {
	    return ChatColor.GREEN + returnMe + " (finished)";
	} else if (claimedBy != null) {
	    return ChatColor.YELLOW + name + " (claimed by " + claimedBy + ")";
	} else {
	    return ChatColor.RED + name + " (unclaimed)";
	}
    }

    public String render() {
	String returnMe = "";
	for (Location l : parsedLocations) {
	    returnMe += "\nWorld: " + l.getWorld().getName() + ". x: " + l.getBlockX() + ". y: " + l.getBlockY() + ". z: " + l.getBlockZ();
	}
	return returnMe;
    }

    public String renderRaw() {
	String returnMe = "";
	for (Location l : breakLocations.keySet()) {
	    returnMe += "\nWorld: " + l.getWorld().getName() + ". x: " + l.getBlockX() + ". y: " + l.getBlockY() + ". z: " + l.getBlockZ();
	}
	return returnMe;
    }
}
