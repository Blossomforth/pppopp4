package me.corriekay.pppopp4.modules.xraylogging;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.ponymanager.PonyGroup;
import me.corriekay.pppopp4.modules.ponymanager.PonyManager;
import me.corriekay.pppopp4.modules.ponymanager.PonyPermission;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.utilities.PonyLogger;

import org.bukkit.*;
import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.BlockBreakEvent;

public class XrayLogger extends PSCmdExe {

    private Connection database;
    private boolean connected = false;
    private final HashSet<Material> advancedTypes = new HashSet<Material>();
    private int day;
    private final XrayLogger logger;
    private DaysLog daysLog = null;
    private String dateString = null;

    public XrayLogger() {
	super("XrayLogger", Ponies.PinkiePie, "xray");
	logger = this;
	if (loadConfig("xraylogger.yml")) {
	    getConfig().set("host", "localhost");
	    getConfig().set("port", "port");
	    getConfig().set("database", "db");
	    getConfig().set("login", "login");
	    getConfig().set("password", "password");
	    getConfig().createSection("advancedMaterials");
	    saveConfig();
	}
	for (String s : getConfig().getStringList("advancedMaterials")) {
	    Material type = Material.getMaterial(s);
	    advancedTypes.add(type);
	}
	day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
	Bukkit.getScheduler().runTaskTimer(Mane.getInstance(), new Runnable() {
	    public void run() {
		int dayy = day;
		day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		if (dayy != day) {
		    logger.broadcastMessage("A new day! Xray logs are now ready!", PonyPermission.IS_OPERATOR);
		}
	    }
	}, 0, 1 * 60 * 20);
	String url = "jdbc:mysql://" + getConfig().getString("host") + ":" + getConfig().getString("port") + "/" + getConfig().getString("database");
	try {
	    Class.forName("com.mysql.jdbc.Driver");
	    database = DriverManager.getConnection(url, getConfig().getString("login"), getConfig().getString("password"));
	    executeSQL("CREATE TABLE IF NOT EXISTS xraylogging (timestamp varchar(20) not null, playername varchar(20) not null, type varchar(20) not null, world varchar(50) not null, x varchar(15) not null, y varchar(15) not null, z varchar(15) not null);", true);
	    executeSQL("CREATE TABLE IF NOT EXISTS xraydays (day varchar(10) not null primary key, completedplayers varchar(5000) not null);", true);
	} catch (Exception e) {
	    Mane.getInstance().getLogger().severe("Connection to xray database failed!");
	    e.printStackTrace();
	    return;
	}
	connected = true;
	Bukkit.getScheduler().runTaskTimer(Mane.getInstance(), new Runnable() {
	    public void run() {
		try {
		    executeSQL("/* ping */ SELECT 1", false);
		} catch (SQLException e) {
		    System.out.println("ERROR RUNNING SQL PING");
		}
	    }
	}, 0, 20 * 60 * 60);
    }

    private void logMine(final Player player, final Material type, final Location location) {
	if (!connected) {
	    return;
	}
	Runnable r = new Runnable() {
	    public void run() {
		String query = "INSERT INTO xraylogging VALUES ('" + System.currentTimeMillis() + "', '" + player.getName() + "', '" + type.name() + "', '" + location.getWorld().getName() + "', '" + location.getBlockX() + "', '" + location.getBlockY() + "', '" + location.getBlockZ() + "');";
		try {
		    executeSQL(query, true);
		} catch (SQLException e) {
		    Mane.getInstance().getLogger().severe("SQL exception caught!!!");
		    PonyLogger.logListenerException(e, "SQL exception caught! sql query: " + query, name, "onMine");
		}
	    }
	};
	Bukkit.getScheduler().runTaskAsynchronously(Mane.getInstance(), r);
    }

    private ResultSet executeSQL(String query, boolean update) throws SQLException {
	Statement stmt = database.createStatement();
	if (!query.endsWith(";")) {
	    query = query += ";";
	}
	if (update) {
	    stmt.executeUpdate(query);
	    return null;
	} else {
	    return stmt.executeQuery(query);
	}
    }

    protected boolean loadDay(Calendar cal, String newDateString) throws SQLException {
	long min, max;
	min = cal.getTimeInMillis();
	Calendar cal2 = (Calendar) cal.clone();
	cal2.set(Calendar.HOUR_OF_DAY, 0);
	cal2.set(Calendar.MINUTE, 0);
	cal2.set(Calendar.SECOND, 0);
	cal2.set(Calendar.MILLISECOND, 0);
	cal2.add(Calendar.DAY_OF_MONTH, 1);
	max = cal2.getTimeInMillis() - 1;
	String query = "SELECT * FROM xraylogging WHERE (timestamp <= " + max + " AND timestamp >= " + min + ");";
	ResultSet set = executeSQL(query, false);
	HashMap<String, HashMap<Location, Material>> breakslist = new HashMap<String, HashMap<Location, Material>>();
	int finds = 0;
	while (set.next()) {
	    finds++;
	    String name = set.getString("playername");
	    Material type = Material.getMaterial(set.getString("type"));
	    World w = Bukkit.getWorld(set.getString("world"));
	    int x, y, z;
	    x = set.getInt("x");
	    y = set.getInt("y");
	    z = set.getInt("z");
	    Location l = new Location(w, x, y, z);
	    if (!breakslist.containsKey(name)) {
		breakslist.put(name, new HashMap<Location, Material>());
	    }
	    breakslist.get(name).put(l, type);
	}
	if (finds == 0) {
	    daysLog = null;
	    return false;
	}
	DaysLog logs = new DaysLog();
	for (String name : breakslist.keySet()) {
	    PlayerBreaks breaks = new PlayerBreaks(name, breakslist.get(name));
	    logs.breaks.put(name, breaks);
	}
	if (dateString != null && daysLog != null) {
	    String intoQuery = daysLog.getCompletedSqlQuery();
	    String query2 = "INSERT INTO xraydays VALUES ('" + dateString + "', '" + intoQuery + "') ON DUPLICATE KEY UPDATE completedPlayers='" + intoQuery + "';";
	    executeSQL(query2, true);
	}
	// Get current days logs completion
	daysLog = logs;
	dateString = newDateString;
	ResultSet completed = executeSQL("SELECT * FROM xraydays WHERE (day='" + dateString + "');", false);
	if (completed.next()) {
	    String[] completeds = completed.getString("completedPlayers").split(",");
	    for (String imComplete : completeds) {
		if (!imComplete.equals("")) {
		    System.out.println("setting marked for " + imComplete);
		    daysLog.breaks.get(imComplete).mark = true;
		}
	    }
	}
	return true;
    }

    /*
     * Bookmark Commands
     */

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case xray: {
	    String arg = args[0];
	    if (arg.equals("load")) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		if (args.length > 1) {
		    String date = args[1];
		    if (!date.matches("^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d$")) {
			sendMessage(player, "Date format error!! Please use mm/dd/yyyy! If the month has only one digit, put a 0 in front of it!");
			return true;
		    }
		    String[] dates = date.split("[-/.:]");
		    int month, day, year;
		    month = Integer.parseInt(dates[0]);
		    day = Integer.parseInt(dates[1]);
		    year = Integer.parseInt(dates[2]);
		    cal.set(Calendar.MONTH, month);
		    cal.add(Calendar.MONTH, -1);
		    cal.set(Calendar.DAY_OF_MONTH, day);
		    cal.set(Calendar.YEAR, year);
		}
		SimpleDateFormat sdf = new SimpleDateFormat();
		sdf.applyPattern("MM/dd/yyyy");
		String newDate = sdf.format(cal.getTime());
		broadcastMessage(pony.says() + "loading xray logs for " + newDate, PonyPermission.IS_OPERATOR);
		if (!loadDay(cal, newDate)) {
		    broadcastMessage(pony.says() + "Day has no logged mines!", PonyPermission.IS_OPERATOR);
		}
		return true;
	    }
	    if (daysLog == null) {
		sendMessage(player, "There are no logs loaded for today!");
		return true;
	    }
	    if (arg.equals("next")) {
		PlayerBreaks breaks = daysLog.getClaimed(player.getName());
		if (breaks == null) {
		    sendMessage(player, "Youve not claimed any logs!");
		    return true;
		}
		if (breaks.hasNext()) {
		    Location l = breaks.next();
		    player.teleport(l);
		    sendMessage(player, "Mined: " + breaks.getType(l).toString());
		    return true;
		}
		sendMessage(player, "No next location! If the finished with this players logs, type /xray markplayerfinished!");
		return true;
	    } else if (arg.equals("last")) {
		PlayerBreaks breaks = daysLog.getClaimed(player.getName());
		if (breaks == null) {
		    sendMessage(player, "Youve not claimed any logs!");
		    return true;
		}
		if (breaks.hasPrevious()) {
		    Location l = breaks.previous();
		    player.teleport(l);
		    sendMessage(player, "Mined: " + breaks.getType(l).toString());
		    return true;
		}
		sendMessage(player, "There is no previous location!");
		return true;
	    } else if (arg.equals("current")) {
		PlayerBreaks breaks = daysLog.getClaimed(player.getName());
		if (breaks == null) {
		    sendMessage(player, "Youve not claimed any logs!");
		    return true;
		}
		Location l = breaks.current();
		player.teleport(l);
		sendMessage(player, "Mined: " + breaks.getType(l).toString());
		return true;
	    } else if (arg.equals("claim")) {
		if (daysLog.hasClaimed(player.getName())) {
		    sendMessage(player, "You cant claim two xray logs at a time!");
		    return true;
		}
		String claim = null;
		if (args.length > 1) {
		    try {
			claim = getOnlineOfflinePlayer(args[1], player).getName();
		    } catch (Exception e) {
			return true;
		    }
		}
		if (!daysLog.claim(player.getName(), claim)) {
		    if (args.length > 1) {
			if (daysLog.isClaimed(claim)) {
			    sendMessage(player, "That xray log was claimed by another OPony!");
			} else if (daysLog.isFinished(claim)) {
			    sendMessage(player, claim + " is already finished!");
			} else {
			    sendMessage(player, claim + " didnt mine any ores in this log!");
			}
		    } else {
			sendMessage(player, "All claims are either finished, or claimed!");
		    }
		} else {
		    broadcastMessage(pony.says() + daysLog.getClaim(player.getName()) + "'s xray logs was claimed by " + player.getDisplayName() + ChatColor.LIGHT_PURPLE + "!", PonyPermission.IS_OPERATOR);
		}
		return true;
	    } else if (arg.equals("markplayerfinished")) {
		if (!daysLog.hasClaimed(player.getName())) {
		    sendMessage(player, "You've not claimed any xray logs!");
		    return true;
		}
		PlayerBreaks bbreaks = daysLog.getClaimed(player.getName());
		bbreaks.mark = true;
		daysLog.claimers.remove(player.getName());
		broadcastMessage(pony.says() + player.getDisplayName() + ChatColor.LIGHT_PURPLE + " has completed " + bbreaks.name + "'s xray logs!", PonyPermission.IS_OPERATOR);
		return true;
	    } else if (arg.equals("markdayfinished")) {

	    } else if (arg.equals("viewday")) {
		sendMessage(player, "The current log statistics:" + daysLog.renderPlayersList());
		return true;
	    } else if (arg.equals("viewplayer") || arg.equals("viewraw")) {
		String target = null;
		if (args.length > 1) {
		    try {
			target = getOnlineOfflinePlayer(args[1], player).getName();
		    } catch (Exception e) {
			return true;
		    }
		} else if (!daysLog.hasClaimed(player.getName())) {
		    sendMessage(player, "You've not claimed a player yet! Please claim a players xray logs before using that command default!");
		    return true;
		} else {
		    target = daysLog.getClaim(player.getName());
		}
		PlayerBreaks breaks = daysLog.breaks.get(target);
		if (arg.equals("viewplayer")) {
		    sendMessage(player, "heres the breakdown of the parsed locations for this players logs!" + breaks.render());
		} else {
		    sendMessage(player, "Heres the raw locations for the target players logs!" + breaks.renderRaw());
		}
		return true;
	    }
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    /*
     * Bookmark Listeners
     */

    @PonyEvent(priority = EventPriority.MONITOR)
    public void onOreBreak(BlockBreakEvent event) {
	Player player = event.getPlayer();
	Pony pony = Ponyville.getPony(player);
	PonyGroup group = PonyManager.ponyManager.getGroup(pony.getGroup());
	if (group.isOPType()) {
	    return;
	}
	Material type = event.getBlock().getType();
	if ((type == Material.DIAMOND_ORE && (!(event.getBlock().getLocation().getBlockY() > 16))) || (pony.getAdvancedXrayLogging() && advancedTypes.contains(type))) {
	    logMine(player, type, event.getBlock().getLocation());
	}
    }

    public void deactivate() {
	try {
	    database.close();
	} catch (SQLException e) {
	    e.printStackTrace();
	}
    }
}
