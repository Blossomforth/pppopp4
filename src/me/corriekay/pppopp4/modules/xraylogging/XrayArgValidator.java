package me.corriekay.pppopp4.modules.xraylogging;

import java.util.HashSet;

import me.corriekay.pppopp4.command.validation.command.BasicArgumentRule;

public class XrayArgValidator extends BasicArgumentRule {

    private final HashSet<String> validator = new HashSet<String>();

    public XrayArgValidator(String failure, boolean showFailure,
	    boolean returnOnFail) {
	super(failure, showFailure, returnOnFail);
	validator.add("next");
	validator.add("last");
	validator.add("current");
	validator.add("load");
	validator.add("claim");
	validator.add("markplayerfinished");
	validator.add("markdayfinished");
	validator.add("viewday");
	validator.add("viewplayer");
	validator.add("viewraw");
    }

    @Override
    public boolean checkRule(String[] args) {
	if (args.length < 0) {
	    return false;
	}
	return validator.contains(args[0]);
    }

}
