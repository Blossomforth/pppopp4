package me.corriekay.pppopp4.modules.xraylogging;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.command.validation.tabbing.BasicTabRule;

import org.bukkit.command.CommandSender;

public class XrayTab implements BasicTabRule {
    private final List<String> baseArgs = new ArrayList<String>();

    public XrayTab() {
	baseArgs.add("next");
	baseArgs.add("last");
	baseArgs.add("current");
	baseArgs.add("load");
	baseArgs.add("claim");
	baseArgs.add("markplayerfinished");
	baseArgs.add("markdayfinished");
	baseArgs.add("viewday");
	baseArgs.add("viewplayer");
	baseArgs.add("viewraw");
    }

    @Override
    public List<String> tab(CommandSender sender, CommandMetadata cmd, String argBit, String[] args) {
	return baseArgs;
    }

}
