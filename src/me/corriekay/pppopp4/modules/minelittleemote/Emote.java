package me.corriekay.pppopp4.modules.minelittleemote;

import java.util.ArrayList;

import me.corriekay.pppopp4.modules.chat.Channel;
import me.corriekay.pppopp4.modules.ponymanager.PonyPermission;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class Emote {

    protected final String name, sender, receiver, server, ownerName;
    protected final boolean isSilent, isPrivate, global, nsfw;

    public Emote(String sender, String receiver, String server,
	    boolean isSilent, boolean isPrivate, String ownerName, String name,
	    boolean global, boolean nsfw) {
	this.name = name;
	this.sender = parseColors(sender);
	this.receiver = parseColors(receiver);
	this.server = parseColors(server);
	this.ownerName = ownerName;
	this.isSilent = isSilent;
	this.isPrivate = isPrivate;
	this.global = global;
	this.nsfw = nsfw;
    }

    public Emote(Pony pony) {
	this(pony.getEmoteSender(), pony.getEmoteReceiver(), pony.getEmoteServer(), pony.getEmoteSilent(), pony.getEmotePrivate(), pony.getName(), pony.getEmoteName(), false, pony.getEmoteNsfw());
    }

    /*
     * Bookmark Utils
     */
    public static String parseColors(String s) {
	return ChatColor.translateAlternateColorCodes('&', s);
    }

    public static String format(String message, Pony sender, Pony receiver) {
	message = message.replaceAll("<s>", sender.getNickname());
	message = message.replaceAll("<r>", receiver.getNickname());
	return ChatColor.translateAlternateColorCodes('&', message);
    }

    public static String format(String message, String sender, String receiver) {
	message = message.replaceAll("<s>", sender);
	message = message.replaceAll("<r>", receiver);
	return ChatColor.translateAlternateColorCodes('&', message);
    }

    public boolean canUse(Player player) {
	if (isPrivate) {
	    if (player.hasPermission(PonyPermission.IS_OPERATOR.getPerm()) || ownerName.equals(player.getName())) {
		return true;
	    }
	    return false;
	}
	return true;
    }

    public String renderName() {
	return (nsfw ? ChatColor.DARK_RED + "(NSFW) " : ChatColor.RED) + name + ChatColor.GRAY + ", ";
    }

    public boolean useEmote(Player sender, Player receiver, Channel channel) {
	if (!channel.isListening(receiver) || !channel.isListening(sender)) {
	    return false;
	}
	if (!channel.isNsfwFlagged() && nsfw) {
	    return false;
	}
	ArrayList<Player> sendTo = channel.getListenerPlayers();
	sendTo.remove(sender);
	sendTo.remove(receiver);
	Pony senderpony, receiverpony;
	senderpony = Ponyville.getPony(sender);
	receiverpony = Ponyville.getPony(receiver);
	String servermsg = format(this.server, senderpony, receiverpony);
	if (!isSilent) {
	    for (Player player : sendTo) {
		player.sendMessage(channelIcon(player, channel) + " " + ChatColor.WHITE + servermsg);
	    }
	}
	Bukkit.getConsoleSender().sendMessage(servermsg);
	// TODO send RPA message
	sender.sendMessage(channelIcon(sender, channel) + " " + format(this.sender, senderpony, receiverpony));
	receiver.sendMessage(channelIcon(receiver, channel) + " " + format(this.receiver, senderpony, receiverpony));
	return true;
    }

    private String channelIcon(Player player, Channel channel) {
	return channel.getColor(player.getName()) + channel.getIcon();
    }
}
