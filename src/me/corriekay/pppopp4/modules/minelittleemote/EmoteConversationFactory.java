package me.corriekay.pppopp4.modules.minelittleemote;

import java.util.HashMap;
import java.util.Map;

import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.ChatColor;
import org.bukkit.conversations.*;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

public class EmoteConversationFactory extends ConversationFactory {

    public EmoteConversationFactory(Plugin plugin) {
	super(plugin);
    }

    public void startEmoteConstruction(Player player) {
	Pony pony = Ponyville.getPony(player);
	Map<Object, Object> map = new HashMap<Object, Object>();
	map.put("emotename", pony.getEmoteName());
	map.put("server", pony.getEmoteServer());
	map.put("sender", pony.getEmoteSender());
	map.put("receiver", pony.getEmoteReceiver());
	map.put("silent", pony.getEmoteSilent());
	map.put("private", pony.getEmotePrivate());
	map.put("nsfw", pony.getEmoteNsfw());
	map.put("pony", pony);
	Conversation conv = withFirstPrompt(new StartPrompt()).withEscapeSequence("/exit").withLocalEcho(false).withModality(false).withInitialSessionData(map).buildConversation(player);
	conv.begin();
    }

    /**
     * This is the starting class for the emote configuration tool.
     */
    private class StartPrompt extends MessagePrompt {

	public String getPromptText(ConversationContext arg0) {
	    Pony pony = (Pony) arg0.getSessionData("pony");
	    String message = ChatColor.GRAY + "===================\n";
	    message += "Welcome, " + pony.getNickname() + ChatColor.GRAY + " to the MLE setup assistant! This module is designed to help streamline, and/or otherwise make setting up your emote to be REALLY easy!\n" + ChatColor.DARK_RED + "Warning: If you must create an emote that is unsavory, violent, rude, explicit, or otherwise NSFW, you MUST flag your emote as nsfw. Failure to do this may get you banned from using MLE. You have been warned.\n";
	    message += ChatColor.RED + "say \"/exit\" at any time to leave the assistant, or \"/save\" to save and exit. Type \"/help\" to repeat what stage in the assistant youre at. Type \"/colors\" for a list of colors.  Type \"/view\" to see your emote so far! Type \"/skip\" to skip to the next part of the emote! If youve made a mistake, and wish to go back, type \"/back\".\n";
	    message += ChatColor.GRAY + "To get started, lets set the name of your emote!";
	    return message;
	}

	protected Prompt getNextPrompt(ConversationContext arg0) {
	    return new NamePrompt();
	}
    }

    /**
     * This class serves as a base for the additional text prompts for the emote
     * handler
     */
    private abstract class AdditionalValidatingPrompt extends ValidatingPrompt {

	public Prompt checkCommands(String message, ConversationContext context) {
	    ChatColor gray = ChatColor.GRAY;
	    Player player = ((Player) context.getForWhom());
	    Pony pony = (Pony) context.getSessionData("pony");
	    String first = message.split(" ")[0];
	    String sendMe = ChatColor.DARK_GRAY + "===================\n";
	    if (first.equals("/help")) {
		sendMe += ChatColor.RED + "say \"/exit\" at any time to leave the assistant. Type \"/help\" to repeat what stage in the assistant youre at. Type \"/colors\" for a list of colors.  Type \"/view\" to see your emote so far! Type \"/skip\" to skip to the next part of the emote! If youve made a mistake, and wish to go back, type \"/back\".\n";
		player.sendMessage(sendMe);
		return this;
	    } else if (first.equals("/save")) {
		return new FinalPrompt();
	    } else if (first.equals("/view")) {
		player.sendMessage(gray + "Emote Name: " + pony.getEmoteName());
		player.sendMessage(gray + "Emote Sender: " + Emote.format(Emote.parseColors(pony.getEmoteSender()), "sender", "receiver"));
		player.sendMessage(gray + "Emote Receiver: " + Emote.format(Emote.parseColors(pony.getEmoteReceiver()), "sender", "receiver"));
		player.sendMessage(gray + "Emote Server: " + Emote.format(Emote.parseColors(pony.getEmoteServer()), "sender", "receiver"));
		player.sendMessage(gray + "Is Private: " + pony.getEmotePrivate());
		player.sendMessage(gray + "Is Silent: " + pony.getEmoteSilent());
	    } else if (first.equals("/colors")) {
		player.sendMessage(ChatColor.DARK_GRAY + "===================");
		player.sendMessage(gray + "Here are a list of colors you can use.");
		String colors = "";
		for (ChatColor color : ChatColor.values()) {
		    colors += color + color.name() + ChatColor.RESET + " &" + color.getChar() + ", ";
		}
		player.sendMessage(colors);
		player.sendMessage(gray + "To use a color, type \"<colorname>\" in the sentance, and all words after it, will have the same color, until you override it with another color.");
		player.sendMessage(gray + "The funky looking one is \"magic\". Try not to use it. its rather annoying. RESET will wipe all formatting, and reset back to just generic white.");
		return this;
	    } else if (first.equals("/back")) {
		return getBack();
	    } else if (first.equals("/next")) {
		return getNext();
	    } else if (first.startsWith("/")) {
		player.sendMessage(gray + "Thats not a valid help command! Type /help, /view, /colors, /back, or /next!");
		return this;
	    }
	    return null;
	}

	protected abstract Prompt getBack();

	protected abstract Prompt getNext();

	protected abstract String getFailedValidationText(ConversationContext arg0, String arg1);
    }

    /**
     * Prompts the user for the name of their Emote
     */
    private class NamePrompt extends AdditionalValidatingPrompt {

	public String getPromptText(ConversationContext arg0) {
	    return ChatColor.GRAY + "The name of your emote is what players type in when they use it. /mee <emoteName> <target>. Type anything you want:";
	}

	protected Prompt acceptValidatedInput(ConversationContext arg0, String arg1) {
	    Prompt p = checkCommands(arg1, arg0);
	    if (p != null) {
		return p;
	    }
	    arg0.setSessionData("emotename", arg1.split(" ")[0].toLowerCase());
	    return getNext();

	}

	protected boolean isInputValid(ConversationContext arg0, String arg1) {
	    String name = arg1.split(" ")[0].toLowerCase();
	    if (EmoteManager.get().emoteList.containsKey(name)) {
		Player player = (Player) arg0.getForWhom();
		player.sendMessage(ChatColor.GRAY + "This emote name is already in use! If you own the emote, No need to set it twice! Type \"/skip\" to skip this part!");
		return false;
	    }
	    return true;
	}

	protected String getFailedValidationText(ConversationContext arg0, String arg1) {
	    return null;
	}

	@Override
	protected Prompt getBack() {
	    return this;
	}

	@Override
	protected Prompt getNext() {
	    return new SenderPrompt();
	}
    }

    /**
     * Promts the user for the sender of their emote
     */
    private class SenderPrompt extends AdditionalValidatingPrompt {

	@Override
	public String getPromptText(ConversationContext arg0) {
	    return ChatColor.DARK_GRAY + "===================\n" + ChatColor.GRAY + "Time to set your sender text! The sender text is the message that the person using your emote will recieve! \"<r>\" and \"<s>\" will be replaced automatically when used with the receiver and senders names, respectively.";
	}

	@Override
	protected String getFailedValidationText(ConversationContext arg0, String arg1) {
	    return null;
	}

	@Override
	protected Prompt acceptValidatedInput(ConversationContext arg0, String arg1) {
	    Prompt p = checkCommands(arg1, arg0);
	    if (p != null) {
		return p;
	    }
	    arg0.setSessionData("sender", arg1);
	    arg0.getForWhom().sendRawMessage((ChatColor.GRAY + "Awesome, your sender message is set to:"));
	    arg0.getForWhom().sendRawMessage(Emote.format(arg1, "<sender>", "<receiver>"));
	    return getNext();
	}

	@Override
	protected boolean isInputValid(ConversationContext arg0, String arg1) {
	    return true;
	}

	@Override
	protected Prompt getBack() {
	    return new NamePrompt();
	}

	@Override
	protected Prompt getNext() {
	    return new ReceiverPrompt();
	}

    }

    /**
     * Promts the user for the sender of their emote
     */
    private class ReceiverPrompt extends AdditionalValidatingPrompt {

	@Override
	public String getPromptText(ConversationContext arg0) {
	    return ChatColor.DARK_GRAY + "===================\n" + ChatColor.GRAY + "Time to set your receiver text! The receiver text is the message that the target will recieve! \"<r>\" and \"<s>\" will be replaced automatically when used with the receiver and senders names, respectively.";
	}

	@Override
	protected Prompt getBack() {
	    return new SenderPrompt();
	}

	@Override
	protected Prompt getNext() {
	    return new ServerPrompt();
	}

	@Override
	protected String getFailedValidationText(ConversationContext arg0, String arg1) {
	    return null;
	}

	@Override
	protected Prompt acceptValidatedInput(ConversationContext arg0, String arg1) {
	    Prompt p = checkCommands(arg1, arg0);
	    if (p != null) {
		return p;
	    }
	    arg0.setSessionData("receiver", arg1);
	    arg0.getForWhom().sendRawMessage(ChatColor.GRAY + "Awesome, your receiver message is set to:");
	    arg0.getForWhom().sendRawMessage(Emote.format(arg1, "<sender>", "<recevier>"));
	    return getNext();
	}

	@Override
	protected boolean isInputValid(ConversationContext arg0, String arg1) {
	    return true;
	}

    }

    /**
     * Promts the user for the sender of their emote
     */
    private class ServerPrompt extends AdditionalValidatingPrompt {

	@Override
	public String getPromptText(ConversationContext arg0) {
	    return ChatColor.DARK_GRAY + "===================\n" + ChatColor.GRAY + "Time to set the server message! The server text is the message that the rest of the server, besides you and the target, will see! \"<r>\" and \"<s>\" will be replaced automatically when used with the receiver and senders names, respectively.";
	}

	@Override
	protected Prompt getBack() {
	    return new ReceiverPrompt();
	}

	@Override
	protected Prompt getNext() {
	    return new PrivatePrompt();
	}

	@Override
	protected String getFailedValidationText(ConversationContext arg0, String arg1) {
	    return null;
	}

	@Override
	protected Prompt acceptValidatedInput(ConversationContext arg0, String arg1) {
	    Prompt p = checkCommands(arg1, arg0);
	    if (p != null) {
		return p;
	    }
	    arg0.setSessionData("server", arg1);
	    arg0.getForWhom().sendRawMessage(ChatColor.GRAY + "Awesome, your server message is set to:");
	    arg0.getForWhom().sendRawMessage(Emote.format(arg1, "<sender>", "<receiver>"));
	    return getNext();
	}

	@Override
	protected boolean isInputValid(ConversationContext arg0, String arg1) {
	    return true;
	}

    }

    /**
     * Promts the user for the sender of their emote
     */
    private class PrivatePrompt extends AdditionalValidatingPrompt {

	@Override
	public String getPromptText(ConversationContext arg0) {
	    return ChatColor.DARK_GRAY + "===================\n" + ChatColor.GRAY + "Time to set your emotes private status! Type either true, or false. If your emote is private, only you can use it!";
	}

	@Override
	protected Prompt getBack() {
	    return new ServerPrompt();
	}

	@Override
	protected Prompt getNext() {
	    return new SilentPrompt();
	}

	@Override
	protected String getFailedValidationText(ConversationContext arg0, String arg1) {
	    return ChatColor.GRAY + "Uh oh, thats not right.. Please type either true or false to set your emotes private setting!";
	}

	@Override
	protected Prompt acceptValidatedInput(ConversationContext arg0, String arg1) {
	    Prompt p = checkCommands(arg1, arg0);
	    if (p != null) {
		return p;
	    }
	    Boolean b = Boolean.parseBoolean(arg1);
	    arg0.setSessionData("private", b);
	    arg0.getForWhom().sendRawMessage(ChatColor.GRAY + "Awesome, your emote privacy is set to " + b.toString().toLowerCase() + "!");
	    return getNext();
	}

	@Override
	protected boolean isInputValid(ConversationContext arg0, String arg1) {
	    if (arg1.equals("true") || arg1.equals("false") || arg1.startsWith("/")) {
		return true;
	    }
	    return false;
	}

    }

    /**
     * Promts the user for the sender of their emote
     */
    private class SilentPrompt extends AdditionalValidatingPrompt {

	@Override
	public String getPromptText(ConversationContext arg0) {
	    return ChatColor.DARK_GRAY + "===================\n" + ChatColor.GRAY + "Time to set your emotes silent status! Type either true, or false. If your emote is silent, only the receiver and you will see the emote!";
	}

	@Override
	protected Prompt getBack() {
	    return new PrivatePrompt();
	}

	@Override
	protected Prompt getNext() {
	    return new NsfwPrompt();
	}

	@Override
	protected String getFailedValidationText(ConversationContext arg0, String arg1) {
	    return ChatColor.GRAY + "Uh oh, thats not right.. Please type either true or false to set your emotes silent setting!";
	}

	@Override
	protected Prompt acceptValidatedInput(ConversationContext arg0, String arg1) {
	    Prompt p = checkCommands(arg1, arg0);
	    if (p != null) {
		return p;
	    }
	    Boolean b = Boolean.parseBoolean(arg1);
	    arg0.setSessionData("silent", b);
	    arg0.getForWhom().sendRawMessage(ChatColor.GRAY + "Awesome, your emote silence is set to " + b.toString().toLowerCase() + "!");
	    return getNext();
	}

	@Override
	protected boolean isInputValid(ConversationContext arg0, String arg1) {
	    if (arg1.equals("true") || arg1.equals("false") || arg1.startsWith("/")) {
		return true;
	    }
	    return false;
	}

    }

    private class NsfwPrompt extends AdditionalValidatingPrompt {

	@Override
	public String getPromptText(ConversationContext arg0) {
	    return ChatColor.DARK_GRAY + "===================\n" + ChatColor.GRAY + "Time to set your emotes nsfw status! Type either true, or false. If your emote is nsfw, it can only be used in channels that are flagged for nsfw emotes!";
	}

	@Override
	protected Prompt getBack() {
	    return new SilentPrompt();
	}

	@Override
	protected Prompt getNext() {
	    return null;
	}

	@Override
	protected String getFailedValidationText(ConversationContext arg0, String arg1) {
	    return ChatColor.GRAY + "Uh oh, thats not right.. Please type either true or false to set your emotes silent setting!";
	}

	@Override
	protected Prompt acceptValidatedInput(ConversationContext arg0, String arg1) {
	    Prompt p = checkCommands(arg1, arg0);
	    if (p != null) {
		return p;
	    }
	    Boolean b = Boolean.parseBoolean(arg1);
	    arg0.setSessionData("nsfw", b);
	    arg0.getForWhom().sendRawMessage(ChatColor.GRAY + "Awesome, your emote nsfw flag is set to " + b.toString().toLowerCase() + "!");
	    return new FinalPrompt();
	}

	@Override
	protected boolean isInputValid(ConversationContext arg0, String arg1) {
	    if (arg1.equals("true") || arg1.equals("false") || arg1.startsWith("/")) {
		return true;
	    }
	    return false;
	}
    }

    private class FinalPrompt extends MessagePrompt {

	@Override
	public String getPromptText(ConversationContext arg0) {
	    ChatColor white, gray;
	    white = ChatColor.WHITE;
	    gray = ChatColor.GRAY;
	    String s = ChatColor.GRAY + "Emote finished! Heres what it looks like:";
	    s += gray + "\nEmote Name: " + white + Emote.format((String) arg0.getSessionData("emotename"), "<sender>", "<receiver>");
	    s += gray + "\nEmote Sender: " + white + Emote.format((String) arg0.getSessionData("sender"), "<sender>", "<receiver>");
	    s += gray + "\nEmote Receiver: " + white + Emote.format((String) arg0.getSessionData("receiver"), "<sender>", "<receiver>");
	    s += gray + "\nEmote Server: " + white + Emote.format((String) arg0.getSessionData("server"), "<sender>", "<receiver>");
	    s += gray + "\nIs Private: " + white + arg0.getSessionData("private");
	    s += gray + "\nIs Silent: " + white + arg0.getSessionData("silent");
	    s += gray + "\nIs NSFW: " + white + arg0.getSessionData("nsfw");
	    return s;
	}

	@Override
	protected Prompt getNextPrompt(ConversationContext arg0) {
	    Pony pony = (Pony) arg0.getSessionData("pony");
	    pony.setEmoteName((String) arg0.getSessionData("emotename"));
	    pony.setEmoteSender((String) arg0.getSessionData("sender"));
	    pony.setEmoteReceiver((String) arg0.getSessionData("receiver"));
	    pony.setEmoteServer((String) arg0.getSessionData("server"));
	    pony.setEmotePrivate((Boolean) arg0.getSessionData("private"));
	    pony.setEmoteSilent((Boolean) arg0.getSessionData("silent"));
	    pony.setEmoteNsfw((Boolean) arg0.getSessionData("nsfw"));
	    pony.save();
	    EmoteManager.endCreateEmote(pony);
	    return null;
	}

    }
}
