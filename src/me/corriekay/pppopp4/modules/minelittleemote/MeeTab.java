package me.corriekay.pppopp4.modules.minelittleemote;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.command.validation.tabbing.BasicTabRule;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class MeeTab implements BasicTabRule {

    @Override
    public List<String> tab(CommandSender sender, CommandMetadata cmd, String argBit, String[] args) {
	Player player;
	List<String> returnMe = new ArrayList<String>();
	if (!(sender instanceof Player)) {
	    returnMe.add("You need to be a player to use this command!");
	    return returnMe;
	} else {
	    player = (Player) sender;
	}
	for (Emote e : EmoteManager.get().emoteList.values()) {
	    if (e.canUse(player)) {
		returnMe.add(e.name);
	    }
	}
	return returnMe;
    }

}
