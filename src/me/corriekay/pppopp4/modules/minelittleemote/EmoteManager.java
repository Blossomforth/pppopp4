package me.corriekay.pppopp4.modules.minelittleemote;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.chat.Channel;
import me.corriekay.pppopp4.modules.chat.ChannelHandler;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;

public class EmoteManager extends PSCmdExe {

    public static EmoteManager manager;

    public static EmoteManager get() {
	return manager;
    }

    protected final HashMap<String, Emote> emoteList = new HashMap<String, Emote>();
    protected final EmoteConversationFactory mle = new EmoteConversationFactory(Mane.getInstance());

    public EmoteManager() {
	super("EmoteManager", Ponies.PinkiePie, "mles", "mle", "mee", "deleteemote", "mleban");
	manager = this;
	init();
    }

    public void init() {
	if (loadConfig("emotes.yml")) {
	    getConfig().createSection("global");
	    getConfig().createSection("user");
	    saveConfig();
	}
	saveConfig();
	reloadEmotes();
    }

    private void reloadEmotes() {
	emoteList.clear();
	for (String emoteName : getConfig().getConfigurationSection("global").getKeys(false)) {
	    emoteList.put(emoteName, new Emote(getConfig().getString("global." + emoteName + ".sender"), getConfig().getString("global." + emoteName + ".receiver"), getConfig().getString("global." + emoteName + ".server"), false, false, "server", emoteName, true, false));
	}
	List<String> registeredEmotes = getConfig().getStringList("user");
	for (String user : getConfig().getStringList("user")) {
	    try {
		Pony pony = Ponyville.getPony(user);
		if (pony == null) {
		    registeredEmotes.remove(user);
		    continue;
		}
		Emote emote = new Emote(pony);
		if (emote.name.equals("none")) {
		    registeredEmotes.remove(user);
		}
		emoteList.put(pony.getEmoteName(), emote);
	    } catch (Exception e) {
		Bukkit.getLogger().warning("Issue loading player emote: " + user);
	    }
	}
	getConfig().set("user", registeredEmotes);
	saveConfig();
    }

    /*
     * Bookmark Command
     */
    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case mles: {
	    mle.startEmoteConstruction(player);
	    return true;
	}
	case mle: {
	    sendMessage(player, "Heres a list of emotes!!");
	    player.sendMessage(renderEmoteList(player));
	    return true;
	}
	case mee: {
	    if (isMuted(player)) {
		sendMessage(player, "Naughty naughty! You're muted!");
		return true;
	    }
	    Channel c = ChannelHandler.getHandler().getChattingChannel(player);
	    Emote e = emoteList.get(args[0]);
	    Player player2 = getOnlinePlayer(args[1], player);
	    if (player2 == null) {
		return true;
	    }
	    if (c != null && e != null) {
		if (!e.canUse(player)) {
		    sendMessage(player, "What the... how did you get ahold of that! You cant use that, put it down!");
		    return true;
		}
		if (!e.useEmote(player, player2, c)) {
		    if (!c.isNsfwFlagged() && e.nsfw) {
			sendMessage(player, "That channel isnt flagged for nsfw emotes, you naughty filly you!");
		    } else {
			sendMessage(player, "That player isnt listening in on that channel! you cant use emotes on a player if they cant hear them!");
		    }
		    return true;
		}
		return true;
	    } else {
		if (e == null) {
		    sendMessage(player, "I couldnt find that emote!");
		}
		if (c == null) {
		    sendMessage(player, "You need to be in a channel to use an emote!");
		}
		return true;
	    }
	}
	case deleteemote: {
	    if (removeEmoteByPlayer(player.getName())) {
		sendMessage(player, "Emote removed! If you want to put it up again, type /mles and skip until the end!");
	    } else {
		sendMessage(player, "You dont have an emote to delete!");
	    }
	    return true;
	}
	case mleban: {

	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    public boolean consoleCommand(ConsoleCommandSender console, CommandMetadata cmd, String label, String[] args) {
	return true;
    }

    /*
     * Bookmark Commands
     */

    /*
     * Bookmark Utils
     */

    public boolean removeEmote(String emotename) {
	Emote e = emoteList.get(emotename);
	return e != null ? removeEmoteByPlayer(e.ownerName) : false;
    }

    public boolean removeEmoteByPlayer(String player) {
	List<String> userlist = getConfig().getStringList("user");
	if (userlist.contains(player)) {
	    userlist.remove(player);
	    getConfig().set("user", userlist);
	    saveConfig();
	    reloadEmotes();
	    return true;
	}
	return false;
    }

    public String renderEmoteList(Player viewer) {
	ArrayList<Emote> user = new ArrayList<Emote>(), publik = new ArrayList<Emote>();
	for (String eName : emoteList.keySet()) {
	    Emote e = emoteList.get(eName);
	    if (e.ownerName.equals("server")) {
		publik.add(e);
	    } else {
		if (e.canUse(viewer)) {
		    user.add(e);
		}
	    }
	}
	if (user.size() == 0 && publik.size() == 0) {
	    return ChatColor.GRAY + "There are no emotes set up! If you're a donator, set one up yourself by typing /mles!";
	}
	String string = "";
	if (publik.size() > 0) {
	    string += ChatColor.GRAY + "Public emotes: ";
	    for (Emote e : publik) {
		string += e.renderName();
	    }
	    if (user.size() > 0) {
		string += "\n\n";
	    }
	}
	if (user.size() > 0) {
	    string += ChatColor.GRAY + "User Emotes: ";
	    for (Emote e : user) {
		if (e.nsfw) {
		    Pony pony = Ponyville.getPony(viewer);
		    if (!pony.isNsfwAccepted() || pony.isNsfwBlocked()) {
			continue;
		    }
		}
		string += e.renderName();
	    }
	}
	return string.substring(0, string.length() - 4);
    }

    public static void endCreateEmote(Pony pony) {
	List<String> users = manager.getConfig().getStringList("user");
	if (!users.contains(pony.getName())) {
	    users.add(pony.getName());
	}
	manager.getConfig().set("user", users);
	manager.saveConfig();
	manager.reloadEmotes();
    }

    private boolean isMuted(Player player) {
	ChannelHandler ch = ChannelHandler.getHandler();
	return ch.isMuted(player);
    }
}
