package me.corriekay.pppopp4.modules.chaos;

import java.util.HashSet;

import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.events.QuitEvent;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.PonyEvent;
import me.corriekay.pppopp4.modules.equestria.GameType;
import me.corriekay.pppopp4.modules.equestria.PonyWorld;
import me.corriekay.pppopp4.modules.ponymanager.PonyPermission;
import me.corriekay.pppopp4.utilities.Utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Creeper;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.entity.CreatureSpawnEvent.SpawnReason;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.bukkit.selections.CuboidSelection;
import com.sk89q.worldedit.bukkit.selections.Selection;

public class ElementsOfHarmony extends PSCmdExe {

    public boolean lava = false, water = false;
    public HashSet<String> pvpBypass = new HashSet<String>();
    private HashSet<Selection> protections = new HashSet<Selection>();
    private final WorldEditPlugin wep = ((WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit"));

    public ElementsOfHarmony() {
	super("ElementsOfHarmony", Ponies.TwilightSparkle, "toggle", "addprotection", "removeprotection");
	loadConfig("protections.yml");
	refreshProtections();
    }

    private void refreshProtections() {
	protections.clear();
	FileConfiguration config = getConfig();
	for (String s : getConfig().getKeys(false)) {
	    Location min, max;
	    World w = Bukkit.getWorld(config.getString(s + ".world"));
	    double x, xx, y, yy, z, zz;
	    x = config.getDouble(s + ".minimum.x");
	    y = config.getDouble(s + ".minimum.y");
	    z = config.getDouble(s + ".minimum.z");
	    xx = config.getDouble(s + ".maximum.x");
	    yy = config.getDouble(s + ".maximum.y");
	    zz = config.getDouble(s + ".maximum.z");
	    min = new Location(w, x, y, z);
	    max = new Location(w, xx, yy, zz);
	    CuboidSelection sel = new CuboidSelection(w, min, max);
	    protections.add(sel);
	}
    }

    // Command

    public boolean playerCommand(Player player, CommandMetadata cmd, String label, String[] args) throws Exception {
	switch (cmd) {
	case toggle: {
	    if (args[0].equals("lava")) {
		toggleLavaFlow(player);
		return true;
	    } else if (args[0].equals("water")) {
		toggleWaterFlow(player);
		return true;
	    } else if (args[0].equals("pvp")) {
		String name = player.getName();
		if (pvpBypass.contains(name)) {
		    pvpBypass.remove(name);
		    sendMessage(player, "Violence cancelled!");
		    return true;
		} else {
		    pvpBypass.add(name);
		    sendMessage(player, "LET THE RIVERS FLOW WITH THE BLOOD OF THE INNOCENT");
		    return true;
		}
	    }
	    return true;
	}
	case addprotection: {
	    Selection selection = wep.getSelection(player);
	    if (selection == null) {
		sendMessage(player, "Silly willy! You need to make a world edit selection first!");
		return true;
	    }
	    String path = args[0].toLowerCase() + ".";
	    Location pos1, pos2;
	    pos1 = selection.getMaximumPoint();
	    pos2 = selection.getMinimumPoint();
	    getConfig().set(path + "world", player.getWorld().getName());
	    getConfig().set(path + "minimum.x", pos2.getBlockX());
	    getConfig().set(path + "minimum.y", pos2.getBlockY());
	    getConfig().set(path + "minimum.z", pos2.getBlockZ());
	    getConfig().set(path + "maximum.x", pos1.getBlockX());
	    getConfig().set(path + "maximum.y", pos1.getBlockY());
	    getConfig().set(path + "maximum.z", pos1.getBlockZ());
	    saveConfig();
	    refreshProtections();
	    sendMessage(player, "Area protection saved!");
	    return true;
	}
	case removeprotection: {
	    String prot = args[0].toLowerCase();
	    getConfig().set(prot, null);
	    saveConfig();
	    refreshProtections();
	    sendMessage(player, "Protection nullified!");
	    return true;
	}
	default: {
	    throwUnhandledCommandException(cmd);
	    return true;
	}
	}
    }

    // Event listeners

    @PonyEvent
    public void explosionNerf(EntityExplodeEvent event) {
	PonyWorld world = PonyWorld.getPonyWorld(event.getLocation());
	if (world.getGameType() == GameType.PVP) {
	    boolean clear = false;
	    for (Block b : event.blockList()) {
		if (isProtected(b.getLocation())) {
		    clear = true;
		    break;
		}
	    }
	    if (clear) {
		event.blockList().clear();
	    }
	} else {
	    event.blockList().clear();
	}
    }

    @PonyEvent
    public void fireDamagePrevent(BlockBurnEvent event) {
	PonyWorld world = PonyWorld.getPonyWorld(event.getBlock().getLocation());
	if (world.getGameType() == GameType.PVP) {
	    event.setCancelled(isProtected(event.getBlock().getLocation()));
	} else {
	    event.setCancelled(true);
	}
    }

    @PonyEvent
    public void fireSpreadControl(BlockSpreadEvent event) {
	if (event.getNewState().getType() != Material.FIRE) {
	    return;
	}
	PonyWorld world = PonyWorld.getPonyWorld(event.getBlock().getLocation());
	if (world.getGameType() == GameType.PVP) {
	    event.setCancelled(isProtected(event.getBlock().getLocation()));
	} else {
	    event.setCancelled(true);
	}
    }

    @PonyEvent
    public void playerFireControl(PlayerInteractEvent event) {
	try {
	    PonyWorld world = PonyWorld.getPonyWorld(event.getPlayer().getWorld());
	    if (!(world.getGameType() == GameType.PVP) || isProtected(event.getClickedBlock().getLocation())) {
		if (event.getItem().getType() == Material.FLINT_AND_STEEL) {
		    if (!event.getPlayer().hasPermission(PonyPermission.IS_OPERATOR.getPerm())) {
			if (event.getClickedBlock().getType() != Material.NETHERRACK) {
			    event.setCancelled(true);
			} else {
			    for (Block b : Utils.getBlocks(2, 2, 2, event.getClickedBlock().getLocation())) {
				if (b.getType() == Material.OBSIDIAN) {
				    event.setCancelled(true);
				}
			    }
			}
		    }
		}
	    }
	} catch (NullPointerException e) {
	    return;
	}
    }

    @PonyEvent
    public void bukkitFill(PlayerBucketFillEvent event) {
	event.setCancelled(isProtected(event.getBlockClicked().getLocation()));
    }

    @PonyEvent
    public void bukkitEmpty(PlayerBucketEmptyEvent event) {
	event.setCancelled(isProtected(event.getBlockClicked().getLocation()));
    }

    @PonyEvent(priority = EventPriority.LOWEST)
    public void liquidFlowEvent(BlockFromToEvent event) {
	if (event.getBlock().getType().equals(Material.STATIONARY_LAVA)) {
	    event.setCancelled(lava);
	}
	if (event.getBlock().getType().equals(Material.STATIONARY_WATER)) {
	    event.setCancelled(water);
	}
    }

    @PonyEvent(eventTypes = { EntityDamageByEntityEvent.class })
    public void pvpHandler(EntityDamageEvent event) {
	if (event.getEntity() instanceof Creeper) {
	    if (event.getCause() == DamageCause.ENTITY_EXPLOSION) {
		event.setCancelled(true);
		return;
	    }
	}
	PonyWorld world = PonyWorld.getPonyWorld(event.getEntity().getWorld());
	if (!(world.getGameType() == GameType.PVP) || isProtected(event.getEntity().getLocation())) {
	    if (event instanceof EntityDamageByEntityEvent) {
		EntityDamageByEntityEvent edbee = (EntityDamageByEntityEvent) event;
		Player target, damager;
		if (edbee.getEntity() instanceof Player) {
		    target = (Player) edbee.getEntity();
		    if (edbee.getDamager() instanceof Player) {
			damager = (Player) edbee.getDamager();
		    } else if (edbee.getDamager() instanceof Projectile) {
			Projectile proj = (Projectile) edbee.getDamager();
			if (proj.getShooter() instanceof Player) {
			    damager = (Player) proj.getShooter();
			} else
			    return;
		    } else
			return;
		} else
		    return;
		if (target != null && damager != null) {
		    event.setCancelled(true);
		    if (pvpBypass.contains(damager.getName())) {
			target.damage(event.getDamage());
		    }
		}
	    }
	}
    }

    @PonyEvent
    public void creatureSpawn(CreatureSpawnEvent event) {
	if (event.getSpawnReason() != SpawnReason.CUSTOM && isProtected(event.getLocation())) {
	    event.setCancelled(true);
	}
    }

    @PonyEvent
    public void blockBreak(BlockBreakEvent event) {
	if (isProtected(event.getBlock().getLocation()) && !event.getPlayer().hasPermission(PonyPermission.IS_OPERATOR.getPerm())) {
	    event.setCancelled(true);
	}
    }

    @PonyEvent
    public void blockPlace(BlockPlaceEvent event) {
	if (isProtected(event.getBlock().getLocation()) && !event.getPlayer().hasPermission(PonyPermission.IS_OPERATOR.getPerm())) {
	    event.setCancelled(true);
	}
    }

    @PonyEvent
    public void quit(QuitEvent event) {
	if (event.isQuitting()) {
	    pvpBypass.remove(event.getPlayer().getName());
	}
    }

    @PonyEvent
    public void portalCreate(EntityCreatePortalEvent event) {
	PonyWorld world = PonyWorld.getPonyWorld(event.getEntity().getWorld());
	if (world.getNether() != null) {
	    if (event.getEntity() instanceof Player) {
		Player player = (Player) event.getEntity();
		if (player.hasPermission(PonyPermission.IS_OPERATOR.getPerm())) {
		    return;
		}
	    }
	}
	event.setCancelled(true);
    }

    // Utility methods

    private boolean isProtected(Location l) {
	for (Selection sel : protections) {
	    if (sel.contains(l)) {
		return true;
	    }
	}
	return false;
    }

    public void toggleWaterFlow(CommandSender player) {
	water = !water;
	if (!water) {
	    sendMessage(player, "Let there be water!");
	} else
	    sendMessage(player, "The water party is over!");
    }

    public void toggleLavaFlow(CommandSender player) {
	lava = !lava;
	if (!lava) {
	    sendMessage(player, "Let there be lava!");
	} else
	    sendMessage(player, "The lava party is over!");
    }
}
