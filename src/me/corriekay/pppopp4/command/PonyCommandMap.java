package me.corriekay.pppopp4.command;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import me.corriekay.pppopp4.Mane;
import me.corriekay.pppopp4.Ponies;
import me.corriekay.pppopp4.command.CommandMetadata.CommandType;
import me.corriekay.pppopp4.command.validation.command.BasicArgumentRule;
import me.corriekay.pppopp4.modules.PSCmdExe;
import me.corriekay.pppopp4.modules.ponymanager.PonyManager;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;
import me.corriekay.pppopp4.utilities.PonyLogger;
import me.corriekay.pppopp4.utilities.Utils;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandMap;
import org.bukkit.command.CommandSender;
import org.bukkit.command.SimpleCommandMap;
import org.bukkit.craftbukkit.v1_7_R1.CraftServer;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

public class PonyCommandMap implements Listener {

    private final CommandMap map;

    private final Map<String, Command> knownCommands;

    private final HashMap<String, CCommand> commands = new HashMap<String, CCommand>();

    @SuppressWarnings("unchecked")
    public PonyCommandMap() throws Exception {
	if (Bukkit.getServer() instanceof CraftServer) {
	    // Get commandmap
	    final Field mapField = CraftServer.class.getDeclaredField("commandMap");
	    mapField.setAccessible(true);
	    map = (CommandMap) mapField.get(Bukkit.getServer());

	    // getKnownCommands
	    final Field commandField = SimpleCommandMap.class.getDeclaredField("knownCommands");
	    commandField.setAccessible(true);
	    knownCommands = (Map<String, Command>) commandField.get(map);
	} else {
	    throw new NullPointerException("Command map cannot be found?!");
	}
	Bukkit.getPluginManager().registerEvents(this, Mane.getInstance());
    }

    public boolean registerCommand(String cmd, PSCmdExe exe) {
	Logger log = Bukkit.getLogger();
	if (cmd != null && exe != null) {
	    CommandMetadata command = CommandMetadata.getEnum(cmd);
	    if (command != null) {
		ArrayList<String> aliases = command.getLabels();
		if (aliases == null) {
		    aliases = new ArrayList<String>();
		}
		CCommand registerMe = new CCommand(command.getCommandName(), command.getDescription(), command.getUsage(), aliases, command);
		String perm = command.getPermission();
		if (perm != null) {
		    registerMe.setPermission(perm);
		}
		registerMe.setPermissionMessage(ChatColor.LIGHT_PURPLE + "Pinkie Pie: Oh no! You can't do this.. :c");
		registerMe.setExecutor(exe);
		commands.put(cmd, registerMe);
		return map.register("pppopp4:", registerMe);
	    }
	    log.severe("command enum not found!");
	    return false;
	}
	log.severe("Command or executor were null!");
	return false;
    }

    public Command getCmd(String alias) {
	return knownCommands.get(alias);

    }

    public boolean checkAliasAvailable(String alias) {
	return knownCommands.get(alias) == null;
    }

    public boolean setAlias(String cmd, String alias) {
	CCommand c = commands.get(cmd);
	knownCommands.put(alias, c);
	return true;
    }

    public void removeAlias(String alias) {
	knownCommands.remove(alias);
    }

    private class CCommand extends Command {

	private PSCmdExe exe = null;

	private final CommandType type;

	private final int minimumArgs;

	private final CommandMetadata meta;

	public CCommand(String name, String desc, String usage,
		ArrayList<String> labels, CommandMetadata meta) {
	    super(name, desc, usage, labels);
	    this.meta = meta;
	    this.type = meta.getCommandType();
	    this.minimumArgs = meta.getMinimumArgs();
	}

	public boolean handle(CommandSender sender, Command cmd, String label, String[] args) {
	    if (exe != null) {
		return exe.onCommand(sender, meta, label, args, type);
	    } else {
		return false;
	    }
	}

	public boolean execute(CommandSender sender, String label, String[] args) {
	    Ponies pony = Ponies.PinkiePie;
	    if (exe != null) {
		pony = exe.pony;
	    }
	    if (meta.getPermission() != null && !sender.hasPermission(meta.getPermission())) {
		sender.sendMessage(pony.says() + "You can't do that :c");
		StringBuilder builder = new StringBuilder();
		builder.append(Utils.getDatedTimestamp() + " Logged command executed by user: " + sender.getName() + ": /" + label + " " + Utils.joinStringArray(args, " ") + " (" + meta.getCommandName() + "). Command permission check failed, and user " + ((sender instanceof Player) ? (PonyManager.ponyManager.getGroup(Ponyville.getPony((Player) sender).getGroup()).isOPType() ? "Is OP." : "Is not OP.") : "Is Console.") + "\n");
		PonyLogger.logAdmin(builder.toString());
		return true;
	    }
	    if (args.length < minimumArgs) {
		sender.sendMessage(pony.says() + "Uh oh, youre gonna need to provide more arguments for that command than that!");
		sender.sendMessage(pony.says() + meta.getUsage());
		return false;
	    }
	    if (meta.getArgRuleset() != null) {
		BasicArgumentRule valid = meta.getArgRuleset().validateArguments(args, sender);
		if (valid != null) {
		    return valid.returnOnFail;
		}
	    }
	    boolean returned = handle(sender, this, label, args);
	    if (meta.shouldLog()) {
		StringBuilder builder = new StringBuilder();
		builder.append(Utils.getDatedTimestamp() + " Logged command executed by user: " + sender.getName() + ": /" + label + " " + Utils.joinStringArray(args, " ") + " (" + meta.getCommandName() + "). Command returned " + returned + " and user " + ((sender instanceof Player) ? (PonyManager.ponyManager.getGroup(Ponyville.getPony((Player) sender).getGroup()).isOPType() ? "Is OP." : "Is not OP.") : "Is Console.") + "\n");
		PonyLogger.logAdmin(builder.toString());
	    }
	    if (!returned) {
		sender.sendMessage(getUsage());
	    }
	    return returned;
	}

	public List<String> tabComplete(CommandSender sender, String label, String[] args) {
	    String arg = args[args.length - 1];
	    List<String> finds = meta.getTabRules().validateTab(sender, meta, label, arg, args);
	    List<String> returns = new ArrayList<String>();
	    for (String s : finds) {
		if (StringUtils.containsIgnoreCase(s, arg)) {
		    returns.add(s);
		}
	    }
	    return returns;
	}

	public void setExecutor(PSCmdExe executor) {
	    exe = executor;
	}
    }
}
