package me.corriekay.pppopp4.command;

import java.util.ArrayList;

import me.corriekay.pppopp4.command.validation.command.ArgumentRuleset;
import me.corriekay.pppopp4.command.validation.command.BasicArgumentRule;
import me.corriekay.pppopp4.command.validation.command.SetArgumentRule;
import me.corriekay.pppopp4.command.validation.tabbing.BasicTabRule;
import me.corriekay.pppopp4.command.validation.tabbing.PlayerTab;
import me.corriekay.pppopp4.command.validation.tabbing.TabRuleset;
import me.corriekay.pppopp4.modules.chat.JoinTab;
import me.corriekay.pppopp4.modules.chat.LeaveTab;
import me.corriekay.pppopp4.modules.chat.PrivateChannelTab;
import me.corriekay.pppopp4.modules.minelittleemote.MeeTab;
import me.corriekay.pppopp4.modules.ponymanager.SetGroupTab;
import me.corriekay.pppopp4.modules.unicornhorn.HornTab;
import me.corriekay.pppopp4.modules.warp.GWTab;
import me.corriekay.pppopp4.modules.warp.PWTab;
import me.corriekay.pppopp4.modules.warp.PwplayerArgValidator;
import me.corriekay.pppopp4.modules.warp.PwplayerTabComplete;
import me.corriekay.pppopp4.modules.xraylogging.XrayArgValidator;
import me.corriekay.pppopp4.modules.xraylogging.XrayTab;

import org.bukkit.ChatColor;

public enum CommandMetadata {

    // Ponyville
    motd("motd", null, "displays the MOTD", "/motd", null, CommandType.OPTIONAL, 0, false),
    rules("rules", null, "Displays the rules", "/rules", null, CommandType.OPTIONAL, 0, false),

    // PonyManager
    setgroup("setgroup", "pppopp.setgroup", "changes a users group", "/setgroup <player> <group>", new String[] { "manuadd" }, CommandType.OPTIONAL, 2, true),
    list("list", null, "views a list of online players", "/list", new String[] { "who", "online", "mothabuckinplayers", "showmethaplayas", "ponies", "whothecoolkids" }, CommandType.OPTIONAL, 0, false),
    groupaddperm("groupaddperm", "pppopp.pmmodify", "adds a permission to a group", "/groupaddperm <group> <perm> [world]", null, CommandType.OPTIONAL, 2, true),
    groupdelperm("groupdelperm", "pppopp.pmmodify", "removes a permission from a group", "/groupdelperm <group> <perm> [world]", null, CommandType.OPTIONAL, 2, true),
    useraddperm("useraddperm", "pppopp.pmmodify", "adds a permission to a user", "/useraddperm <player> <perm> [world]", null, CommandType.OPTIONAL, 2, true),
    userdelperm("userdelperm", "pppopp.pmmodify", "removes a permission from a user", "/useraddperm <player> <perm> [world]", null, CommandType.OPTIONAL, 2, true),
    testperm("testperm", "pppopp.testperm", "tests if a user currently has a permission", "/testperm <player> <permission>", null, CommandType.OPTIONAL, 2, false),

    // Channel handler
    channel("channel", "pppopp.channel", "displays a list of available channels", "/channel", null, CommandType.DIFFERENTIATE, 0, false),
    join("join", "pppopp.channel", "join a chat channel", "/join <channel> [password]", new String[] { "ch", "focus" }, CommandType.REQUIRES_PLAYER, 1, false),
    leave("leave", "pppopp.channel", "leave a chat channel", "/leave <channel>", null, CommandType.REQUIRES_PLAYER, 0, false),
    say("say", "pppopp.say", "say a message from the console", "/say <message>", null, CommandType.REQUIRES_CONSOLE, 1, false),
    me("me", "pppopp.me", "sends a basic emote to the channel youre in", "/me <message>", null, CommandType.DIFFERENTIATE, 1, false),
    mute("mute", "pppopp.mute", "toggles a players global mute flag", "/mute <player>", null, CommandType.OPTIONAL, 1, true),
    pm("pm", "pppopp.pm", "sends a private message to another player", "/pm <player> <message>", new String[] { "msg", "tell", "whisper" }, CommandType.REQUIRES_PLAYER, 2, false),
    r("r", "pppopp.pm", "responds to the last private message you recieved", "/r <message>", null, CommandType.REQUIRES_PLAYER, 1, false),
    silence("silence", "pppopp.silence", "toggles silenced state for a player", "/silence <player>", null, CommandType.REQUIRES_PLAYER, 1, false),
    silenced("silenced", "pppopp.silence", "lists silenced players", "/silence", null, CommandType.REQUIRES_PLAYER, 0, false),
    channelcolor("channelcolor", "pppopp.channel", "changes the channels color to a custom one", "/channelcolor <color> [channel]", new String[] { "cc" }, CommandType.REQUIRES_PLAYER, 1, false),
    pc("pc", "pppopp.privatechannel", "Base command for creating and managing private channels", "/pc <command> [additional args]", null, CommandType.REQUIRES_PLAYER, 1, true),
    ponyspy("ponyspy", "pppopp.ponyspy", "Ponyspy command", "/ponyspy", new String[] { "ps" }, CommandType.REQUIRES_PLAYER, 0, true),
    toggleplayernsfw("toggleplayernsfw", "pppopp.toggleplayernsfw", "toggle a players ability to enter into nsfw channels", "/toggleplayernsfw <player>", new String[] { "tpn" }, CommandType.OPTIONAL, 1, true),
    chatmacros("chatmacros", null, "issues chat macros", "/<macro>", new String[] { "wat", "tableflip", "fliptable", "glasses" }, CommandType.REQUIRES_PLAYER, 0, false),

    // Herd Manager
    herd("herd", "pppopp.herd", "base command for creating, managing, and interacting with herds", "/herd <command> [additional args]", null, CommandType.REQUIRES_PLAYER, 1, true),
    createherd("createherd", "pppopp.herd", "command for creating a herd", "/createherd <name>", null, CommandType.REQUIRES_PLAYER, 1, true),
    deleteherd("deleteherd", "pppopp.herd", "command for deleting a herd", "/deleteherd", null, CommandType.REQUIRES_PLAYER, 0, true),
    setherdprotection("setherdprotection", "pppopp.herdprotection", "Sets the protection area for a herd", "/setherdprotection", null, CommandType.REQUIRES_PLAYER, 1, true),

    // Derpy Hooves
    sendletter("sendletter", "pppopp.mail", "Sends a letter", "/sendletter <to> <message>", new String[] { "write", "mail" }, CommandType.DIFFERENTIATE, 2, false),
    composeletter("composeletter", "pppopp.mail", "Composes a letter", "/composeletter", new String[] { "compose" }, CommandType.DIFFERENTIATE, 0, false),
    viewinbox("viewinbox", "pppopp.mail", "views your inbox", "/viewinbox", new String[] { "inbox" }, CommandType.REQUIRES_PLAYER, 0, false),
    viewletter("viewletter", "pppopp.mail", "views a specific letter", "/viewletter <number>", new String[] { "letter", "read", "readletter" }, CommandType.REQUIRES_PLAYER, 1, false),
    deleteletter("deleteletter", "pppopp.mail", "deletes a letter from your inbox", "/deleteletter <number>", null, CommandType.REQUIRES_PLAYER, 1, false),
    markunread("markunread", "pppopp.mail", "marks a letter as unread", "/markunread <number>", null, CommandType.REQUIRES_PLAYER, 1, false),
    markallread("markallread", "pppopp.mail", "marks all letters as read", "/markallread", null, CommandType.REQUIRES_PLAYER, 0, false),
    deleteallmail("deleteallmail", "pppopp.mail", "deletes all letters", "/deleteallmail", new String[] { "clearinbox" }, CommandType.REQUIRES_PLAYER, 0, false),
    respond("respond", "pppopp.mail", "responds to a letter. the letter will go to the sender of the chosen message", "/respond <letternumber> <message>", new String[] { "rr" }, CommandType.REQUIRES_PLAYER, 2, false),
    forward("forward", "pppopp.mail", "forwards the letter to another player", "/forward <number> <player> [message]", new String[] { "fwd" }, CommandType.REQUIRES_PLAYER, 2, false),

    // MLE 4
    mles("mles", "pppopp.mles", "Sets up an emote", "/mles", null, CommandType.REQUIRES_PLAYER, 0, false),
    mle("mle", "pppopp.mle", "lists all emotes", "/mle", null, CommandType.REQUIRES_PLAYER, 0, false),
    mee("mee", "pppopp.mle", "uses an emote", "/mee <emote> <target>", null, CommandType.REQUIRES_PLAYER, 2, false),
    deleteemote("deleteemote", "pppopp.mles", "removes your emote from existence", "/deleteemote", null, CommandType.REQUIRES_PLAYER, 0, false),
    mleban("mleban", "pppopp.mleban", "bans a player from using mle", "/mleban <player>", null, CommandType.OPTIONAL, 1, true),

    // Invisibility Handler
    hide("hide", "pppopp.invisibility", "toggles invisibility", "/hide", new String[] { "fakehide" }, CommandType.REQUIRES_PLAYER, 0, false),
    nopickup("nopickup", "pppopp.invisibility", "toggles nopickup", "/nopickup", new String[] { "np" }, CommandType.REQUIRES_PLAYER, 0, false),
    setinvislevel("setinvislevel", "pppopp.setinvislevel", "sets a players invisibility detection level", "/setinvislevel <player> <level>", null, CommandType.REQUIRES_CONSOLE, 2, true),

    // InvSee
    invsee("invsee", "pppopp.invsee", "Views another players inventory", "/invsee <player>", null, CommandType.REQUIRES_PLAYER, 0, true),
    commitinventorychange("commitinventorychange", "pppopp.invsee", "commits an inventory change", "/cic [player]", new String[] { "cic" }, CommandType.REQUIRES_PLAYER, 0, true),
    invupdate("invupdate", "pppopp.invsee", "updates the inventory you're viewing to current", "/invupdate", null, CommandType.REQUIRES_PLAYER, 0, false),
    clearinventory("clearinventory", "pppopp.clearinventory", "clears your inventory", "/clearinventory", null, CommandType.REQUIRES_PLAYER, 0, false),

    // Afk Handler
    afk("afk", "pppopp.afk", "flags yourself as AFK", "/afk", null, CommandType.REQUIRES_PLAYER, 0, false),

    // Warp Handler
    gw("gw", "pppopp.gw", "teleports to a global warp", "/gw <warp>", null, CommandType.REQUIRES_PLAYER, 1, false),
    pw("pw", "pppopp.pw", "teleports to a private warp", "/pw <warp>", null, CommandType.REQUIRES_PLAYER, 1, false),
    pwlist("pwlist", "pppopp.pwlist", "shows a list of warps that a player can use", "/pwlist", null, CommandType.REQUIRES_PLAYER, 0, false),
    pwdel("pwdel", "pppopp.pw", "deletes a private warp", "/pwdel <warp>", null, CommandType.REQUIRES_PLAYER, 1, false),
    pwset("pwset", "pppopp.pw", "sets a private warp", "/pwset <warp>", null, CommandType.REQUIRES_PLAYER, 1, false),
    gwset("gwset", "pppopp.warpadmin", "sets a global warp", "/gwset <warp>", null, CommandType.REQUIRES_PLAYER, 1, true),
    gwdel("gwdel", "pppopp.warpadmin", "deletes a global warp", "/gwdel <warp>", null, CommandType.REQUIRES_PLAYER, 1, true),
    pwplayer("pwplayer", "pppopp.warpadmin", "shows a list of player warp commands", "/pwplayer <player> [command or warp]", null, CommandType.REQUIRES_PLAYER, 0, false),
    tp("tp", "pppopp.tp", "requests teleportation to another player", "/tp <player>", null, CommandType.REQUIRES_PLAYER, 1, false),
    tpa("tpa", "pppopp.tp", "accepts the current teleport request", "/tpa", null, CommandType.REQUIRES_PLAYER, 0, false),
    tpd("tpd", "pppopp.tp", "denies the current teleport request", "/tpd", null, CommandType.REQUIRES_PLAYER, 0, false),
    tphere("tphere", "pppopp.warpadmin", "teleports a player to you instantaneously", "/tphere <player>", null, CommandType.REQUIRES_PLAYER, 1, true),
    home("home", "pppopp.home", "teleports a player home", "/home", new String[] { "127.0.0.1" }, CommandType.REQUIRES_PLAYER, 0, false),
    sethome("sethome", "pppopp.home", "sets your home location", "/sethome", null, CommandType.REQUIRES_PLAYER, 0, false),
    back("back", "pppopp.back", "teleports back to the last location that you had teleported from", "/back", null, CommandType.REQUIRES_PLAYER, 0, false),
    spawn("spawn", "pppopp.spawn", "teleports to a spawn", "/spawn [world]", null, CommandType.REQUIRES_PLAYER, 0, false),
    setspawn("setspawn", "pppopp.warpadmin", "sets the current universe's spawn location", "/setspawn", null, CommandType.REQUIRES_PLAYER, 0, true),
    top("top", "pppopp.warpadmin", "teleports to the top solid block in the current collumn of blocks youre in", "/top", null, CommandType.REQUIRES_PLAYER, 0, false),
    j("j", "pppopp.warpadmin", "teleports in the direction you're pointing", "/j", new String[] { "jump" }, CommandType.REQUIRES_PLAYER, 0, false),
    tploc("tploc", "pppopp.warpadmin", "teleports to a specific coordinate in a specific world", "/tploc [world] <x> [y] <z>", null, CommandType.REQUIRES_PLAYER, 2, false),

    // Remote Chest Manager
    chest("chest", "pppopp.remotechest", "opens your remote chest", "/chest", new String[] { "c" }, CommandType.REQUIRES_PLAYER, 0, false),
    workbench("workbench", "pppopp.remotechest", "opens your remote workbench", "/workbench", new String[] { "w" }, CommandType.REQUIRES_PLAYER, 0, false),
    transferchest("transferchest", "pppopp.remotechest", "utilizes the transferchest tool", "/transferchest [type]", new String[] { "tc" }, CommandType.REQUIRES_PLAYER, 0, false),
    viewenderchest("viewenderchest", "pppopp.chestadmin", "views another players ender chest", "/viewenderchest <player> [world]", new String[] { "vec" }, CommandType.REQUIRES_PLAYER, 1, true),
    viewremotechest("viewremotechest", "pppopp.chestadmin", "views another players remote chest", "/viewremotechest <player> [world]", new String[] { "vrc" }, CommandType.REQUIRES_PLAYER, 1, true),

    // RPA
    alert("alert", "pppopp.alert", "alerts an opony!", "/alert [message]", null, CommandType.DIFFERENTIATE, 0, false),
    setpassword("setpassword", "pppopp.rpa", "sets your RPA password", "/setpassword <password>", null, CommandType.REQUIRES_PLAYER, 1, false),
    rpadebug("rpadebug", "pppopp.rpadebug", "toggles rpa debug", "/rpadebug", null, CommandType.OPTIONAL, 0, false),

    // Xray Logger
    xray("xray", "pppopp.xray", "brings up the xray logging menu", "/xray [command]", null, CommandType.REQUIRES_PLAYER, 1, false),

    // UnicornHorn
    horn("horn", "pppopp.horn", "command for modifying the unicorn horn", "/horn [left|right|on|off]", null, CommandType.REQUIRES_PLAYER, 1, false),

    // Lockdown
    lockdown("lockdown", "pppopp.lockdown", "command for assisting with attacks against the server", "/lockdown", null, CommandType.OPTIONAL, 0, true),

    // MiscCommands
    nick("nick", "pppopp.nick", "command for setting your nickname", "/nick <name>", null, CommandType.REQUIRES_PLAYER, 1, false),
    whopony("whopony", "pppopp.whopony", "command for checking a users name", "/whopony <name>", null, CommandType.DIFFERENTIATE, 1, false),

    // RainbowDash
    weather("weather", "pppopp.weather", "command for editing weather", "/<sun|rain|storm|superstorm> [targetworld]", new String[] { "sun", "rain", "storm", "superstorm", "day", "night" }, CommandType.DIFFERENTIATE, 0, true),
    time("time", "pppopp.time", "command for editing or displaying time", "/<time|day|night>", null, CommandType.DIFFERENTIATE, 0, true),
    zeus("zeus", "pppopp.zeus", "command for toggling zeus. Type 'true' to use real lightning", "/zeus [true]", null, CommandType.DIFFERENTIATE, 0, false),

    // ElementsOfHarmony
    toggle("toggle", "pppopp.toggle", "command for toggling flows and pvp", "/toggle <pvp|water|lava>", null, CommandType.REQUIRES_PLAYER, 1, true),
    addprotection("addprotection", "pppopp.protection", "command for adding a spawn protection to an area", "/addprotection <name> | Must have world edit selection!", null, CommandType.REQUIRES_PLAYER, 1, true),
    removeprotection("removeprotection", "pppopp.protection", "command for removing a spawn protection from an area", "/removeprotection <name>", null, CommandType.REQUIRES_PLAYER, 1, true),

    // Princess Celestia
    gm("gm", "pppopp.gm", "command for toggling game mode", "/gm", null, CommandType.REQUIRES_PLAYER, 0, false),
    celestia("celestia", "pppopp.celestia", "command for toggling celestia mode", "/celestia", new String[] { "god" }, CommandType.REQUIRES_PLAYER, 0, false),

    // Admin Tools
    playerinformation("playerinformation", "pppopp.pi", "command for viewing player information", "/pi <player>", new String[] { "pi" }, CommandType.OPTIONAL, 1, false),
    rcv("rcv", "pppopp.rcv", "ROYAL CANTERLOT VOICE BROADCASTING SYSTEM!", "/rcv <message>", null, CommandType.DIFFERENTIATE, 1, false),
    runcustomtask("runcustomtask", "pppopp.rct", "runs the custom task. ONLY USABLE BY CORRIE", "/runcustomtask", null, CommandType.OPTIONAL, 0, true),
    matchip("matchip", "pppopp.matchip", "matches an ip address", "/matchip <ip>", null, CommandType.OPTIONAL, 1, true),
    runalttask("runalttask", "pppopp.rct", "runs the custom alt task. ONLY USABLE BY CORRIE", "/runalttask", null, CommandType.OPTIONAL, 0, true),
    extinguish("extinguish", "pppopp.extinguish", "extinguishes all flames around you", "/extinguish [radius|defaults to 50]", new String[] { "ext" }, CommandType.REQUIRES_PLAYER, 0, false),
    spawnmob("spawnmob", "pppopp.spawnmob", "command used for mob spawning", "/spawnmob <mob> [count]", new String[] { "spawnmobtarget" }, CommandType.REQUIRES_PLAYER, 1, false),
    killmob("killmob", "pppopp.spawnmob", "command for killing mobs", "/killmob <type> [radius]", null, CommandType.REQUIRES_PLAYER, 1, false),
    heal("heal", "pppopp.heal", "command for healing", "/heal [target]", null, CommandType.DIFFERENTIATE, 0, false),
    sudo("sudo", "pppopp.sudo", "command used for controlling others", "/sudo <target> <command>", null, CommandType.OPTIONAL, 2, true),
    roundup("roundup", "pppopp.roundup", "command for rounding up farm animals", "/roundup", null, CommandType.REQUIRES_PLAYER, 0, false),

    // Doctor Whooves
    rollback("rollback", "pppopp.rollback", "command for issuing a full rollback on a player", "/rollback <target>", new String[] { "fullrollback" }, CommandType.REQUIRES_PLAYER, 1, true),

    // BanHammer
    ban("ban", "pppopp.banhammer", "command for banning players", "/ban <player> <reason>", new String[] { "banana" }, CommandType.OPTIONAL, 2, true),
    kick("kick", "pppopp.kick", "command for kicking players", "/kick <player> <reason>", null, CommandType.OPTIONAL, 2, true),
    tempban("tempban", "pppopp.banhammer", "command for tempbanning players", "/tempban <player> <time in hours> <reason>", null, CommandType.OPTIONAL, 3, true),
    unban("unban", "pppopp.banhammer", "command for unbanning banned players", "/unban <player>", null, CommandType.OPTIONAL, 1, true),
    notes("notes", "pppopp.banhammer", "command for viewing the notes of a player", "/notes <player>", null, CommandType.OPTIONAL, 1, false),
    createnote("createnote", "pppopp.banhammer", "command for creating a custom note for a player", "/createnote <player> <notes>", null, CommandType.OPTIONAL, 2, false),

    // EntityLogger
    finddeaths("finddeaths", "pppopp.finddeaths", "command for findingg entity deaths", "/finddeaths <param1> <param2> //Requires two sets of paramters", new String[] { "fd" }, CommandType.REQUIRES_PLAYER, 4, false),

    // WorldEdit hook
    claimplot("claimplot", "pppopp.worldedithook", "command for claiming a plot", "/claimplot", null, CommandType.REQUIRES_PLAYER, 0, false),
    releaseplot("releaseplot", "pppopp.worldedithook", "command for releasing your plot", "/releaseplot", null, CommandType.REQUIRES_PLAYER, 0, false),

    // FillyHandler
    gethandbook("gethandbook", "pppopp.gethandbook", "command for getting a fresh handbook", "/gethandbook", null, CommandType.DIFFERENTIATE, 0, false),

    // MissCheerilee
    tutorial("tutorial", null, "Command for initiating a server tutorial", "/tutorial", new String[] { "tutorials" }, CommandType.REQUIRES_PLAYER, 0, false);

    private String commandName, permission, description, usage;
    private ArrayList<String> aliases;
    private CommandType type;
    private int minimumArgs;
    private boolean logCommand;
    private TabRuleset tabRules = new TabRuleset();
    private ArgumentRuleset argRules = new ArgumentRuleset();

    private CommandMetadata(String name, String perm, String desc, String use,
	    String[] labelArray, CommandType type, int minArgs, boolean log) {
	commandName = name;
	permission = perm;
	description = desc;
	usage = use;
	if (labelArray != null) {
	    ArrayList<String> labels = new ArrayList<String>();
	    for (String s : labelArray) {
		labels.add(s);
	    }
	    aliases = labels;
	} else {
	    aliases = null;
	}
	this.type = type;
	minimumArgs = minArgs;
	logCommand = log;
    }

    public String getCommandName() {
	return commandName;
    }

    public String getPermission() {
	return permission;
    }

    public String getDescription() {
	return description;
    }

    public String getUsage() {
	return usage;
    }

    public ArrayList<String> getLabels() {
	return aliases;
    }

    public TabRuleset getTabRules() {
	return tabRules;
    }

    public void addTabRule(int i, BasicTabRule rule) {
	tabRules.addTabRule(i, rule);
    }

    public ArgumentRuleset getArgRuleset() {
	return argRules;
    }

    public void addArgRule(BasicArgumentRule rule) {
	argRules.addRule(rule);
    }

    /**
     * Sets the labels. Warning: labels must be set before registering the
     * commands, otherwise this will do nothing.
     * 
     * @param labels
     */
    public void setLabels(ArrayList<String> labels) {
	aliases = labels;
    }

    public CommandType getCommandType() {
	return type;
    }

    public int getMinimumArgs() {
	return minimumArgs;
    }

    public boolean shouldLog() {
	return logCommand;
    }

    public static CommandMetadata getEnum(String arg) {
	arg = arg.toLowerCase();
	for (CommandMetadata cmd : CommandMetadata.values()) {
	    String name = cmd.name().toLowerCase();
	    if (name.equals(arg)) {
		return cmd;
	    }
	}
	return null;
    }

    public static void setupCommandValidationAndTabbing() {
	String pinkieSays = ChatColor.LIGHT_PURPLE + "Pinkie Pie: ";
	// Set up arg validators

	// ChannelHandler
	SetArgumentRule pcRule = new SetArgumentRule(pinkieSays + "Thats not a valid argument! type /pc <tab> to tab through available commands!", true, false, 0, "createchannel", "setcolor", "setpassword", "setmoderator", "removemoderator", "removepassword", "setadmin", "ban", "kick", "unban", "mute", "closechannel", "setquick", "help", "setnsfw");
	pc.addArgRule(pcRule);
	// HerdManager
	SetArgumentRule herdRule = new SetArgumentRule(pinkieSays + "Thats not a valid argument! type /herd <tab> to tab through available commands!", true, false, 0, "join", "leave", "accept", "deny", "promote", "demote", "setleader", "kick", "setdisplayname", "list", "setmotd", "setenter", "setleave", "toggleprotection", "debug");
	herd.addArgRule(herdRule);
	// pwplayer subargs
	PwplayerArgValidator pwplayerRule = new PwplayerArgValidator(pinkieSays + "Thats not a valid argument!(Possibly invalid warp or player?) type /pwplayer player <tab> to tab through available commands!", true, false);
	pwplayer.addArgRule(pwplayerRule);
	// xray subargs
	XrayArgValidator xrayValidator = new XrayArgValidator(pinkieSays + "Thats not a valid argument! type /xray <tab> to tab through a list of available commands!", true, false);
	xray.addArgRule(xrayValidator);
	// horn subargs
	SetArgumentRule hornRule = new SetArgumentRule(pinkieSays + "Thats not a valid argument! Type /horn <tab> to tab through available commands!", true, false, 0, "left", "right", "on", "off", "help");
	horn.addArgRule(hornRule);

	// Set up tab completers
	// Playername Tab Completers:
	setgroup.addTabRule(0, new PlayerTab());
	useraddperm.addTabRule(0, new PlayerTab());
	userdelperm.addTabRule(0, new PlayerTab());
	testperm.addTabRule(0, new PlayerTab());
	mute.addTabRule(0, new PlayerTab());
	pm.addTabRule(0, new PlayerTab());
	silence.addTabRule(0, new PlayerTab());
	toggleplayernsfw.addTabRule(0, new PlayerTab());
	sendletter.addTabRule(0, new PlayerTab());
	forward.addTabRule(1, new PlayerTab());
	mee.addTabRule(1, new MeeTab());
	mleban.addTabRule(0, new PlayerTab());
	invsee.addTabRule(0, new PlayerTab());
	commitinventorychange.addTabRule(0, new PlayerTab());
	pwplayer.addTabRule(0, new PlayerTab());
	tp.addTabRule(0, new PlayerTab());
	tphere.addTabRule(0, new PlayerTab());

	// Other Tab Completers
	setgroup.addTabRule(1, new SetGroupTab());
	join.addTabRule(0, new JoinTab());
	leave.addTabRule(0, new LeaveTab());
	pc.addTabRule(0, new PrivateChannelTab());
	mee.addTabRule(0, new MeeTab());
	pw.addTabRule(0, new PWTab());
	gw.addTabRule(0, new GWTab());
	pwplayer.addTabRule(1, new PwplayerTabComplete());
	xray.addTabRule(0, new XrayTab());
	horn.addTabRule(0, new HornTab());
    }

    public enum CommandType {
	REQUIRES_PLAYER, REQUIRES_CONSOLE, DIFFERENTIATE, OPTIONAL;
    }
}
