package me.corriekay.pppopp4.command.validation.tabbing;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.corriekay.pppopp4.command.CommandMetadata;

import org.bukkit.command.CommandSender;

public class TabRuleset implements BasicTabRuleset {

    private final HashMap<Integer, BasicTabRule> rules = new HashMap<Integer, BasicTabRule>();

    public List<String> validateTab(CommandSender sender, CommandMetadata cmd, String label, String arg, String[] args) {
	if (cmd.getPermission() != null && !(sender.hasPermission(cmd.getPermission()))) {
	    return new ArrayList<String>();
	}
	BasicTabRule rule = rules.get(args.length - 1);
	List<String> tabs = new ArrayList<String>();
	tabs.add(arg);
	if (rule != null) {
	    tabs = rule.tab(sender, cmd, arg, args);
	}
	return tabs;
    }

    public void addTabRule(int index, BasicTabRule rule) {
	rules.put(index, rule);
    }

}
