package me.corriekay.pppopp4.command.validation.tabbing;

import java.util.List;

import me.corriekay.pppopp4.command.CommandMetadata;

import org.bukkit.command.CommandSender;

public interface BasicTabRule {

    public abstract List<String> tab(CommandSender sender, CommandMetadata cmd, String argBit, String[] args);
}
