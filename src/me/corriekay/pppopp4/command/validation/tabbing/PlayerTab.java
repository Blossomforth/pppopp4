package me.corriekay.pppopp4.command.validation.tabbing;

import java.util.ArrayList;
import java.util.List;

import me.corriekay.pppopp4.command.CommandMetadata;
import me.corriekay.pppopp4.modules.invisibilityhandler.InvisibilityHandler;
import me.corriekay.pppopp4.modules.ponyville.Pony;
import me.corriekay.pppopp4.modules.ponyville.Ponyville;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class PlayerTab implements BasicTabRule {

    @Override
    public List<String> tab(CommandSender sender, CommandMetadata cmd, String argBit, String[] args) {
	Player senderplayer = (Player) sender;
	Pony senderPony = Ponyville.getPony(senderplayer);
	List<String> returnMe = new ArrayList<String>();
	for (Player player : Bukkit.getOnlinePlayers()) {
	    Pony pony = Ponyville.getPony(player);
	    if (InvisibilityHandler.handler.isHidden(player.getName())) {
		if (!senderPony.canSeeInvisible(pony)) {
		    continue;
		}
	    }
	    returnMe.add(pony.getName());
	}
	return returnMe;
    }
}
