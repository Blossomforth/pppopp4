package me.corriekay.pppopp4.command.validation.command;

public abstract class BasicArgumentRule {

    public String failureMessage;
    public boolean showFailureMessage;
    public boolean returnOnFail;

    public BasicArgumentRule(String failure, boolean showFailure,
	    boolean returnOnFail) {
	failureMessage = failure;
	showFailureMessage = showFailure;
	this.returnOnFail = returnOnFail;
    }

    public abstract boolean checkRule(String[] args);

}
