package me.corriekay.pppopp4.command.validation.command;

import org.bukkit.command.CommandSender;

public interface BasicArgumentRuleset {
    public BasicArgumentRule validateArguments(String[] args, CommandSender sender);
}
