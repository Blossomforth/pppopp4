package me.corriekay.pppopp4.command.validation.command;

import java.util.HashSet;

public class SetArgumentRule extends BasicArgumentRule {

    private int index;
    private HashSet<String> valids;

    public SetArgumentRule(String failure, boolean showFailure,
	    boolean returnOnFail, int indexToCheck, String... validArgs) {
	super(failure, showFailure, returnOnFail);
	index = indexToCheck;
	valids = new HashSet<String>();
	for (String arg : validArgs) {
	    valids.add(arg);
	}
    }

    @Override
    public boolean checkRule(String[] args) {
	try {
	    String checkMe = args[index];
	    return valids.contains(checkMe);
	} catch (Exception e) {
	    return false;
	}

    }

}
