package me.corriekay.pppopp4.command.validation.command;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class RequirePlayernameRule extends BasicArgumentRule {

    private int index;

    public RequirePlayernameRule(String failure, boolean showFailure,
	    boolean returnOnFail, int indexToCheck) {
	super(failure, showFailure, returnOnFail);
	index = indexToCheck;
    }

    @Override
    public boolean checkRule(String[] args) {
	try {
	    String s = args[index];
	    for (Player player : Bukkit.getOnlinePlayers()) {
		String name = player.getName().toLowerCase();
		String display = ChatColor.stripColor(player.getDisplayName()).toLowerCase();
		if (name.contains(s) || display.contains(s)) {
		    return true;
		}
	    }
	    return false;
	} catch (Exception e) {
	    return false;
	}
    }

}
