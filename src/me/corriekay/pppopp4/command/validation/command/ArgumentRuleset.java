package me.corriekay.pppopp4.command.validation.command;

import java.util.HashSet;

import org.bukkit.command.CommandSender;

public class ArgumentRuleset implements BasicArgumentRuleset {

    private final HashSet<BasicArgumentRule> rules = new HashSet<BasicArgumentRule>();

    public ArgumentRuleset() {}

    public ArgumentRuleset(BasicArgumentRule... rules) {
	for (BasicArgumentRule rule : rules) {
	    this.rules.add(rule);
	}
    }

    public void addRule(BasicArgumentRule rule) {
	rules.add(rule);
    }

    public BasicArgumentRule validateArguments(String[] args, CommandSender sender) {
	for (BasicArgumentRule rule : rules) {
	    if (!rule.checkRule(args)) {
		if (rule.showFailureMessage) {
		    sender.sendMessage(rule.failureMessage);
		}
		return rule;
	    }
	}
	return null;
    }

}
